# SampleCode

Example Code

## BERT-Instrument

Sample code from a desktop application to control a signal generator / error tester instrument for high frequency signals.

This application uses C++ and the QT Framework. It has been built and tested under Windows.

## Linux-CANTools

C code to generate a test application for testing CAN bus data transfer on a device running Linux

## PIC-DataLogger

C code for a data logger using a PIC micro controller to record temperatures from an array of temperature sensor ICs


NOTE: All code within this repository remains the property of Riverhead Technology and should not be reused without written permission from the owner.
