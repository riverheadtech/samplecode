/*!
 * \brief CAN Bus Test with Auto Filtering
 *
 * This test app will listen for ALL incoming CAN frames. For each 
 * frame, it will check the ID against a list it already knows about, 
 * and create a new item to track if it hasn't seen the ID before
 * (up to some max number of IDs). Then, it will record stats 
 * for each CAN ID.
 * 
 * Nb: Based on:
 *     linux-can/can-utils/cansend.c
 *     linux-can/can-utils/candump.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <time.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/select.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "cantools.h"

#define MAX_IDS 100     // Maximum number of IDs to track


int main(int argc, char **argv)
{
    printf("CAN Stats By ID\n");

    // Check command line options:
    int dump_frames = 0;

    char *can_if = NULL;
    int usage_ok = 1;
    int i, j; 
    for (i=1; i<argc; i++)
    {
        if (argv[i][0] == '-')
        {
            // Options: Parse them (backwards for convenience!)
            for (j=strlen(argv[i]) - 1; j > 0; j--)
            {
                switch (argv[i][j])
                {
                    case 'd':
                        dump_frames = 1;
                        break;

                    default:
                        // Unknown option!
                        usage_ok = 0;
                        printf("Unknown option: -%c\n", argv[i][j]);
                        break;
                }
                if (!usage_ok) break; 
            }
        }
        else
        {
            // Non-option! Should be CAN interface. 
            can_if = argv[i];
            break;
        }
    }
    if (!usage_ok || can_if == NULL) 
    {
        printf("Usage: %s [options] <CAN interface>\n"
               "  Options:\n"
               "   -d = Display frames\n"
               "\n",
               argv[0]);
        return 1;
    }

    int s_can; // CAN sockets
    struct sockaddr_can addr;
    int ret;

    // Create sockets:
    printf("Create CAN Socket...             ");
    fflush(stdout);
    s_can = socket(PF_CAN, SOCK_RAW, CAN_RAW);
    if (s_can < 0)
    {
        printf("FAIL - Error opening CAN socket (%d - %d)\n", 
               errno, s_can);
        return 1;
    }
    printf("OK\n");

    // Query the index of the supplied interface name: 
    printf("Check interface name...          ");
    fflush(stdout);
    unsigned int if_index = if_nametoindex(can_if);
    if (if_index == 0)
    {
        printf("FAIL - Invalid CAN interface '%s' (%d)\n", can_if, errno);
        return 1;
    }
    printf("OK\n");

    // Bind CAN device to socket created above:
    printf("Bind CAN device to socket...     ");
    fflush(stdout);
    addr.can_family = AF_CAN;
    addr.can_ifindex = if_index;
    int bind_result = bind(s_can, (struct sockaddr *)&addr, sizeof(addr));
    if (bind_result < 0)
    {
        printf("FAIL - Error binding socket (%d - %d)\n", errno, bind_result);
        return 1;
    }
    printf("OK\n");

    //==================================================================
    // Receive CAN frames and analyse:
    printf("Receive frames...\n");
    fflush(stdout);

    struct can_frame frame_in;

    unsigned long frames_received = 0;
    unsigned long bytes_received  = 0;

    time_t time_now, time_last;
    double time_diff;
    time(&time_last);

    typedef struct can_channel_t
    {
        canid_t  can_id;
        __u32    frame_count;
        __u8     n_bytes;
    } can_channel_t;

    can_channel_t channels[MAX_IDS] = { 0 };
    unsigned int channel_count = 0;

    int channel_found; 

    while (1)
    {
        ret = can_wait_frame(s_can, 30, 0);
        if (ret <= 0)
        {
            printf("FAIL\n  Error or Timeout waiting for data on CAN (%d)\n", ret);
            return 1;
        }
        ret = can_receive_frame(s_can, &frame_in);
        if (ret != CAN_ERROR_OK)
        {
            printf("FAIL\n  CAN read error (%d)\n", ret);
            return 1;
        }

        frames_received++;
        bytes_received += frame_in.can_dlc;

        if (dump_frames) can_frame_dump_debug(&frame_in);

        // Is this a new frame?
        channel_found = 0;
        for (i = 0; i < channel_count; i++)
        {
            if (channels[i].can_id == frame_in.can_id)
            {
                // Channel found: 
                channels[i].frame_count++;
                if (frame_in.can_dlc != channels[i].n_bytes)
                {
                    printf("FAIL\nChannel %X had %u bytes per frame, but this frame has %u!\n",
                           channels[i].can_id, channels[i].n_bytes, frame_in.can_dlc);
                    return 1;
                }
                channel_found = 1;
                break;
            }
        }
        if (!channel_found)
        {
            // New channel! Find a free channel slot: 
            // This will be the first slot with a frame count
            // of 0, OR the slot with the lowest frame count. 
            // FOR NOW: Just quit if we run out of channels.
            channel_count++;
            if (channel_count >= MAX_IDS)
            {
                printf("FAIL\nRan out of channels!\n");
                return 1;
            }
            i = channel_count - 1;
            channels[i].can_id = frame_in.can_id;
            channels[i].frame_count = 1;
            channels[i].n_bytes = frame_in.can_dlc;
            printf("New Channel: 0x%X - %u bytes\n", channels[i].can_id, channels[i].n_bytes);
        }

        time(&time_now);
        time_diff = difftime(time_now, time_last);
        if (time_diff >= 2.0)
        {
            // Display stats: 
            printf("Frames Rcvd: %7lu;  Bytes Rcvd: %8lu;  %6.1f kbits/s\n",
                   frames_received, 
                   bytes_received,
                   (((double)bytes_received*8.0) / 1024.0) / time_diff);

            printf("Channel:     Msgs:     Bytes/Msg:\n");
            for (i = 0; i < channel_count; i++)
            {
                printf("%8X     %5u       %2u\n",
                       channels[i].can_id,
                       channels[i].frame_count,
                       channels[i].n_bytes);
            }
            printf("\n");

            time(&time_last);
            frames_received = 0;
            bytes_received = 0;
        }

    }

    //==================================================================

//tests_finished:

    close(s_can);

    printf("PASS\n");
    return 0;
}

