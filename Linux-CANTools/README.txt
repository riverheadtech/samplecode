CAN Bus Test Tools for Linux
----------------------------

This directory contains a test tool for testing CAN bus data
on a device running Linux. 

It uses the SocketCAN interface. It was used with a USB2CAN 
USB-CAN adapter, and also a CAN interface on an embedded device.

It was designed to demonstrate and test CAN bus performance, 
and was also used for tests of reading CAN bus data from a 
vehicle.
