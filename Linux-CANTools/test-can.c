/*!
 * \brief CAN Bus Test App
 *
 * This test app can run in several modes:
 *   - Free run mode (default):
 *      Receive CAN frames indefinitely, and periodically display 
 *      summary data. Add -d option to dump CAN frames. Add a filter
 *      option (see usage) to apply a filter to incoming frames.
 *      Received data are checked for 'sequence': The data must
 *      be 8 bytes long and each frame must be the same in all bytes 
 *      except the last, which must increment on each frame. 
 *      Where a filter is applied, the expected sequence is adjusted
 *      accordingly. 
 * 
 *   - Echo test:
 *      Send out a CAN frame with CAN ID 0x1FFFF800 (extended address);
 *      Wait for a reply with ID 0x1FFFF801. To pass, the reply must
 *      arrive within 5 seconds and the data must match the data that
 *      was sent.
 * 
 *   - Sequence Test:
 *      Receive frames for all IDs, and check that they are in sequence
 *      (see free run mode above). Fail if not in sequence. After 5 
 *      seconds, apply a filter (receive only IDs 0x1FFFF700), and 
 *      continue checking sequence. 
 *
 * Some tests require a suitable test app to be running on a host PC 
 * with a CAN interface, generating test frames.
 * 
 * Nb: Based on:
 *     linux-can/can-utils/cansend.c
 *     linux-can/can-utils/candump.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>
#include <time.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/select.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "cantools.h"

#define SEQ_CHECK 1  // Remove this to disable sequence check on incoming frames

int main(int argc, char **argv)
{
    printf("CAN Test App\n");

    // Check command line options:
    int run_all_tests = 0;
    int run_echo_test = 0;
    int filter_test_phase = 0;
    int dump_frames = 0;

    int use_filter = 0;   // Filter test: filter incoming frames
    int n_filters = 1;

    char *can_if = NULL;
    int usage_ok = 1;
    int i, j; 
    for (i=1; i<argc; i++)
    {
        if (argv[i][0] == '-')
        {
            // Options: Parse them (backwards for convenience!)
            for (j=strlen(argv[i]) - 1; j > 0; j--)
            {
                switch (argv[i][j])
                {
                    case 't':
                        run_all_tests = 1;
                        break;
                    case 'd':
                        dump_frames = 1;
                        break;
                    case 'e':
                        run_echo_test = 1;
                        break;
                    case 'f':
                        use_filter = 1;
                        break;
                    case '1':
                        n_filters = 1;
                        break;
                    case '2':
                        n_filters = 2;
                        break;
                    case '4':
                        n_filters = 4;
                        break;
                    case '8':
                        n_filters = 8;
                        break;

                    default:
                        // Unknown option!
                        usage_ok = 0;
                        printf("Unknown option: -%c\n", argv[i][j]);
                        break;
                }
                if (!usage_ok) break; 
            }
        }
        else
        {
            // Non-option! Should be CAN interface. 
            can_if = argv[i];
            break;
        }
    }
    if (!usage_ok || can_if == NULL) 
    {
        printf("Usage: %s [options] <CAN interface>\n"
               "  Options:\n"
               "   -t = Run all tests and exit\n"
               "   -e = Run echo test and exit\n"
               "   -fn = Filter test: Filter by CAN ID\n"
               "         n specifies number of filters:\n"
               "             1 = Receive only 0x1FFFF700\n"
               "             2 = Receive 0x1FFFF700 + 0x1FFFF708\n"
               "             4 = ...700 + ...704 + ...708 + ...70C\n"
               "             8 = ...700 + ...702 + ...704 + ...706\n"
               "               + ...708 + ...70A + ...70C + ...70E\n"
               "\n",
               argv[0]);
        return 1;
    }

    if (run_all_tests)
    {
        printf("Full Test Mode: Filter settings ignored.\n");
        use_filter = 0;
        n_filters = 0;
    }

    int s_echo, s_dump; // CAN sockets
    struct sockaddr_can addr;
    int ret;

    // Create sockets:
    printf("Create CAN Sockets...            ");
    fflush(stdout);
    s_echo = socket(PF_CAN, SOCK_RAW, CAN_RAW);  // For "echo" test (write a frame and wait for reply)
    s_dump = socket(PF_CAN, SOCK_RAW, CAN_RAW);  // for "dump" test (read frames at max rate)
    if (s_echo < 0 || s_dump < 0)
    {
        printf("FAIL - Error opening CAN socket (%d - %d %d)\n", 
               errno, s_echo, s_dump);
        return 1;
    }
    printf("OK\n");

    // Query the index of the supplied interface name: 
    printf("Check interface name...          ");
    fflush(stdout);
    unsigned int if_index = if_nametoindex(can_if);
    if (if_index == 0)
    {
        printf("FAIL - Invalid CAN interface '%s' (%d)\n", can_if, errno);
        return 1;
    }
    printf("OK\n");

    uint8_t expected_step = 1;  
      // Default: we expect the last data byte in each frame to 
      // increment by one for each frame. If filtering is enabled, 
      // we will only process one frame in 16, so the expected step is 
      // set to 16 below if filtering is on. 

    if (use_filter)
    {
        if (n_filters != 1 && n_filters != 2 && n_filters != 4 && n_filters != 8)
        {
            printf("ERROR - Invalid number of filters (%d)\n", n_filters);
            return 1;
        }
        // Set up filtering by ID: 
        printf("Add CAN ID filter...             ");
        fflush(stdout);
        struct can_filter rfilter[8];
        rfilter[0].can_id = 0x1FFFF700;
        rfilter[0].can_mask = 0x1FFFFFFF;
        rfilter[0].can_mask &= ~CAN_ERR_FLAG;

        rfilter[1].can_id = 0x1FFFF708;
        rfilter[1].can_mask = 0x1FFFFFFF;
        rfilter[1].can_mask &= ~CAN_ERR_FLAG;

        rfilter[2].can_id = 0x1FFFF704;
        rfilter[2].can_mask = 0x1FFFFFFF;
        rfilter[2].can_mask &= ~CAN_ERR_FLAG;
        rfilter[3].can_id = 0x1FFFF70C;
        rfilter[3].can_mask = 0x1FFFFFFF;
        rfilter[3].can_mask &= ~CAN_ERR_FLAG;

        rfilter[4].can_id = 0x1FFFF702;
        rfilter[4].can_mask = 0x1FFFFFFF;
        rfilter[4].can_mask &= ~CAN_ERR_FLAG;
        rfilter[5].can_id = 0x1FFFF706;
        rfilter[5].can_mask = 0x1FFFFFFF;
        rfilter[5].can_mask &= ~CAN_ERR_FLAG;
        rfilter[6].can_id = 0x1FFFF70A;
        rfilter[6].can_mask = 0x1FFFFFFF;
        rfilter[6].can_mask &= ~CAN_ERR_FLAG;
        rfilter[7].can_id = 0x1FFFF70E;
        rfilter[7].can_mask = 0x1FFFFFFF;
        rfilter[7].can_mask &= ~CAN_ERR_FLAG;

        ret = setsockopt(s_dump, SOL_CAN_RAW, CAN_RAW_FILTER, rfilter, sizeof(struct can_filter) * n_filters);
        if (ret != 0)
        {
            printf("FAIL - Error setting up filter (%d)\n", errno);
            return 1;
        }
        // Filtering: We will only see every N th frame
        expected_step = 16 / n_filters;
        printf("OK (filters: %d; step: %d)\n", n_filters, expected_step);
    }

    // Echo test socket should receive only CAN ID 0x1FFFF801:
    struct can_filter efilter;
    efilter.can_id = 0x1FFFF801;
    efilter.can_mask = 0x1FFFFFFF;
    efilter.can_mask &= ~CAN_ERR_FLAG;
    ret = setsockopt(s_echo, SOL_CAN_RAW, CAN_RAW_FILTER, &efilter, sizeof(struct can_filter));
    if (ret != 0)
    {
        printf("FAIL - Error setting up filter for echo test (%d)\n", errno);
        return 1;
    }

    // Bind CAN device to sockets created above:
    printf("Bind CAN device to sockets...    ");
    fflush(stdout);
    addr.can_family = AF_CAN;
    addr.can_ifindex = if_index;
    int bind_echo = bind(s_echo, (struct sockaddr *)&addr, sizeof(addr));
    int bind_dump = bind(s_dump, (struct sockaddr *)&addr, sizeof(addr));
    
    if (bind_echo < 0 || bind_dump < 0)
    {
        printf("FAIL - Error binding socket (%d - %d %d)\n", errno, bind_echo, bind_dump);
        return 1;
    }
    printf("OK\n");



    if (run_all_tests || run_echo_test)
    {
        // ECHO test: 
        // Send a CAN frame:
        // Initialise the CAN frame content to 0:
        struct can_frame frame_echo_out;
        struct can_frame frame_echo_in;
        memset(&frame_echo_out, 0, sizeof(frame_echo_out));
        memset(&frame_echo_in, 0, sizeof(frame_echo_in));

        // Example CAN frame: 8 bytes; 0x11,0x22,0x33,...
        frame_echo_out.can_id = 0x1FFFF800U;  // CAN_ID for echo test (see test-can-host tool)
        frame_echo_out.can_id |= CAN_EFF_FLAG;
        int i;
        uint8_t data_byte = 0x11;
        for (i=0; i<8; i++)
        {
            frame_echo_out.data[i] = data_byte; 
            data_byte += 0x11;
        }
        frame_echo_out.can_dlc = 8;

        // Send:
        printf("ECHO Test:\n");
        printf("Send a frame...                  ");
        fflush(stdout);
        ret = can_send_frame(s_echo, &frame_echo_out);
        if (ret != CAN_ERROR_OK)
        {
            printf("FAIL - Error writing CAN frame (%d)\n", errno);
            return 1;
        }
        printf("OK\n");
        printf("Check for a reply...             ");
        fflush(stdout);
        ret = can_wait_frame(s_echo, 3, 0);
        if (ret == 0)
        {
            // Timeout! No message arrived. 
            printf("FAIL\n  Timeout waiting for CAN frame on ID 0x1FFFF801\n", errno);
            return 1;
        }
        else if (ret < 0)
        {
            printf("FAIL\n  System error calling select\n", errno);
            return 1;
        }
        else
        {
            ret = can_receive_frame(s_echo, &frame_echo_in);
            if (ret != CAN_ERROR_OK)
            {
                printf("FAIL\n  Error reading back CAN frame (%d)\n", ret);
                return 1;
            }
            if (dump_frames) can_frame_dump_debug(&frame_echo_in);
            // Check the data: 
            ret = -1; 
            if (frame_echo_out.can_dlc == frame_echo_in.can_dlc)
            {
                ret = memcmp( frame_echo_out.data, frame_echo_in.data, frame_echo_out.can_dlc);
            }
            if (ret != 0)
            {
                printf("FAIL\n  Data mismatch in received CAN frame\n");
                return 1;
            }
        }
        printf("OK\n");
    }

    if (run_echo_test) goto tests_finished;  // Done!

    //==================================================================
    // Receive CAN frames and analyse:
    printf("Frame Receive / Filter Test:\n");
    printf("Receive frames...\n");
    fflush(stdout);

    struct can_frame frame_dump;
    struct can_frame frame_dump_prev;

    int flag_first_frame = 1;
    unsigned long frames_received = 0;
    unsigned long frames_lost     = 0;
    unsigned long bytes_received  = 0;

    time_t time_now, time_last;
    double time_diff;
    time(&time_last);

    while (1)
    {
        ret = can_wait_frame(s_dump, 3, 0);
        if (ret <= 0)
        {
            printf("FAIL\n  Error or Timeout waiting for data on CAN (%d)\n", ret);
            return 1;
        }
        ret = can_receive_frame(s_dump, &frame_dump);
        if (ret != CAN_ERROR_OK)
        {
            printf("FAIL\n  CAN read error (%d)\n", ret);
            return 1;
        }

        frames_received++;
        bytes_received += frame_dump.can_dlc;
        
        if (dump_frames) can_frame_dump_debug(&frame_dump);

#ifdef SEQ_CHECK

        // Data sequence check: Note that we might see a loopback 
        // of frame sent to 0x1FFFF80x from earlier test; this frame
        // should be ignored. 
        if ((frame_dump.can_id & ~CAN_EFF_FLAG) != 0x1FFFF800 && 
            (frame_dump.can_id & ~CAN_EFF_FLAG) != 0x1FFFF801)
        {
            int i;
            int mismatch_flag = 0;

            if (!flag_first_frame && (frame_dump.can_dlc != frame_dump_prev.can_dlc))
            {
                printf("FAIL\n  Frame length mismatch\n");
                return 1;
            }

            for (i=0; i<frame_dump.can_dlc-1; i++)
            {
                if (!flag_first_frame)
                {
                    if (frame_dump.data[i] != frame_dump_prev.data[i])
                    {
                        // Mismatch in bytes 0-6! 
                        mismatch_flag = 1;
                    }
                }
            }

            // Check last byte: should be prev byte plus n, where n
            // is 1 (no filtering) or a number based on how many records 
            // were skipped by filter.
            uint8_t expected_data = (uint8_t)(frame_dump_prev.data[i]+expected_step);
            if (!flag_first_frame && frame_dump.data[i] != expected_data)
            {
                // Mismatch in last byte (out of sequence):
                mismatch_flag = 1;
            }
            flag_first_frame = 0;

            if (mismatch_flag)
            {
                frames_lost++;
                printf("MISMATCH: 0x%08X - ", frame_dump_prev.can_id);
                for (i=0; i<frame_dump.can_dlc; i++)
                {
                    printf("%02X ", frame_dump_prev.data[i]);
                }
                printf("--> 0x%08X - ", frame_dump.can_id);
                for (i=0; i<frame_dump.can_dlc; i++)
                {
                    printf("%02X ", frame_dump.data[i]);
                }
                printf("\n");
                if (run_all_tests)
                {
                    // Automated test mode: Exit on mismatch error. 
                    printf("FAIL\n  Frame data mismatch\n");
                    return 1;
                }
            }
            memcpy(&frame_dump_prev, &frame_dump, sizeof(frame_dump_prev));
        }

#endif // SEQ_CHECK

        time(&time_now);
        time_diff = difftime(time_now, time_last);

        if (run_all_tests)
        {
            // Automated test mode:
            // Run filtered test for 5 seconds, then unfiltered test 
            // for 5 seconds: 
            if (time_diff >= 5.0)
            {
                printf("Raw frame receive test OK\n");
                printf(" Frames Rcvd: %7lu;  Frames Lost: %5lu;  Bytes Rcvd: %8lu; %6.1f kbits/s\n",
                       frames_received, 
                       frames_lost,
                       bytes_received,
                       (((double)bytes_received*8.0) / 1024.0) / time_diff);

                time(&time_last);
                frames_received = 0;
                bytes_received = 0;
                
                // Move to next test stage: 
                if (filter_test_phase == 0)
                {
                    filter_test_phase = 1;
                    use_filter = 1;   // Filter test: filter incomming frames
                    n_filters = 1;
                    // Set up filtering by ID: 
                    printf("Add CAN ID filter...             ");
                    fflush(stdout);
                    struct can_filter dfilter;
                    dfilter.can_id = 0x1FFFF700;
                    dfilter.can_mask = 0x1FFFFFFF;
                    dfilter.can_mask &= ~CAN_ERR_FLAG;
                    ret = setsockopt(s_dump, SOL_CAN_RAW, CAN_RAW_FILTER, &dfilter, sizeof(struct can_filter));
                    if (ret != 0)
                    {
                        printf("FAIL - Error setting up filter (%d)\n", errno);
                        return 1;
                    }
                    // Filtering: We will only see every N th frame
                    expected_step = 16;
                    flag_first_frame = 1;
                    printf("OK (filters: %d; step: %d)\n", n_filters, expected_step);
                    // Flush any buffered frames:
                    printf("Flush Buffer...                  ");
                    fflush(stdout);
                    ret = can_flush_recieve_buffer(s_dump);
                    if (ret != CAN_ERROR_OK)
                    {
                        printf("FAIL - Error flushing buffer (%d)\n", ret);
                        return 1;
                    }
                    printf("OK\n");
                }
                else
                {
                    // Test finished! Break out. 
                    break;
                }
            }
        }
        else
        {
            // Free-Run Mode: Update the summary...
            if (time_diff >= 1.0)
            {
                printf(" Frames Rcvd: %7lu;  Frames Lost: %5lu;  Bytes Rcvd: %8lu; %6.1f kbits/s\n",
                       frames_received, 
                       frames_lost,
                       bytes_received,
                       (((double)bytes_received*8.0) / 1024.0) / time_diff);
                time(&time_last);
                frames_received = 0;
                bytes_received = 0;
            }
        }
    }
   
    //==================================================================

tests_finished:

    close(s_echo);
    close(s_dump);

    printf("PASS\n");
    return 0;
}

