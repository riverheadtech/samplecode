/**
 * \brief Shared CAN Bus Functions
 *
 * Implemented using SocketCAN interface
 *
 */

#include <errno.h>
#include <unistd.h>
#include <stdio.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/select.h>

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>

#include "cantools.h"


// Wait for a frame to arrive
int can_wait_frame(const int sock_fd,
                   unsigned int timeout_secs,
                   unsigned int timeout_usecs)
{
    int ret;
    fd_set fds;
    struct timeval timeout = { .tv_sec=timeout_secs, .tv_usec=timeout_usecs };
    FD_ZERO(&fds);
    FD_SET(sock_fd, &fds);
    ret = select(sock_fd+1, &fds, NULL, NULL, &timeout);
    return ret;
}


// Send a CAN frame
int can_send_frame(const int sock_fd, struct can_frame *frame)
{
    ssize_t nbytes;
    nbytes = write(sock_fd, frame, sizeof(struct can_frame));

    if (nbytes < 0) 
    {
        if (errno == ENOBUFS) return CAN_ERROR_BUFFER_FULL;
        else                  return CAN_ERROR_READ;
    }
    else if (nbytes != (ssize_t)CAN_MTU)
    {
        return CAN_ERROR_FRAME_SIZE;
    }
    else 
    {
        return CAN_ERROR_OK;
    }
}


// Receive a CAN frame
int can_receive_frame(const int sock_fd, struct can_frame *frame)
{
    ssize_t nbytes;
    nbytes = read(sock_fd, frame, sizeof(struct can_frame));

    if (nbytes < 0) 
    {
        if (errno == EAGAIN) return CAN_ERROR_NO_DATA;
        else                 return CAN_ERROR_READ;
    }
    else if (nbytes != (ssize_t)CAN_MTU)
    {
        return CAN_ERROR_FRAME_SIZE;
    }
    else 
    {
        return CAN_ERROR_OK;
    }
}


// Flush the receive buffer of a CAN socket
int can_flush_recieve_buffer(const int sock_fd)
{
    int ret;
    int max_count = 1000;  // buffer should be around 255 frames. 
    struct can_frame frame;
    while (max_count > 0)
    {
        ret = can_wait_frame(sock_fd, 0, 0);  // Check for frame in socket buffer
        if (ret == 0) return CAN_ERROR_OK;   // No frames left in buffer.
        can_receive_frame(sock_fd, &frame);   // Receive a frame and throw it away. 
        max_count--;
    }
    return CAN_ERROR_FLUSH_FAIL;  // Error: Looped too many times without emptying buffer!
}


// Write formatted CAN frame data to a string buffer
void can_frame_dump_str(const struct can_frame *frame, char *str, size_t size)
{
    int i;
    size_t bytes_this; 
    size_t bytes_left = size;
    size_t bytes_used = 0;

    bytes_this = snprintf(str,
                          bytes_left,
                          "Recvd: 0x%08X - ",
                          (frame->can_id & ~CAN_EFF_FLAG));
    bytes_used += bytes_this;
    bytes_left = size - bytes_used;

    for (i = 0; i < frame->can_dlc; i++)
    {
        bytes_this = snprintf(str + bytes_used,
                              bytes_left,
                              "%02X ",
                              frame->data[i]);
        bytes_used += bytes_this;
        bytes_left = size - bytes_used;
    }
    str[size-1] = 0x00; // Ensure NULL terminator.
}


// Print out ID and data from CAN frame
void can_frame_dump_debug(const struct can_frame *frame)
{
    int i; 
    printf("Recvd: 0x%08X - ", (frame->can_id & ~CAN_EFF_FLAG));
    for (i = 0; i < frame->can_dlc; i++)
    { 
        printf("%02X ", frame->data[i]);
    }
    printf("\n");
}


// Print out details from a CAN error frame
void can_error_dump_debug(const struct can_frame *frame)
{
    int i; 
    printf("ERROR: 0x%08X - ", frame->can_id);
    for (i = 0; i < frame->can_dlc; i++)
    { 
        printf("%02X ", frame->data[i]);
    }
    printf("\n");
}









/*
 * Receive a frame using "recvmsg" call.
 * More complex, but can also get ancillary data. 

int can_receive_frame_OLD(const int sock_fd, 
                      canid_t *can_id, 
                      uint8_t *data,
                      uint8_t *received_len)
{
    // Receive a CAN frame:
    char ctrlmsg[CMSG_SPACE(sizeof(struct timeval)) + CMSG_SPACE(sizeof(__u32))];
    struct sockaddr_can addr;
    struct canfd_frame frame = { 0 };

    struct iovec iov;
    struct msghdr msg;

    // Initialise the CAN frame content to 0:
    //memset(&frame, 0, sizeof(frame));

    iov.iov_base = &frame;
    msg.msg_name = &addr;
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = &ctrlmsg;
    int maxdlen, ret;
    ssize_t nbytes;

    iov.iov_len = sizeof(frame);
    msg.msg_namelen = sizeof(addr);
    msg.msg_controllen = sizeof(ctrlmsg);
    msg.msg_flags = 0;

    nbytes = recvmsg(sock_fd, &msg, 0);

    if (nbytes < 0) 
    {
        return CAN_ERROR_READ;
    }
    else if (nbytes != (ssize_t)CAN_MTU)
    {
        return CAN_ERROR_FRAME_SIZE;
    }
    else 
    {
               /
                // Get ancillary data (dropped frame count, timestamp, etc)
                struct cmsghdr *cmsg;
                for (cmsg = CMSG_FIRSTHDR(&msg);
                     cmsg && (cmsg->cmsg_level == SOL_SOCKET);
                     cmsg = CMSG_NXTHDR(&msg,cmsg)) 
                {
                    / Unused: Timestamp
                    if (cmsg->cmsg_type == SO_TIMESTAMP)
                    {
                        timestamp = *(struct timeval *)CMSG_DATA(cmsg);
                    }
                    /
                    if (cmsg->cmsg_type == SO_RXQ_OVFL)
                    {
                        dropcnt = *(uint32_t *)CMSG_DATA(cmsg);
                    }
                }
                if (dropcnt != 0)
                {
                    printf("WARNING - Dropped %d CAN frames\n", dropcnt);
                }
                /        

        *can_id = frame.can_id;
        *received_len = frame.len;
        memcpy(data, frame.data, frame.len);
        return CAN_ERROR_OK;
    }
}
*/
