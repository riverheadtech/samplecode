/**
 * \brief Shared CAN Bus Functions
 *
 * Implemented using SocketCAN interface
 *
 */

#ifndef CANTOOLS_H
#define CANTOOLS_H

#include <linux/can.h>
#include <linux/can/raw.h>

// Error codes returned by functions:
#define CAN_ERROR_OK           0   ///< No error
#define CAN_ERROR_NO_DATA     -1   ///< Socket returned "EAGAIN" (no data to read)
#define CAN_ERROR_READ        -1   ///< Error reading from socket
#define CAN_ERROR_FRAME_SIZE  -2   ///< Unexpected frame size 
#define CAN_ERROR_BUFFER_FULL -3   ///< Write buffer full (delay and retry needed)
#define CAN_ERROR_FLUSH_FAIL  -4   ///< Failed to flush receive buffer (frames arriving too fast?)



/**
 * \brief Wait for a frame to arrive on a CAN socket, or timeout to occur
 *
 * \param sock_fd
 *            Socket to look for CAN frames on (file descriptor)
 * 
 * \param timeout_secs
 *            Max time to wait (seconds) - can be combined with 
 *            timeout_usecs; one or both may be 0. 
 * 
 * \param timeout_usecs
 *            Max time to wait (uSeconds) - can be combined with 
 *            timeout_secs; one or both may be 0. 
 *
 * \return  1 - Frame arrived (read with can_receive_frame)
 * \return  0 - Timeout, No frame arrived
 * \return -1 - System error in select call
 *
 */
int can_wait_frame(const int sock_fd,
                   unsigned int timeout_secs,
                   unsigned int timeout_usecs);



/**
 * \brief Send a CAN frame
 * Supports only CAN 2.0 (NOT CAN-FD) - max data size of 8 bytes. 
 *
 * \param sock_fd
 *            CAN Socket to use (file descriptor)
 *
 * \param frame
 *            Pointer to a CAN frame to send
 * 
 * \return  CAN_ERROR_OK  Success
 * \return  [Error code]   Error - see cantools.h
 *
 */
int can_send_frame(const int sock_fd, struct can_frame *frame);



/**
 * \brief Receive a CAN frame
 * Assumes there is a frame ready to read from the adaptor. 
 * Supports only CAN 2.0 (NOT CAN-FD) - max data size of 8 bytes. 
 * 
 * \param sock_fd
 *            CAN Socket to use (file descriptor)
 *
 * \param frame
 *            Pointer to a CAN frame to write the received data to
 * 
 * \return  CAN_ERROR_OK  Success
 * \return  [Error code]   Error - see cantools.h
 *
 */
int can_receive_frame(const int sock_fd, struct can_frame *frame);



/**
 * \brief Flush the receive buffer of a CAN socket
 * 
 * \param sock_fd
 *            CAN Socket to use (file descriptor)
 *
 * \return  CAN_ERROR_OK  Success
 * \return  CAN_ERROR_FLUSH_FAIL  
 *              Error - May be receiving data faster than it 
 *              can be pulled from the buffer.
 *
 */
int can_flush_recieve_buffer(const int sock_fd);



/**
 * \brief Write formatted CAN frame data to a string buffer
 * 
 * \param frame
 *            Pointer to a valid CAN frame
 *
 * \param str
 *            Pointer to a string buffer allocated by caller
 *
 * \param size
 *            Number of bytes available in str
 *
 */
void can_frame_dump_str(const struct can_frame *frame, char *str, size_t size);



/**
 * \brief Print out ID and data from CAN frame
 *
 * \param frame
 *            Pointer to a valid CAN frame
 *
 */
void can_frame_dump_debug(const struct can_frame *frame);



/**
 * \brief Print out details from a CAN error frame
 *
 * \param frame
 *            Pointer to a valid CAN error frame
 *
 */
void can_error_dump_debug(const struct can_frame *frame);


#endif  // CANTOOLS_H

