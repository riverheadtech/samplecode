#ifndef DS18B20_H_
#define DS18B20_H_

/************************************************
 *  Pin Macros: 
 *  These set the TRIS buffer and IO port to use.
 * 
 *  DSPIN - Set to the I/O pin to use (e.g. RC3)
 *  DSTRI - Set to the Tristate buffer bit for  
 *          the pin (e.g. TRISC3)
 * **********************************************/

#define DSPIN RC3
#define DSTRI TRISC3


unsigned char DS18ResetAndCheck( void );

void DS18WriteBitOne( void );
void DS18WriteBitZero( void );
unsigned char DS18ReadBit( void );

void DS18WriteChar( unsigned char );
unsigned char DS18ReadChar( void );

unsigned char DS18ReadAddress( unsigned char * );
unsigned char DS18Measure( void );
unsigned char DS18ReadTemp( unsigned char *, unsigned char *, unsigned char *);

void DS18TempToString( unsigned char, unsigned char );

#endif /*DS18B20_H_*/
