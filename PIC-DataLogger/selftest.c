#include <htc.h>
#include "globals.h"
#include "QLTools.h"
#include "bitbang.h"
#include "ds18b20.h"
#include "EEPROM.h"

/**************************************************************
 *  Self-Test Code
 **************************************************************/

void SelfTest( void )
  {
  unsigned char i, ucProbeAddPtr, ucResult, ucTempL, ucTempH;
   
  // **** Perform self-tests: ****
  // Some of this is commented out because there isn't enough room in the
  // program space for it!! 
  // Only performs a probe check.
  StringDump(ESSTEST);
  
  ///////// EEPROM Check: //////////////
  StringDump(ESMEMOR);
  ucResult = EEPROMWake();
  if (ucResult == 0x29)
    {  StringDump(ESOK);   }  // EEPROM Responded.
  else
    { StringDump(ESERROR); }  // No response from EEPROM!!   
  NEWLINE

  ///////// Check for probes: /////////////
  StringDump(ESPROBE);
  ucProbeAddPtr = PROBEADDBLK;  
  for (i=1; i<=(NPROBEBANKS*4); i++) 
    {
    // For each probe in a bank:
    eeprom_read_n(sProbeAddress, ucProbeAddPtr, 8);
    ucProbeAddPtr += 8;
    ucResult = DS18ReadTemp( sProbeAddress, &ucTempL, &ucTempH );
    if (ucResult == 0x00)    // At least one probe responded...
      {
      if ( (ucTempL == 0xFF) && (ucTempH == 0xFF) )
        {  StringDump(ESXX);    }  // Probe NOT found.
      else
        {  StringDump(ESOK);    }  // Probe Found.    
      }
    else    // NO probes responded! 
      {  
      StringDump(ESNOPRO);
      break;
      }
    }
  NEWLINE  
  }
