#ifndef BITBANG_H_
#define BITBANG_H_

/*****************************************************
 *  Serial BAUD rates (Pic @ 4 MHz):
 *    98 =  9600
 *    67 = 14400
 *    48 = 19200
 *****************************************************/
#define BAUD 67 

#define BBIN RC4    // Serial Input Pin
#define BBOUT RC5   // Serial Output Pin
 
void SerialOut( unsigned char );
unsigned char SerialIn( void );

#endif /*BITBANG_H_*/
