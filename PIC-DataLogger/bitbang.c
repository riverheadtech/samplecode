#include <htc.h>
#include "globals.h"
#include "bitbang.h"

/*********************************************************************
 * BitBang Serial Module:
 * 
 * Perform serial transmission on an I/O pin using 'bit-bang' 
 * brute force method. 
 *
 * USES:
 *   TIMER 0 - Should be set up for no prescaler, no interrupts. 
 *   2 x Digital I/O pins (one configured as Output, other as Input).
 *   I/O pins defined in bitbang.h by BBIN and BBOUT macros.  
 *   
 ***********************************************************************/


//---------------------------------------------------------
void SerialOut( unsigned char ucChr )
  {
  // This function sends a byte of data out an I/O pin of Port C
  // using 'bit-bang' approach. 
  // NOTE! serial is inverted to connect direct to PC serial port.
  // Baud timing is done by using TMR0 and removing
  // timer error after each baud.
  // Byte to send is in ucChr. 
  unsigned char i;
  BBOUT = 1;                      // Start bit.
  TMR0 = 30;                      //
  i=8;                            // 8 data bits to send
  TMR0 -= BAUD; T0IF = 0; while (!T0IF); // Wait one BAUD time
  while(i)                        // Send 8 serial bits, LSB first:
    {                             //
    if (ucChr & 0x01) BBOUT = 0;  // Invert and send data bit
    else              BBOUT = 1;  //
    ucChr = (ucChr >> 1);         // Rotate right to get next bit
    i--;                          //
    TMR0 -= BAUD; T0IF = 0; while ( !T0IF ); // Wait one BAUD time
    }
  BBOUT = 0;                      // Stop Bit.
  TMR0 -= BAUD; T0IF = 0; while ( !T0IF );
  TMR0 -= BAUD; T0IF = 0; while ( !T0IF );
  }
//---------------------------------------------------------





//---------------------------------------------------------
unsigned char SerialIn()
  {
  // Read a character from the serial pin using Bit-Banging. 
  // This function is called when the start bit is received 
  // and assumes that not very much time has passed since the
  // rising edge of the start bit. 
  // Times 0.5 BAUD, then checks to see if input is still 
  // high (start bit still active). 
  // If it is, times 1.0 BAUD then starts looking for bits. 
  // Bits of character transmitted in LSB first order, inverted.
  //
  unsigned char i, ucChr;
  //
  if (!BBIN) return 0x00;                  // False alarm - start bit has gone back to 0.
  i     = 1;
  ucChr = 0x00;                   
  TMR0  = 45;                     
  TMR0 -= BAUD; T0IF = 0; while (!T0IF);   // Wait for first bit. 
  while(i != 0)                            // Receive 8 serial bits, LSB first:
    {
    if (!BBIN) ucChr = (ucChr | i);        // Read value of next bit (inverted!)
    i = (i << 1);
    TMR0 -= BAUD; T0IF = 0; while (!T0IF); // Wait one BAUD
    }                            //        
  TMR0 -= BAUD; T0IF = 0; while (!T0IF);  //
  TMR0 -= BAUD; T0IF = 0; while (!T0IF);  // Wait a few bit times to be safe.
  return ucChr;
  }
//---------------------------------------------------------


/*
//---------------------------------------------------------
void StrOut( unsigned char *ucStrPtr )
  {
  unsigned char i;
  i = 0;
  while (i<100)
    {
    if (ucStrPtr[i]==0x00) return;
    SerialOut(ucStrPtr[i]);         // Send each digit of the result.
    i++;
    }
  }
//---------------------------------------------------------

*/



