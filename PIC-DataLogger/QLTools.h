#ifndef QLTOOLS_C_
#define QLTOOLS_C_


void PICWait( unsigned int );
void UIntToStr( unsigned int );
void AddressToStr( unsigned long *);

void eeprom_write_n(unsigned char *, unsigned char, unsigned char );
void eeprom_read_n(unsigned char *, unsigned char, unsigned char );

void Save24( unsigned long *, unsigned char );
void Restore24( unsigned long *, unsigned char );

void AddressAdvance( unsigned long *, unsigned long * );

void StringDump( unsigned char );

#endif /*QLTOOLS_C_*/
