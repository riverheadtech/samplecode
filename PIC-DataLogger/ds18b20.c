#include <htc.h>
#include "globals.h"
#include "ds18b20.h"
#include "QLTools.h"
#include "bitbang.h"

/*****************************************************
 *  DS18B20 1-wire digital thermometer
 * 
 * This file provides functions to support the DS18B20 
 * digital thermometer IC from Maxim. 
 * 
 * Note: The DS18B20 must be connected to a pin in 
 * IO port C. 
 * 
 * Uses Timer 0 (interrupts off) and assumes the chip
 * is clocked at 4 MHz (=1 MHz instruction rate). 
 *  
 * See ds18b20.h for pin macros 
 * (use to define which I/O pin to use) 
 * 
 *****************************************************/
unsigned char DS18ResetAndCheck()
  {
  // Reset temperature probes, and check for a response.
  // Returns 0 if sensors responded, 1 if no response. 
  unsigned char ucTemp;
  // Pull the signal line low:
  DSTRI = 0;      // Set DS18 pin to be output.   
  DSPIN = 0;      // Pull signal line low.
  // Wait 510 uS for reset: 
  TMR0 = 0; T0IF = 0; while ( !T0IF );  // Wait 255 uS
  TMR0 = 0; T0IF = 0; while ( !T0IF );  // Wait 255 uS
  DSPIN = 1;      // Raise signal line
  DSTRI = 1;      // Set DS18 pin to be input.
  TMR0 = 235; T0IF = 0; while ( !T0IF );  // Wait 20 uS
  // Check for a response from sensors (every 10 uS for 240 uS):
  ucTemp = 25;
  while (ucTemp > 1)
    {
    TMR0 = 245; T0IF = 0; while ( !T0IF );         // Wait 10 uS
    if ( DSPIN == 0) ucTemp = 1;        // Check for response.
    ucTemp--;
    }
  // Wait an additional 450 uS:
  TMR0 = 55; T0IF = 0; while ( !T0IF );     // Wait 200 uS
  TMR0 = 0; T0IF = 0; while ( !T0IF );      // Wait 255 uS
  return ucTemp;
  }


void DS18WriteBitOne()
  {
  // Write a 1 to the DS18 temperature probes:
  DSTRI = 0;                              // Set DS18 pin to be output   
  DSPIN = 0;                              // Pull signal line low
  DSPIN = 0; DSPIN = 0; DSPIN = 0;        // Repeat (adds 3 uS delay)
  DSPIN = 1;                              // Pull signal line high
  TMR0 = 195; T0IF = 0; while ( !T0IF );  // Wait 70 uS
  DSTRI = 1;                              // Set DS18 pin to be input  
  }


void DS18WriteBitZero()
  {
  // Write a 0 to the DS18 temperature probes:
  DSTRI = 0;                              // Set DS18 pin to be output   
  DSPIN = 0;                              // Pull signal line low
  TMR0 = 195; T0IF = 0; while ( !T0IF );  // Wait 70 uS
  DSPIN = 1;                              // Pull signal line high
  TMR0 = 245; T0IF = 0; while ( !T0IF );  // Wait extra 20 uS
  DSTRI = 1;                              // Set DS18 pin to be input  
  }



unsigned char DS18ReadBit()
  {
  // Issue a Read slot and sample the resulting bit:
  // Returns unsigned char value of 0x00 or 0x01.
  unsigned char ucTemp;
  ucTemp = 0x01;
  DSTRI = 0;                              // Set DS18 pin to be output
  DSPIN = 0; DSPIN = 0;                   // Pull signal line low (repeated to add delay)
  DSPIN = 1;                              // Pull the signal line high
  DSTRI = 1; DSTRI = 1; DSTRI = 1;        // Release the signal line (Set DS18 pin to be input) (Repeat to add delay)
  // Sample the bit: 
  if ( DSPIN == 0 ) ucTemp = 0x00;
  TMR0 = 195; T0IF = 0; while ( !T0IF );  // Wait extra 60 uS
  return ucTemp;
  }


void DS18WriteChar( unsigned char ucChr )
  {
  // Write an 8-bit character to the DS18 device:
  // LSB transmitted first. 
  unsigned char i;
  i = 8;
  while (i)
    {
    if (ucChr & 0x01)
      {
      DS18WriteBitOne();
      }
    else
      {
      DS18WriteBitZero();
      }
    ucChr = (ucChr >> 1);
    i--;
    }
  }


unsigned char DS18ReadChar()
  {
  // Read an 8-bit character from the DS18 device:
  // LSB transmitted first.  
  unsigned char i, ucTemp, ucAllBits;
  i = 0;
  ucAllBits = 0x00;
  while (i < 8)
    {
    ucTemp = DS18ReadBit();
    ucAllBits = ( ucAllBits | (ucTemp << i) );
    i++;
    }
  return ucAllBits;
  }



unsigned char DS18ReadAddress(unsigned char *sBuffer)
  {
  // Reads a 64-bit address from a DS18 device. 
  // NOTE: Uses the 'READ ROM' command which only works when there
  // is a single temperature IC connected to the input pin. 
  // The first 8 bytes of the string buffer pointed to by sBuffer
  // are set to the address (no null terminator is added). 
  // Bytes are stored in the order they are returned from the IC. 
  // Return Values: 
  //   0x00 - Device found and address read.
  //   0x01 - No device responded
  //
  unsigned char ucByteCount;
  // ###### Step 1: Reset and check: ########
  if (DS18ResetAndCheck() != 0x00) return 0x01;   // Probe not found... 
  // ##### 2 - ROM operation: ########
  DS18WriteChar(0x33);                    // Read ROM
  TMR0 = 40; T0IF = 0; while ( !T0IF );   // Wait 250 uS
  // ##### 3 - Issue READ slots to read back the address:
  for (ucByteCount=0; ucByteCount<8; ucByteCount++)
    { 
    sBuffer[ucByteCount] = DS18ReadChar(); 
    }
  return 0x00;
  }





unsigned char DS18Measure()
  {
  // Tell all probes on the specified pin to carry out a temperature measurement.
  // Return Values:
  //    0 - At least one probe was found and measurement was completed. 
  //    1 - No temperature probes responded
  //    2 - At least one probe was found, but a timeout occurred waiting 
  //        for measurement to be completed after measure instruction was 
  //        issued. 
  //
  unsigned int uiCounter;
  // ####### Step 1: Reset and check: ######## 
  if (DS18ResetAndCheck() != 0x00) return 0x01;   // Probe not found... 
  // ##### 2 - ROM operation: ########
  DS18WriteChar(0xCC);         // No ROM operation. 
  // ##### 3 - Issue Measure Temperature command: ########
  DS18WriteChar(0x44);
  // #### 4 - Issue 'Read' time slots and wait for completion signal: ########
  uiCounter = 0;
  while (uiCounter < 2000)
    {
    TMR0 = 40; T0IF = 0; while ( !T0IF );  //
    TMR0 = 40; T0IF = 0; while ( !T0IF );  // Wait 1000 uS
    TMR0 = 40; T0IF = 0; while ( !T0IF );  //
    TMR0 = 40; T0IF = 0; while ( !T0IF );  //
    if (DS18ReadBit()) break;
    uiCounter++;
    }
  if (uiCounter >= 2000)
    {
    return 0x02;  // Timeout waiting for temperature to be read.  
    }
  else
    {
    return 0x00;  
    }
  }





unsigned char DS18ReadTemp( unsigned char *sDS18Address, unsigned char *ucLoByte, unsigned char *ucHiByte )
  {
  // Read the temperature from a DS18 temperature IC on the specified pin:
  // 
  // ucLoByte, ucHiByte - Pointers to the lower and upper bytes of the temperature value, 
  //                      respectively. The values these point to will be set by the function. 
  // 
  // Return Values:
  //    0 - At least one probe was found and resutls were read. 
  //    1 - No temperature probes responded
  //
  //   sDS18Address - First 8 bytes of this string specify the address of the device to read.
  //   If there is only one device on the I/O pin, no address is required - 
  //   Set the first byte of sDS18Address to 0x00. (all DS18 devices will have a non-zero 
  //   first address byte)
  //  
  unsigned char ucByteIndex;
  // ###### Step 1: Reset and check: ########
  if (DS18ResetAndCheck() != 0x00) return 0x01;  // Probe not found... 
  // ##### 2 - ROM operation: ########
  if (sDS18Address[0] > 0)
    {
    //Address Supplied: 
    DS18WriteChar(0x55);            // use "Match ROM" operation...
    for (ucByteIndex=0; ucByteIndex<8; ucByteIndex++)
      {
      DS18WriteChar(sDS18Address[ucByteIndex]);   
      } // Sent an address byte.
    }
  else
    {
    // No address supplied:
    DS18WriteChar(0xCC);           // use "Skip ROM" operation...
    }
  // ##### Read RAM: ####
  DS18WriteChar(0xBE);
  *ucLoByte = DS18ReadChar();
  *ucHiByte = DS18ReadChar();
  return 0x00;
  }






void DS18TempToString( unsigned char ucTL, unsigned char ucTU )
  {
  // Convert the temperature value (upper and lower byte)
  // to an ASCII string.
  // ucTL, ucTU - Lower and upper bytes of the temperature value as read from the DS18
  //
  // UPDATE: Limited range to 0 - 99.99 degrees to make output more compact. Also 
  // ouptupts only 2 decimal places and uses rounding from remaining digit.
  // Negative values are output as 00.00
  //
  // Resulting ASCII string it sent out the serial port using SerialOut()
  //
  unsigned int uiValue;
  unsigned char ucPart;
  unsigned char ucTens, ucOnes, ucTenths, ucHundredths;
  //
  uiValue = ( ((unsigned int)ucTU << 8) | (unsigned int)ucTL );
  //
  if (ucTU & 0x80)
    {
    // Sign bit set - number is negative.
    StringDump(ESZEROV);
    return;
    }

  /*** Whole number part: ***/
  ucPart = (unsigned char)(uiValue >> 4);      // Extract whole number part.
  ucPart = ucPart % 100;                       // Hundreds digit is ignored. 
  ucTens = (ucPart / 10) + 0x30;               // Tens digit
  ucPart = ucPart % 10;                        // 
  ucOnes = ucPart + 0x30;                      // Ones digit

  /*** Fractional part: ***/ 
  ucPart = (unsigned char)(uiValue & 0x000F);  // Extract fractional part.
  // Convert value to decimal * 10,000: 
  uiValue = ((unsigned int)( ucPart         >> 3) * 5000) + 
            ((unsigned int)((ucPart & 0x04) >> 2) * 2500) +
            ((unsigned int)((ucPart & 0x02) >> 1) * 1250) +
            ((unsigned int)((ucPart & 0x01)     ) *  625);
  ucTenths = (uiValue / 1000) + 0x30;          // Tenths digit
  uiValue = uiValue % 1000;                    // 
  ucHundredths = (uiValue / 100) + 0x30;       // Hundredths digit
  uiValue = uiValue % 100;                     //
  // Check Remaining digits for rounding: 
  if ( (uiValue / 10) > 4 ) ucHundredths++;    // Round up! 
  if (ucHundredths > 0x39) 
    {
    ucHundredths = 0x30;  // Overflow 
    ucTenths++;           // to next!
    }
  if (ucTenths > 0x39) 
    {
    ucTenths = 0x30;  // Overflow 
    ucOnes++;         // to next!
    }
  if (ucOnes > 0x39) 
    {
    ucOnes = 0x30;    // Overflow 
    ucTens++;         // to next!
    }
  if (ucTens > 0x39) 
    {
    ucTens = 0x30;    // Overflow 
    }
  SerialOut(ucTens);        //
  SerialOut(ucOnes);        //
  SerialOut(0x2E);          // '.'
  SerialOut(ucTenths);      //
  SerialOut(ucHundredths);  //
  
  }

