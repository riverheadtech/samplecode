#ifndef GLOBALS_H_
#define GLOBALS_H_

// Sample rate: Sets the sample frequency (in seconds):
#define SAMPLERATE 3600 

// Number of probes: Probes are in banks of 4. Specify the number of BANKS here:
// (i.e. for 12 probes, NPROBEBANKS should be 3):
#define NPROBEBANKS 3

// Bank Size in bytes: 
#define BANKSIZE 8

// Record size in bytes: Should be NPROBEBANKS * BANKSIZE: 
#define RECORDSIZE 24 

/* *************************************************************
 *  Notes on Record Sizes: 
 *  Results from one bank of probes are written to EEPROM in 
 *  a single write cycle. Thus, the bank size should be a factor 
 *  of the 128 byte page size so that the write operation does 
 *  not overflow the page size. 
 *  ************************************************************ */ 

// Internal EEPROM address locations:
//  
//  0x00: WriteAddress HI
//  0x01: WriteAddress MED
//  0x02: WriteAddress LO
//  0x03: ReadAddress HI
//  0x04: ReadAddress MED
//  0x05: ReadAddress LO
//  0x06: StartAddress HI
//  0x07: StartAddress LO
//  0x08: StartAddress LO
//  0x09: Status Flag  0x55 = Normal; Other = First Boot!
//  0x0A - 0x0F: Reserved
//  0x10 - 0x89: Probe IDs
//  0x90 - 0xFF: String Table
//
#define WRITEADDRESS 0x00
#define READADDRESS  0x03
#define STARTADDRESS 0x06
#define STARTUPFLAG  0x09
#define PROBEADDBLK  0x10    // Start of EEPROM block containing probe addresses

// String Table Addresses: Address of the start of each string in EEPROM. 
#define ESTITLE   0x90
#define ESRESET   0x9E
#define ESSTEST   0xA5
#define ESMEMOR   0xB0
#define ESPROBE   0xB9
#define ESOK      0xC2
#define ESXX      0xC6
#define ESERROR   0xCA
#define ESNOPRO   0xD1
#define ESNOVAL   0xDC
#define ESZEROV   0xE3



#define NEWLINE SerialOut(0x0A);
#define SPACE SerialOut(0x20);

#define ONESECONDH 0x80
#define ONESECONDL 0x00

bit bOneSecond, bExtInt;

// String Buffers: Used for collecting Byte values and formatting output: 
unsigned char sDataBuffer[16];
unsigned char sProbeAddress[8];


unsigned long ulWriteAddress, ulReadAddress;

#endif /*GLOBALS_H_*/
