#include <htc.h>
#include "globals.h"
#include "RTC.h"



void RTCInit( void )
  {
  TMR1ON = 0;

  // Set up Timer 1 to be used for RTC:
//  TRISA4 = 1; TRISA5 = 1;  // Pins 2 and 3 set up to be used with 
//  ANS3 = 0;                // internal clock (can't be outputs)
  
  T1SYNC = 1;
  T1OSCEN = 1;    // Enable built-in 32.768 kHz oscillator for Timer 1
  TMR1CS = 1;     // Use 32.768 kHz oscillator as clock source
  TMR1GE = 0;     // Not gated

  T1CKPS1 = 0; T1CKPS0 = 0;  // Prescaler = 1
 
 /*
  // Delay while clock stabilises:
  unsigned int uiWaitTime;
  uiWaitTime = 2000;
  while (uiWaitTime > 0) 
    { 
    TMR0 = 40; T0IF = 0; while ( !T0IF );  // Use Timer 0 to make a 250 uS pause.
    uiWaitTime--;
    }
   
  TMR1H = ONESECONDH;  // 
  TMR1L = ONESECONDL;  // Reset timer 1 ready to time one second.
  */
  TMR1ON = 1;   // Start timer 1!
 
  }

