#include <htc.h>
#include "globals.h"
#include "bitbang.h"    // Bit-Bang serial comms
#include "ds18b20.h"    // DS18B20 1-wire digital thermometer code
#include "EEPROM.h"     // Serial EEPROM
#include "QLTools.h"    // Various useful functions
#include "selftest.h"   // Startup Self-Test

/////////////////////////////////////////////////////////
// Quick Logger Program
/////////////////////////////////////////////////////////


// Set config bits: 
__CONFIG(WDTDIS & PWRTEN & MCLRDIS & UNPROTECT & BORDIS & IESODIS & FCMDIS & INTIO); 


// *********** Program On-Chip EEPROM: **************************************
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  );  // 0x00 - 0x07: Address recovery
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  );  // 0x08 - 0x0F: Address recovery / Flags / Reserved

// ****** Probe IDs for probe string 006 (12 sensors): ********
__EEPROM_DATA( 0x28, 0x81, 0x11, 0xD9, 0x02, 0x00, 0x00, 0xAF );
__EEPROM_DATA( 0x28, 0x8F, 0x15, 0xD9, 0x02, 0x00, 0x00, 0xA3 );
__EEPROM_DATA( 0x28, 0x50, 0x47, 0xD9, 0x02, 0x00, 0x00, 0x55 );
__EEPROM_DATA( 0x28, 0x20, 0x3F, 0xD9, 0x02, 0x00, 0x00, 0x9E );
__EEPROM_DATA( 0x28, 0xE2, 0x12, 0xD9, 0x02, 0x00, 0x00, 0x7B );
__EEPROM_DATA( 0x28, 0xF7, 0x10, 0xD9, 0x02, 0x00, 0x00, 0x48 );
__EEPROM_DATA( 0x28, 0x50, 0x28, 0xD9, 0x02, 0x00, 0x00, 0x2B );
__EEPROM_DATA( 0x28, 0xAF, 0x34, 0xD9, 0x02, 0x00, 0x00, 0x20 );
__EEPROM_DATA( 0x28, 0x00, 0x3E, 0xD9, 0x02, 0x00, 0x00, 0xE5 );
__EEPROM_DATA( 0x28, 0x1B, 0x34, 0xD9, 0x02, 0x00, 0x00, 0xFB );
__EEPROM_DATA( 0x28, 0xAC, 0x33, 0xD9, 0x02, 0x00, 0x00, 0x28 );
__EEPROM_DATA( 0x28, 0x8D, 0x84, 0xD9, 0x02, 0x00, 0x00, 0xB7 );




// Probe IDs: Bank 3: (UNUSED)
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  );
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  );
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  );
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00  );

// Addresses 0x90 - : String Table: 
__EEPROM_DATA( 'Q', 'u', 'i', 'c', 'k', 'L', 'o', 'g' );            // 0x90
__EEPROM_DATA( ' ', '1', '.', '5', 0x0A, 0x00, 'R', 'E' );          // 0x98
__EEPROM_DATA( 'S', 'E', 'T', 0x0A, 0x00, 'S', 'e', 'l' );          // 0xA0
__EEPROM_DATA( 'f', ' ', 'T', 'e', 's', 't', 0x0A, 0x00 );          // 0xA8
__EEPROM_DATA( 'M', 'e', 'm', 'o', 'r', 'y', ':', ' ' );            // 0xB0
__EEPROM_DATA( 0x00, 'P', 'r', 'o', 'b', 'e', 's', ':' );           // 0xB8
__EEPROM_DATA( ' ', 0x00, 'O', 'K', ' ', 0x00, 'X', 'X' );          // 0xC0
__EEPROM_DATA( ' ', 0x00, 'E', 'R', 'R', 'O', 'R', '!' );           // 0xC8
__EEPROM_DATA( 0x00, 'N', 'o', ' ', 'P', 'r', 'o', 'b' );           // 0xD0
__EEPROM_DATA( 'e', 's', '!', 0x00, ',', '9', '9', '9'  );          // 0xD8
__EEPROM_DATA( '.', '9', 0x00, '0', '0', '.', '0', '0' );           // 0xE0
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 );
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 );
__EEPROM_DATA( 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 );





// **************************************************************************





/* ******** Interrupt service routine: ********************** */
void interrupt PICInterrupt(void)
  {
  /***** Timer 1 Rollover *****/
  if (TMR1IF)
    {
    TMR1IF=0;   // clear event flag
    bOneSecond = 1;   // Signal that one second has elapsed.
    TMR1ON = 0;
    TMR1H = ONESECONDH;  // 
    TMR1L = ONESECONDL;  // Reset timer 1 ready to time next second.
    TMR1ON = 1;
    }
  /* **** External interrupt (Serial Comm Started) ******** */
  if (INTF)
    {
    INTE = 0;     // Disable External Interrupt
    INTF = 0;     // Clear the interrupt flag.
    bExtInt = 1;  // External interrupt occrred... 
    }
  }
  






/* ******** Initialisation Code: ********************** */
void InitPIC(void)
  {
  SCS = 0;                          // Internal Oscillator
  IRCF2 = 1; IRCF1 = 1; IRCF0 = 0;  // @ 4 MHz
  
  // Turn off all interrupts:
  INTCON = 0x00; 
  PIE1 = 0x00; 
  PIE2 = 0x00;
  // Turn off analog on all pins:
  ANSEL = 0x00; ANSELH = 0x00;
  // Make all pins digital out: 
  TRISA = 0; TRISB = 0; TRISC = 0;
  PORTA = 0; PORTB = 0; PORTC = 0;
    
  // ***** Bitbang serial: *****
  TRISC4 = 1;  // C5 = Serial Input
  TRISA2 = 1;  // INT pin = Input (used to wake on serial comms)
  T0CS = 0;    // Set up Timer 0 as a timer driven off main clock
  T0IE = 0;    // Leave Timer 0 Interrupt off initially
  PSA = 1;     // No prescaler on Timer 0. 
  
  // ***** Probes: *****
  TRISC3 = 1;  // RC3 connected to temp probes.  

  // ***** EEPROM: ***** 
  TRISB4 = 1;               // SDI pin = Input
  SSPEN = 0;                // Disable SSP while setting config.
    SMP = 0;                // Sample at middle of output time
    CKP = 0;                // Clock line idles high
    CKE = 1;                // Sample on rising edge of clock
    SSPM3 = 0; SSPM2 = 0; SSPM1 = 1; SSPM0 = 0;   // SPI Master mode, clock = Fosc/64 
  SSPEN = 1;                // Enable SSP!
  EEPROM_CSPIN = 1;         // Pull CS High
  
 
  // **** Real Time timing with Timer 1: ***** 
  T1SYNC = 1;
  T1OSCEN = 1;                // Enable built-in 32.768 kHz oscillator for Timer 1
  TMR1CS = 1;                 // Use 32.768 kHz oscillator as clock source
  TMR1GE = 0;                 // Not gated
  T1CKPS1 = 0; T1CKPS0 = 0;   // Prescaler = 1
  TMR1H = 0;                  // 
  TMR1L = 0;                  // Reset timer 1.
  TMR1ON = 1;                 // Start.
  
  // **** Enable the interrupts we want to use: *****
  TMR1IE = 1;   // Timer 1
  PEIE   = 1;   // Peripherals
  INTEDG = 1;   // External interrupt on rising edge  
  INTE   = 0;   // Disable external interrupt pin (for now)
  GIE    = 1;   // Global interrupt enable!
 
  }












void main(void)
  {
  static bit bTestRead;
  unsigned char ucTL, ucTU, i, j, ucChr, ucProbeAddPtr, ucBankPtr;
  unsigned int uiTimer, uiRecordCount;

  
  InitPIC();
  NEWLINE
  StringDump(ESTITLE);
  bOneSecond = 0;
  bTestRead = 0;
  bExtInt = 0;

  // *** Address Recovery: ***************************************
  // Chip reset has occurred. Find the old Start and Stop addresses, 
  // and set Read and Write addresses. Also, add a 'Break' flag at
  // current location so that we know that there has been a chip 
  // reset. 
  if (eeprom_read(STARTUPFLAG) != 0x55)  // Check to see if this is the first boot...
    {  // First Boot!
    ulWriteAddress = 0x0;
    ulReadAddress  = 0x0;
    Save24( &ulWriteAddress, WRITEADDRESS );    // Save the new address pointer.
    Save24( &ulWriteAddress, READADDRESS  );    // (including Read and Log Start)
    Save24( &ulWriteAddress, STARTADDRESS );    //
    }
  else
    { // Normal Boot... read the address pointers from onchip EEPROM:
    Restore24( &ulWriteAddress, WRITEADDRESS ); 
    Restore24( &ulReadAddress,  READADDRESS  );
    }
  // Add break to show that device has been restarted:
  EEPROMWriteBreak( ulWriteAddress );
  AddressAdvance( &ulWriteAddress, &ulReadAddress );  
  eeprom_write(STARTUPFLAG, 0x55);                      // Update Status: Boot successful!. 
  // ***************************************************************

  
  // ********** Self Test ******************************************
  SelfTest();    
  // ***************************************************************
  
  // ***** Display Sample Rate: *******
  SerialOut('@');
  UIntToStr(SAMPLERATE);
  NEWLINE
  // **********************************
  
  
  uiTimer = SAMPLERATE;  
     
  // ########### MAIN LOOP: #####################
  while (1)
    {
  
    if (bOneSecond) 
      {
      // ***** One Second Timer has elapsed: *******
      uiTimer++;
      if ((uiTimer >= SAMPLERATE) || bTestRead )
        {
        // **************** SAMPLE!! ***********************************************
        // Display Address: 
        AddressToStr( &ulWriteAddress );
        SerialOut(',');
        // Tell sensors to make a reading:
        DS18Measure();
        // ***** Read Results: *****
          ucProbeAddPtr = PROBEADDBLK;  
          for (i=0; i<NPROBEBANKS; i++)
            {
            // For each bank of probes:
            ucBankPtr = 0;
            for (j=0; j<4; j++)
              {
              // For each probe in a bank: 
              eeprom_read_n(sProbeAddress, ucProbeAddPtr, 8);
              ucProbeAddPtr += 8;            // ...Get 8 bytes of Probe ID from EEPROM 
              DS18ReadTemp(sProbeAddress, &sDataBuffer[ucBankPtr], &sDataBuffer[ucBankPtr+1] ); 
                                             // ...Read this temperature sensor 
              DS18TempToString( sDataBuffer[ucBankPtr], sDataBuffer[ucBankPtr+1] );
              ucBankPtr = ucBankPtr + 2;     // Move bank data pointer.
              SerialOut(',');              
              }
            // Store the results from this bank to EEPROM:
            if (!bTestRead) EEPROMWriteBank( ulWriteAddress + (i*BANKSIZE), sDataBuffer );
            }
        // ***************************************************
        NEWLINE
        if (!bTestRead) AddressAdvance(&ulWriteAddress, &ulReadAddress); // Move to next EEPROM record.
        // *************************************************************************          
        if (!bTestRead) uiTimer = 0;
        }
      bOneSecond = 0;
      ucChr = 0x00;
      bTestRead = 0;
      }
   
    
    // ################ SLEEP: ############################################
    // If nothing is happening, go to sleep until the next timer interrupt.
    INTF = 0; // Clear any existing INT interrupt
    INTE = 1; // Enable external interrupt (wake up on serial comms)
    asm("sleep");  // Issue chip sleep command (direct assembler)
    asm("nop");    // First instruction after sleep. Use Nop.
    // ####################################################################

    
    
    if (BBIN) ucChr = SerialIn();  // Check to see if serial comms active
    
    switch (ucChr)  // Take an action if a control code was received: 
      {    
        
    case 0x00:     // Nothing to do!
      break;       
              
      
      
      
    case 't': // 't' received. Test Probes:
      bTestRead = 1;
      ucChr = 0x00;
      break;

    case 'T': // 'T' received. Self Test:
      SelfTest();
      ucChr = 0x00;
      break;
  
    
      
      
    case 'd':    // Download Data...
    case 'D':    // Download ALL Data...
      // Turn on External Interrupt... Allows interruption of download by sending serial character.
      bExtInt = 0;
      INTF = 0;   // Clear any existing INT interrupt
      INTE = 1;   // Enable external interrupt
      TMR1IE=0;   // Stop Timer Interrupt (may cause break in serial data).      
      if (ucChr == 'D')      // Download ALL data - set Read address to Log Start address.
        { Restore24( &ulReadAddress, STARTADDRESS );  }     
      NEWLINE
      // Output the number of records which are going to be sent:
      uiRecordCount = (ulWriteAddress - ulReadAddress)/RECORDSIZE;
      SerialOut('#');
      UIntToStr( uiRecordCount );
      NEWLINE  // Output blank lines to signal the start of the data. 
      NEWLINE  //
      // Output the data: 
      while (ulReadAddress != ulWriteAddress)
        {
        AddressToStr( &ulReadAddress );          
        // Output Temperatures (one bank at a time):
        for (i=0; i<NPROBEBANKS; i++)
          {
          // For each bank of results: 
          EEPROMReadBank( ulReadAddress + (i*BANKSIZE), sDataBuffer );
          ucBankPtr = 0;
          for (j=0; j<4; j++)
            {
            // For each result in bank: 
            if ( (sDataBuffer[0] == EBREAK) && (sDataBuffer[1] == EBREAK) )  // BREAK flag:
              {  StringDump(ESNOVAL);    }
            else
              {
              SerialOut(',');
              DS18TempToString( sDataBuffer[ucBankPtr], sDataBuffer[ucBankPtr+1] );
              ucBankPtr = ucBankPtr + 2;
              }
            }    
          }
        if (bExtInt)  // An interrupt has occurred - a serial command was received!
          { break; }
        ulReadAddress = ulReadAddress + RECORDSIZE;  
        NEWLINE
        }
      NEWLINE  // Output blank lines to signal the end of the data. 
      NEWLINE  //
      if (!bExtInt)
        {
        // Save the new READ address:
        Save24( &ulReadAddress, READADDRESS );
        }
      TMR1IE=1;              // Re-enabe Timer Interrupt.
      ucChr = 0x00;
      break;
    
   
    // Reset Logger Addresses:
    case 'R':    // Reset read to current write point
    case 'Z':    // Reset all addresses to 0. 
      if (ucChr == 'Z') ulWriteAddress = 0x00;   // FULL reset.
      StringDump(ESRESET);
      Save24( &ulWriteAddress, WRITEADDRESS );
      Save24( &ulWriteAddress, READADDRESS  );
      Save24( &ulWriteAddress, STARTADDRESS );
      ulReadAddress = ulWriteAddress;
      uiTimer = SAMPLERATE;                      // Reset main timer
      ucChr = 0x00;
      break;     
      

      
      
    // Show hello message: 
    case '*': 
      StringDump(ESTITLE);
      ucChr = 0x00;
      break;
      
      }  // switch case
    
    
    }   // while (1) 
  }    // main()
