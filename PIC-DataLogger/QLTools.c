#include <htc.h>
#include "globals.h"
#include "QLTools.h"
#include "bitbang.h"
#include "ds18b20.h"
#include "EEPROM.h"


void PICWait( unsigned int uiWaitTime )
  {
  // Wait the specified interval;
  // uiWaitTime specifies the number of 250 uS units.
  while (uiWaitTime > 0) 
    { 
    TMR0 = 40; T0IF = 0; while ( !T0IF );  // Use Timer 0 to make a 250 uS pause.
    uiWaitTime--;
    }
  }







void UIntToStr( unsigned int uiValue )
  {
  // Make a text string representing an unsigned integer: 
  // 16 Bit Unsigned Int: Range is 0 - 65535.
  //unsigned char ucWritePtr = 0;
  unsigned char ucOutChr;
  
  ucOutChr = (unsigned char)(uiValue / 10000) + 0x30;  // 10,000's
  uiValue = uiValue % 10000;
  SerialOut(ucOutChr);
  
  ucOutChr = (unsigned char)(uiValue / 1000) + 0x30;   // 1,000's
  uiValue = uiValue % 1000;
  SerialOut(ucOutChr);

  ucOutChr = (unsigned char)(uiValue / 100) + 0x30;   // 100's
  uiValue = uiValue % 100;
  SerialOut(ucOutChr);
   
  ucOutChr = (unsigned char)(uiValue / 10) + 0x30;   // 10's
  SerialOut(ucOutChr);
  
  ucOutChr = (unsigned char)(uiValue % 10) + 0x30;   // 1's
  SerialOut(ucOutChr);
 
  }


void AddressToStr( unsigned long *ulAddress )
  {
  // Output an unsigned long address as a srting to the serial port:  
  UIntToStr( (unsigned int)(*ulAddress>>16) );
  SerialOut('-');
  UIntToStr( (unsigned int)(*ulAddress & 0x0000FFFF) );
  }









/*******************************************************************
 * Internal EEPROM Functions: 
 * *****************************************************************/

void eeprom_write_n(unsigned char *sBuffer, unsigned char ucAddress, unsigned char nCount)
  {
  // Writes n Bytes from sBuffer to internal EEPROM starting at address ucAddress.
  unsigned char i;
  for (i=0; i<nCount; i++)
    {
    eeprom_write( ucAddress, sBuffer[i] );
    ucAddress++;
    }
  }

void eeprom_read_n(unsigned char *sBuffer, unsigned char ucAddress, unsigned char nCount)
  {
  // Reads n Bytes from internal EEPROM to sBuffer starting at eeprom address ucAddress:
  unsigned char i;
  for (i=0; i<nCount; i++)
    {
    sBuffer[i] = eeprom_read(ucAddress);
    ucAddress++;
    }
  }



void Save24(unsigned long *ulSaveValue, unsigned char ucAddress)
  {
  // Writes a 24 bit value to on-chip EEPROM at 
  // ucAddress (MSByte) to ucAddress+2 (LSByte)
  eeprom_write(  ucAddress,   (unsigned char)((*ulSaveValue & 0x00FF0000) >> 16)  );
  eeprom_write(  ucAddress+1, (unsigned char)((*ulSaveValue & 0x0000FF00) >> 8)   );
  eeprom_write(  ucAddress+2, (unsigned char)(*ulSaveValue & 0x000000FF)          );
  }

void Restore24(unsigned long *ulRestoreValue, unsigned char ucAddress)
  {
  // Reads a 24 bit value from on-chip EEPROM starting at 
  // ucAddress (MSByte) to ucAddress+2 (LSByte)
  *ulRestoreValue = ((unsigned long)eeprom_read(ucAddress  ) << 16) |
                    ((unsigned long)eeprom_read(ucAddress+1) << 8)  |
                     (unsigned long)eeprom_read(ucAddress+2);    
  }


/*******************************************************************
 * Serial EEPEOM Addressing Functions: 
 * *****************************************************************/


void AddressAdvance(unsigned long *ulWriteAddress, unsigned long *ulReadAddress)
  {
  // Advance the Write Address by one record. 
  // If it passes 0xFFFFFFFF it will automatically wrap to 0x00000000.
  //
  // Also check the Read Address and Log Start Address; 
  // If they have been reached (i.e. data wrapped around), 
  // They should also be incremented ('pushed out').
  //
  // Note that the lower 24 bits are used for the EEPROM address, of which
  // only the lower 17 are actually important for a 1024 KBit EEPROM.
  // However, using a 32-bit unsigned long address is convenient. If the 
  // value goes past 0x1FFFF (maximum EEPROM address), the lower 17 bits will 
  // go back to 0 and the address space will appear to 'wrap' around which is 
  // what we want to happen.
  //
  *ulWriteAddress = *ulWriteAddress + RECORDSIZE;  // Advance write address.
  Save24( ulWriteAddress, WRITEADDRESS );          // Save the new address pointer.  
  if (*ulWriteAddress == *ulReadAddress) 
    {
    // Caught up with Read Address:  
    *ulReadAddress = *ulReadAddress + RECORDSIZE;  // Push out read address.
    // Update addresses in EEPROM:
    Save24( ulReadAddress, READADDRESS  );  // Read Address
    Save24( ulReadAddress, STARTADDRESS );  // Start Address (=Read Address since we have now wrapped.)
    }  
  }



void StringDump( unsigned char ucAddress)
  {
  // Outputs string data from the internal EEPROM starting at 
  // address ucAddress, until 0x00 is encoutered or the address
  // 0xFF is reached.
  unsigned char ucTemp;
  while ( ucAddress < 0xFF)
    {
    ucTemp = eeprom_read(ucAddress);
    if (ucTemp == 0x00) break;
    SerialOut(ucTemp);
    ucAddress++;
    }
  }


