#include <htc.h>
#include "globals.h"
#include "EEPROM.h"

/* ****************************************************************
 * Serial EEPROM Code
 * 
 * Functions to write from and read to an Serial EEPROM. 
 * 
 * USES: 
 *   SSP Module (Synchronous serial port)
 *   PORT C (one pin used to control the CS line of the EEPROM,
 *           should be configured as DIGITAL OUTPUT).
 *  RECORDSIZE - Macro (defined in globals.h) which resolves to 
 *               record size in Bytes 
 * 
 * ****************************************************************/



unsigned char EEPROMByte( unsigned char ucChr )
  {
  // Write a byte to EEPROM (don't change Chip Select Line)
  // After the byte has been written, the SSPBUF register
  // is read again to get back any received byte. 
  // This function is used to both send and receive data.
  unsigned char ucTemp;
  ucTemp = SSPBUF;                 // Read the SSP buffer to clear it.
  WCOL = 0;                        // Clear write collision bit
  SSPBUF = ucChr;                  // Load the buffer.
  while (!BF);                     // Wait for transmission to finish.
  ucTemp = SSPBUF;                 // Read the buffer
  return ucTemp;
  }





void EEPROMWriteByte( unsigned char ucChr )
  {
  EEPROM_CSPIN = 0;        // Pull CS Low
  EEPROMByte(ucChr);       // Write Data
  EEPROM_CSPIN = 1;        // Pull CS High
  }




void EEPROMStartWrite( unsigned long ulAddress )
  {
  // Set Write enable latch: 
  EEPROMWriteByte(0x06);                            // Send Write Enable instruction. 
  // Send the 'WRITE' instruction:
  EEPROM_CSPIN = 0;                                 // Pull CS Low
  EEPROMByte(0x02);                                 // Send 'WRITE' instruction
  EEPROMByte( (unsigned char)((ulAddress >> 16) & 0x000000FF) );  // Send upper byte of address
  EEPROMByte( (unsigned char)((ulAddress >> 8) & 0x000000FF)  );  // Send middle byte of address  
  EEPROMByte( (unsigned char)(ulAddress & 0x000000FF)         );  // Send lower byte of address
  }



void EEPROMEndWrite( void )
  {
  unsigned char ucTemp;    
  EEPROM_CSPIN = 1;                                    // Pull CS High to start write
  // Wait for the Write to complete:
  ucTemp = 0xFF;
  while (ucTemp & 0x01)
    {
    // Reads EEPROM Status reg:  
    EEPROM_CSPIN = 0;                // Pull CS Low
    // ****** Send 'Read Status' instruction: **********
    EEPROMByte(0x05);     
    // ****** Read Result: **********
    ucTemp = EEPROMByte(0x00); 
    EEPROM_CSPIN = 1;                // Pull CS High
    }
  }


unsigned char EEPROMReadAt( unsigned long ulAddress )
  {
  unsigned char ucTemp;
  EEPROM_CSPIN = 0;                                 // Pull CS Low
  EEPROMByte(0x03);                                 // Send 'READ' instruction
  EEPROMByte( (unsigned char)((ulAddress >> 16) & 0x000000FF) );  // Send upper byte of address
  EEPROMByte( (unsigned char)((ulAddress >> 8) & 0x000000FF)  );  // Send middle byte of address  
  EEPROMByte( (unsigned char)(ulAddress & 0x000000FF)         );  // Send lower byte of address 
  ucTemp = EEPROMByte(0xFF);                        // Read the returned data
  EEPROM_CSPIN = 1;                                 // Pull CS High
  return ucTemp;
  }



void EEPROMWriteBank( unsigned long ulAddress, unsigned char *sBuffer )
  {
  // Writes the number of bytes specified by BANKSIZE to the 
  // EEPROM address uiAddress. sBuffer is a pointer to the first
  // byte to be written.
  unsigned char i;
  EEPROMStartWrite( ulAddress );
  for (i=0; i<BANKSIZE; i++) EEPROMByte(sBuffer[i]);
  EEPROMEndWrite();
  }


void EEPROMReadBank( unsigned long ulAddress, unsigned char *sBuffer )
  {
  // Reads the number of bytes specified by RECORDSIZE from the 
  // EEPROM address uiAddress. sBuffer is a pointer to a buffer
  // to hold the read data. 
  unsigned char i;
  for (i=0; i<BANKSIZE; i++) sBuffer[i] = EEPROMReadAt(ulAddress+i);
  }


void EEPROMWriteBreak( unsigned long ulAddress )
  {
  // Write a 'Break' record to the EEPROM: 
  unsigned char i,j;
  for (i=0; i<NPROBEBANKS; i++)
    {
    EEPROMStartWrite( ulAddress + (i*BANKSIZE) );
    for (j=0; j<BANKSIZE; j++) EEPROMByte(EBREAK);  
    EEPROMEndWrite();
    }
  }




void EEPROMSleep()
  {
  // Place the EEPROM in low power sleep mode:
  EEPROMWriteByte(0xB9);
  }




unsigned char EEPROMWake()
  {
  // Wake the EEPROM from low power sleep mode:
  // Returns the EEPROM electronic signature.
  unsigned char ucTemp = 0;
  EEPROM_CSPIN = 0;          // Pull CS Low
  EEPROMByte(0xAB);          // RDID instruction
  EEPROMByte(0x00);          //
  EEPROMByte(0x00);          // Dummy Address
  EEPROMByte(0x00);          //
  ucTemp = EEPROMByte(0xFF); // Read electronic signature
  EEPROM_CSPIN = 1;          // Done! Pull CS High
  return ucTemp;
  }

