Simple Data Logger for PIC Micro Controller
-------------------------------------------

This directory contains C code for a data logger using a PIC16F690.
Temperatures are sampled from DS18B20 digital thermometer ICs, and
stored to a serial EEPROM. Temperature readings are stored 
periodically in EEPROM a ring-buffer, and the next storage address
is stored in FLASH memory in the controller so that logging can
be resumed after a power loss. 

A simple serial interface is used, to allow data to be downloaded
and the storage address to be reset. 

To minimise power usage, the PIC is kept in low power sleep for
most of the time, using a 32 kHz oscillator and low power timer to 
wake the device every second. Logging is carried out at pre-set 
intervals which are multiples of the 1-second timer.

This program was originally compiled with HiTech C. 
