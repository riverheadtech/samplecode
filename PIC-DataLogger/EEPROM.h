#ifndef EEPROM_H_
#define EEPROM_H_

#define EEPROM_CSPIN RC2

#define EBREAK 0xFF

unsigned char EEPROMByte( unsigned char );

void EEPROMWriteByte( unsigned char );

void EEPROMStartWrite( unsigned long );
void EEPROMEndWrite( void );

unsigned char EEPROMReadAt( unsigned long );

void EEPROMWriteBank( unsigned long, unsigned char * );
void EEPROMReadBank( unsigned long, unsigned char * );

void EEPROMWriteBreak( unsigned long );

void EEPROMSleep( void );
unsigned char EEPROMWake( void );

#endif /*EEPROM_H_*/
