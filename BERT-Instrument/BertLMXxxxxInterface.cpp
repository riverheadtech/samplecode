/*!
 \file   BertLMXxxxxInterface.cpp
 \brief  Texas Instruments LMX Clock Synth Hardware Interface Implementation
         This class provides a common interface for the LMX2592 and LMX2594.
         Some functionality is part-specific and provided by derived clases.
 \author J Cole-Baker (For Smartest)
 \date   Apr 2018
*/

#include <QDebug>

#include "globals.h"
#include "BertComms.h"
#include "BertLMXxxxxInterface.h"
#include "BertFile.h"


// Constants used to populate the Trigger Divide Ratio list:
#ifdef BERT_DEBUG_DIV_RATIOS
const QStringList BertLMXxxxxInterface::TRIGGER_DIVIDE_LIST =
  { "1", "1/2", "1/4", "1/8", "1/16", "1/32", "1/128", "1/192" };
const size_t BertLMXxxxxInterface::DEFAULT_DIVIDE_RATIO_INDEX = 6;
#else
const QStringList BertLMXxxxxInterface::TRIGGER_DIVIDE_LIST =
  { "1", "1/2", "1/4" };
const size_t BertLMXxxxxInterface::DEFAULT_DIVIDE_RATIO_INDEX = 1;
#endif

// Constants used for the "OUTA_POW" and "OUTB_POW" register fields.
// These entries correspond to the list of power settings supplied
// by getOutputPowerList method.
const uint8_t BertLMXxxxxInterface::POWER_M5DBM = 0;
const uint8_t BertLMXxxxxInterface::POWER_0DBM  = 10;
const uint8_t BertLMXxxxxInterface::POWER_5DBM  = 20;
/* REMOVED 1-Sep-16:
const uint8_t BertLMXxxxxInterface::POWER_10DBM = 31;
const uint8_t BertLMXxxxxInterface::POWER_15DBM = 60;
*/
const uint8_t BertLMXxxxxInterface::POWER_CONSTS[] =
  { POWER_M5DBM, POWER_0DBM, POWER_5DBM }; // REMOVED 1-Sep-16: , POWER_10DBM, POWER_15DBM };
const size_t BertLMXxxxxInterface::POWER_CONSTS_SIZE =
         sizeof(POWER_CONSTS) / sizeof(POWER_CONSTS[0]);

const size_t BertLMXxxxxInterface::DEFAULT_FOUT_POWER_INDEX = 2;    // Default power setting for main clock (5 DBM)
const size_t BertLMXxxxxInterface::DEFAULT_TRIG_POWER_INDEX = 1;    // Default power setting for trigger out (0 DBM)

const QStringList BertLMXxxxxInterface::OUTPUT_POWER_LIST =
  { "-5 dBm", " 0 dBm", " 5 dBm" }; // REMOVED 1-Sep-16: , "10 dBm", "15 dBm" };



/*!
* \brief BertLMXxxxxInterface Base Class Constructor
* Sets up the base class for an LMX clock source (common features for LMX2592 and LMX2594)

* \param useBertComms           Pointer to a BertComms class (handles adaptor-level comms with instrument)
* \param useI2CSlaveAddress     I2C Slave address of the LMX part
* \param useMaxRegisterAddress  Maximum allowed register address of this part
*                               (Nb: different for LMX2592 vs LMX2594)
*/
BertLMXxxxxInterface::BertLMXxxxxInterface(BertComms *useBertComms, const uint8_t useI2CSlaveAddress, const uint8_t useMaxRegisterAddress)
{
    // DEBUG qDebug() << "BertLMXxxxxInterface Constructor: Slave Address: " << (int)useI2CSlaveAddress << "; Max Register: " << useMaxRegisterAddress;
    maxRegisterAddress = useMaxRegisterAddress;
    bertComms = useBertComms;
    i2cSlaveAddress = useI2CSlaveAddress;
    spiAdaptorIsOpen = false;

    selectedTrigDivideIndex = DEFAULT_DIVIDE_RATIO_INDEX;
    selectedFOutOutputPowerIndex = DEFAULT_FOUT_POWER_INDEX;
    selectedTrigOutputPowerIndex = DEFAULT_TRIG_POWER_INDEX;
}


BertLMXxxxxInterface::~BertLMXxxxxInterface()
{ }



//*****************************************************************************************
//   Comms Interface Methods
// *****************************************************************************************

/* DEPRECATED Move to BertComms module!
 \brief Lock Bert Interface
 This method will prevent other interface users (e.g. timer events)
 from calling BERT interface functions. This is useful when carrying
 out a sequence of operations which shouldn't be interrupted.
 The caller MUST call interfaceFree when finished to release the
 BERT interface for other users.

 \param timeout   Time to wait if the interface is already locked

 \return true   Interface Locked OK
 \return false  Interface was already in use and timeout expired
*
DEPRECATED int BertLMXxxxxInterface::interfaceLock(int timeout)
{
    if (!interfaceMutex.tryLock(timeout))
    {
        return false;
    }
    else
    {
        return true;
    }
}
*/

/* DEPRECATED Move to BertComms module!
 \brief Free Bert Interface
DEPRECATED  Frees the interface (Note: Must be previously locked by interfaceLock)
*
DEPRECATED void BertLMXxxxxInterface::interfaceFree()
{
    interfaceMutex.unlock();
}
*/




//*****************************************************************************************
//   LMX Clock Synth Methods
// *****************************************************************************************

/*!
 \brief Initialise LMX Interface
 This method sets up the I2C->SPI adaptor used to communicate
 with the LMX, then initialises the LMX.
 Assumes that general comms are already open (bertComms object).
 \return globals::OK   Success
 \return [error code]  Failed to initialise the LMX
*/
int BertLMXxxxxInterface::init()
{
    // Initialise the comms (I2C to SPI adaptor):
    int result = initAdaptor();
    if (result != globals::OK)
    {
        spiAdaptorIsOpen = false;
        return result;  // ERROR setting up comms!
    }
    spiAdaptorIsOpen = true;

    // Initialise the LMX to safe startup state:
    // Nb: Implementation may be part-specific
    result = initPart();

    return result;
}

/*!
  \brief Determine the clock part number by looking at register definition files
         Register definitions are stored in ".tcs" file format (generated by
         Texas Instruments TICS Pro software). This method searches a specified
         directory for the first suitable file, and returns a string containing
         the part number, e.g. "LMX2592".

  \param searchPath   File path to search for files
  \param partNo       On success, this will be set to the part number

 \return globals::OK
 \return globals::DIRECTORY_NOT_FOUND
 \return globals::FILE_ERROR
*/
int BertLMXxxxxInterface::getPartFromRegisterFiles(const QString searchPath, QString &partNo)
{
    qDebug() << "Find clock part number from frequency profiles in " << searchPath;
    QStringList freqFiles;
    int result;
    result = BertFile::readDirectory(searchPath, freqFiles);
    if (freqFiles.empty())
    {
        qDebug() << "No frequency profiles found. (Error: " << result << ")";
        return result;
    }
    qDebug() << freqFiles.count() << " files found. Parsing...";
    bool flagSetupSection = false;
    foreach( QString fileName, freqFiles )
    {
        qDebug() << "File: " << fileName;
        QStringList defLines;
        result = BertFile::readFile(searchPath + QString("\\") + QString(fileName), 100, defLines);  // Setup section must be in the first 100 lines.
        if (defLines.empty())
        {
            qDebug() << "  -Couldn't read file! (Error: " << result << ")";
        }
        else
        {
            foreach( QString defLineFull, defLines )
            {
                QString defLine = defLineFull.trimmed();
                if (defLine.left(1) == "[")
                {
                    // Section break;
                    if (defLine == "[SETUP]") flagSetupSection = true;  // New section is "[SETUP]"
                    else                      flagSetupSection = false;
                    continue;
                }
                else
                {
                    if (flagSetupSection
                     && defLine.length() > 5
                     && defLine.left(5) == "PART=")
                    {
                        // PART=... line found!
                        partNo = defLine.mid(5);
                        qDebug() << "  -Found part number: " << partNo;
                        return globals::OK;
                    }
                }
            }
        }
    }
    qDebug() << "Warning: No part number found!";
    return globals::GEN_ERROR;  // Part number not found!
}




/*!
 \brief Build a list of frequency profiles based on ".tcs" files found in a specified directory

 \param searchPath   File path to search for files
 \param partNo       Clock part number, e.g. "LMX2592". Any files which don't have this part
                     number (i.e. "PART=..." under "[SETUP]" section) will be rejected.

 \return globals::OK
 \return globals::DIRECTORY_NOT_FOUND
 \return globals::FILE_ERROR
*/
int BertLMXxxxxInterface::initFrequencyProfiles(const QString searchPath, const QString partNo)
{
    qDebug() << "Load frequency profiles from " << searchPath;
    frequencyProfiles.clear();
    frequencyList.clear();
    QStringList freqFiles;
    int result;
    result = BertFile::readDirectory(searchPath, freqFiles);
    if (freqFiles.empty())
    {
        qDebug() << "No frequency profiles found. (Error: " << result << ")";
        return result;
    }
    qDebug() << freqFiles.count() << " files found. Parsing...";
    foreach( QString fileName, freqFiles )
    {
        qDebug() << "File: " << fileName;
        QStringList defLines;
        result = BertFile::readFile(searchPath + QString("\\") + QString(fileName), 400, defLines);
        if (defLines.empty())
        {
            qDebug() << "  -Couldn't read file! (Error: " << result << ")";
        }
        else
        {
            BertLMXFrequencyProfile thisFrequencyProfile(partNo, defLines, maxRegisterAddress);
            if (thisFrequencyProfile.isValid())
            {
                // File was parsed OK! (valid frequency profile):
                if (frequencyProfiles.empty())
                {
                    frequencyProfiles.append(thisFrequencyProfile);
                }
                else
                {
                    // Already have frequencies. We want to insert this one
                    // in the correct place in the list (sorted by frequency):
                    QList<BertLMXFrequencyProfile>::iterator i;
                    for (i = frequencyProfiles.begin(); i != frequencyProfiles.end(); ++i)
                    {
                        if (i->getFrequency() >= thisFrequencyProfile.getFrequency())
                        {
                            frequencyProfiles.insert(i, thisFrequencyProfile);
                            break;
                        }
                    }
                    if (i == frequencyProfiles.end())
                    {
                        frequencyProfiles.append(thisFrequencyProfile);
                    }
                }
                qDebug() << "Parsed OK; Freq = " << thisFrequencyProfile.getFrequency();
            }
        }
    }

    // Set safe defaults for frequency profiles:
    setSafeDefaults();

    // Create list of frequency values to display in UI:
    uint16_t thisIndex = 0;
    QList<BertLMXFrequencyProfile>::iterator i;
    for (i = frequencyProfiles.begin(); i != frequencyProfiles.end(); ++i)
    {
        float thisFrequency = i->getFrequency();
        frequencyList.append(
                    QString().sprintf("%2.2f GHz  (%2.2f Gbps)",
                                      thisFrequency / 1000.0,
                                      thisFrequency / 500.0)
                    );
        // Profiles should already be sorted by frequency.
        // Set profileIndexDefault to the index of the highest frequency
        // less than or equal to 10GHz.
        if (thisFrequency <= 10000.0) profileIndexDefault = thisIndex;
        thisIndex++;
    }
    qDebug() << "-----------------------------";
    qDebug() << frequencyProfiles.count() << " frequency profiles found.";
    qDebug() << "Default profile: " << profileIndexDefault
             << " (" << frequencyProfiles.at(profileIndexDefault).getFrequency() << " MHz)";
    qDebug() << "-----------------------------";

    return globals::OK;
}


/*!
 \brief Get clock synth frequency for a given profile
 This can be used after initFrequencyProfiles, to return the
 frequency of a specific profile. Nb: Generally used in
 conjunction with getFrequencyList; i.e. a combo box in the
 UI is populated with the frequency list, and when an item is
 selected, getFrequency is used to find the frequency of the
 selected item.
 \param index               Index of profile to get frequency from - 0 is first
 \param frequency           Pointer to a float; set to the frequency on success (MHz)
 \return globals::OK        Item found for requested index.
 \return globals::OVERFLOW  Index was larger than the list of frequency profiles
*/
int BertLMXxxxxInterface::getFrequency(const size_t index, float *frequency) const
{
    if (index >= (size_t)frequencyProfiles.count()) return globals::OVERFLOW;
    if (frequency) *frequency = frequencyProfiles.at(index).getFrequency();
    return globals::OK;
}


/*!
 \brief Select Frequency
 Searches the profile table for the specified frequency, and
 loads that profile. If no exact match is found, the loaded
 frequency will be the next LOWER frequency to the specified value.
 \param frequency Frequency in MHz
 \param index (OUT) Returns the index of the selected clock profile, or 0.
 \return globals::OK       Item found for requested index.
 \return globals::OVERFLOW Frequency list not loaded
 \return [Error Code]      Comms error, etc
*/
int BertLMXxxxxInterface::selectFrequency(const float frequency, size_t *index)
{
    *index = 0;
    if ((size_t)frequencyProfiles.count() < 1) return globals::OVERFLOW;
    size_t i;
    float thisFrequency;
    for (i=0; i<(size_t)frequencyProfiles.count(); i++)
    {
        thisFrequency = frequencyProfiles.at(i).getFrequency();
        if (thisFrequency == frequency)
        {
            *index = i;
            return selectProfile(i); // Found!
        }
        if (thisFrequency > frequency)
        {
            // Past the requested frequency: select previous frequency.
            if (i > 0)
            {
                *index = i - 1;
                return selectProfile(i-1);
            }
            else
            {
                *index = 0;
                return selectProfile(0);
            }
        }
    }
    *index = i - 1;
    return selectProfile(i-1); // Not found. Select LAST frequency.
}


/*!
 \brief Select Frequency Profile by Index
 \param index               Index of profile to get frequency from - 0 is first
 \return globals::OK        Item found for requested index.
 \return globals::OVERFLOW  Index was larger than the list of frequency profiles
 \return [Error Code]       Error from derived class implementation (selectProfilePart)
*/
int BertLMXxxxxInterface::selectProfile(const size_t index)
{
    return selectProfilePart(index);
}


/*!
 \brief Select FOut Power
 \param index              Index of new power setting (see getOutputPowerList)
 \return globals::OK       Power set OK
 \return globals::OVERFLOW Invalid index
 \return [Error Code]      Error code from configureOutputs (derived class)
*/
int BertLMXxxxxInterface::selectFOutPower(const size_t index)
{
    if (index >= POWER_CONSTS_SIZE) return globals::OVERFLOW;
    qDebug() << "LMX: Select FOut power index " << index;
    selectedFOutOutputPowerIndex = index;
    return configureOutputs();
}


/*!
 \brief Select Trig Out Power
 \param index              Index of new power setting (see getOutputPowerList)
 \return globals::OK       Power set OK
 \return globals::OVERFLOW Invalid index
 \return [Error Code]      Error code from configureOutputs (derived class)
*/
int BertLMXxxxxInterface::selectTrigOutPower(const size_t index)
{
    selectedTrigOutputPowerIndex = index;
    return configureOutputs();
}


/*!
 \brief Select Trigger Divide Ratio
 \param index              Index of new divide ratio (see getTriggerDivideList)
 \return globals::OK       Ratio set OK
 \return globals::OVERFLOW Invalid index
 \return [Error Code]      Error code from selectTriggerDividePart (derived class)
*/
int BertLMXxxxxInterface::selectTriggerDivide(const size_t index, const bool doFCal)
{
    return selectTriggerDividePart(index, doFCal);
}



/*!
 \brief Turn output drivers On
 \return globals::OK       Outputs turned on OK
 \return [Error Code]      Error code from configureOutputs (derived class)
*/
int BertLMXxxxxInterface::outputsOn()
{
    qDebug() << "LMX: Ouptut drivers ON";
    flagOutputsOn = true;
    return configureOutputs();
}


/*!
 \brief Turn output drivers Off
 \return globals::OK       Outputs turned off OK
 \return [Error Code]      Error code from configureOutputs (derived class)
*/
int BertLMXxxxxInterface::outputsOff()
{
    qDebug() << "LMX: Ouptut drivers OFF";
    flagOutputsOn = false;
    return configureOutputs();
}


/*!
 \brief Run frequency calibration
 \return globals::OK       Calibration OK
 \return [Error Code]      Error code from writeRegister
*/
int BertLMXxxxxInterface::runFCal()
{
    qDebug() << "LMX: FCAL...";
    // Need to preserve FCAL_HPFD_ADJ and FCAL_LPFD_ADJ bits from
    // profile (these are fields in R0):
    bool bFound = false;
    uint16_t R0;
    if (selectedProfileIndex < frequencyProfiles.count())
    {
        R0 = frequencyProfiles.at(selectedProfileIndex).getRegisterValue(0, &bFound);
    }
    if (!bFound) R0 = getDefaultR0();  // Default to use if no profile selected.

    int result = writeRegister(0, R0 | getFCalEnR0());  // Set FCAL_EN
    if (result != globals::OK) return result;
    globals::sleep(100);
    result = writeRegister(0, R0);  // FCCAL_EN low.
    globals::sleep(100);
    return result;
}



/*!
 \brief Reset the LMX device
 \return globals::OK       Calibration OK
 \return [Error Code]      Error code from resetPart (derived class)
*/
int BertLMXxxxxInterface::resetDevice()
{
    qDebug() << "LMX: RESET...";
    // Need to preserve FCAL_HPFD_ADJ and FCAL_LPFD_ADJ bits from
    // profile (these are fields in R0):
    bool bFound = false;
    uint16_t R0;
    if (selectedProfileIndex < frequencyProfiles.count())
    {
        R0 = frequencyProfiles.at(selectedProfileIndex).getRegisterValue(0, &bFound);
    }
    if (!bFound) R0 = getDefaultR0();  // Default to use if no profile selected.

    return resetPart(R0);

}





/*****************************************************************************************/
/* PRIVATE Methods                                                                       */
/*****************************************************************************************/


/*!
 \brief Initialise I2C -> SPI Adaptor
 We expect there to be an SC18IS602B I2C->SPI adaptor available
 on the comms port represented by bertComms object.
 This method configures the adaptor to the correct mode:
   * MSB of data first
   * SPICLK LOW when idle; data clocked in on leading edge
   * 58 kHz SPI Clock Rate
  => Config word = 0x03
  See SC18IS602B docs for other options.

 \return globals::OK   Comms online; Ack from SC18IS602B
 \return globals::NOT_CONNECTED   bertComms object is not connected to the instrument
 \return globals::BUSY_ERROR      Operation already in progress
 \return globals::GEN_ERROR       Read / Write error
 \return globals::TIMEOUT         Timeout waiting for data read/write or macro completion
 \return [Error code]             Error code from BertComms->read or BertComms->write
*/
int BertLMXxxxxInterface::initAdaptor()
{
    int result;
    const uint8_t configData[] = { 0xF0, 0x03 };
    qDebug() << "Set up SC18IS602B I2C->SPI adaptor on I2C address "
             << (int)i2cSlaveAddress;
    result = bertComms->writeRaw(i2cSlaveAddress, configData, 2);
    if (result == globals::OK) qDebug() << "SC18IS602B Ready.";
    else                       qDebug() << "Error: SC18IS602B didn't respond!";
    return result;
}


/*!
 \brief Write value to LMX register
 \param regAddress  Register number to write to (0 to maxRegisterAddress)
 \param regValue    Value to write

 \return globals::OK               Success... Register data written
 \return globals::NOT_INITIALISED  Clock interface hasn't been initialised yet
 \return globals::OVERFLOW         Invalid register address
 \return [error code]              Comms or adaptor error writing to LMX
*/
int BertLMXxxxxInterface::writeRegister(const uint8_t regAddress, const uint16_t regValue)
{
    if (!spiAdaptorIsOpen) return globals::NOT_INITIALISED;
    if (regAddress > maxRegisterAddress) return globals::OVERFLOW;

    const uint8_t rawData[] = { 0x04,                     // Function ID: Selects the !SS pin to use
                                regAddress,               // Register Address byte
                               (uint8_t)(regValue >> 8),  // Upper byte of value
                               (uint8_t)(regValue) };     // Lower byte of value

     int result = bertComms->writeRaw(i2cSlaveAddress, rawData, 4);
#ifdef LMX_REGISTER_DEBUG
     qDebug() << "[LMX Register WRITE] Addr: " << regAddress << " Data: " << QString().sprintf("0x%04X", regValue) << " Result: " << result;
#endif
     return result;
}



/*!
 \brief Set specific bits in a register value, leaving other bits unchanged

 \param registerInputValue Initial value of register
 \param nBits              Number of bits to set within the register
 \param shift              Number of bits to shift the value left
 \param value              Value to set for selected bits

 \return value  Updated register value
*/
uint16_t BertLMXxxxxInterface::setRegisterBits(const uint16_t registerInputValue,
                                               const uint8_t  nBits,
                                               const uint8_t  shift,
                                               const uint16_t value)
{

    uint16_t maskIn = (0xFFFF >> (16-nBits)) << shift;  // Mask with 1 in the bits we want to set
    uint16_t maskOut = ~maskIn;                         // Mask with 0 in the bits we want to set

    //qDebug() << "setRegisterBits: nBits: " <<  nBits << "; shift: " << shift << endl
    //         << "                maskIn: " << maskIn << "; MaskOut: " << maskOut << endl
    //         << " Data In: " << registerInputValue;

    uint16_t registerValue;
    registerValue = registerInputValue & maskOut;  // Zero existing bits in selected field
    registerValue = registerValue | (value << shift);  // Set bits
    //qDebug() << " Data Out: " << registerValue;
    return registerValue;
}




/*!
 \brief Write specific bits of one register

 Uses setRegisterBits to set only specific bits of a register
 then writes to that register of the LMX.

 This method tries to find the CURRENT value of the register
 using the current profile selection. A bitwise AND/OR is then
 used to set ONLY the bits specified, and the value is written
 to the register.

 If the current profile is invalid or profiles have not been
 loaded, registerDefaultValue is used as the initial value for the
 register.

 See setRegisterBits for more info.

 \param address  Register to set (0 to maxRegisterAddress)
 \param nBits  Number of bits to set within the register
 \param shift  Number of bits to shift the value left
 \param value  Value to set
 \param registerDefaultValue
               Default initial value for register if no
               profile is selected

 \return globals::OK
 \return [Error code] - See writeRegister errors
*/
int BertLMXxxxxInterface::writeRegisterBits(const uint8_t  address,
                                            const uint8_t  nBits,
                                            const uint8_t  shift,
                                            const uint16_t value,
                                            const uint16_t registerDefaultValue)
{
    uint16_t registerOldValue;
    uint16_t registerValue;
    bool bFound = false;
    // Get current setting for register (only want to change some bits):
    if (selectedProfileIndex < frequencyProfiles.count())
    {
        registerOldValue = frequencyProfiles.at(selectedProfileIndex).getRegisterValue(address, &bFound);
    }
    if (!bFound) registerOldValue = registerDefaultValue;  // Default to use if no profile selected.

    registerValue = setRegisterBits(registerOldValue,
                                    nBits,
                                    shift,
                                    value);

    // qDebug() << "Set Reg " << address << " to " << registerValue;
    return writeRegister(address, registerValue);
}






/*********************************************************************************/

/*!
 \brief LMX Frequency Profile Constructor
 This constructor takes a list of strings, which are lines
 read from a register definition file (.tcs) generated by the
 Texas Instruments TICS Pro program (or manually).
 The constructor parses the file and generates the new object
 using the values it finds.
 If the data can't be parsed, the valid flag of the new object
 is set to false (check with isValid() after constructing).

 \param partNo        String containing part number to search for, e.g. "LMX2592". If the "PART=...."
                      line is missing (from [SEUP] section) or doesn't contain this part number string,
                      the defs are rejected ('valid' flag set to fasle).

 \param registerDefs  List of strings read from register definintion file

*/
BertLMXFrequencyProfile::BertLMXFrequencyProfile(const QString partNo, QStringList &registerDefs, const uint8_t maxRegsterAddress)
{
    maxAddress = maxRegsterAddress;
    if (registerDefs.empty()) return; // Nothing to parse.

    enum fileSection_t
    {
        SECTION_UNKNOWN,
        SECTION_SETUP,
        SECTION_PINS,
        SECTION_MODES,
        SECTION_FLEX
    };

    bool flagSetupOK = false;
    bool flagFrequencyOK = false;
    enum fileSection_t currentSection = SECTION_UNKNOWN;
    LMXRegister foundRegister;
    foundRegister.address = 0;
    foundRegister.value = 0;
    QString partNoFull = "PART=";
    partNoFull.append(partNo);

    foreach( QString defLineFull, registerDefs )
    {
        QString defLine = defLineFull.trimmed();

        // Check for new section: //////////////////
        if (defLine == "[SETUP]")
        {
            currentSection = SECTION_SETUP;
            continue;
        }
        if (defLine == "[PINS]")
        {
            currentSection = SECTION_PINS;
            continue;
        }
        if (defLine == "[MODES]")
        {
            // Check to make sure we've seen "PART=LMXxxxx":
            if (!flagSetupOK)
            {
                qDebug() << "Parse Error: Got to [MODES] but didn't see '" << partNoFull << "'" << endl
                         << "Giving up.";
                return;
            }
            currentSection = SECTION_MODES;
            continue;
        }
        if (defLine == "[FLEX]")
        {
            currentSection = SECTION_FLEX;
            continue;
        }
        // Check for certain strings, depending on section: /////
        switch (currentSection)
        {
        case SECTION_SETUP:
            if (defLine == partNoFull) flagSetupOK = true;
            break;
        case SECTION_PINS:
            break;  // Don't care about anything in this section.
        case SECTION_MODES:
            if ( defLine.startsWith(QString("VALUE"), Qt::CaseSensitive) )
            {
                QStringList parts = defLine.split("=");
                if (parts.count() > 1)
                {
                    uint32_t readValue = (uint32_t)parts.at(1).toLong();
                    foundRegister.address = (uint8_t)(readValue >> 16);
                    foundRegister.value   = (uint16_t)(readValue & 0xFFFF);
                }
                else
                {
                    qDebug() << "Error parsing register value line: " << defLine;
                }
                // qDebug() << "Found value " <<  QString().sprintf("0x%04X",foundRegister.value) << " for Reg. " << (int)foundRegister.address;
                if (foundRegister.address <= maxAddress) registers.append(foundRegister);
                else                                     qDebug() << "IGNORED Invalid register address: " << (int)foundRegister.address;
            }
            break;
        case SECTION_FLEX:
            // Check for "FoutB_FREQ=xxxxxx"
            if ( defLine.startsWith(QString("FoutB_FREQ="), Qt::CaseSensitive) )
            {
                QStringList parts = defLine.split("=");
                if (parts.count() > 1)
                {
                    frequency = parts.at(1).toFloat();
                    flagFrequencyOK = true;
                    //qDebug() << "Freq for this profile: " << frequency << " MHz";
                }
            }
            break;
        case SECTION_UNKNOWN:
            // Unknown section. Ignore contents.
            break;
        }  // switch (currentSection)
    }  // foreach( QString defLineFull, freqFiles )...

    // For this to be a valid register profile, certain sections must have
    // been found, including at least one register value:
    if (   flagSetupOK
        && flagFrequencyOK
        && (registers.count() > 0) )
    {
        valid = true;
    }
    else
    {
        qDebug() << "Invalid frequency profile file: missing part number, register settings or frequency";
    }

    //qDebug() << "Register profile parsing finished. Valid? " << valid << "; "
    //         << registers.count() << " register values found.";
}


BertLMXFrequencyProfile::~BertLMXFrequencyProfile()
{}


/*!
 \brief Set register value
 Sets a profile register to a specified value. If the register
 hasn't been added to the profile yet, it is created.
 \param address  Register address to set a value for (0 to maxAddress)
 \param value    Value for register
 \return globals::OK        Success
 \return globals::OVERFLOW  Address out of range (> maxAddress)
*/
int BertLMXFrequencyProfile::setRegisterValue(const uint8_t address, const int16_t value)
{
    if (address > maxAddress) return globals::OVERFLOW;
    int i;
    for (i = 0; i < registers.count(); i++)
    {
        if (registers[i].address == address)
        {
            registers[i].value = value;
            return globals::OK;
        }
    }
    // Not found. Add a new register:
    LMXRegister newRegister;
    newRegister.address = address;
    newRegister.value = value;
    registers.append(newRegister);
    return globals::OK;
}


/*!
 \brief Get register value
 Returns the value of a register loaded from a profile
 (see constructor)

 If the register isn't in the internal list (i.e. no value
 was found for the register when parsing the profile data),
 0 is returned, and *registerFound is set to false.

 \param address        Register address to get a value for (0-64)
 \param registerFound  Pointer to a bool, or null. If specified, the target will be
                       set to true if the register was found in the frequency profile,
                       or false if not.
 \return Register value (0 if register not found in profile)
*/
int16_t BertLMXFrequencyProfile::getRegisterValue(const uint8_t address, bool *registerFound) const
{
    if (registerFound) *registerFound = true;
    int i;
    for (i = 0; i < registers.count(); i++)
    {
        if (registers.at(i).address == address) return registers.at(i).value;
    }
    //qDebug() << "WARNING: No value found for this address. Using default (0).";
    if (registerFound) *registerFound = false;   // Not found...
    return 0;
}



