/*!
 \file   BertLMX2592Interface.h
 \brief  LMX2592 Clock Hardware Interface Header - Derived from BertLMXxxxxInterface
 \author J Cole-Baker (For Smartest)
 \date   Jul 2016; Modified Apr 2018
*/


#ifndef BERTLMX2592INTERFACE_H
#define BERTLMX2592INTERFACE_H

#include <QMutex>
#include <QList>

#include <stdint.h>

#include "globals.h"
#include "BertComms.h"
#include "BertLMXxxxxInterface.h"


/*!
 \brief LMX2592 Interface Class

 This class implements functions specific to the Texas Instruments LMX2592
 clock generator chip. It derives from the generic BertLMXxxxxInterface
 class which covers both LMX2592 and LMX2594

*/
class BertLMX2592Interface : public BertLMXxxxxInterface
{

  public:

    BertLMX2592Interface(BertComms *useBertComms, const uint8_t useSlaveAddress);
    ~BertLMX2592Interface();

  protected:

    // ****** CONSTANTS: *****************************************************
    // Initial register values:
    static const registerData_t INIT_REGISTERS[];
    static const size_t INIT_REGISTERS_SIZE;

    // Bit patterns for Register 0:
    static const uint16_t R0_NOTHING;
    static const uint16_t R0_LD_EN;
    static const uint16_t R0_FCAL_EN;
    static const uint16_t R0_MUXOUT_LD;
    static const uint16_t R0_RESET;
    static const uint16_t R0_POWERDOWN;
    static const uint16_t R0_DEFAULT;

    // Register 35 and 36 values used to select divide ratio:
    static const uint16_t R35_VALUES[];
    static const uint16_t R36_VALUES[];
    static const size_t R35R36_VALUES_SIZE;
    //  **********************************************************************

    uint16_t getDefaultR0() { return R0_DEFAULT; }                       // Get default value for R0
    uint16_t getFCalEnR0()  { return R0_FCAL_EN; }                       // Get R0 bit to enable FCAL
    int initPart();                                                      // Part Initialisation
    int configureOutputs();                                              // Output driver setup
    int selectProfilePart(const size_t index);                           // Select a frequency profile
    int selectTriggerDividePart(const size_t index, const bool doFCal);  // Trigger output divide ratio setup
    void setSafeDefaults();                                              // Set safe default settings
    int resetPart(uint16_t defaultR0);                                   // Part-specific reset function: Implemented by derived versions

};

#endif // BERTLMX2592INTERFACE_H
