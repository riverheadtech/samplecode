/*!
 \file   BertSerial.h
 \brief  Asynchronous Serial Port Handler for BERT Comms - Header
 \author J Cole-Baker (For Smartest)
 \date   May 2016
*/

#ifndef BERTSERIAL_H
#define BERTSERIAL_H

#include <QSerialPort>
#include <QObject>
#include <QMutex>

/*!
 \brief BERT Asynchronous Serial Port Class

 Manages comms via the serial port, using the
 QT serial port (with signals and slots)

 Provides a buffer for serial port transactions
 carried out by higher level classes

*/
class BertSerial : public QObject
{
Q_OBJECT

public:
    BertSerial(QObject *parent);
    ~BertSerial();

    int open(const QString portName);
    void close();
    uint8_t *getData(size_t *nBytes);

public slots:
    void transactionStart(const uint8_t *data, const size_t nBytesData, const size_t nBytesResponseExpected);
    void transactionCancel();

signals:
    void transactionFinished();

private slots:
    void dataAvailable();
    void dataWritten();

private:
    QSerialPort   *serialPort;
    QByteArray     inputBuffer;
    size_t         nBytesExpected = 0;

};

#endif // BERTSERIAL_H
