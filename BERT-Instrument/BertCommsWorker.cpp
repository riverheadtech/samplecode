/*!
 \file   BertCommsWorker.cpp
 \brief  BERT Board Comms Worker Thread Class Implementation
 \author J Cole-Baker (For Smartest)
 \date   Jun 2018
*/

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QEventLoop>
#include <QTimer>
#include <QDebug>

#include "globals.h"
#include "BertSerial.h"
#include "BertCommsWorker.h"


// I2C Slave Address Macros: Add the appropriate
// read / write bit to a 7 bit slave address:
#define I2CWRITE(A) (uint8_t)(A << 1)       // Make 7 bit address into Master Write
#define I2CREAD(A)  (uint8_t)((A << 1) + 1) // Make 7 bit address into Master Read

// MACRO to handle comms error:
#define COMMSERROR_RETRY(ECODE) {                      \
        errorClear(0);                                 \
        errorCounter++;                                \
        if (errorCounter >= MAX_RETRIES)               \
        {                                              \
            return(ECODE);                             \
        }                                              \
        qDebug() << "   -->RETRY...";                  \
        continue;                                      \
        }


/*
 Nb: For comms via USB-ISS INTERFACE ADAPTER, see:
   http://www.robot-electronics.co.uk/htm/usb_iss_tech.htm
*/

// ***** Pre-defined I2C Operations Data Blocks: ****
const uint8_t BertCommsWorker::I2C_OP_GET_VERSION[] = { ISS_CMD, 0x01 };
const uint8_t BertCommsWorker::I2C_OP_GET_VERSION_SIZE = sizeof(I2C_OP_GET_VERSION);

const uint8_t BertCommsWorker::I2C_OP_GET_SERIAL[] = { ISS_CMD, 0x03 };
const uint8_t BertCommsWorker::I2C_OP_GET_SERIAL_SIZE = sizeof(I2C_OP_GET_SERIAL);

const uint8_t BertCommsWorker::I2C_OP_SET_MODE[] = { ISS_CMD, 0x02, 0x40, 0x04 };
const uint8_t BertCommsWorker::I2C_OP_SET_MODE_SIZE = sizeof(I2C_OP_SET_MODE);
// Nb: I2C_OP_SET_MODE sets 50 KHz I2C Mode (hardware driver), with IO pins set high (not used).



BertCommsWorker::BertCommsWorker()
{
    isOpen = false;        // Signifies that the port is closed.
    flagStop = false;
}

BertCommsWorker::~BertCommsWorker()
{
    // Make sure the comms port is closed.
    commsClose();
}


/*!
 \brief Request that the worker thread stops
        The thread may take a couple of seconds to actually stop
*/
void BertCommsWorker::requestStop()
{
    commsFlagMutex.lock();
    flagStop = true;
    commsFlagMutex.unlock();
}



/*!
 \brief Start a new Comms Job using the comms worker thread
        This method will return when the job is finished, or a timeout occurs.

 \param opCode           Operation to carry out (see Job Op Codes in header)
 \param useSerialPort    String identifying the serial port ('open' only; set to "" for other operations)
 \param timeoutMs        Timeout for commsLock operation; 0 for other operations.
 \param slaveAddress     I2C Slave Address (for all Read / Write ops)
 \param regAddress       Register Address for read or write; not used for RAW read / write ops - 16 bit version
 \param regAddress24     Register Address for read or write; not used for RAW read / write ops - 24 bit version
 \param data             Pointer to buffer of data to read or write - must be nBytes in size
 \param nBytes           Number of bytes to read or write (READ / WRITE operations)
 \param eventLoop        Pointer to an EventLoop object owned by the UI thread. We call "exec" on this
                         event loop to keep the UI alive while serial operations are underway.
                         When the worker thread is finished, it sends a signal which breaks us out
                         of this event loop (via commsJobFinished slot in BertComms.cpp)

 \return globals::OVERFLOW       Invalid op code or parameter
 \return globals::NOT_CONNECTED  Comms not open yet
 \return globals::BUSY_ERROR     Timeout occurred while waiting for a previous operation to finish
 \return [error code]            Other error code from serial port, etc.
*/
int BertCommsWorker::startCommsJob(const int        opCode,
                                   const QString    useSerialPort,
                                   const int        timeoutMs,
                                   const uint8_t    slaveAddress,
                                   const uint16_t   regAddress,
                                   const uint32_t   regAddress24,
                                   const uint8_t   *dataWrite,
                                   uint8_t         *dataRead,
                                   const size_t     nBytes,
                                   QEventLoop      *eventLoop)
{
#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "startCommsJob: NEW job: Opcode " << opCode << "; Thread: " << QThread::currentThreadId();
    qDebug() << "startCommsJob: WAITING for job mutex...";
#endif

    if (!jobMutex.tryLock(0))
    {
        qDebug() << "Start Comms Job: ERROR: Comms busy on another job!";
        return globals::BUSY_ERROR;
    }

#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "startCommsJob: WAITING for sync mutex (i.e. worker ready)...";
#endif
    if (!commsSyncMutex.tryLock(JOB_WAIT_TIMEOUT))
   {
        qDebug() << "Start Comms Job: ERROR: Comms Busy!";
        jobMutex.unlock();
        return globals::BUSY_ERROR;
    }

    // qDebug() << "startCommsJob: job mutex locked! Set up job...";
    // Comms job mutex now locked: This means no other job is using the job data,
    // so it's safe to modify the job data. Also means the worker thread is ready for a new job.
    commsJob.opCode = opCode;
    commsJob.useSerialPort = useSerialPort;
    commsJob.timeoutMs = timeoutMs;
    commsJob.slaveAddress = slaveAddress;
    commsJob.regAddress = regAddress;
    commsJob.regAddress24 = regAddress24;
    commsJob.dataWrite = dataWrite;
    commsJob.dataRead = dataRead;
    commsJob.nBytes = nBytes;
    commsJob.returnCode = globals::NOT_IMPLEMENTED;   // Placeholder until we get real result
    // qDebug() << "startCommsJob: Job ready; Freeing job mutex...";
    commsSyncMutex.unlock();

#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Start Comms Job: Job ready; Sending wakeup to worker...";
#endif

    // Tell the worker thread to start the job:
    jobStart.wakeAll();
    // qDebug() << "startCommsJob: ***** Calling exec (Event Loop)...";
    eventLoop->exec();
    // qDebug() << "startCommsJob: ***** exec exited...";

#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Start Comms Job: Job finished!";
#endif

    // OK, job should be finished. Lock the sync mutex, then copy the return value:
    // qDebug() << "startCommsJob: Grab the job mutex, to get result...";
    commsSyncMutex.lock();
    // qDebug() << "startCommsJob: Copy result...";
    int result = commsJob.returnCode;
    // Finished with job. Unlock the job mutex ready for other UI calls to this fn:
    commsSyncMutex.unlock();

#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Start Comms Job: Mutex unlocked; returning result.!";
#endif

    // Unlock the job mutex. Other jobs can proceed!
    jobMutex.unlock();
    return result;
}



bool BertCommsWorker::portIsOpen()
{
    bool result;
    commsFlagMutex.lock();
    result = isOpen;
    commsFlagMutex.unlock();
    return result;
}




////////////////////////////////////////////////////////////////////////
//////// PRIVATE Methods ///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////


/*!
 \brief Comms Thread Run Method
 Called when the worker thread is started. This method doesn't return
 until the worker thread exists (i.e. on shut down).
*/
void BertCommsWorker::run()
{
    qDebug() << "=== Comms Worker START ===";
    // Start with our job control mutex locked.
    commsSyncMutex.lock();
    // qDebug() << "Creating comms timer on thread: " << currentThreadId();
    commsTimer = new QTimer(this);
    commsTimer->setSingleShot(true);
    connect(commsTimer, SIGNAL(timeout()), this, SLOT(commsTimeout()));
    bool stopRequested = false;
    int jobResult;

    forever
    {
        // Check stop flag:
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: Checking stop flag...";
#endif
        commsFlagMutex.lock();
        if (flagStop) stopRequested = true;
        commsFlagMutex.unlock();
        if (stopRequested) break;

        // Wait for the jobStart condition (new job ready to start):
        if (jobStart.wait(&commsSyncMutex, 1000))
        {
            // We were woken up by a new job! We have jobStartMutex again.
#ifdef BERT_COMMSWORKER_DEBUG
            qDebug() << "Comms Worker: New job!";
#endif
            jobResult = processJob();
#ifdef BERT_COMMSWORKER_DEBUG
            qDebug() << "Comms Worker: Job finished.";
#endif
            commsJob.returnCode = jobResult;

            // Job finished. Notify the UI thread.
            // qDebug() << "Comms Worker: Notify client...";
            emit jobFinished();
        }
        else
        {
            // Timed out waiting for the job start signal...
#ifdef BERT_COMMSWORKER_DEBUG
            qDebug() << "[Comms Worker: Tick]";
#endif
        }
    }
    qDebug() << "=== Comms Worker FINISHED ===";
    delete commsTimer;
}


/*!
 \brief Close Comms Port (Nb: No locking!)
*/
void BertCommsWorker::commsClose()
{
    commsStatus = COMMS_ERROR;
    commsTimer->stop();
    if (bertSerial)
    {
        bertSerial->close();
        delete bertSerial;
        bertSerial = NULL;
        commsFlagMutex.lock();
        isOpen = false;
        commsFlagMutex.unlock();
    }
}



/*!
 \brief Process a Comms Job
*/
int BertCommsWorker::processJob()
{
    // Are the comms connected?
    if ((commsJob.opCode != COMMS_open) && (!portIsOpen())) return globals::NOT_CONNECTED;

    // Select an action based on op-code:
    switch (commsJob.opCode)
    {

    case COMMS_open:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: OPEN";
#endif
        commsFlagMutex.lock();
        isOpen = false;
        commsFlagMutex.unlock();
        int result = globals::OK;
        commsClose();  // In case the comms were already open.
        bertSerial = new BertSerial(this);
        connect(bertSerial, SIGNAL(transactionFinished()),
                this,       SLOT(transactionFinished()));
        result = bertSerial->open(commsJob.useSerialPort);
        if (result != globals::OK)
        {
            qDebug() << "Comms Worker: Error opening serial port (" << result << ")";
            return result;
        }
        globals::sleep(100);
        commsFlagMutex.lock();
        isOpen = true;
        commsFlagMutex.unlock();
        // qDebug() << "Serial Opened OK!";
        // Probe the adaptor to check that it is working:
        bool bResult = probeAdaptor();
        if (!bResult)
        {
            qDebug() << "Error: I2C Adaptor didn't respond!";
            commsClose();
            return globals::GEN_ERROR;
        }
        return globals::OK;
        break;
    }



    case COMMS_close:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: CLOSE";
#endif
        commsClose();
        return globals::OK;
        break;
    }



    case COMMS_reset:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: RESET";
#endif
        emit bertSerial->transactionCancel();
        globals::sleep(600);
        probeAdaptor();  // Hopefully this will clear the adaptor's serial buffer.
        return globals::OK;
        break;
    }


/* DEPRECATED
    case COMMS_commsLock:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: COMMS LOCK";
#endif
        // TODO: This won't work; need look on other calls as well.
        bool lockResult = batchMutex.tryLock(commsJob.timeoutMs);
        if (lockResult) return globals::OK;          // Locked OK
        else            return globals::BUSY_ERROR;  // Lock timed out (already busy!)
        break;
    }
    case COMMS_commsFree:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: COMMS FREE";
#endif
        batchMutex.unlock();
        return globals::OK;
        break;
    }
*/

    case COMMS_writeRaw:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: WRITE RAW";
#endif
        int errorCounter = 0;
        uint8_t i2cData[commsJob.nBytes+3];  // Buffer for data to be sent, plus header
        i2cData[0] = I2C_AD0;
        i2cData[1] = I2CWRITE(commsJob.slaveAddress);
        i2cData[2] = commsJob.nBytes;
        memcpy(i2cData + 3, commsJob.dataWrite, commsJob.nBytes);
        uint8_t adaptorResponse;
        int result;
        while (true)
        {
            //qDebug() << "   Write to I2C Device (NO address): "
            //         << QString("%1 bytes").arg((int)commsJob.nBytes);
            result = i2cOp(sizeof(i2cData),
                           i2cData,
                           1,
                           &adaptorResponse);
            if (result != globals::OK) COMMSERROR_RETRY(result)
            if (adaptorResponse == 0x00)
            {
    #ifdef BERT_USBISS_DEBUG
                qDebug() << "   -->Response code 0x00: I2C adaptor reports write failed!";
    #endif
                COMMSERROR_RETRY(globals::ADAPTOR_WRITE_ERROR)
            }
            errorCounter = 0;
            return globals::OK;
        }
        return globals::ADAPTOR_WRITE_ERROR;
        break;
    }



    case COMMS_write:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: WRITE";
#endif
        if (commsJob.nBytes > 59) return globals::OVERFLOW;       // Transmission buffer is limited to 59 bytes.
        // PROFILING: runTime.start();
        int errorCounter = 0;
        uint8_t i2cData[commsJob.nBytes+5];  // Buffer for data to be sent, plus header
        i2cData[0] = I2C_AD2;
        i2cData[1] = I2CWRITE(commsJob.slaveAddress);
        i2cData[2] = (uint8_t)(commsJob.regAddress >> 8);
        i2cData[3] = (uint8_t)(commsJob.regAddress);
        i2cData[4] = commsJob.nBytes;
        memcpy(i2cData + 5, commsJob.dataWrite, commsJob.nBytes);
        uint8_t adaptorResponse;
        int result;
        while (true)
        {
    #ifdef BERT_USBISS_DEBUG
            qDebug() << "   Write to I2C Device " << QString("0x%1").arg((int)(commsJob.slaveAddress),2,16,QChar('0'))
                     << " (16 bit address): "
                     << QString("[0x%1%2]; %3 bytes")
                        .arg((int)(commsJob.regAddress >> 8),2,16,QChar('0'))
                        .arg((int)(commsJob.regAddress & 0xFF),2,16,QChar('0'))
                        .arg((int)commsJob.nBytes);
    #endif
            result = i2cOp(sizeof(i2cData),
                           i2cData,
                           1,
                           &adaptorResponse);
            if (result != globals::OK) COMMSERROR_RETRY(result)
            if (adaptorResponse == 0x00)
            {
    #ifdef BERT_USBISS_DEBUG
                qDebug() << "   -->Response code 0x00: I2C adaptor reports write failed!";
    #endif
                COMMSERROR_RETRY(globals::ADAPTOR_WRITE_ERROR)
            }
            errorCounter = 0;
            return globals::OK;
        }
        return globals::ADAPTOR_WRITE_ERROR;
        break;
    }



    case COMMS_write24:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: WRITE 24 Bit Address";
#endif
        //    qDebug() << "   Write to I2C Device (24 bit address): "
        //             << QString("[0x%1%2%3]; %4 bytes")
        //                .arg((int)((commsJob.regAddress24 >> 16) & 0x000000FF),2,16,QChar('0'))
        //                .arg((int)((commsJob.regAddress24 >> 8)  & 0x000000FF),2,16,QChar('0'))
        //                .arg((int)(commsJob.regAddress24         & 0x000000FF),2,16,QChar('0'))
        //                .arg((int)commsJob.nBytes);
        // Use custom I2C command to handle 3 byte address:
        // We add a header with address, etc, followed by
        // a series of 'write' sub commands which can each send
        // 16 bytes (see docs for adaptor). Max size data frame
        // for the adaptor is 60 bytes so we may need to send
        // multiple requests, remembering to update the address
        // we are writing to each time.
        uint8_t  i2cFrame[60];   // Maximum data size the adaptor can handle
        uint32_t writeAddress = commsJob.regAddress24;
        size_t bytesRemaining = commsJob.nBytes;
        size_t thisFrameSize;
        size_t bytesThisSubFrame;
        size_t frameBytesRemaining;
        int frameWritePtr;
        while (bytesRemaining > 0)
        {
            i2cFrame[0] = I2C_DIR;              // USB-I2C adaptor command (I2C DIRECT)
            i2cFrame[1] = 0x01;                 //   SUB COMMAND: I2C Start
            i2cFrame[2] = 0x33;                 //   SUB COMMAND: Write next 4 bytes
            i2cFrame[3] = I2CWRITE(commsJob.slaveAddress); // Address of device + R/W bit (=0 for write)
            i2cFrame[4] = writeAddress >> 16;   // Write to address - High byte
            i2cFrame[5] = writeAddress >> 8;    //
            i2cFrame[6] = writeAddress;         //
            frameBytesRemaining = (60 - 7);     // Already used 7 bytes above.
            frameWritePtr = 7;                  // Loop below starts adding more sub-commands and data here.
            // 18 bytes is room for one more 'write' sub-command, plus the
            // 'stop bit' command. If there's fewer than 18 bytes left in the
            // buffer, finish the frame and sent it; Otherwise, add another
            // write sub-command and more data.
            while ( (frameBytesRemaining >= 18) && (bytesRemaining > 0) )
            {
                // Make a sub-frame with the next (up to) 16 bytes:
                bytesThisSubFrame = bytesRemaining;
                if (bytesThisSubFrame > 16) bytesThisSubFrame = 16;
                i2cFrame[frameWritePtr] = (0x30 + bytesThisSubFrame) - 1; //   SUB COMMAND: Write next n bytes
                frameWritePtr++;
                memcpy(i2cFrame + frameWritePtr, commsJob.dataWrite, bytesThisSubFrame);
                bytesRemaining -= bytesThisSubFrame;
                frameBytesRemaining -= (bytesThisSubFrame + 1);
                frameWritePtr += bytesThisSubFrame;
            }
            i2cFrame[frameWritePtr] = 0x03;  // Add I2C STOP
            frameWritePtr++;
            thisFrameSize = frameWritePtr;
            uint8_t adaptorResponse[2] = { 0,0 };
            //qDebug() << "   -Send Frame: " << thisFrameSize << " bytes";
            int result = i2cOp(thisFrameSize,
                               i2cFrame,
                               2,
                               adaptorResponse);
            if (result != globals::OK) return result;
            if (adaptorResponse[0] == 0x00)
            {
                qDebug() << "   -->NACK (0x00): I2C adaptor reports error!";
                qDebug() << "   -->Error Code: " << adaptorResponse[1];
                return errorClear(globals::ADAPTOR_WRITE_ERROR);
            }
            writeAddress += (commsJob.nBytes - bytesRemaining);  // Advance the write-to address by the number of bytes we just sent.
        }
        globals::sleep(5);
        return globals::OK;
        break;
    }



    case COMMS_readRaw:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: READ RAW";
#endif
        int errorCounter = 0;
        while (true)
        {
            //qDebug() << "   Read to I2C Device (NO address): "
            //         << QString("%1 bytes").arg((int)commsJob.nBytes);
            uint8_t i2cData[] = { I2C_AD0,
                                  I2CREAD(commsJob.slaveAddress),
                                  (uint8_t)commsJob.nBytes };
            int result = i2cOp(sizeof(i2cData),
                               i2cData,
                               commsJob.nBytes,
                               commsJob.dataRead);
            if (result != globals::OK) COMMSERROR_RETRY(result)
            errorCounter = 0;
            return globals::OK;
        }
        return globals::ADAPTOR_READ_ERROR;
        break;
    }



    case COMMS_read:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: READ";
#endif
        if (commsJob.nBytes > 64) return globals::OVERFLOW; // Transmission buffer is limited to 64 bytes.
        int errorCounter = 0;
        while (true)
        {
    #ifdef BERT_USBISS_DEBUG
                qDebug() << "   Read from I2C Device " << QString("0x%1").arg((int)(commsJob.slaveAddress),2,16,QChar('0'))
                         << " (16 bit address): "
                         << QString("[0x%1%2]; %3 bytes")
                            .arg((int)(commsJob.regAddress >> 8),2,16,QChar('0'))
                            .arg((int)(commsJob.regAddress & 0xFF),2,16,QChar('0'))
                            .arg((int)commsJob.nBytes);
    #endif

            uint8_t i2cData[] = { I2C_AD2,
                                  I2CREAD(commsJob.slaveAddress),
                                  (uint8_t)(commsJob.regAddress >> 8),
                                  (uint8_t)commsJob.regAddress,
                                  (uint8_t)commsJob.nBytes };

            int result = i2cOp(sizeof(i2cData),
                               i2cData,
                               commsJob.nBytes,
                               commsJob.dataRead);

            if (result != globals::OK) COMMSERROR_RETRY(result)
            errorCounter = 0;
            return globals::OK;
        }
        return globals::ADAPTOR_READ_ERROR;
        break;
    }



    case COMMS_read24:
    {
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "Comms Worker: READ 24 Bit Address";
#endif
        // Use custom I2C command to handle 3 byte address:
        // We add a header with address, etc, followed by
        // a series of 'read' sub commands which can each fetch
        // 16 bytes (see docs for adaptor). Max size data frame
        // for the adaptor is 60 bytes so we may need to send
        // multiple requests, remembering to update the address
        // we are reading from each time.
        uint32_t readAddress;
        size_t bytesTotalRead = 0;
        size_t bytesRemaining = commsJob.nBytes;
        size_t thisFrameSize;
        size_t bytesRequestedThisRead;

        int errorCounter = 0;
        uint8_t adaptorResponse[18] = { 0 };
            // Nb: Max data per read is 16 bytes, plus 2 byte header from adaptor
        while (bytesTotalRead < commsJob.nBytes)
        {
            bytesRemaining = commsJob.nBytes - bytesTotalRead;
            // Number of bytes to read this time: We must leave at least
            // two for the last read operation.
            if (bytesRemaining >= 18)
            {
                bytesRequestedThisRead = 16;
            }
            else
            {
                if (bytesRemaining >= 17) bytesRequestedThisRead = 15;
                else                      bytesRequestedThisRead = bytesRemaining;
            }
            readAddress = commsJob.regAddress24 + bytesTotalRead;
    //        qDebug() << "   -->Next frame read from "
    //                 << QString("[0x%1%2%3]")
    //                    .arg((int)((readAddress >> 16) & 0x000000FF),2,16,QChar('0'))
    //                    .arg((int)((readAddress >> 8)  & 0x000000FF),2,16,QChar('0'))
    //                    .arg((int)(readAddress         & 0x000000FF),2,16,QChar('0'));
            uint8_t i2cFrame[] =
            {
                I2C_DIR,     // USB-I2C adaptor command (I2C DIRECT)
                0x01,                   //   SUB COMMAND: I2C Start
                0x33,                   //   SUB COMMAND: Write next 4 bytes
                I2CWRITE(commsJob.slaveAddress), //   Address of device + R/W bit (=0 for write)
                (uint8_t)(readAddress >> 16),  //   Write to address - High byte
                (uint8_t)(readAddress >> 8),   //
                (uint8_t)(readAddress),        //
                0x02,                   // Add I2C Restart
                0x30,                   //  SUB COMMAND: Write next 1 byte
                I2CREAD(commsJob.slaveAddress),  // Address of device + R/W bit (=1 for READ)
                (uint8_t)(0x20 + (bytesRequestedThisRead - 2)),  //   SUB COMMAND: Read all but one of requested bytes
                0x04,                   // Add I2C NACK
                (uint8_t)(0x20),        //   SUB COMMAND: Read last byte
                0x03                    // Add I2C STOP
            };
            thisFrameSize = sizeof(i2cFrame);
            //qDebug() << "   -Send Frame: " << thisFrameSize << " bytes; Requests read of " << bytesRequestedThisRead << " bytes";
            int result = i2cOp(thisFrameSize,
                               i2cFrame,
                               bytesRequestedThisRead + 2,
                               adaptorResponse);
            if (result != globals::OK)
            {
                COMMSERROR_RETRY(result)
            }
            if (adaptorResponse[0] == 0x00)
            {
                qDebug() << "   -->NACK (0x00): I2C adaptor reports error!";
                qDebug() << "   -->Error Code: " << adaptorResponse[1];
                COMMSERROR_RETRY(globals::ADAPTOR_WRITE_ERROR)
            }
            memcpy(commsJob.dataRead + bytesTotalRead, adaptorResponse + 2, bytesRequestedThisRead);
            bytesTotalRead += bytesRequestedThisRead;
            errorCounter = 0;
        }
        return globals::OK;
        break;
    }

    }

    // Unknown op code!
    return globals::OVERFLOW;
}



/*!
 \brief transactionFinished Slot
 Activated by the transactionFinished signal when the
 serial class has recevied the expected amount of data.
*/
void BertCommsWorker::transactionFinished()
{
#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Comms Worker: transactionFinished slot called";
#endif
    if (commsStatus == COMMS_BUSY)
    {
        commsStatus = COMMS_OK;
        commsTimer->stop();
        exit();
    }
}


/*!
 \brief commsTimeout Slot
 Activated when the Comms timer expires; indicates that
 the expected data has not been recevied from the serial port
*/
void BertCommsWorker::commsTimeout()
{
#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Comms Worker: commsTimeout slot called";
#endif
    if (commsStatus == COMMS_BUSY)
    {
        commsStatus = COMMS_ERROR;
        commsTimer->stop();
        exit();
    }
}




/*!
 \brief Carry out I2C operation with USB-ISS Module

 Output data should be a module command, followed by sub commands
 and data to send - see:
  http://www.robot-electronics.co.uk/htm/usb_iss_tech.htm

 This method writes the output data to the module via the serial
 class.

 It then waits for the "transactionFinished" signal from the
 serial class, which indicates that the expected response data
 has been received. The wait operation is also broken by
 the "commsTimeout" signal. The commsStatus flag is checked
 to see which of these conditions occurred.

 If the comms were successful, the method makes a copy of the
 recieved data and stores it to the buffer supplied by dataRead.

 \param nBytesToWrite  Number of bytes to write from dataWrite
 \param dataWrite      Pointer to output data

 \param nBytesToRead  Number of bytes expected in the response.
                      Nb: ALL I2C transactions must generate at
                      least one response byte.

 \param dataRead      Pointer to input data buffer. Must be at least
                      nBytesToRead bytes in size. Should be NULL if
                      nBytesToRead is 0.

 \return globals::OK               Operation completed successfully.
                                   Nb: This means the data was written out to
                                   the adaptor, and (if required) the correct
                                   number of bytes were read back. Check the
                                   response to see whether the I2C operation
                                   actually worked as expected!

 \return globals::NOT_CONNECTED    Comms not open yet
 \return globals::READ_ERROR       Expected number of bytes were not recevied
*/
int BertCommsWorker::i2cOp(const uint8_t  nBytesToWrite,
                           const uint8_t *dataWrite,
                           const uint8_t  nBytesToRead,
                           uint8_t *dataRead)
{
    if (!isOpen) return globals::NOT_CONNECTED;  // Comms not open!

#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Comms Worker: Starting comms timer on thread: " << QThread::currentThreadId();
#endif

    commsStatus = COMMS_BUSY;
    commsTimer->start(COMMS_TIMEOUT);

#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Comms Worker: Emitting transactionStart signal";
#endif

    emit bertSerial->transactionStart( dataWrite, nBytesToWrite, nBytesToRead );

#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Comms Worker: ** Start WAIT event loop... **";
#endif

    exec();

#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Comms Worker: ** WAIT event loop finished **";
#endif

    size_t nBytes = 0;
    uint8_t *data = bertSerial->getData( &nBytes );

    if ( (commsStatus == COMMS_OK) &&
         (nBytes == nBytesToRead) )
    {
        memcpy(dataRead, data, nBytes);
#ifdef BERT_COMMSWORKER_DEBUG
        qDebug() << "** Got back data: "
                 << "; " << nBytes << " bytes; "
                 << " [" << QString( (const char *)data) << "]";
#endif
        return globals::OK;
    }
    else
    {
        qDebug() << "  I2C Op: TIMEOUT or Comms Error!";
        emit bertSerial->transactionCancel();
        return globals::READ_ERROR;
    }

}




/*!
 \brief Probe the USB-ISS Adaptor and read ID and firmware version
 \return true  - Adaptor found
 \return fasle - Adaptor didn't respond, or comms not open
*/
bool BertCommsWorker::probeAdaptor()
{
    if (!isOpen) return false;
    // Check for the USB-ISS adaptor:
    uint8_t responseData[3] = { 0,0,0 };
    int result = i2cOp( I2C_OP_GET_VERSION_SIZE,
                        I2C_OP_GET_VERSION,
                        3, responseData );
    if (result != globals::OK)
    {
        qDebug() << "I2C Adaptor not found on serial port!";
        return false;
    }
    qDebug() << "I2C Adaptor Found: USB-ISS" << endl
             << "  Module ID: " << responseData[0]
             << "; FW Version: " << responseData[1]
             << "; Mode: " << responseData[2];
    return true;
}




/*!
 \brief Clear Comms Error

 This method flushes the input buffer and stops comms for a delay
 period, to allow error conditions to clear. Hopefully, if the
 board has become confused, it will time out and go back to an
 idle state.

 \param returnCode - An integer return code to return. This
                     makes it convenient to splice the error
                     handler into a Return-On-Error condition,
                     e.g. "if (error) return errorClear( globals::ERROR );"
 \return returnCode
*/
int BertCommsWorker::errorClear(const int returnCode)
{
    emit bertSerial->transactionCancel();
    globals::sleep(5);
    return returnCode;
}

