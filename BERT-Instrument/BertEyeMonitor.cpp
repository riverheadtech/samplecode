/*!
 \file   BertEyeMonitor.cpp
 \brief  BERT Eye Monitor Functions - Implementation
 \author J Cole-Baker (For Smartest)
 \date   Feb 2016
*/

#include <QMutex>
#include <QDebug>

#include <QCoreApplication>

#include <math.h>
#include <stdint.h>
#include <windows.h>

#include "globals.h"

#include "BertEyeMonitor.h"

/*!
  TODO: This class was written to run one eye scan using a
  separate worker thread. It has been adapted to run witout
  the thread (hence passing the instance pointer to eyeScanRun).
  It has also been adapted to run several scans (repeat scanning)
  and then to run repeat scans which alternate between channels
  (so some parameters are reset at scan start and some are not,
  and channel-specific parameters are stored, e.g. shift).

  The scan set up (eyeScan method) was also copied and slightly
  changed to make the bathtub scanner (bathtubScan). This led to
  repeated code.

  Hence the class has been extended beyond its original design
  and needs some refactoring, i.e.:
   * Tidy up "instance" stuff - Make all methods instance specific
     (not static) and remove instance from eyeScanRun

   * Make a base class and derived classes for eye and
     bathtub scans? OR: Unite setup method

   * Change overall usage pattern so that there is one instance
     for each channel, and also separate instances for the bathtub
     scanner. This should remove confusion over channel swapping
     during channel alternating repeated scans

   * Place data normailsation / processing / memory management from
     mainwindow.cpp inside this class (i.e. mainwindow.cpp will
     just run the scan, then request the data pointer, then
     create a plot from the data, then throw away the pointer).

*/

BertEyeMonitor::BertEyeMonitor( BertInterface *useBertInterface )
{
    bertInterface = useBertInterface;
}


BertEyeMonitor::~BertEyeMonitor()
{
    if (eyeDataBuffer) delete [] eyeDataBuffer;
}


/*!
 \brief Set up and start an eye scan

 Depending on the selected resolution options, the scan
 may require multiple macro calls and take some time to
 complete. The caller should lock other UI functions that
 could result in macro calls, probably leaving only a
 "Cancel Scan" button. To cancel the scan, call cancelScan().

 The method doesn't return any value; if comms are not connected,
 or other error occurs, a scanFinished signal will be emitted
 with an error code as the parameter.

 \param channel  ED channel to apply scan to (1 to 4). This
                 value is internally translated into the
                 appropriate board and input lane of the device.

 \param hStep    Horizontal (phase) step  (1 - 128)
 \param vStep    Vertical (voltage offset) step (1 - 127)

 \param countRes Resolution of error count at each data point
                 (i.e. 'z' resolution):
                   0 = 1 bit
                   1 = 2 bit
                   2 = 4 bit
                   3 = 8 bit

 \param rate  Bit rate (in Bit/S)
 \param reset Reset old scan data before starting new scan.
              Must be true for single-pass scan, and true for the
              first pass in a multi-pass scan (false for subsequent
              passes).

 Typical Result Codes (included in scanFinished signal):
  globals::OVERFLOW      Invalid parameter value
  globals::NOT_CONNECTED The BertInterface object used by this
                         instance isn't valid, or isn't connected
                         to the instrument yet.
*/
int BertEyeMonitor::eyeScan( const uint8_t channel,
                             const uint8_t hStep,
                             const uint8_t vStep,
                             const uint8_t countRes,
                             const double  rate,
                             const bool    reset )
{
    if ( (channel < 1) || (channel > 4) ||
         (hStep < 1) || (hStep > 128) ||
         (vStep < 1) || (vStep > 127) ||
         (countRes > 3) ) return globals::OVERFLOW;
    if (!bertInterface) return globals::NOT_CONNECTED;
    scanChannel  = channel;
    channelToBoardLane(channel, &scanBoard, &scanLane);

    if (reset)
    {
        stopFlag     = false;
        scanType     = SCAN_EYE_SCAN;
        scanHStep    = hStep;
        scanVStep    = vStep;
        bitRate      = rate;
        resetFlag    = reset;

        scanCountResIndex = countRes;
          // Count res as index (0-3),  as used by 'Control Eye Sweep' macro
        scanCountResBits  = 1 << countRes;
          // Converted to number of bits (1, 2, 4 or 8), for size calcs.
    }

    qDebug() << "Eye Scan Configuration:";
    qDebug() << " Lane:       " << scanLane;
    qDebug() << " H Step:     " << scanHStep;
    qDebug() << " V Step:     " << scanVStep;
    qDebug() << " Resolution: " << scanCountResBits;

    return eyeScanRun();
}



/*!
 \brief Set up and start a bathtub scan

 The bathtub scan is essentially the same as an eye scan,
 excapt that it scans only one "row" (i.e. voltage offset level).

 The method doesn't return any value; if comms are not connected,
 or other error occurs, a scanFinished signal will be emitted
 with an error code as the parameter.

 \param channel  ED channel to apply scan to (1 - 4). This
                 value is internally translated into the
                 appropriate input lane of the device.
 \param hStep    Horizontal (phase) step  (1 - 128)

 \param vOffset  Offset of row to scan (1 - 127)

 \param countRes Resolution of error count at each data point
                 (i.e. 'z' resolution):
                   0 =  1 bit
                   1 =  2 bit
                   2 =  4 bit
                   3 =  8 bit

 \param rate  Bit rate (in Bit/S)

  Typical Result Codes (included in scanFinished signal):
  globals::OVERFLOW      Invalid parameter value
  globals::NOT_CONNECTED The BertInterface object used by this
                         instance isn't valid, or isn't connected
                         to the instrument yet.
*/
int BertEyeMonitor::bathtubScan( const uint8_t channel,
                                 const uint8_t hStep,
                                 const uint8_t vOffset,
                                 const uint8_t countRes,
                                 const double  rate,
                                 const bool    reset )
{
    if ( (channel < 1) || (channel > 4) ||
         (hStep < 1) || (hStep > 128) ||
         (vOffset < 1) || (vOffset > 127) ||
         (countRes > 3) ) return globals::OVERFLOW;
    if (!bertInterface) return globals::NOT_CONNECTED;
    scanChannel  = channel;
    channelToBoardLane(channel, &scanBoard, &scanLane);

    if (reset)
    {
        stopFlag     = false;
        scanType     = SCAN_BATHTUB_SCAN;
        scanHStep    = hStep;
        scanVStep    = 1;
        scanVOffset  = vOffset;
        bitRate      = rate;

        scanCountResIndex = countRes;
          // Count res as index (0-3),  as used by 'Control Eye Sweep' macro
        scanCountResBits  = 1 << countRes;
         // Converted to number of bits (1, 2, 4 or 8), for size calcs.
    }

    qDebug() << "Bathtub Scan Configuration:";
    qDebug() << " Lane:       " << scanLane;
    qDebug() << " H Step:     " << scanHStep;
    qDebug() << " V Offset:   " << scanVOffset;
    qDebug() << " Resolution: " << scanCountResBits;

    return eyeScanRun();
}










/*!
 \brief Cancel the eye scan
*/
void BertEyeMonitor::cancelScan()
  {  stopFlag = true;  }



/*!
 \brief Get data from last eye scan run
 \return [data] Pointer to an array of data points (uint8_t)
 \return NULL   No data available (scan not run / not finished)
*/
double *BertEyeMonitor::getEyeData()
{
    double *retVal;
    dataMutex.lock();
    //----------------------------
    retVal = eyeDataBuffer;
    //----------------------------
    dataMutex.unlock();
    return retVal;
}












////////////// PRIVATE: ////////////////////////////////////////////////////////////

/*!
 \brief Run the Eye Scan
*/
int BertEyeMonitor::eyeScanRun()
{
// Define for debug: define EYE_SCAN_DEBUG
#ifdef EYE_SCAN_DEBUG
    // FAKE eye data for debugging
    scanVRes = 20;
    scanHRes = 20;
    if (!eyeDataBuffer)
    {
        eyeDataBuffer = new double [scanHRes * scanVRes];
    }
    int i;
    for (i=0; i < (scanHRes * scanVRes); i++)
    {
        if (scanChannel == 1) eyeDataBuffer[i] = (double)(qrand() % 128);
        else                            eyeDataBuffer[i] = (((i/2) % scanHRes) > (scanHRes/2)) ? 128.0 : 0.0;
    }

    for (int repeat = 0; repeat < 20; repeat++)
    {
        QCoreApplication::processEvents();
        if (stopFlag) return globals::CANCELLED;
        emit progressUpdate( (repeat * 100)/20 );
        QCoreApplication::processEvents();
        if (stopFlag) return globals::CANCELLED;
        globals::sleep(50);
    }
    return globals::OK;
#else
    int scanResult = globals::OK;
    uint8_t *rawDataBuffer       = NULL;  // Used to store raw (packed) data for one scan
    double *eyeDataBufferTmp     = NULL;  // Used to construct final data while scanning the eye
    double *eyeDataBufferTmpShf  = NULL;  // Used to construct shifted data while scanning the eye
    double *eyeDataBufferTmpExt  = NULL;  // Used to construct extended data while scanning the eye

    const uint8_t phaseStart = 0;
    const uint8_t phaseStop  = 127;
    size_t numSamples = 1;

    ///////// Determine the output memory attributes: ///////////////////////
    uint16_t imageStartAddress;
    uint16_t imageMaximumSize;
    uint8_t imageAddressMSB, imageAddressLSB;
    uint8_t sizeMSB, sizeLSB;
    imageAddressMSB = 0;
    imageAddressLSB = 0;
    sizeMSB    = 0;
    sizeLSB    = 0;
    scanResult = queryEyeScanMem( scanBoard, &imageAddressMSB, &imageAddressLSB, &sizeMSB, &sizeLSB );
    if (scanResult != globals::OK)
    {
        qDebug() << "Error reading output memory attributes: " << scanResult;
        goto finished;
    }
    imageStartAddress = ((uint16_t)(imageAddressMSB)<<8) + imageAddressLSB;
    imageMaximumSize = ((uint16_t)(sizeMSB)<<8) + sizeLSB;
    qDebug() << "Eye Scan - Image Start Address: " << imageStartAddress
             << "; Size: " << imageMaximumSize;

    /**** Eye Scan: *****************************************************/
    qDebug() << "**Starting Eye Scan**";

    double *eyeDataBufferPtr;
    double *eyeDataBufferOVERFLOW;

    uint8_t numPhaseSteps;
    uint8_t numOffsetStepsMax;
    uint8_t numOffsetSteps;

    uint8_t offsetStart, offsetStop;
    uint8_t thisOffsetStart, thisOffsetStop;

    uint8_t outputSizeMSB, outputSizeLSB;
    uint16_t outputSize;

    // Calculate the number of steps in one scan line:
    numPhaseSteps = (uint8_t)
            ceil( (double)(phaseStop - phaseStart + 1) / (double)scanHStep );
    scanHRes = numPhaseSteps;

    // calculate number of lines that will fit in memory:
    // From section 5.5.2 of datasheet
    numOffsetStepsMax = (uint8_t)
            (  (double)imageMaximumSize /
               (
                   (double)numPhaseSteps * ( (double)scanCountResBits / 8.0 )
                   )
               );

    // Calculate the number of lines in the full scan:
    if (scanType == SCAN_EYE_SCAN)
    {
        // FULL eye scan:
        offsetStart = 1;
        offsetStop  = 127;
        numOffsetSteps = (uint8_t)
                ceil(  (double)(offsetStop - offsetStart + 1) / (double)scanVStep  );
        scanVRes = numOffsetSteps;
        qDebug() << "Eye Scan - Max lines per scan: " << numOffsetStepsMax
                 << "; Total number of lines: " << numOffsetSteps;
    }
    else
    {
        // One line scan (bathtub plot):
        offsetStart = scanVOffset;
        offsetStop  = scanVOffset;
        thisOffsetStart = offsetStart;
        thisOffsetStop = offsetStop;
        numOffsetSteps = 1;
        scanVRes = 1;
        scanVStep = 1;
        qDebug() << "Bathtub Scan - One line at specified offset.";
    }

    // Calculate total number of samples, and allocate buffer for data:
    numSamples = numPhaseSteps * numOffsetSteps;
    rawDataBuffer = new uint8_t[imageMaximumSize];
    eyeDataBufferTmp = new double[numSamples];
    if ( (!rawDataBuffer) || (!eyeDataBufferTmp) )
    {
        scanResult = globals::MALLOC_ERROR;
        goto finished;
    }
    size_t i;
    for (i=0; i<numSamples; i++) eyeDataBufferTmp[i] = 0.0;
    eyeDataBufferOVERFLOW = eyeDataBufferTmp + numSamples; // For sanity check when writing to output buffer!


    ////// Eye scan will be run more than once for resolutions > 8 bit: /////////////////
    thisOffsetStart = offsetStart;
    outputSizeMSB = 0;
    outputSizeLSB = 0;

    //////////////////////////////////////////////////////////////
    /////// Scan the eye (in several steps if needed): ///////////
    //////////////////////////////////////////////////////////////

    eyeDataBufferPtr = eyeDataBufferTmp;

    if (numOffsetSteps > numOffsetStepsMax)
    {  thisOffsetStop = offsetStart + (numOffsetStepsMax - 1) * scanVStep;  }
    else
    {  thisOffsetStop = offsetStop;  }

    emit progressUpdate(0);

    while (thisOffsetStart <= offsetStop)
    {
        if (stopFlag)
        {
            qDebug() << "--Scan Cancelled. Stop.--";
            scanResult = globals::CANCELLED;
            goto finished;
        }
        //// SCAN: /////////////////////////////////////////////
        qDebug() << "** Starting part scan: **\n"
                 << "   phaseStart: " << phaseStart
                 << "   phaseStop: " << phaseStop
                 << "   phaseStep: " << scanHStep << "\n"
                 << "   thisOffsetStart: " << thisOffsetStart
                 << "   thisOffsetStop: " << thisOffsetStop
                 << "   offsetStep: " << scanVStep << "\n"
                 << "   resolution: " << scanCountResIndex;

        scanResult = controlEyeSweep( scanBoard,
                                                scanLane,
                                                phaseStart,
                                                phaseStop,
                                                scanHStep,
                                                thisOffsetStart,
                                                thisOffsetStop,
                                                scanVStep,
                                                scanCountResIndex,
                                                &outputSizeMSB,
                                                &outputSizeLSB );
        qDebug() << "** Part scan finished. Result: " << scanResult;
        if (scanResult != globals::OK)
        {
            qDebug() << "Error running scan: " << scanResult;
            goto finished;
        }
        if (stopFlag)
        {
            qDebug() << "--Scan Cancelled. Stop.--";
            scanResult = globals::CANCELLED;
            goto finished;
        }
        // Read back data:

        outputSize = ((uint16_t)outputSizeMSB << 8) + outputSizeLSB;
        //// READ DATA: /////////////////////////////////////////////
        qDebug() << "--Reading back scan data: " << outputSize << " bytes --";

        scanResult = bertInterface->rawRead24( scanBoard,
                                               0xFC,
                                               imageAddressMSB,
                                               imageAddressLSB,
                                               &rawDataBuffer[0],
                                               (size_t)outputSize );
        if (scanResult != globals::OK)
        {
            qDebug() << "Error reading back scan data: " << scanResult;
            goto finished;
        }
        //// UNPACK DATA: //////////////////////////////////////////
        qDebug() << "--Unpacking scan data...\n"
                 << "  Tot Num Samples: " << numSamples << "\n"
                 << "  THIS block size: " << outputSize << "\n"
                 << "  Bytes Remaining: " << (int)(eyeDataBufferOVERFLOW - eyeDataBufferPtr);
        for (size_t sampleIndex = 0; sampleIndex < (size_t)outputSize; sampleIndex++)
        {
            // Sanity check: Make sure we aren't about to overflow the output buffer:
            if (eyeDataBufferPtr >= eyeDataBufferOVERFLOW) { scanResult = globals::OVERFLOW; goto finished; }

            switch (scanCountResBits)
            {
            case 1: // 1  bit per sample:
                for (int bitIndex = 0; bitIndex < 8; bitIndex++)
                {
                    // For each bit, shift and mask, and store to output as uint8_t:
                    *eyeDataBufferPtr += (double)((rawDataBuffer[sampleIndex] >> (7-bitIndex)) & 0x01);
                    eyeDataBufferPtr++;
                }
                break;
            case 2: // 2  bits per sample:
                for (int bitIndex = 0; bitIndex < 8; bitIndex+=2)
                {
                    // For each bit, shift and mask, and store to output as uint8_t:
                    *eyeDataBufferPtr += (double)((rawDataBuffer[sampleIndex] >> (6-bitIndex)) & 0x03);
                    eyeDataBufferPtr++;
                }
                break;
            case 4: // 4  bits per sample:
                for (int bitIndex = 0; bitIndex < 8; bitIndex+=4)
                {
                    // For each bit, shift and mask, and store to output as uint8_t:
                    *eyeDataBufferPtr += (double)((rawDataBuffer[sampleIndex] >> (4-bitIndex)) & 0x0F);
                    eyeDataBufferPtr++;
                }
                break;
            case 8: // 8  bits per sample:
                *eyeDataBufferPtr += (double)rawDataBuffer[sampleIndex];
                eyeDataBufferPtr++;
            }
        }

        //// Advance start and stop offsets: ///////////////////////
        qDebug() << "--Adjusting offsets for next part...";
        //Start = Stop + OffsetStep:
        thisOffsetStart = thisOffsetStop + scanVStep;
        //Stop = Start + (NumOffsetStepsMax - 1) * OffsetStep:
        thisOffsetStop = thisOffsetStart + ( (numOffsetStepsMax - 1) * scanVStep );
        //If Stop is greater than OffsetStop then Stop = OffsetStop:
        if (thisOffsetStop > offsetStop) thisOffsetStop = offsetStop;

        // Update progress:
        emit progressUpdate( ((eyeDataBufferPtr - eyeDataBufferTmp) * 100) / numSamples );
    }

    if (scanResult == globals::OK)
    {
        // Finished successfully.

        if (scanType == SCAN_EYE_SCAN)
        {
            //////// SHIFT: //////////////////////////////////////////////////
            // Only adjust the shift amount on the first scan after a reset:
            if (resetFlag)
            {
                uint8_t peakIndex = peakFind(eyeDataBufferTmp, numPhaseSteps, numOffsetSteps);
                const uint8_t xMid = (numPhaseSteps / 2);  // Index of mid point
                if (peakIndex <= xMid) nShift[scanChannel] = (int)(peakIndex/scanHStep) + 1;
                else                   nShift[scanChannel] = ((int)peakIndex - (int)numPhaseSteps) / (int)scanHStep - 1;
                nShift[scanChannel] = nShift[scanChannel] - (int)(0.11*(double)numPhaseSteps);
                if (nShift[scanChannel] < (-1 * (int)numPhaseSteps)) nShift[scanChannel] = (-1 * (int)numPhaseSteps);
            }
            qDebug() << "SHIFT: Rotate eye plot " << nShift[scanChannel] << " samples";

            eyeDataBufferTmpShf = dataShift( eyeDataBufferTmp,
                                             numPhaseSteps,
                                             numOffsetSteps,
                                             nShift[scanChannel] );
            if (!eyeDataBufferTmpShf)
            {
                scanResult = globals::MALLOC_ERROR;
                goto finished;
            }

            //////// EXTEND: /////////////////////////////////////////////////
            // For eye plot: "extend" the eye width by copying the left side
            // and adding it to the right side (improves readability):
            uint8_t extraPhaseSteps = (uint8_t)((float)numPhaseSteps * 0.215f);
            qDebug() << "EXTEND: Extending eye by " << extraPhaseSteps << " steps.";

            uint8_t numPhaseStepsExt = numPhaseSteps + extraPhaseSteps;
            size_t numSamplesExt = numPhaseStepsExt * numOffsetSteps;

            eyeDataBufferTmpExt = new double[numSamplesExt];
            if (!eyeDataBufferTmpExt)
            {
                scanResult = globals::MALLOC_ERROR;
                goto finished;
            }
            size_t x, y, sampleIndex, storeIndex;
            storeIndex = 0;
            for (y = 0; y < numOffsetSteps; y++)
            {
                for (x = 0; x < numPhaseStepsExt; x++)
                {
                    if (x < numPhaseSteps) sampleIndex = (y * numPhaseSteps) + x;
                    else                   sampleIndex = (y * numPhaseSteps) + (x - numOffsetSteps);
                    eyeDataBufferTmpExt[storeIndex] = eyeDataBufferTmpShf[sampleIndex];
                    storeIndex++;
                }
            }
            scanHRes = numPhaseStepsExt;
            ////////////////////////////////////////////////////////////////////
        }
        else
        {
            //////// SHIFT: ////////////////////////////////////////////////////
            // For bathtub plots: We want the plot to show one "eye" (tub),
            // with the left side being the max error point at the start of the
            // eye; however sample 0 of scan data is actually before the eye
            // start. Cut the left end and splice it onto the right end:
            // Nb: Only adjust the shift amount on the first scan after a reset:
            if (resetFlag)
            {
                uint8_t peakIndex = peakFind(eyeDataBufferTmp, numPhaseSteps, numOffsetSteps);
                const uint8_t xMid = (numPhaseSteps / 2);  // Index of mid point
                if (peakIndex <= xMid) nShift[scanChannel] = (int)(peakIndex/scanHStep);
                else                   nShift[scanChannel] = ((int)peakIndex - (int)numPhaseSteps) / (int)scanHStep;
            }
            qDebug() << "SHIFT: Rotate bathtub plot " << nShift[scanChannel] << " samples";

            eyeDataBufferTmpExt = dataShift( eyeDataBufferTmp,
                                             numPhaseSteps,
                                             numOffsetSteps,
                                             nShift[scanChannel] );
            if (!eyeDataBufferTmpExt)
            {
                scanResult = globals::MALLOC_ERROR;
                goto finished;
            }
            ////////////////////////////////////////////////////////////////////
        }
        // Only update REAL data buffer pointer if everything finished successfully:
        qDebug() << "--Scan data aquired! Updating output buffer...";
//qDebug() << "Data Mutex: " << ((NULL == dataMutex) ? "NULL" : "OK") << "  " << &(dataMutex);
        dataMutex.lock();
        //----------------------------
        if (eyeDataBuffer) delete [] eyeDataBuffer;
          // Last minute deletion policy... delete old data.
        eyeDataBuffer = eyeDataBufferTmpExt;

#ifdef BERT_EYESCAN_EXTRA_DEBUG
        //// DEBUG: Dump Eye Data: ///////////////////////////
        qDebug() << endl << "---- RAW Data ------------------------------------------" << endl;
        size_t xx, yy;
        for (yy=0; yy < numOffsetSteps; yy++)
        {
            QString debugString;
            for (xx=0; xx < scanHRes; xx++)
            {
                debugString += QString::number(eyeDataBuffer[(yy * scanHRes) + xx]);
                if (xx < (scanHRes-1)) debugString += QString(", ");
            }
            qDebug() << debugString;
        }
        qDebug() << "--------------------------------------------------------" << endl;
        //////////////////////////////////////////////////////
#endif
        //----------------------------
        dataMutex.unlock();
    }
    qDebug() << "**Eye Scan finshed OK. **";
  finished:
    // Clean up temp buffers:
    if (rawDataBuffer) delete [] rawDataBuffer;
    if (eyeDataBufferTmp) delete [] eyeDataBufferTmp;
    if (eyeDataBufferTmpShf) delete [] eyeDataBufferTmpShf;

    if ( (eyeDataBufferTmpExt) &&
         (eyeDataBufferTmpExt != eyeDataBuffer) ) delete [] eyeDataBufferTmpExt;
        // Temp buffer hasn't been used for final data, so delete it.

    if (scanResult == globals::OVERFLOW)  qDebug() << "ERROR: Ran out of space in output buffer!";

    return scanResult;

#endif  // ifdef EYE_SCAN_DEBUG
}







/*!
 \brief Find the location of the "peak" in the centre row of the eye scan
 \param data   Pointer to array of raw scan data (doubles)
               The array pointed to by data must contain
               sizeX * sizeY elements
 \param sizeX  Number of sample points horizontally (minimum 2)
 \param sizeY  Number of rows (sample points vertically) (minimum 1)

 \return index  Index of maximum value on the x axis, for
                the row through the vertical centre of the plot
                This is a value between 0 and sizeX inclusive.
*/
uint8_t BertEyeMonitor::peakFind( const double *data,
                                  const size_t sizeX,
                                  const size_t sizeY )
{
    double zMax = 0.0;
    size_t maxIndex = 0;
    const size_t yMid = sizeY / 2;
    const size_t startIndex = (yMid * sizeX);
    size_t sampleIndex = startIndex;
    for (size_t x = 0; x < sizeX; x++)
    {
        if (data[sampleIndex] >= zMax)
        {
            maxIndex = (sampleIndex - startIndex);
            zMax = data[sampleIndex];
        }
        sampleIndex++;
    }
    return maxIndex;
}




/*!
 \brief Rotate data array left or right

 Allocates a NEW data array of the same size as the
 source array. The CALLER must free the new array
 with  delete [] (BUT see notes on return value below)

 Each element is shifted left or right within its row, with
 the end of the row being 'wrapped' around, according to the
 value of nShift

 \param data   Pointer to array of raw scan data (doubles)
               The array pointed to by data must contain
               sizeX * sizeY elements
 \param sizeX  Number of sample points horizontally (minimum 2)
 \param sizeY  Number of rows (sample points vertically) (minimum 1)
 \param nShift Number of places to shift the data.
                 Positive: Shifts the corresponding plot RIGHT
                 Negative: Shifts the corresponding plot LEFT
               The absolute value of nShift must be between 0
               and sizeX (inclusive)

 \return pointer  Pointer to a NEW arry of doubles, the same size
                  as data, containing the shifted data
                  Caller must call delete [] on this pointer
                  when data is no longer required.

         NULL     Error: Either the memory allocation failed, or
                  the shift value was too large. Caller should
                  check the returned pointer for NULL.
*/
double *BertEyeMonitor::dataShift( const double *data,
                                   const size_t sizeX,
                                   const size_t sizeY,
                                   const int nShift )
{
    // SHIFT: Cut one side off the array and place it on the other side.
    // A negative shift (shift plot to RIGHT) is actually a
    // complimentary positive shift, i.e. (size - abs(shift))
    size_t nShiftAbs;
    if (nShift >= 0)
      {  nShiftAbs = (size_t)nShift;  }
    else
      {  nShiftAbs = (size_t)(sizeX + nShift);  }
    // Check bounds: Must be between 0 and sizeX:
    if (nShiftAbs > sizeX) return NULL;

    const size_t nSamples = (sizeX * sizeY);
    double *dataShifted = new double[nSamples];
    if (!dataShifted) return NULL;  // Malloc Error!

    size_t x, y, sampleIndex, storeIndex;
    storeIndex = 0;
    for (y = 0; y < sizeY; y++)
    {
        for (x = 0; x < sizeX; x++)
        {
            sampleIndex = (y * sizeX) + x;
            if (x < nShiftAbs) storeIndex = (y * sizeX) + x + (sizeX - nShiftAbs);
            else               storeIndex = (y * sizeX) + x - nShiftAbs;
            dataShifted[storeIndex] = data[sampleIndex];
        }
    }
    return dataShifted;
}




/*!
 \brief Run Query Eye Scanner Output Memory Attributes Macro
 \param board       Board to use (0 or 1)
 \param addressMSB  Pointer to a uint8_t to store MSB of address for scan data
 \param addressLSB  Pointer to a uint8_t to store LSB of address for scan data
 \param sizeMSB     Pointer to a uint8_t to store MSB of max data size
 \param sizeLSB     Pointer to a uint8_t to store LSB of max data size
                    If an error occurs, the values of the parameters will be set to 0.
 \return globals::OK      Success
 \return [Error Code]     Error from hardware/comms functions
*/
int BertEyeMonitor::queryEyeScanMem( const uint8_t board,
                                     uint8_t *addressMSB,
                                     uint8_t *addressLSB,
                                     uint8_t *sizeMSB,
                                     uint8_t *sizeLSB )
{
    // Default results: Set to 0 (in case of error...).
    *addressMSB = 0;
    *addressLSB = 0;
    *sizeMSB    = 0;
    *sizeLSB    = 0;
    // Use Query Eye Scanner Output Memory Attributes:
    uint8_t outputData[4];
    int result;
    result = bertInterface->runMacro( board, 0x41, NULL, 0, outputData, 4 );
    if (result != globals::OK) return result;     //  Macro error...
    // Copy results to output parameters:
    *addressMSB = outputData[0];
    *addressLSB = outputData[1];
    *sizeMSB    = outputData[2];
    *sizeLSB    = outputData[3];
    return globals::OK;
}




/*!
 \brief Set up and start an eye scan sweep
 Calls the "Control Eye Sweep" macro. See notes in
 GT1724 data sheet for notes on how to select the
 parameters.

 \param board       Selects the board to perform eye sweep on (0-1)
 \param lane        Selects the lane to perform eye sweep on (0-3)
 \param phaseStart  Starting Phase offset. Valid range is 0 to 127
 \param phaseStop   Phase offset limit. Valid range is 0 to 127
                    Phase Stop must be greater than or equal to
                    Phase Start
 \param phaseStep   Unsigned value for phase step size. Valid
                    range is 1 to 128
 \param offsetStart Starting voltage offset. Valid range is 1 to 127
 \param offsetStop  Voltage offset limit. Valid range is 1 to 127.
                    Offset Stop must be greater than or equal to
                    Offset Start
 \param offsetStep  Unsigned value for voltage offset step size.
                    Valid range is 1 to 127
 \param resolution  Resolution of the eye error count for each
                    data point:
                        0 = 1-bit
                        1 = 2-bit
                        2 = 4-bit
                        3 = 8-bit
 \param sizeMSB    Used to return MSB of size of the output image
 \param sizeLSB    Used to return LSB of size of the output image

 \return globals::OK        Success
 \return globals::OVERFLOW  Parameter out of range
 \return [Error Code]     Error from hardware/comms functions
*/
int BertEyeMonitor::controlEyeSweep( const uint8_t board,
                                     const uint8_t lane,
                                     const uint8_t phaseStart,
                                     const uint8_t phaseStop,
                                     const uint8_t phaseStep,
                                     const uint8_t offsetStart,
                                     const uint8_t offsetStop,
                                     const uint8_t offsetStep,
                                     const uint8_t resolution,
                                     uint8_t *sizeMSB,
                                     uint8_t *sizeLSB )
{
    if ( (lane > 3) ||
         (phaseStart > 127) ||
         (phaseStop > 127) ||
         (phaseStart > phaseStop) ||
         (phaseStep < 1) || (phaseStep > 128) ||
         (offsetStart < 1) || (offsetStart > 127) ||
         (offsetStop < 1) || (offsetStop > 127) ||
         (offsetStart > offsetStop) ||
         (offsetStep < 1) || (offsetStep > 127) ||
         (resolution > 3) ) return globals::OVERFLOW;  // Parameter out of range

    // Default results: Set to 0 (in case of error...).
    *sizeMSB = 0;
    *sizeLSB = 0;
    // Run the "Control Eye Sweep" macro:
    uint8_t inputData[8] = { lane,
                             phaseStart,
                             phaseStop,
                             phaseStep,
                             offsetStart,
                             offsetStop,
                             offsetStep,
                             resolution };
    uint8_t outputData[2];

    int result;
    result = bertInterface->runMacro( board, 0x42, inputData, 8, outputData, 2 );
    if (result != globals::OK) return result;     //  Macro error...

    // Unpack the output data (memory size used):
    *sizeMSB = outputData[0];
    *sizeLSB = outputData[1];
    return globals::OK;
}



/*!
 \brief Convert ED channel number to instrument board and lane number
 "Channel number" is the number displayed on the UI / front of box, i.e. 1 - 4.
 Channels 1 and 2 map to the master board (0); channels 3 and 4 map to the slave board
 On each board, the lane used for ED is 1 or 3.
 \param channel  IN:  Channel number (1-4)
 \param board    OUT: Board number (0 or 1)
 \param lane     OUT: Lane number (1 or 3)
*/
void BertEyeMonitor::channelToBoardLane( const uint8_t channel, uint8_t *board, uint8_t *lane )
{
    // Convert channel number into board number and lane number:
    switch (channel)
    {
    case 1:
        *board = 0;
        *lane = 1;
        break;
    case 2:
        *board = 0;
        *lane = 3;
        break;
    case 3:
        *board = 1;
        *lane = 1;
        break;
    case 4:
        *board = 1;
        *lane = 3;
        break;
    }
}




