/*!
 \file   main.cpp
 \brief  Smartest BERT Controller Software

 Generates a binary to provide UI and control functions for a
 Bit Error Rate Tester (BERT) instrument.

 Expected Hardware:

  - Embedded single-board PC (e.g. BeagleBone Black) with Linux
  - Touch-screen display for UI (use Qt for layout control)
  - USB-I2C Adaptor
  - Semtech GT1724 BERT generator chip (I2C Interface)

 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/


#include <cstdlib>
#include <iostream>
#include <pthread.h>
#include <sstream>
#include <string>
#include <unistd.h>

#include "globals.h"

#include "Serial.h"
#include "ThreadMessage.h"


void *threadLoop1(void *nothing)
  {
  std::cout << "Launch Thread 1..." << std::endl;

  std::string tempString;
  std::ostringstream tempConvert;
  int counter = 0;
  ThreadMessage *myMessage;

  while (globals::TRUE)
    {
    usleep(800000);
    tempString.clear();
    tempString.append("Thread 1 - ");
    tempConvert.str("");
    tempConvert << counter;
    tempString.append( tempConvert.str() );
    std::cout << std::endl << "Thread 1: Sending: " << tempString << std::endl;
    std::cout.flush();
    myMessage = new ThreadMessage( ThreadMessage::TYPE_DATA, tempString );
    counter++;
    }
  return NULL;
  }




void *threadLoop2(void *nothing)
  {
  std::cout << "Launch Thread 2..." << std::endl;

  std::string *tempString = NULL;
  int type;

  while (globals::TRUE)
    {
    ThreadMessage::waitQueue();

    if (ThreadMessage::pollQueue(&type, &tempString))
      {
      std::cout << std::endl << "  ----->Thread 2: Recd: \"" << *tempString << "\"" << std::endl;
      std::cout.flush();
      delete tempString;
      }
    else
      {
      std::cout << std::endl << "  ----->Thread 2: ??? Thought there was a message..." << std::endl;
      }
    usleep(10000000);
    }
  return NULL;
  }





void *threadLoop3(void *nothing)
  {
  std::cout << "Launch Thread 3..." << std::endl;

  std::string *tempString = NULL;
  int type;

  while (globals::TRUE)
    {
    ThreadMessage::waitQueue();

    if (ThreadMessage::pollQueue(&type, &tempString))
      {
      std::cout << std::endl << "  ----->Thread 3: Recd: \"" << *tempString << "\"" << std::endl;
      std::cout.flush();
      delete tempString;
      }
    else
      {
      std::cout << std::endl << "  ----->Thread 3: ??? Thought there was a message..." << std::endl;
      }
    }
  return NULL;
  }



/*!
 \brief Entry point for BERT controller
*/
int main(int argc, char **argv)
  {

  std::cout << "=== Smartest BERT Generator ===" << std::endl;

/*
  // Create threads for testing:
  pthread_t thread1ID;
  pthread_t thread2ID;
  pthread_t thread3ID;

  pthread_create( &thread1ID, NULL, &threadLoop1, NULL );
  std::cout << "Main: Started Thread: " << thread1ID << std::endl;

  usleep(100);
  pthread_create( &thread2ID, NULL, &threadLoop2, NULL );
  std::cout << "Main: Started Thread: " << thread2ID << std::endl;

  usleep(100);
  pthread_create( &thread3ID, NULL, &threadLoop3, NULL );
  std::cout << "Main: Started Thread: " << thread3ID << std::endl;

  // Main halts:
  std::cout << "Main: Waiting for threads to finish." << std::endl;
  pthread_join(thread1ID, NULL);
  pthread_join(thread2ID, NULL);
  pthread_join(thread3ID, NULL);
*/

  std::cout << "Opening serial port ttyUSB0, 9600 Baud..." << std::endl;

  Serial serial;
  int result, error;
  error = 0;
  result = serial.open( "/dev/ttyUSB0", B9600, Serial::MODE_RAW, &error );

  std::cout << "Result: " << result << "; Error Code: " << error << std::endl;
  std::cout << "Sending Data..." << std::endl;

  serial.write("Hello!", 6);

  std::cout << "Waiting for Data..." << std::endl;

  ssize_t nRead;
  char buffer[100];
  nRead = serial.read(buffer, 3, 5);
  buffer[nRead] = 0x00;

  std::cout << "Received: " << buffer << " (" << nRead << " bytes)" << std::endl;

  std::cout << "Closing Port..." << std::endl;

  serial.close();

  std::cout << "Done!" << std::endl;

//  while (1)
//    {
//    usleep(1000000000);
//    }

  }
