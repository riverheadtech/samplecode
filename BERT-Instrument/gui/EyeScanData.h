/*!
 \file   EyeScanData.h
 \brief  Raster Data for Eye Scan - Header
 \author J Cole-Baker (For Smartest)
 \date   Feb 2016
*/

#ifndef EYESCANDATA_H
#define EYESCANDATA_H

#include <stdint.h>
#include <qwt_raster_data.h>
#include <qwt_interval.h>


/*!
 \brief The EyeScanData class
 Based on QwtRasterData class; represents the underlying
 data (matrix of double values) for an eye scan plot.
 Data are passed to the constructor, and COPIED internally
 to the object (original array can be deleted after
 constructor returns). Matrix dimensions and scale intervals
 are also passed to the constructor.
 The eye scan plot will project the supplied data matrix
 on to x and y scales with the specified intervals.
*/
class EyeScanData : public QwtRasterData
{
public:
    EyeScanData(  const double *data,
                  const int arraySizeX,
                  const int arraySizeY,
                  const QwtInterval intervalX,
                  const QwtInterval intervalY  );

    ~EyeScanData();

    virtual QwtRasterData *copy() const;
    virtual QwtInterval range() const;
    virtual double value(double x, double y) const;

private:
    double *dataArray = NULL;
    size_t  dataArraySize = 0;

    QwtInterval plotDataRange  = { 0.0, 1.0 };
    QwtInterval plotRangeX     = { 0.0, 1.0 };
    QwtInterval plotRangeY     = { 0.0, 1.0 };


    struct structXY
    {
        double x;
        double y;
    };

    structXY dataSize;
    structXY quantization;

    void    setData(const double *data, const int sizex, const int sizey);
    size_t  arrPos(const int x, const int y) const;
    void    arrayValueMinMax(const double *data, const size_t size, QwtInterval *range);
};

#endif // EYESCANDATA_H
