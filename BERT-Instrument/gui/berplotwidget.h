/*!
 \file   BERPlotWidget.h
 \brief  Bit Error Ratio Plot - Header
 \author J Cole-Baker (For Smartest)
 \date   Jan 2016
*/


#ifndef BERPLOTWIDGET_H
#define BERPLOTWIDGET_H

#include <QWidget>
#include <QFont>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_scale_draw.h>
#include <qwt_scale_engine.h>
#include <qwt_text_label.h>



/*!
 \brief Custom QwtScaleDraw class to supply custom formatting for Y scale labels
*/
class BERPlotYScaleDraw : public QwtScaleDraw
{
public:
    BERPlotYScaleDraw() : QwtScaleDraw()
    { }

protected:
    QwtText label(double value) const
    {
        // Custom scale formatter:
        return QwtText( QString("%1").arg(value, 0, 'e', 0) );
    }
};




/*!
 \brief Bit Error Ratio Plot widget
 Derived from QwtPlot class. Generates a
 plot of bit error ratio over time.
*/
class BERPlotWidget : public QwtPlot
{
Q_OBJECT

public:
    explicit BERPlotWidget(  const QwtText &title,
                             const int      width,
                             const int      height,
                             const int      seconds,
                             const int      logScale,
                             const int      plotIndex,
                             QWidget       *parent  );
    ~BERPlotWidget();

    void addPoint( const double values );
    void clear();

signals:
public slots:

private:

    static const double LOG_PLOT_FLOOR;

    const size_t bufferSeconds;
    const int    logScale;

    QwtPlotCurve *channelPlot = NULL;

    // Data series values:
    std::vector<double> xs;
    std::vector<double> ys;

    // Plot Elements:
    QFont *titleFont;

    BERPlotYScaleDraw *plotScaleDraw;
    QwtScaleEngine *scaleEngineLog = NULL;
    QPen plotPen;
    QBrush plotBrush;
};

#endif // BERPLOTWIDGET_H
