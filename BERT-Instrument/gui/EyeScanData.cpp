/*!
 \file   EyeScanData.cpp
 \brief  Raster Data for Eye Scan
 \author J Cole-Baker (For Smartest)
 \date   Feb 2016
*/

#include <limits>
#include <cstddef>

#include <QDebug>
#include <QRectF>

#include "EyeScanData.h"


/*!
 \brief Constructor
 Create internal data structures for the EyeScanData
 \param data        Pointer to array of doubles containing source data - see setData
 \param arraySizeX  Data matrix X dimensions - see setData
 \param arraySizeY  Data matrix Y dimensions - see setData
 \param intervalX   X scale interval. Underlying data matrix will be projected
                    on to this scale range in the X dimension
 \param intervalY   Y scale interval. Underlying data matrix will be projected
                    on to this scale range in the Y dimension
*/
EyeScanData::EyeScanData(  const double *data,
                           const int arraySizeX,
                           const int arraySizeY,
                           const QwtInterval intervalX,
                           const QwtInterval intervalY  ) : QwtRasterData()
{
    dataArray = NULL;
    setInterval( Qt::XAxis, intervalX );
    setInterval( Qt::YAxis, intervalY );

    plotRangeX = intervalX;
    plotRangeY = intervalY;

    setData(data, arraySizeX, arraySizeY);
}



/*!
 \brief Destructor - delete the data and clean up
*/
EyeScanData::~EyeScanData()
{  if (dataArray) delete [] dataArray;  }



/*!
 \brief Copy method
 Copies the EyeScanData object
 \return Copy of the object
 */
QwtRasterData *EyeScanData::copy() const
{
    EyeScanData *clone = new EyeScanData(dataArray, dataSize.x, dataSize.y, plotRangeX, plotRangeY);
    return clone;
}


QwtInterval EyeScanData::range() const
{  return plotDataRange;  }



/*!
 \brief Get a data value for a given (x,y) coordinate
 Coordinates will be in terms of the scale intervals. This
 method looks up the closest value in the underlying data
 matrix and returns it.
 \param   x  Co-ordinate
 \param   y  Co-ordinate
 \return  data
*/
double EyeScanData::value(double x, double y) const
{
    int xpos = (int)((x - plotRangeX.minValue()) / quantization.x);
    int ypos = (int)((y - plotRangeY.minValue()) / quantization.y);
    int pos = arrPos(xpos, ypos);
    double dvalue = dataArray[pos];
    //qDebug() << "-Value request for " << x << "," << y << ": " << dataArray[pos] << " = " << dvalue;
    return dvalue;
}


/*!
 \brief Set up the internal data data structures
 This is called by the constructor. Creates a matrix of data
 points using the supplied input array and matrix dimensions.
 The matrix dimensions are used in conjunction with the
 scale intervals to select sample points when plotting the data.
 \param data   Array of doubles. Must contain
               sizeX * sizeY elements.
 \param sizeX  Data matrix width
 \param sizeY  Data matrix height
*/
void EyeScanData::setData(const double *data, const int sizeX, const int sizeY)
{
    dataSize.x = sizeX;
    dataSize.y = sizeY;
    dataArraySize = sizeX * sizeY;
    arrayValueMinMax(data, dataArraySize, &plotDataRange);
    setInterval( Qt::ZAxis, plotDataRange );

    if (dataArray != NULL) delete [] dataArray;
    dataArray = new double [dataArraySize];
    memcpy(dataArray, data, dataArraySize * sizeof(double));

    quantization.x = (plotRangeX.maxValue() - plotRangeX.minValue()) / (dataSize.x - 1);
    quantization.y = (plotRangeY.maxValue() - plotRangeY.minValue()) / (dataSize.y - 1);
}


/*!
 \brief Convert x,y coordinate into linear address in array
 Uses global dataSize property to calculate the offset
 \param x   X coordinate
 \param y   Y coordinate
 \return  Linear offset into the array for the given coordinates
*/
size_t EyeScanData::arrPos(const int x, const int y) const
{
    size_t pos = (dataSize.x * y) + x;
    if (pos >= dataArraySize) pos = (dataArraySize-1);
    return pos;
}


/*!
 \brief Search an array of doubles for the minimum and maximum data values
 \param data   Pointer to arry to search
 \param size   Number of elements in the array
 \param range  Pointer to a QwtInterval used to return the min and max values
*/
void EyeScanData::arrayValueMinMax(const double *data, const size_t size, QwtInterval *range)
{
    size_t i;
    double minValue = std::numeric_limits<double>::max();
    double maxValue = std::numeric_limits<double>::min();
    for (i=0; i<size; i++)
    {
        if (data[i] < minValue) minValue = data[i];
        if (data[i] > maxValue) maxValue = data[i];
    }
    range->setMinValue(minValue);
    range->setMaxValue(maxValue);
}

