/*!
 \file   EyePlotWidget.h
 \brief  Eye Scan Plot - Header
 \author J Cole-Baker (For Smartest)
 \date   Feb 2016
*/


#ifndef EYEPLOTWIDGET_H
#define EYEPLOTWIDGET_H

#include <QWidget>
#include <QFont>
#include <QLabel>

#include <qwt_plot.h>
#include <qwt_plot_spectrogram.h>
#include <qwt_scale_engine.h>
#include <qwt_text_label.h>
#include <qwt_color_map.h>

#include "stdint.h"

#include "EyeScanData.h"

class EyePlotWidget : public QwtPlot //, QWidget
{
    Q_OBJECT
public:
    explicit EyePlotWidget( const QwtText &title,
                            const int      width,
                            const int      height,
                            QWidget       *parent );
    ~EyePlotWidget();

    void showData( const double  *data,
                   const int      xRes,
                   const int      yRes );

    void clear();

signals:
public slots:

private:
    const QWidget       parent;
    QwtPlotSpectrogram *eyeScanPlot = NULL;
    EyeScanData        *eyeScanData = NULL;

    // Plot Elements:
    QFont titleFont;
    QwtLinearColorMap *colorMap;

    /* FOR CONTOURS:  QList<double> contourLevels; */

    QwtScaleDiv mvScaleDiv;
    QList<double> mvMajTicks = { -250.0, -125.0, 0.0, 125.0, 250.0 };
    QList<double> mvMinTicks = {     -187.5, -62.5, 62.5, 187.5    };
};

#endif // EYEPLOTWIDGET_H
