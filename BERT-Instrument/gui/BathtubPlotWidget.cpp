/*!
 \file   BathtubPlotWidget.cpp
 \brief  Bathtub Plot
 \author J Cole-Baker (For Smartest)
 \date   May 2016
*/


#include "BathtubPlotWidget.h"


/*!
 \brief BathtubPlotWidget Constructor
 Create a new Bit Error Ratio Plot widget
 \param title    Title of plot - displayed at top
 \param width    Plot width - pixels
 \param height   Plot height - pixels
 \param channel  Sets plot appearance - color, thickness, etc
                   Used to make several plots visually distinct;
                   channel must be 1 to 4.
 \param parent   UI Widget which will contain the plot
*/
BathtubPlotWidget::BathtubPlotWidget(  const QwtText &title,
                                       const int      width,
                                       const int      height,
                                       const int      channel,
                                       QWidget       *parent  )
       : QwtPlot(title, parent)
  {
      int useChannel = channel;
      if (useChannel < 1) useChannel = 1;
      if (useChannel > 4) useChannel = 4;

      QColor colours[] = { Qt::magenta, Qt::magenta, Qt::blue, Qt::red, Qt::green };
      titleFont = new QFont( "MS Shell Dlg 2", 10, 4, false);
      titleLabel()->setFont(*titleFont);
      setGeometry(4,4,width+4,height+4);

      plotPen.setColor( colours[useChannel] );
      plotPen.setWidth( 2 );

      plotBrush.setColor( colours[useChannel] );
      plotBrush.setStyle( Qt::Dense6Pattern );

      setAxisScale(QwtPlot::xBottom, -0.5, 0.5 );

      channelPlot = new QwtPlotCurve( QString("Bathtub") );
      channelPlot->setBaseline(0.0);
      channelPlot->setPen( plotPen );
      channelPlot->setBrush( plotBrush );
      channelPlot->attach( this );
      channelPlot->setAxes( QwtPlot::xBottom, QwtPlot::yLeft);

      setAxisFont(QwtPlot::yLeft,QFont("Arial", 8));
      QwtText yAxisTitle("Log BER");
      yAxisTitle.setFont(QFont("Arial", 8));
      setAxisTitle(QwtPlot::yLeft, yAxisTitle);

      QwtText xAxisTitle("Phase Offset (UI)");
      xAxisTitle.setFont(QFont("Arial", 8));
      setAxisTitle(QwtPlot::xBottom, xAxisTitle);

      replot();
  }


BathtubPlotWidget::~BathtubPlotWidget()
{}



/*!
 \brief Update the Bathtub Plot widget to show new data
 \param data  Pointer to an array of doubles containing
              the data. Data are copied so array may be
              deleted after call returns.
 \param dataSize  Number of elements in data
*/
void BathtubPlotWidget::showData( const double  *data,
                                  const size_t   dataSize )
{
    xs.clear();
    ys.clear();
    double x = -0.5;
    for (size_t i = 0; i < dataSize; i++)
    {
        xs.push_back(x);
        ys.push_back(data[i]);
        x += (1.0/128.0);
    }
    channelPlot->setSamples( &xs[0],
                             &ys[0],
                              ys.size() );
    replot();
}

/*!
 \brief Bathtub Plot clear
 Clear any existing data from the plot
*/
void BathtubPlotWidget::clear()
{
    ys.clear();
    xs.clear();
    channelPlot->setSamples( &xs[0],
                             &ys[0],
                              ys.size() );
    replot();
}


