/*!
 \file   BERPlotWidget.h
 \brief  Bit Error Ratio Plot - Header
 \author J Cole-Baker (For Smartest)
 \date   Jan 2016
*/


#ifndef BATHTUBPLOTWIDGET_H
#define BATHTUBPLOTWIDGET_H

#include <QWidget>
#include <QFont>

#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_scale_draw.h>
#include <qwt_scale_engine.h>
#include <qwt_text_label.h>



/*!
 \brief Custom QwtScaleDraw class to supply custom formatting for Y scale labels
*
class BathtubPlotYScaleDraw : public QwtScaleDraw
{
public:
    BathtubPlotYScaleDraw() : QwtScaleDraw()
    { }

protected:
    QwtText label(double value) const
    {
        // Custom scale formatter:
        return QwtText( QString("%1").arg(value, 0, 'e', 0) );
    }
};
*/



/*!
 \brief Bathtub Plot widget
 Derived from QwtPlot class.
*/
class BathtubPlotWidget : public QwtPlot
{
Q_OBJECT

public:
    explicit BathtubPlotWidget(  const QwtText &title,
                                 const int      width,
                                 const int      height,
                                 const int      channel,
                                 QWidget       *parent  );
    ~BathtubPlotWidget();

    void showData( const double  *data,
                   const size_t   dataSize );

    void clear();

signals:
public slots:

private:

    QwtPlotCurve *channelPlot = NULL;

    // Data series values:
    std::vector<double> xs;
    std::vector<double> ys;

    // Plot Elements:
    QFont *titleFont;
    QPen plotPen;
    QBrush plotBrush;
};

#endif // BATHTUBPLOTWIDGET_H
