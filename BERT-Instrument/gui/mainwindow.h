#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QRadioButton>
#include <QCheckBox>
#include <QLabel>
#include <QComboBox>
#include <QMutex>

#include <math.h>

#include "BERPlotWidget.h"
#include "EyePlotWidget.h"
#include "BathtubPlotWidget.h"
#include "ui_mainwindow.h"

#include "../BertComms.h"
#include "../BertInterface.h"
#include "../BertLMX2592Interface.h"
#include "../BertLMX2594Interface.h"
#include "../BertCommands.h"
#include "../BertEyeMonitor.h"
#include "../BertFile.h"

#define DECL_WIDGET_ARRAY4(WIDGET) WIDGET[5]
#define INST_WIDGET_ARRAY4(WIDGET) WIDGET[0]=NULL;  \
                                   WIDGET[1]=ui->WIDGET ## _1; \
                                   WIDGET[2]=ui->WIDGET ## _2; \
                                   WIDGET[3]=ui->WIDGET ## _3; \
                                   WIDGET[4]=ui->WIDGET ## _4

namespace Ui { class MainWindow; }

class BertWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit BertWindow(QWidget *parent = 0);
    ~BertWindow();

    QString getAppPath();

    void enablePageChanges();

    void updateStatus(QString text);
    void appendStatus(QString text);

private slots:
    void on_buttonConnect_clicked();

    void on_checkChannelOn_1_clicked(bool checked) { muteClick(1, checked); }
    void on_checkChannelOn_2_clicked(bool checked) { muteClick(2, checked); }
    void on_checkChannelOn_3_clicked(bool checked) { muteClick(3, checked); }
    void on_checkChannelOn_4_clicked(bool checked) { muteClick(4, checked); }

    void on_comboAmplitude_1_currentIndexChanged(int index) { amplitudeChanged(1, index); }
    void on_comboAmplitude_2_currentIndexChanged(int index) { amplitudeChanged(2, index); }
    void on_comboAmplitude_3_currentIndexChanged(int index) { amplitudeChanged(3, index); }
    void on_comboAmplitude_4_currentIndexChanged(int index) { amplitudeChanged(4, index); }

    void on_comboPattern_1_currentIndexChanged(int index) { prbsOptionsChanged(index); }
    void on_comboPattern_2_currentIndexChanged(int index) { prbsOptionsChanged(index); }
    void on_comboPattern_3_currentIndexChanged(int index) { prbsOptionsChanged(index); }
    void on_comboPattern_4_currentIndexChanged(int index) { prbsOptionsChanged(index); }

    void on_checkInverted_1_clicked(bool checked) { invertedClick(1, checked); }
    void on_checkInverted_2_clicked(bool checked) { invertedClick(2, checked); }
    void on_checkInverted_3_clicked(bool checked) { invertedClick(3, checked); }
    void on_checkInverted_4_clicked(bool checked) { invertedClick(4, checked); }

    void on_comboDeemphLevel_1_currentIndexChanged(int index) { deemphChanged(1, index, 255); }
    void on_comboDeemphLevel_2_currentIndexChanged(int index) { deemphChanged(2, index, 255); }
    void on_comboDeemphLevel_3_currentIndexChanged(int index) { deemphChanged(3, index, 255); }
    void on_comboDeemphLevel_4_currentIndexChanged(int index) { deemphChanged(4, index, 255); }

    void on_comboDeemphPrecSel_1_currentIndexChanged(int index) { deemphChanged(1, 255, index); }
    void on_comboDeemphPrecSel_2_currentIndexChanged(int index) { deemphChanged(2, 255, index); }
    void on_comboDeemphPrecSel_3_currentIndexChanged(int index) { deemphChanged(3, 255, index); }
    void on_comboDeemphPrecSel_4_currentIndexChanged(int index) { deemphChanged(4, 255, index); }

    void on_comboCrossPoint_1_currentIndexChanged(int index) { crosspointChanged(1, index); }
    void on_comboCrossPoint_2_currentIndexChanged(int index) { crosspointChanged(2, index); }
    void on_comboCrossPoint_3_currentIndexChanged(int index) { crosspointChanged(3, index); }
    void on_comboCrossPoint_4_currentIndexChanged(int index) { crosspointChanged(4, index); }

    // void on_checkForceCDRBypass_clicked(bool checked);

    void on_buttonResync_clicked();

    // DEBUG ONLY void on_buttonCommsCheck_clicked();

    void on_tabWidget_currentChanged(int index);
    void on_tabWidget_tabBarClicked(int index);

    void uiUpdateTimerTick();

    void on_buttonEDStart_clicked();
    void on_buttonEDStop_clicked();

    void on_comboEDPattern_1_currentIndexChanged(int index) { edOptionsChanged[1] = TRUE; Q_UNUSED(index); }
    void on_comboEDPattern_2_currentIndexChanged(int index) { edOptionsChanged[2] = TRUE; Q_UNUSED(index); }
    void on_comboEDPattern_3_currentIndexChanged(int index) { edOptionsChanged[3] = TRUE; Q_UNUSED(index); }
    void on_comboEDPattern_4_currentIndexChanged(int index) { edOptionsChanged[4] = TRUE; Q_UNUSED(index); }

    void on_checkEDPatternInvert_1_clicked(bool checked) { edOptionsChanged[1] = TRUE; Q_UNUSED(checked); }
    void on_checkEDPatternInvert_2_clicked(bool checked) { edOptionsChanged[2] = TRUE; Q_UNUSED(checked); }
    void on_checkEDPatternInvert_3_clicked(bool checked) { edOptionsChanged[3] = TRUE; Q_UNUSED(checked); }
    void on_checkEDPatternInvert_4_clicked(bool checked) { edOptionsChanged[4] = TRUE; Q_UNUSED(checked); }

    void on_checkEDEnable_1_clicked() { edChannelEnableChanged(1); }
    void on_checkEDEnable_2_clicked() { edChannelEnableChanged(2); }
    void on_checkEDEnable_3_clicked() { edChannelEnableChanged(3); }
    void on_checkEDEnable_4_clicked() { edChannelEnableChanged(4); }

    void on_comboEQBoost_1_currentIndexChanged(int index) { eqBoostSet(1, index); }
    void on_comboEQBoost_2_currentIndexChanged(int index) { eqBoostSet(2, index); }
    void on_comboEQBoost_3_currentIndexChanged(int index) { eqBoostSet(3, index); }
    void on_comboEQBoost_4_currentIndexChanged(int index) { eqBoostSet(4, index); }

    void on_buttonEDInjectError_1_clicked() { errorInject(1); }
    void on_buttonEDInjectError_2_clicked() { errorInject(2); }
    void on_buttonEDInjectError_3_clicked() { errorInject(3); }
    void on_buttonEDInjectError_4_clicked() { errorInject(4); }

    void on_buttonEyeScanStart_clicked();
    void on_buttonEyeScanStop_clicked();
    void on_buttonBathtubStart_clicked();
    void on_buttonBathtubStop_clicked();

    void eyeScan_ProgressUpdate(int progressPercent);

    void on_comboInternalFreq_currentIndexChanged(int index);

    void on_comboTriggerOutDivideRatio_currentIndexChanged(int index);

    void on_comboInternalClkOutPower_currentIndexChanged(int index);

    void on_comboInternalTrigOutPower_currentIndexChanged(int index);

private:
    static Ui::MainWindow *ui;

    BertComms            *bertComms = NULL;       // USB-I2C Adaptor - Low-level comms
    BertInterface        *bertInterface = NULL;   // Higher level, BERT-specific comms
    BertLMXxxxxInterface *clockInterface = NULL;  // Higher level interface for TI clock IC
    BertCommands         *bert = NULL;            // BERT commands - e.g. setPRBSOptions
    BertEyeMonitor       *eyeMonitor = NULL;      // BERT eye monitor commands and functions

    static QMutex uiMutex;

    // Flags for UI elements which might change while the ED is running.
    // Note: We set flags and do all the work inside the timer update because
    // otherwise the UI can deadlock on the Bert command interface mutex.
    bool flagEDStart = FALSE;
    bool flagEDStop = FALSE;
    bool flagEDErrorInject = FALSE;
    bool flagEDEQChange = FALSE;

    uint8_t edErrorInjectChannel = 1;
    uint8_t eqBoostChannel = 1;
    uint8_t eqBoostNewIndex = 0;

    QTimer *uiUpdateTimer = NULL;  // Fires every 250 mS

    int tickCountStatusTextReset;
    int tickCountTempUpdate;

    QTime *edRunTime = NULL;

    BERPlotWidget     *berPlot[5] = { NULL, NULL, NULL, NULL, NULL };
    EyePlotWidget     *eyePlot[5] = { NULL, NULL, NULL, NULL, NULL };
    BathtubPlotWidget *tubPlot[5] = { NULL, NULL, NULL, NULL, NULL };

    int  eyeScanRepeatsTotal = 1;
    int  eyeScanRepeatsDone  = 0;
    int  eyeScanChannelCount = 0;

    bool connectInProgress = FALSE;
    bool commsConnected;
    bool pageChangesEnabled = FALSE;

    int  edUpdateCounter = 0;
    bool edErrorflasherOn[5] = { FALSE, FALSE };
    bool edOptionsChanged[5] = { FALSE, FALSE };

    bool edPending = FALSE;
    bool edRunning = FALSE;
    bool eyeScanRunning = FALSE;
    bool bathtubRunning = FALSE;

    double *eyeScanData[5] = { NULL, NULL, NULL, NULL, NULL };  // Will be used for both eye plot and bathtub plot
    size_t eyeScanDataSize[5] = { 0, 0, 0, 0, 0 };              // Number of bytes in eyeScanData

    int currentTabIndex = 0;
    int currentAnalysisTabIndex = 0;

    typedef struct berMeasurement
      {
      double bits = 0;                // Number of bits so far (total)
      double errors = 0;              // Number of errors so far (total)
      int lastMeasureTimeMs = 0;      // "elapsed()" value (milliseconds) of edRunTime for the last time the error count
                                      // was read on THIS channel. Used to calculate the elapsed time and hence the estimated
                                      // bit count. Note that the "elapsed()" reading of edRunTime wraps every 24 hours,
                                      // so we need to check whether it's gone backwards when calculating actual elapsed time.
      } berMeasurement;

    double bitRate = 0;

    berMeasurement ber[5]; // Bit and error counts for error detectors.

    static const int PG_OUTPUT_SWING_LOOKUP[];
    static const int PG_OUTPUT_SWING_LOOKUP_SIZE;

    static const char *PATTERN_NAMES[];

    static const uint8_t PG_CROSS_POINT_LOOKUP[];
    static const int PG_CROSS_POINT_LOOKUP_SIZE;

    static const float PG_VCO_FREQ_LOOKUP[];
    static const int PG_VCO_FREQ_LOOKUP_SIZE;

    static const uint8_t PG_EQ_BOOST_LOOKUP[];
    static const int PG_EQ_BOOST_LOOKUP_SIZE;

    static const int PG_TRIG_OUT_DIVIDE_LOOKUP[];
    static const int PG_TRIG_OUT_DIVIDE_LOOKUP_SIZE;

    static const int PG_BATHTUB_VOFF_LOOKUP[];
    static const int PG_BATHTUB_VOFF_LOOKUP_SIZE;

    static const int PG_EYESCAN_REPEATS_LOOKUP[];
    static const int PG_EYESCAN_REPEATS_LOOKUP_SIZE;

    void refreshUI();
    void closeEvent(QCloseEvent *event);

    void muteReflect();
    void muteClick(const uint8_t channel, const bool checked);

    void amplitudeReflect();
    void amplitudeChanged(const uint8_t channel, const uint8_t index);

    void updateBitRate();

    void internalClocksourceReflect();

    void prbsOptionsReflect();
    void prbsOptionsChanged(const uint8_t index);

    void invertedReflect();
    void invertedClick(const uint8_t channel, const bool checked);

    void deemphReflect();
    void deemphChanged(const uint8_t channel, const uint8_t indexLevel, const uint8_t indexCursor);

    void crosspointReflect();
    void crosspointChanged(const uint8_t channel, const uint8_t index);

    // DEPRECATED void forceCDRBypassReflect();

    void edControlInit();
    void edStartStopReflect();
    void edResetCountersAndUI(const int8_t channel);
    void edChannelEnableControls(const uint8_t channel, const bool enabled);
    void edChannelEnableChanged(const uint8_t channel);
    void edSetUpAndStart();
    void edOptionsReflect();

    void eqBoostSet(const uint8_t channel, const uint8_t eqBoostIndex);
    void eqBoostReflect();
    void errorInject(const uint8_t channel);

    void setRed(QLabel *item, QString text);
    void setGreen(QLabel *item, QString text);
    void setGrey(QLabel *item, QString text);
    void setWhite(QLabel *item, QString text);

    int currentIndexSafe(QComboBox *combo);

    // Convert between OUTPUT lane (as recognised by the BERT) and
    // channel (numbers on the front of the panel)
    uint8_t channelToLane(const uint8_t channel) { return (channel - 1) * 2; }
    uint8_t laneToChannel(const uint8_t lane) { return (lane / 2) + 1; }
    // Convert between OUTPUT channel (numbers on the front of the panel) and board ID
    uint8_t channelToBoard(const uint8_t channel) { return (channel - 1) / 2; }

    // Convert between INPUT lane (as recognised by the BERT) and
    // channel (numbers on the front of the panel)
    uint8_t channelToInputLane(const uint8_t channel) { return (channel * 2) - 1; }
    uint8_t laneToInputChannel(const uint8_t lane) { return (lane + 1) / 2; }


    // Convert a displayed ED channel (1 - 4) to an index into
    // the array of LOS and LOL bits:
    uint8_t channelToLOSLOLIndex(const uint8_t channel) { return (channel * 2) - 1; }

    // Convert a displayed ED channel (1 - 4) to an ED Lane number (0 - 3)
    uint8_t channelToEDLane(const uint8_t channel) { return channel - 1; }

    // Plot types: Constants to indicate which type
    // of plot we are currently generating.
    static const int PLOT_EYESCAN = 1;
    static const int PLOT_BATHTUB = 2;

    int eyeScanPlotType = PLOT_EYESCAN;

    void initAnalysisPlots();
    void eyeScanUIUpdate(bool isRunning);
    bool eyeScanStart(const uint8_t channel, const bool reset);

    void bathtubUIUpdate(bool isRunning);
    bool bathtubScanStart(const uint8_t channel, const bool reset);

    void eyeScanDataReset(const uint8_t channel);

    void eyeScanFinished();

    // DECLARE Lists of multi-channel widgets, set up here so that they can be updated
    // individually by index. Note use of 5 entries with the first set to NULL,
    // so that the index used to access each one corresponds to the displayed CHANNEL.
    // Nb: Each must also be defined with INST_WIDGET_ARRAY4 some time after setupUi() is called.
    QLabel      *DECL_WIDGET_ARRAY4(valueBERBits);
    QLabel      *DECL_WIDGET_ARRAY4(valueBERErrors);
    QLabel      *DECL_WIDGET_ARRAY4(valueBER);
    QLabel      *DECL_WIDGET_ARRAY4(labelErrorFlasher);
    QLabel      *DECL_WIDGET_ARRAY4(labelEQBoost);
    QLabel      *DECL_WIDGET_ARRAY4(valueDataLock);
    QLabel      *DECL_WIDGET_ARRAY4(valueCDRLock);

    QCheckBox   *DECL_WIDGET_ARRAY4(checkEDEnable);
    QCheckBox   *DECL_WIDGET_ARRAY4(checkEDPatternInvert);
    QCheckBox   *DECL_WIDGET_ARRAY4(checkEyeScanChannel);
    QCheckBox   *DECL_WIDGET_ARRAY4(checkBathtubChannel);

    QComboBox   *DECL_WIDGET_ARRAY4(comboAmplitude);
    QComboBox   *DECL_WIDGET_ARRAY4(comboEDPattern);
    QComboBox   *DECL_WIDGET_ARRAY4(comboEQBoost);
    QComboBox   *DECL_WIDGET_ARRAY4(comboDeemphLevel);
    QComboBox   *DECL_WIDGET_ARRAY4(comboDeemphPrecSel);
    QComboBox   *DECL_WIDGET_ARRAY4(comboCrossPoint);

    QGroupBox   *DECL_WIDGET_ARRAY4(groupEDBERChannel);

    QPushButton *DECL_WIDGET_ARRAY4(buttonEDInjectError);

    QFrame      *DECL_WIDGET_ARRAY4(frameBERPlot);
    QFrame      *DECL_WIDGET_ARRAY4(framePlotEyeScan);
    QFrame      *DECL_WIDGET_ARRAY4(framePlotBathtub);

};

#endif // MAINWINDOW_H
