/*!
 \file   BERPlotWidget.cpp
 \brief  Bit Error Ratio Plot
 \author J Cole-Baker (For Smartest)
 \date   Jan 2016
*/


#include "BERPlotWidget.h"

const double BERPlotWidget::LOG_PLOT_FLOOR = 1e-16;


/*!
 \brief BERPlotWidget Constructor
 Create a new Bit Error Ratio Plot widget
 \param title      Title of plot - displayed at top
 \param width      Plot width - pixels
 \param height     Plot height - pixels
 \param seconds    Max number of seconds to display on plot.
                   The plot will start at second 1 (left),
                   and move across as data points are added.
                   When the specified number of seconds is
                   reached, the plot will start to scroll
                   left as points are added.
 \param logScales  Use log scales for y axes (boolean)
 \param channel    Sets plot appearance - color, thickness, etc
                   Used to make several plots visually distinct;
                   channel must be 1 to 4.
 \param parent   UI Widget which will contain the plot
*/
BERPlotWidget::BERPlotWidget(  const QwtText &title,
                               const int      width,
                               const int      height,
                               const int      seconds,
                               const int      logScale,
                               const int      channel,
                               QWidget       *parent  )
       : QwtPlot(title, parent),
         bufferSeconds((size_t)seconds),
         logScale(logScale)
  {
      int useChannel = channel;
      if (useChannel < 1) useChannel = 1;
      if (useChannel > 4) useChannel = 4;

      QColor colours[] = { Qt::magenta, Qt::magenta, Qt::blue, Qt::red, Qt::green };
      titleFont = new QFont( "MS Shell Dlg 2", 10, 4, false);
      titleLabel()->setFont(*titleFont);
      setGeometry(4,4,width+4,height+4);

      plotPen.setColor( colours[useChannel] );
      plotPen.setWidth( 2 );

      plotBrush.setColor( colours[useChannel] );
      plotBrush.setStyle( Qt::Dense6Pattern );

      setAxisScale( QwtPlot::xBottom, 1.0, (float)seconds );

      double baseLine;
      if (logScale)
      {
          scaleEngineLog = new QwtLogScaleEngine();
          setAxisScaleEngine( QwtPlot::yLeft, scaleEngineLog );
          setAxisScale( QwtPlot::yLeft, 1e-16, 1.0 );
          baseLine = LOG_PLOT_FLOOR;
      }
      else
      {
          baseLine = 0.0;
      }
      plotScaleDraw = new BERPlotYScaleDraw();
      setAxisScaleDraw(QwtPlot::yLeft, plotScaleDraw);

      // Generate X axis values:
      double x = 1.0;
      for (size_t i = 0; i < bufferSeconds; i++)
      {
          xs.push_back(x);
          x += 1.0;
      }

      channelPlot = new QwtPlotCurve( QString("BER") );
      channelPlot->setBaseline(baseLine);
      channelPlot->setPen( plotPen );
      channelPlot->setBrush( plotBrush );
      channelPlot->attach( this );
      channelPlot->setAxes( QwtPlot::xBottom, QwtPlot::yLeft);
      enableAxis(QwtPlot::xBottom, false);
      this->setAutoDelete(false);
      replot();
  }


BERPlotWidget::~BERPlotWidget()
{}




/*!
 \brief Add a data point to the BER Plot
 \param value   Instantaneous Error Ratio
*/
void BERPlotWidget::addPoint( const double value )
{
    // Add a new data point to the Y series:
    double valueUse;
    valueUse = value;
    if (logScale)
    {
        // If log scales used, can't show "0.0". Clip at LOG_PLOT_FLOOR:
        if (valueUse < LOG_PLOT_FLOOR) valueUse = LOG_PLOT_FLOOR;
    }
    ys.push_back( valueUse );
    size_t nSeconds = ys.size();
    if (nSeconds >= bufferSeconds) ys.erase( ys.begin() );
    // Update the plot data series:
    channelPlot->setSamples( &xs[0],
                             &ys[0],
                              ys.size() );
    // Redraw:
    replot();
}


/*!
 \brief Clear the plot data
*/
void BERPlotWidget::clear()
{
    ys.clear();
    replot();
}





