/*!
 \file   EyePlotWidget.cpp
 \brief  Eye Scan Plot - Implementation
 \author J Cole-Baker (For Smartest)
 \date   Feb 2016
*/

#include "EyePlotWidget.h"

#include <QPainter>
#include <QRectF>
#include <QList>
#include <QPixmap>
#include <QLabel>

#include <qwt_scale_map.h>
#include <qwt_plot_layout.h>
#include <qwt_scale_widget.h>

/*!
 \brief EyePlotWidget Constructor
 Create a new eye scan plot widget
 \param title    Title of plot - displayed at top
 \param width    Plot width - pixels
 \param height   Plot height - pixels
 \param parent   UI Widget which will contain the plot
*/
EyePlotWidget::EyePlotWidget( const QwtText &title,
                              const int      width,
                              const int      height,
                              QWidget       *parent )
    : QwtPlot(title, parent)
{
    titleLabel()->setFont(QFont( "Arial", 8));
    setGeometry(4,4,width+4,height+4);
    plotLayout()->setAlignCanvasToScales(true);

    colorMap = new QwtLinearColorMap(Qt::black, Qt::darkRed);
    colorMap->addColorStop(0.05, Qt::darkBlue);
    colorMap->addColorStop(0.10, Qt::cyan);
    colorMap->addColorStop(0.50, Qt::green);
    colorMap->addColorStop(0.80, Qt::yellow);
    colorMap->addColorStop(0.90, Qt::red);

    titleFont = QFont( "MS Shell Dlg 2", 8, 4, false);
    titleLabel()->setFont(titleFont);

    eyeScanPlot = new QwtPlotSpectrogram( QString("Eye Scanish") );
    eyeScanPlot->setColorMap(colorMap);
    /* FOR CONTOURS:
    contourLevels.clear();
    double countourInterval = (eyeScanData->range().maxValue() - eyeScanData->range().minValue()) / 10.0;
    for (double level = eyeScanData->range().minValue();
                level < eyeScanData->range().maxValue();
                level += countourInterval) contourLevels.append(level);
    eyeScanPlot->setContourLevels(contourLevels);
    eyeScanPlot->pixelHint( QRectF(-0.01, -0.01, 0.01, 0.01));
    eyeScanPlot->setDefaultContourPen(QPen("white"));
    eyeScanPlot->setDisplayMode( QwtPlotSpectrogram::ContourMode, true );
    eyeScanPlot->setDisplayMode( QwtPlotSpectrogram::ImageMode, true );
    */

    eyeScanPlot->attach( this );

    // qDebug() << "Plot Data Range: " << eyeScanData->range().minValue() << " - " << eyeScanData->range().maxValue();

    /////// Axis Titles and Fonts ////////////////////////////////
    mvScaleDiv.setLowerBound(-250.0);
    mvScaleDiv.setUpperBound( 250.0);
    mvScaleDiv.setTicks(QwtScaleDiv::MajorTick, mvMajTicks);
    mvScaleDiv.setTicks(QwtScaleDiv::MinorTick, mvMinTicks);
    setAxisScaleDiv(QwtPlot::yLeft, mvScaleDiv);
    setAxisFont(QwtPlot::yLeft,QFont("Arial", 8));
    QwtText yAxisTitle("Slice Level (mV)");
    yAxisTitle.setFont(QFont("Arial", 8));
    setAxisTitle(QwtPlot::yLeft, yAxisTitle);

    QwtText xAxisTitle("Phase Offset (UI)");
    xAxisTitle.setFont(QFont("Arial", 8));
    setAxisTitle(QwtPlot::xBottom, xAxisTitle);

    // A color bar on the right axis
    QwtScaleWidget *rightAxis = axisWidget(QwtPlot::yRight);
    rightAxis->setFont(QFont("Arial", 8));
    rightAxis->setColorBarEnabled(true);
    rightAxis->setColorMap( QwtInterval(0.0,1.0), colorMap);
    setAxisScale(QwtPlot::yRight, -1.0, 0.0 );
    QwtText colourBarTitle("Log BER");
    setAxisTitle(QwtPlot::yRight, colourBarTitle);
    enableAxis(QwtPlot::yRight);

    replot();


}

EyePlotWidget::~EyePlotWidget()
{
    eyeScanPlot->detach();
}



/*!
 \brief Update the Eye Plot widget to show new data
 \param data  Pointer to an array of doubles containing
              the data. Data are copied so array may be
              deleted after call returns.
              Array represents an x by y matrix of data
              points; it must contain (xRes * yRes) elements

 \param xRes  Number of elements in 'x' axis of data
 \param yRes  Number of elements in 'y' axis of data
*/
void EyePlotWidget::showData( const double  *data,
                              const int      xRes,
                              const int      yRes )
{
    eyeScanData = new EyeScanData( data,
                                   xRes, yRes,
                                   QwtInterval(   -0.6, 0.6   ),
                                   QwtInterval( -250.0, 250.0 )  );

    QwtScaleWidget *rightAxis = axisWidget(QwtPlot::yRight);
    rightAxis->setColorMap( eyeScanData->range(), colorMap);
    setAxisScale(QwtPlot::yRight, eyeScanData->range().minValue(), eyeScanData->range().maxValue() );

    eyeScanPlot->setData(eyeScanData);
    replot();
}


/*!
 \brief Eye Plot clear
 Clear any existing data from the plot
*/
void EyePlotWidget::clear()
{
    eyeScanPlot->setData(NULL);
    replot();
}

