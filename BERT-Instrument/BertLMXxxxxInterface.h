/*!
 \file   BertLMXxxxxInterface.h
 \brief  Texas Instruments LMX Clock Synth Hardware Interface Header
         This class provides a common interface for the LMX2592 and LMX2594.
         Some functionality is part-specific and provided by derived clases.
 \author J Cole-Baker (For Smartest)
 \date   Apr 2018
*/


#ifndef BERTLMXXXXXINTERFACE_H
#define BERTLMXXXXXINTERFACE_H

#include <QMutex>
#include <QList>

#include <stdint.h>

#include "globals.h"
#include "BertComms.h"

class BertLMXFrequencyProfile;


/*!
 \brief LMX Clock Synth Interface - Base Class for all versions of the clock

 This class implements the interface to the Texas Instruments
 LMX clock generator chip at a functional level (Supported: LMX2592 and LMX2594).

 This class uses BertComms to actually connect to the board
 via whatever low level connection is used (e.g. USB to I2S
 adaptor).

*/
class BertLMXxxxxInterface
{

  public:
    BertLMXxxxxInterface(BertComms *useBertComms, const uint8_t useI2CSlaveAddress, const uint8_t maxRegisterAddress);
    virtual ~BertLMXxxxxInterface();

    uint8_t maxRegisterAddress = 255;

    // **** Comms Interface Methods: *******************
    int  interfaceIsOpen() const { return (bertComms->portIsOpen() && spiAdaptorIsOpen); }

    // DEPRECATED int  interfaceLock(int timeout);
    // DEPRECATED void interfaceFree();

    // **** LMX Clock Synth Methods: ******************
    int init();                                                                     // Initialise the part
    static int getPartFromRegisterFiles(const QString searchPath, QString &partNo); // Find the clock part number from the register def files
    int initFrequencyProfiles(const QString searchPath, const QString partNo);      // Read a list of frequency profile files (contain register values)

    QStringList getFrequencyList() const { return frequencyList; }             // Return a reference to the list of available frequencies (use to populate UI)
    QStringList getOutputPowerList() const { return OUTPUT_POWER_LIST; }       // Return a reference to the list of available ouput power levels (use to populate UI)
    QStringList getTriggerDivideList() const { return TRIGGER_DIVIDE_LIST; }   // Return a reference to the list of available trigger out divide otions (use to populate UI)

    int getFrequency(const size_t index, float *frequency) const;    // Find the frequency (MHz) of the frequency profile at the specificed index
    int selectFrequency(const float frequency, size_t *index);       // Switch to the specified frequency, or one lower if exact match is not found in profiles
    int selectProfile(const size_t index);                           // Switch to the frequency profile specified by index

    int selectFOutPower(const size_t index);                         // Select an output power level for MAIN output
    int selectTrigOutPower(const size_t index);                      // Select an output power level for TRIGGER (divided) output
    int selectTriggerDivide(const size_t index, const bool doFCal);  // Select a trigger out divide ratio

    size_t getSelectedProfileIndex()         const { return selectedProfileIndex; }           // Return index of current selection (for UI update)
    size_t getSelectedTrigOutputPowerIndex() const { return selectedTrigOutputPowerIndex; }   //  "
    size_t getSelectedFOutOutputPowerIndex() const { return selectedFOutOutputPowerIndex; }   //  "
    size_t getSelectedTrigDivideIndex()      const { return selectedTrigDivideIndex; }        //  "

    int outputsOn();    // Enable outputs
    int outputsOff();   // Disable / mute outputs

    int runFCal();      // Run frequency calibration (?? Should this even be public?? Rqd after changing F or long run time with Temperature drift. CHECK USAGE IN UI)
    int resetDevice();  // Reset the part to default settings


  protected:

    // One device register: Address + 16 bits of data:
    typedef struct registerData_t
    {
        uint8_t address;
        uint16_t value;
    } registerData_t;

    // *** Constants for Trigger Divide Ratio and Output Power: ******
    static const QStringList TRIGGER_DIVIDE_LIST;
    static const size_t DEFAULT_DIVIDE_RATIO_INDEX;

    static const uint8_t POWER_M5DBM;
    static const uint8_t POWER_0DBM;
    static const uint8_t POWER_5DBM;
    static const uint8_t POWER_CONSTS[];
    static const size_t POWER_CONSTS_SIZE;
    static const QStringList OUTPUT_POWER_LIST;
    static const size_t DEFAULT_FOUT_POWER_INDEX;
    static const size_t DEFAULT_TRIG_POWER_INDEX;
    // ****************************************************************

    BertComms *bertComms;

    uint8_t    i2cSlaveAddress;
    int        spiAdaptorIsOpen = false;

    uint16_t   profileIndexDefault = 0;    // Index of default start-up frequency profile

    /****** Clock Gen State: **********************/
    uint16_t   selectedProfileIndex = 0;
    uint16_t   selectedTrigOutputPowerIndex = 0;
    uint16_t   selectedFOutOutputPowerIndex = 0;
    uint16_t   selectedTrigDivideIndex = 0;
    bool       flagOutputsOn = false;

    // DEPRECATED QMutex     interfaceMutex;

    QList<BertLMXFrequencyProfile> frequencyProfiles;
    QStringList frequencyList;

    static const int BUSY_TIMEOUT = 0;  // milliseconds: time to wait if comms are busy

    int initAdaptor();

    // Part-specific Methods: Implemented by derived classes
    virtual uint16_t getDefaultR0() = 0;  // Return the part-specific default value of register 0.
    virtual uint16_t getFCalEnR0() = 0;   // Return the part-specific value for FCalEn bit of register 0.

    virtual int initPart() = 0;  // Part-specific Initialisation
    virtual int configureOutputs() = 0;  // Output driver setup
    virtual int selectProfilePart(const size_t index) = 0;  // Select a frequency profile
    virtual int selectTriggerDividePart(const size_t index, const bool doFCal) = 0;  // Trigger output divide ratio setup
    virtual void setSafeDefaults() = 0;  // Set safe default settings
    virtual int resetPart(uint16_t defaultR0) = 0;  // Part-specific reset functions: Implemented by derived versions


    // Register writing: Shared by all parts

    int writeRegister(const uint8_t regAddress, const uint16_t regValue);

    uint16_t setRegisterBits(const uint16_t registerInputValue,
                             const uint8_t  nBits,
                             const uint8_t  shift,
                             const uint16_t value);

    int writeRegisterBits(const uint8_t  address,
                          const uint8_t  nBits,
                          const uint8_t  shift,
                          const uint16_t value,
                          const uint16_t registerDefault);

  private:

};






/*!
  \brief LMX Frequency Profile Class
  Stores the register values for a frequency setting
*/
class BertLMXFrequencyProfile
{
public:
    BertLMXFrequencyProfile(const QString partNo, QStringList &registerDefs, const uint8_t maxRegsterAddress);
    ~BertLMXFrequencyProfile();

    bool isValid()       const {  return valid;      }
    float getFrequency() const {  return frequency;  }

    int setRegisterValue(const uint8_t address, const int16_t value);
    int16_t getRegisterValue(const uint8_t address, bool *registerFound) const;

private:

    /*!
    \brief Struct to represent ONE LMX register
    'address' is register address
    'value' is register value (unsigned 16 bit)
    */
    struct LMXRegister
    {
        uint8_t address;
        int16_t value;
    };

    uint8_t maxAddress = 255;
    bool valid = false;
    float frequency = 0.0;
    QList<LMXRegister> registers;

};





#endif // BERTLMXXXXXINTERFACE_H
