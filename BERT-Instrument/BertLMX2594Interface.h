/*!
 \file   BertLMX2594Interface.h
 \brief  LMX2594 Clock Hardware Interface Header - Derived from BertLMXxxxxInterface
 \author J Cole-Baker (For Smartest)
 \date   Apr 2018
*/


#ifndef BERTLMX2594INTERFACE_H
#define BERTLMX2594INTERFACE_H

#include <QMutex>
#include <QList>

#include <stdint.h>

#include "globals.h"
#include "BertComms.h"
#include "BertLMXxxxxInterface.h"


/*!
 \brief LMX2594 Interface Class

 This class implements functions specific to the Texas Instruments LMX2594
 clock generator chip. It derives from the generic BertLMXxxxxInterface
 class which covers both LMX2592 and LMX2594

*/
class BertLMX2594Interface : public BertLMXxxxxInterface
{

  public:

    BertLMX2594Interface(BertComms *useBertComms, const uint8_t useSlaveAddress);
    ~BertLMX2594Interface();

  protected:

    // ****** CONSTANTS: *****************************************************
    // Initial register values:
    static const registerData_t INIT_REGISTERS[];
    static const size_t INIT_REGISTERS_SIZE;

    // Bit patterns for Register 0:
    static const uint16_t R0_NOTHING;
    static const uint16_t R0_DEFAULT;

    static const uint16_t R0_RAMP_EN;
    static const uint16_t R0_VCO_PHASE_SYNC_EN;
    static const uint16_t R0_OUT_MUTE;
    static const uint16_t R0_FCAL_EN;
    static const uint16_t R0_MUXOUT_LD_SEL;
    static const uint16_t R0_RESET;
    static const uint16_t R0_POWERDOWN;
    static const uint16_t R0_FCAL_XXXX_ADJ_MASK;

    // On the LMX2594, R1 and R7 are also used as part of basic configuration
    // (CAL_CLK_DIV field and OUT_FORCE bit):
    static const uint16_t R1_DEFAULT;
    static const uint16_t R7_DEFAULT;

    static const uint16_t R44_DEFAULT;
    static const uint16_t R45_DEFAULT;
    static const uint16_t R45_OUTA_VCO;
    static const uint16_t R46_DEFAULT;

    // Register 31 and 75 values used to select divide ratio:
    static const uint16_t R31_VALUES[];
    static const uint16_t R75_VALUES[];
    static const size_t R31R75_VALUES_SIZE;

    //  **********************************************************************


    uint16_t getDefaultR0() { return R0_DEFAULT; }                       // Get default value for R0
    uint16_t getFCalEnR0()  { return R0_FCAL_EN; }                       // Get R0 bit to enable FCAL
    int initPart();                                                      // Part Initialisation
    int configureOutputs();                                              // Output driver setup
    int selectProfilePart(const size_t index);                           // Select a frequency profile
    int selectTriggerDividePart(const size_t index, const bool doFCal);  // Trigger output divide ratio setup
    void setSafeDefaults();                                              // Set safe default settings
    int resetPart(uint16_t defaultR0);                                   // Part-specific reset function: Implemented by derived versions

  private:
    const uint32_t LMX2594_RESET_SLEEP = 600;        // Number of milliseconds to sleep during RESET (while RESET bit HIGH).
    const uint32_t LMX2594_RESET_POST_SLEEP = 400;   // Number of milliseconds to sleep AFTER RESET, before loading other registers.
};

#endif // BERTLMX2594INTERFACE_H
