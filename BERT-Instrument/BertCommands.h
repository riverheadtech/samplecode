/*!
 \file   BertCommands.h
 \brief  BERT Functional Commands Class Header
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/

#ifndef BERTCOMMANDS_H
#define BERTCOMMANDS_H

#include <stdint.h>

#include "globals.h"
#include "BertInterface.h"
#include "../BertComms.h"

class BertWindow;

/*!
 \brief BERT Functional Commands Class

 This class implements BERT functions such as enabling channels
 or setting up the PRBS checker

*/
class BertCommands
{
  public:
    BertCommands(BertWindow *useParentWindow, BertInterface *useBertInterface);
    virtual ~BertCommands();

    // Options for Error Detector Setup
    typedef struct edOptions_t
    {
        uint8_t invert;      // Invert pattern checker: 0 = Normal; 1 = Inverted
        uint8_t pattern;     // Pattern Select: 0=PRBS9; 1=PRBS15; 2=PRBS31
        uint8_t laneSelect;  // Select input lane for checker: 0 = Lane 0 (ED 0) / 2 (ED 1) ; 1 = Lane 1 (ED 0) / 3 (ED 1)
        uint8_t enable;      // Enable checker (1 = Enable)
    } edOptions_t;

    int  connect(int *coldStart);
    void disconnect();

    int  commsCheck();

    void macroCheck(int *resultMaster, int *resultSlave);

    /* DEPRECATED
    bool commandsLock(int timeout);
    void commandsFree();
    */

    // *** GT1724 Register Addresses: ***
    static const uint8_t GTREG_CDR_REG_0      = 0x00;
    static const uint8_t GTREG_OFF_COR_REG_2  = 0x08;
    static const uint8_t GTREG_EQ_REG_0       = 0x09;
    static const uint8_t GTREG_EQ_REG_2       = 0x0B;
    static const uint8_t GTREG_DRV_REG_0      = 0x32;
    static const uint8_t GTREG_DRV_REG_2      = 0x34;
    static const uint8_t GTREG_DRV_REG_5      = 0x37;
    static const uint8_t GTREG_PD_REG_1       = 0x3D;
    static const uint8_t GTREG_PD_REG_6       = 0x42;

    static const uint16_t GTREG_LOS_REG_0           = 0x0404;  // = 1028d (16 bit address)
    static const uint16_t GTREG_LOSL_OUTPUT         = 0x0411;  // = 1041d (16 bit address)
    static const uint16_t GTREG_LOSL_OUTPUT_LATCHED = 0x0412;  // = 1042d (16 bit address)

    static const double EXT_CLOCK_FREQ_MIN;
    static const double EXT_CLOCK_FREQ_MAX;

    // NOTE: Lanes used by GT1724:
    //
    // Each GT1724 IC (master and slave) has 4 lanes (0-3).
    // For ALL calls which require a 'lane' parameter below, the CALLER
    // supplies a lane number from 0 to 7, where:
    //  0 - 3 = MASTER board lanes 0 - 3
    //  4 - 7 = SLAVE board lanes 0 - 3
    //
    // Each chip also has 2 x PRBS checkers (also referred to as "ED"s),
    // each of which is associated with TWO lanes (lanes 0/1 and lanes 2/3).
    // We number the ED "lanes" 0 to 3:
    //  0 = Master Board, PRBS Checker 0/1
    //  1 = Master Board, PRBS Checker 2/3
    //  2 = Slave Board, PRBS Checker 0/1
    //  3 = Slave Board, PRBS Checker 2/3
    //

    // *** BERT Commands: ***********

    // Set up the PG with a new pattern, and enable.
    // Also use to retrain PG after frequency change.
    // NOTE: This method DOESN'T set any of the PG settings visible on the
    // "Pattern Generator" page, e.g. Channel On, Amplititude, Inverted, De-Emhpasis
    // or Cross Point. These should be set seperately when they change, and also at
    // startup to ensure UI consistency.
    int configPG(const uint8_t board, const uint8_t pattern, const int forceCDRBypass);

    // Set Pattern Generator setting to default values (e.g. PRBS31, outputs ON).
    // NOTE: Resets some settings that are controlled from the "Pattern Generator" page,
    // e.g. output swing. The UI should read back the pattern generator settings after
    // calling this method, to ensure UI consistency. This method is designed to
    // be called at connect or "Resync" to set the PG to a known state.
    int configSetDefaults();

    QString getMacroVersionString() const { return macroVersion[0]->macroVersionString; }

    bool hasSlaveBoard() const { return (maxBoard > 0); }

    int     setLaneMute        (const uint8_t lane, const int muteOn, const int powerDownOnMute);
    uint8_t getLaneMute        (const uint8_t lane);

    int     setAutoBypassOnLOL (const uint8_t lane, const int autoBypassOn);
    uint8_t getAutoBypassOnLOL (const uint8_t lane);

    int     setForceCDRBypass  (const uint8_t lane, const int forceBypassOn);
    uint8_t getForceCDRBypass  (const uint8_t lane);

    int     setOutputSwing              (const uint8_t lane, const int swing);
    int     getOutputSwing              (const uint8_t lane);
    int     configOutputDriverMainSwing (const uint8_t board, const int swings[4]);
    int     queryOutputDriverMainSwing  (const uint8_t board, int swings[4]);

    int     setPRBSOptions (const uint8_t board, const uint8_t pattern, const uint8_t vcoFreq, const uint8_t source, const uint8_t enable );
    int     getPRBSOptions (const uint8_t board, uint8_t *pattern, uint8_t *vcoFreq, uint8_t *source, uint8_t *enable);

    int     setLaneInverted (const uint8_t lane, const uint8_t inverted);
    uint8_t getLaneInverted (const uint8_t lane);

    int setPDDeEmphasis (const uint8_t lane, const uint8_t powerDown);
    int getPDDeEmphasis (const uint8_t lane, uint8_t *powerDown);

    int setPDLaneOutput (const uint8_t lane, const uint8_t powerDown);
    int getPDLaneOutput (const uint8_t lane, uint8_t *powerDown);

    int setDeEmphasis   (const uint8_t lane, const uint8_t level, const uint8_t prePost);
    int getDeEmphasis   (const uint8_t lane, uint8_t *level, uint8_t *prePost);

    int setPDLaneMainPath (const uint8_t lane, const uint8_t powerDown);
    int getPDLaneMainPath (const uint8_t lane, uint8_t *powerDown);

    int setCrossPoint   (const uint8_t lane, const uint8_t crossPoint);
    int getCrossPoint   (const uint8_t lane, uint8_t *crossPoint);

    int setEQBoost      (const uint8_t lane, const uint8_t eqBoost);
    int getEQBoost      (const uint8_t lane, uint8_t *eqBoost);

    /* DEPRECATED Not modified for multiple boards:
    int configDeviceLaneMode (const uint8_t lane, const uint8_t mode, const uint8_t freqDivider);
    int queryDeviceLaneMode  (uint8_t *lane, uint8_t *mode, uint8_t *freqDivider);
    */

    int setEDOptions(const uint8_t board, const edOptions_t *edOptions[2]);
    int getEDOptions(const uint8_t board, edOptions_t *edOptions[2]);

    int controlED(const uint8_t board, const uint8_t enableED0, const uint8_t enableED1);

    int getEDCount(const uint8_t edLane, double *bits, double *errors);

    int setLosEnable(const uint8_t board, const uint8_t state);
    int getLosLol(uint8_t los[8], uint8_t lol[8]);

    int getTemperature(const uint8_t board, int *temperatureDegrees);

    int debugPowerStatus(const uint8_t lane);

  protected:
  private:
    BertWindow *parentWindow;      // Reference to main UI so that we can update the 'status' text
    BertInterface *bertInterface;  // Ref to an BertInterface used to communicate with hardware

    const globals::MacroFileInfo *macroVersion[2] = { &(globals::MACRO_FILES[0]), &(globals::MACRO_FILES[0]) };
       // Pointer to information about the macro version for each board (initially unknown)

    uint8_t maxBoard = 0;

    // *** Private methods to drive the BERT, load macros, etc: ******
    int     downloadHexFile(const bool includeSlave);
    uint8_t hexCharToInt(uint8_t byte);
    bool    hexCharsToInt(uint8_t charHi, uint8_t charLo, uint8_t *result );
    double  edBytesToDouble( const uint8_t bytes[2] );

    // Convert an input ED lane (spanning board, i.e. 0-3) into a separate board ID and lane ID (both 0-1)
    void edLaneToBoardAndLane(const uint8_t edLaneIn, uint8_t *board, uint8_t *edLane) { *board = edLaneIn / 2; *edLane = edLaneIn % 2; }

    int  commsCheckOneRegister(const uint8_t board, const uint8_t lane, uint8_t value, int &countGood, int &countError);
};

#endif // BERTCOMMANDS_H
