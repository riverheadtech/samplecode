/*!
 \file   BertInterface.cpp
 \brief  BERT Hardware Interface Class Implementation
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/

#include <QDebug>
#include <QThread>

#include "globals.h"
#include "BertComms.h"
#include "BertInterface.h"


/*!
 \brief BertInterface Constructor
 \param useBertComms   Reference to comms implementation class (low-level comms, e.g. comms via USB-I2C Adaptor)
 \param useI2cAddress  Pointer to an array of I2C addresses, one for each board. Must be at least ONE;
                       First entry is "Master" board.
 \param useNBoards     Number of addresses in i2cAddresses; this is the number of boards
*/
BertInterface::BertInterface(BertComms *bertComms, const uint8_t *i2cAddress, const size_t nBoards)
{
    qDebug() << "BertInterface Constructor: " << nBoards << "I2C Addresses";
    this->bertComms = bertComms;
    this->nBoards = nBoards;

    // COPY I2C addresses to our address array:
    this->i2cAddress = new uint8_t[nBoards];
    size_t i;
    for (i = 0; i < this->nBoards; i++)
    {
        this->i2cAddress[i] = i2cAddress[i];
        // qDebug() << "--Copied index " << i << ": " << this->i2cAddress[i] << " <- " << i2cAddress[i];
    }

}



BertInterface::~BertInterface()
{
    delete [] i2cAddress;
}




/*****************************************************************************************
   Run Macros
 *****************************************************************************************/


// MACRO to handle error running macro:
// NOTE: No retry for Macro 18 (it normally
// fails if macros haven't been loaded yet;
// This is used to check whether they have
// been loaded already).
#define MACROERROR_RETRY(ECODE) {                      \
        if (code == 0x18)                              \
        {                                              \
            result = ECODE;                            \
            goto macroFinished;                        \
        }                                              \
        globals::sleep(500);                           \
        errorCounter++;                                \
        if (errorCounter >= 4)                         \
        {                                              \
            result = ECODE;                            \
            goto macroFinished;                        \
        }                                              \
        else                                           \
        {                                              \
            qDebug() << "  -->RETRY MACRO...";         \
            bertComms->reset();                        \
            continue;                                  \
        }                                              \
    }


/*!
 \brief Execute a macro

 NB: Possible improvement:
 Currently, we write the ENTIRE* macro input data buffer before
 running the macro, then read back the entire macro output data
 buffer. This may be inefficient. The documentation allows for
 reading / writing to specific bytes in the buffers (by using
 a suitable offset on the address - i.e. the whole buffer doesn't
 have to be updated or read. Could add this at some point.

 *But bear in mind, this only applies if there is any buffer
 to read/write (can specify 0), and also that we already
 read/write n bytes at the start of the buffer, so if only
 the first few bytes are involved, we're already there...

 \param board       Board ID: 0 = Master; 1 = Slave, etc
 \param code        Macro ID code (see board documentation)
 \param dataIn      Pointer to a buffer containing input parameters for macro.
                    If macro doesn't use any input parameters, set to NULL and
                    set dataInSize to 0.
 \param dataInSize  Number of bytes in dataIn, which will be copied to the
                    macro input buffer. Max. 16 bytes; If > 16, function will
                    return globals::OVERFLOW.
 \param dataOut     Pointer to a buffer allocated by the CALLER for output
                    data from the macro. If macro doesn't use any output
                    parameters, set to NULL and set dataOutSize to 0.
 \param dataOutSize Number of bytes in dataOut, which will be read from the
                    macro output buffer. Max. 16 bytes; If > 16, function will
                    return globals::OVERFLOW.
 \param timeoutMs   Timeout in mS. If the macro doesn't complete and return a
                    result (OK or Error) in this time, TIMEOUT is returned.

 \return globals::OK              Success
 \return globals::NOT_CONNECTED   No connection to BERT
 \return globals::OVERFLOW        dataInSize or dataOutSize too big (>16 bytes)
 \return globals::INVALID_BOARD   Tried to write to slave board, but no slave present
 \return globals::BUSY_ERROR      Operation already in progress
 \return globals::GEN_ERROR       Read / Write error
 \return globals::TIMEOUT         Timeout waiting for data read/write or macro completion
 \return globals::MACRO_ERROR     The macro completed, but with an "error" result
 \return [Error code]             Error code from BertComms->read or BertComms->write
*/
int BertInterface::runLongMacro(const uint8_t  board,
                                const uint8_t  code,
                                const uint8_t *dataIn,
                                const uint8_t  dataInSize,
                                uint8_t *dataOut,
                                const uint8_t  dataOutSize,
                                const uint16_t timeoutMs )
{
    if (!bertComms->portIsOpen()) return globals::NOT_CONNECTED;
    if ( (dataInSize > 16) || (dataOutSize > 16) ) return globals::OVERFLOW;
    if (board >= nBoards) return globals::INVALID_BOARD;

    int result = globals::OK;
    int pollCount;

    // DEPRECATED if (bertComms->commsLock(2000) != globals::OK) return globals::BUSY_ERROR;
    //-----------------------------------------------------------------------------------
    int errorCounter = 0;
    while (TRUE)
    {
#ifdef BERT_MACRO_DEBUG
        qDebug() << QString("[Macro B %1 I2C 0x%2] {0x%3}[0x%4][0x%5]")
                    .arg(board)
                    .arg((int)i2cAddress[board],2,16,QChar('0'))
                    .arg((int)code,2,16,QChar('0'))
                    .arg((int)( (dataInSize>0) ? dataIn[0] : 0 ),2,16,QChar('0'))
                    .arg((int)( (dataInSize>1) ? dataIn[1] : 0 ),2,16,QChar('0'));
#endif
        // ***** If input data specified, write to the macro input buffer: **********
        // if (dataInSize > 0) qDebug() << QString("  Write Input Data...");
        if (dataInSize > 0)  result = bertComms->write(i2cAddress[board], 0x0C00, dataIn, dataInSize);
        if (result != globals::OK) goto macroFinished;  // I2C command error
        // if (dataInSize > 0) qDebug() << QString("  -->OK");

        // ***** Write the Macro code to the Macro register (this starts macro): *******
        // qDebug() << QString("  Write Macro Code: {%1}").arg((int)(code),2,16,QChar('0') );
        result = bertComms->write(i2cAddress[board], 0x0C10, &code, 1);
        if (result != globals::OK) goto macroFinished;  // I2C command error
        // qDebug() << QString("  -->OK");

        // ***** Poll the macro code register and wait for it to change to 0x00 or 0x01: ******
        // qDebug() << QString("  Polling Code/Status Register for result code...");
        uint8_t macroResult;
        pollCount = timeoutMs / 100;  // Note: Polling every 100 mS (see below).
        while (pollCount > 0)
        {
            globals::sleep(100);
            // Read back the Macro code (indicates macro status):
            result = bertComms->read(i2cAddress[board], 0x0C10, &macroResult, 1);
            // qDebug() << "  -->Poll Read Result: " << result;
            if (result != globals::OK) goto macroFinished;  // I2C command error
            // qDebug() << "  -->Status: " << QString("{%1} (00=OK, 01=Error, xx=Still Running)").arg((int)macroResult,2,16,QChar('0') );
            if ( (macroResult == 0x00) ||
                 (macroResult == 0x01) ) break;  // Macro finished.
            pollCount--;
        }

#ifdef BERT_MACRO_DEBUG
        // ------ DEBUG --------
        if (pollCount == 0)      qDebug() << "  -->Macro TIMEOUT!";
        if (macroResult == 0x01) qDebug() << "  -->Macro ERROR!";
        // ---------------------
#endif

        if (pollCount == 0)
        {
            //result = globals::TIMEOUT;      // Timeout! Macro didn't finish, or returned error.
            //goto macroFinished;
            MACROERROR_RETRY(globals::TIMEOUT)
        }
        if (macroResult == 0x01)
        {
            //result = globals::MACRO_ERROR;  // Macro error
            //goto macroFinished;
            MACROERROR_RETRY(globals::MACRO_ERROR)
        }
        // qDebug() << "  -->Macro Finished OK";

        // ***** If output data requested, read from the macro output buffer: ********
        if (dataOutSize > 0)
        {
            // qDebug() << "  Reading back macro output...";
            result = bertComms->read(i2cAddress[board], 0x0C11, dataOut, dataOutSize);
            // qDebug() << "  -->Read Result:" << result << " (0 = Success)";
        }
        break;
    }
macroFinished:
    errorCounter = 0;
    //-----------------------------------------------------------------------------------
    // DEPRECATED bertComms->commsFree();
    // DEPRECATED commsMutex.unlock();
    return result;
}


/*!
 \brief Execute a macro with 0.5 second timeout
 This method executes a macro with a default timeout of 0.5 seconds.
 For parameters and return code, see runLongMacro
*/
int BertInterface::runMacro( const uint8_t  board,
                             const uint8_t  code,
                             const uint8_t *dataIn,
                             const uint8_t  dataInSize,
                             uint8_t *dataOut,
                             const uint8_t  dataOutSize )
{
    return runLongMacro( board, code, dataIn, dataInSize, dataOut, dataOutSize, 800 );
}


/*!
 \brief Execute a simple macro
 Runs a macro which uses only 1 byte of data and doesn't return any data.
 See runMacro for more info.
*/
int BertInterface::runSimpleMacro( const uint8_t board, const uint8_t code, const uint8_t dataIn )
  {
  return runMacro( board, code,  &dataIn, 1, NULL, 0 );
  }


/*!
 \brief Execute a very simple macro
 Runs a macro which uses no data and doesn't return any data.
 See runMacro for more info.
*/
int BertInterface::runVerySimpleMacro( const uint8_t board, const uint8_t code )
  {
  return runMacro( board, code, NULL, 0, NULL, 0 );
  }






/*****************************************************************************************
   Get / Set Registers
 *****************************************************************************************/

/*!
 \brief Set a register
 \param board   Board ID: 0 = Master; 1 = Slave, etc
 \param lane    Lane select - 0 to 3 to select a lane,
                ALL_LANES = 'broadcast' (set register for all lanes on specified board).
 \param address Register address (low byte)
 \param data    Data to write to the register
 \return globals::OK              Success
 \return globals::BUSY_ERROR      Comms already in use
 \return globals::INVALID_BOARD   Tried to write to slave board, but no slave present
 \return globals::NOT_CONNECTED   No connection to BERT
 \return globals::GEN_ERROR       Read / Write error
*/
int BertInterface::setRegister( const uint8_t board, const uint8_t lane, const uint8_t address, const uint8_t data )
{
    if (board >= nBoards) return globals::INVALID_BOARD;
    // DEPRECATED if (!commsMutex.tryLock(BUSY_TIMEOUT)) return globals::BUSY_ERROR;
    int result;
    result = bertComms->write(i2cAddress[board], ((uint16_t)lane << 8) + (uint16_t)address, &data, 1);
    #ifdef BERT_REGISTER_DEBUG
        qDebug() << "[Register WRITE " << board << "]: Lane: " << (int)lane << " Addr: " << (int)address << " Data: " << (int)data << " Result: " << result;
    #endif
    // DEPRECATED commsMutex.unlock();
    return result;
}



/*!
 \brief Get the value of a register
 \param board   Board ID: 0 = Master; 1 = Slave, etc
 \param lane    Lane select - 0 to 3
                Used as the upper byte of the register address ('page').
 \param address Register address (low byte)
 \param data    Pointer to a uint8_t which will be set to the register
                value on success.
 \return globals::OK              Success
 \return globals::BUSY_ERROR      Comms already in use
 \return globals::INVALID_BOARD   Tried to write to slave board, but no slave present
 \return globals::NOT_CONNECTED   No connection to BERT
 \return globals::GEN_ERROR       Read / Write error
 \return globals::TIMEOUT         Timeout waiting for data read
*/
int BertInterface::getRegister( const uint8_t board, const uint8_t lane, const uint8_t address, uint8_t *data )
{
    if (board >= nBoards) return globals::INVALID_BOARD;
    // DEPRECATED if (!commsMutex.tryLock(BUSY_TIMEOUT)) return globals::BUSY_ERROR;
    int result = bertComms->read(i2cAddress[board], ((uint16_t)lane << 8) + (uint16_t)address, data, 1);
    // DEPRECATED commsMutex.unlock();
#ifdef BERT_REGISTER_DEBUG
    qDebug() << "[Register READ " << board << "]: Lane: " << (int)lane << " Addr: " << (int)address << " Data: " << (int)*data << " Result: " << result;
#endif
    return result;
}



/*!
 \brief Set a register (16 bit address)
 \param board   Board ID: 0 = Master; 1 = Slave, etc
 \param address Register address (16 bit)
 \param data    Data to write to the register
 \return globals::OK              Success
 \return globals::BUSY_ERROR      Comms already in use
 \return globals::INVALID_BOARD   Tried to write to slave board, but no slave present
 \return globals::NOT_CONNECTED   No connection to BERT
 \return globals::GEN_ERROR       Read / Write error
*/
int BertInterface::setRegister16( const uint8_t board, const uint16_t address, const uint8_t data )
{
    if (board >= nBoards) return globals::INVALID_BOARD;
    // DEPRECATED if (!commsMutex.tryLock(BUSY_TIMEOUT)) return globals::BUSY_ERROR;
    int result = bertComms->write(i2cAddress[board], address, &data, 1);
    // DEPRECATED commsMutex.unlock();
#ifdef BERT_REGISTER_DEBUG
    qDebug() << "[Register WRITE " << board << "]: Addr (16 bit): " << (int)address << " Data: " << (int)data << " Result: " << result;
#endif
    return result;
}


/*!
 \brief Get the value of a register (16 bit address)
 \param board   Board ID: 0 = Master; 1 = Slave, etc
 \param address Register address (16 bit)
 \param data    Pointer to a uint8_t which will be set to the register
                value on success.
 \return globals::OK              Success
 \return globals::BUSY_ERROR      Comms already in use
 \return globals::INVALID_BOARD   Tried to write to slave board, but no slave present
 \return globals::NOT_CONNECTED   No connection to BERT
 \return globals::GEN_ERROR          Read / Write error
 \return globals::TIMEOUT         Timeout waiting for data read
*/
int BertInterface::getRegister16( const uint8_t board, const uint16_t address, uint8_t *data )
{
    if (board >= nBoards) return globals::INVALID_BOARD;
    // DEPRECATED if (!commsMutex.tryLock(BUSY_TIMEOUT)) return globals::BUSY_ERROR;
    int result = bertComms->read(i2cAddress[board], address, data, 1);
    // DEPRECATED commsMutex.unlock();
#ifdef BERT_REGISTER_DEBUG
    qDebug() << "[Register READ " << board << "]: Addr (16 bit): " << (int)address << " Data: " << (int)*data << " Result: " << result;
#endif
    return result;
}





/********************************************************************
  Raw memory write to board
  Used to send macro update data
 ********************************************************************/

/*!
 \brief Write raw data to device memory, using a 24 bit address

 Wrapper for comms class method.

 \param board     Board ID: 0 = Master; 1 = Slave, etc
 \param address   16 bit address of memory to write to
 \param data      Pointer to an array of uint8_t containing data to write (may be only one byte)
 \param nBytes    Number of bytes to write from data.
                  Nb: nBytes is not limited in size - any write size
                  restrictions will be managed in comms class.

 \return globals::OK            Data written
 \return globals::INVALID_BOARD Tried to write to slave board, but no slave present
 \return [Error Code]           See BertComms->write24
*/
int BertInterface::rawWrite24( const uint8_t board,
                               const uint8_t addressHi,
                               const uint8_t addressMid,
                               const uint8_t addressLow,
                               const uint8_t *data,
                               const uint8_t nBytes )
{
    if (board >= nBoards) return globals::INVALID_BOARD;
    // DEPRECATED if (!commsMutex.tryLock(BUSY_TIMEOUT)) return globals::BUSY_ERROR;

    uint32_t address = ((uint32_t)addressHi << 16) |
                       ((uint32_t)addressMid << 8) |
                       ((uint32_t)addressLow);

    int result = bertComms->write24( i2cAddress[board],
                                     address,
                                     data,
                                     nBytes);
    // DEPRECATED commsMutex.unlock();
    return result;
}





/*!
 \brief Read raw data from device memory, using a 24 bit address

  Wrapper for comms class method.

 \param board       Board ID: 0 = Master; 1 = Slave, etc
 \param addressHi   Upper byte of address (MSB)
 \param addressMid  Middle byte of address
 \param addressLow  Lower bite of address (LSB)

 \param data      Pointer to an array of uint8_t to store read data
 \param nBytes    Number of bytes to read. Data array must be large enough.
                  Nb: nBytes is not limited in size - any read size
                  restrictions will be managed in comms class.

 \return globals::OK            Data read
 \return globals::INVALID_BOARD Tried to write to slave board, but no slave present
 \return [Error Code]           See BertComms->read24
*/
int BertInterface::rawRead24( const uint8_t board,
                              const uint8_t addressHi,
                              const uint8_t addressMid,
                              const uint8_t addressLow,
                              uint8_t *data,
                              const size_t nBytes )
{
    if (board >= nBoards) return globals::INVALID_BOARD;
    // DEPRECATED if (!commsMutex.tryLock(BUSY_TIMEOUT)) return globals::BUSY_ERROR;

    uint32_t address = ((uint32_t)addressHi << 16) |
                       ((uint32_t)addressMid << 8) |
                       ((uint32_t)addressLow);

    int result = bertComms->read24( i2cAddress[board],
                                    address,
                                    data,
                                    nBytes );
    // DEPRECATED commsMutex.unlock();
    return result;
}


