/*!
 \file   BertLMX2592Interface.cpp
 \brief  LMX2592 Clock Hardware Interface Class Implementation - Derived from BertLMXxxxxInterface
 \author J Cole-Baker (For Smartest)
 \date   Jul 2016; Modified Apr 2018
*/

#include <QDebug>

#include "globals.h"
#include "BertComms.h"
#include "BertFile.h"

#include "BertLMX2592Interface.h"

/* Initial Register Values:
 * These values are loaded into the registers during initialisation,
 * and provide default values prior to setting any frequency and
 * turning on the output.
 */
const BertLMXxxxxInterface::registerData_t BertLMX2592Interface::INIT_REGISTERS[] =
{
 /* Reg N / Data  */
    {  0, 0x2214 },  // NB: LD_EN = 1; MUXOUT_SEL = 1
    {  1, 0x0808 },
    {  4, 0x1943 },
    {  7, 0x28B2 },
    {  8, 0x1084 },
    {  9, 0x0302 },
    { 10, 0x10D8 },
    { 11, 0x0018 },
    { 12, 0x7001 },
    { 13, 0x4000 },
    { 14, 0x018C },
    { 19, 0x0965 },
    { 20, 0x0064 },
    { 22, 0x2300 },
    { 23, 0x8842 },
    { 24, 0x0509 },
    { 28, 0x2924 },
    { 29, 0x0084 },
    { 30, 0x0034 },
    { 31, 0x0601 },
    { 32, 0x4210 },
    { 33, 0x4210 },
    { 34, 0xC3F0 },
    { 35, 0x119F },
    { 36, 0x0048 },
    { 37, 0x4000 },
    { 38, 0x0046 },
    { 39, 0x8204 },
    { 40, 0x0000 },
    { 41, 0xC350 },
    { 42, 0x0000 },
    { 43, 0x0000 },
    { 44, 0x0000 },
    { 45, 0x61A8 },
    { 46, 0x00D0 },
    { 47, 0x00CE },
    { 48, 0x03FD },
    { 59, 0x0000 },
    { 61, 0x0001 },
    { 64, 0x03AF }
};
const size_t BertLMX2592Interface::INIT_REGISTERS_SIZE =
         sizeof(INIT_REGISTERS) / sizeof(INIT_REGISTERS[0]);

// Bit Masks for setting Register 0:
const uint16_t BertLMX2592Interface::R0_NOTHING   = 0b0000001000010000;  // No bits set other than X bits
const uint16_t BertLMX2592Interface::R0_LD_EN     = 0b0010000000000000;  // LD_EN = Enable
const uint16_t BertLMX2592Interface::R0_FCAL_EN   = 0b0000000000001000;  // FCAL_EN = Enable
const uint16_t BertLMX2592Interface::R0_MUXOUT_LD = 0b0000000000000100;  // MUXOUT_SEL = LD (not set = Readback mode)
const uint16_t BertLMX2592Interface::R0_RESET     = 0b0000000000000010;  // RESET bit set
const uint16_t BertLMX2592Interface::R0_POWERDOWN = 0b0000000000000001;  // POWER DOWN bit set

const uint16_t BertLMX2592Interface::R0_DEFAULT = BertLMX2592Interface::R0_NOTHING
                                                | BertLMX2592Interface::R0_LD_EN
                                                | BertLMX2592Interface::R0_MUXOUT_LD;

// Settings for R35 and R36 for various trigger out divide ratios:
const uint16_t BertLMX2592Interface::R35_VALUES[] =
{
    0x001B,  // 1/1 (TODO: This will be the same as 1/2 at the moment...)
    0x001B,  // 1/2
    0x029B,  // 1/4
#ifdef BERT_DEBUG_DIV_RATIOS
    0x039B,  // 1/8
    0x039B,  // 1/16
    0x039B   // 1/32
    ,0x119B   // 1/128
    ,0x119F   // 1/192
#endif
};
const uint16_t BertLMX2592Interface::R36_VALUES[] =
{
    0x0410,  // 1/1 (TODO: This will be the same as 1/2 at the moment...)
    0x0410,  // 1/2
    0x0420,  // 1/4
#ifdef BERT_DEBUG_DIV_RATIOS
    0x0441,  // 1/8
    0x0442,  // 1/16
    0x0448   // 1/32
    ,0x0448  // 1/128
    ,0x0448  // 1/192
#endif
};
const size_t BertLMX2592Interface::R35R36_VALUES_SIZE =
         sizeof(R35_VALUES) / sizeof(R35_VALUES[0]);

BertLMX2592Interface::BertLMX2592Interface(BertComms *useBertComms, const uint8_t useSlaveAddress) : BertLMXxxxxInterface(useBertComms, useSlaveAddress, 64) { }

BertLMX2592Interface::~BertLMX2592Interface() { }



/*****************************************************************************************
   LMX2592 Functions
 *****************************************************************************************/

/*!
 \brief Select Frequency Profile by Index - part specfic implementation
 \param index               Index of profile to get frequency from - 0 is first
 \return globals::OK        Item found for requested index.
 \return globals::OVERFLOW  Index was larger than the list of frequency profiles
 \return [Error Code]       Error from writeRegister
*/
int BertLMX2592Interface::selectProfilePart(const size_t index)
{
    if (index >= (size_t)frequencyProfiles.count()) return globals::OVERFLOW;
    qDebug() << "LMX2592: Select frequency profile " << index
             << ": " << frequencyProfiles.at(index).getFrequency() << " MHz";

    uint8_t registerAddress;
    int16_t registerValue;
    bool registerFound;
    int result;

    // Power down output drivers:
    outputsOff();

    resetDevice();

    // Load registers: Don't load R0 (power control)
    for (registerAddress = 1; registerAddress < 65; registerAddress++)
    {
        registerValue = frequencyProfiles.at(index).getRegisterValue(registerAddress, &registerFound);
        if (registerFound)
        {
            qDebug() << "LMX2592: Write " << QString().sprintf("0x%04X", registerValue)
                     << " to register " << registerAddress;
            result = writeRegister(registerAddress, registerValue);
            if (result != globals::OK)
            {
                qDebug() << "LMX2592: ERROR writing register!";
                return globals::WRITE_ERROR;
            }
        }
    }
    selectedProfileIndex = index;

    // Restore the previous power and divider settings
    selectTriggerDivide(selectedTrigDivideIndex, false);

    selectFOutPower(selectedFOutOutputPowerIndex);
    selectTrigOutPower(selectedTrigOutputPowerIndex);

    // Turn outputs back on:
    outputsOn();

    // Calibrate: Required after changing PLL settings
    runFCal();

    qDebug() << "LMX2592: Registers set for profile!";
    return globals::OK;
}






/*!
 \brief Select Trigger Divide Ratio - Part specific
 \param index              Index of new divide ratio (see getTriggerDivideList)
 \return globals::OK       Ratio set OK
 \return globals::OVERFLOW Invalid index
 \return [Error Code]      Error code from writeRegister
*/
int BertLMX2592Interface::selectTriggerDividePart(const size_t index, const bool doFCal)
{
    if (index >= R35R36_VALUES_SIZE) return globals::OVERFLOW;
    qDebug() << "LMX2592: Select trigger divide index " << index;
    qDebug() << "  R34 = " << QString().sprintf("0x%04X", 0xC3F0)
             << "; R35 = " << QString().sprintf("0x%04X", R35_VALUES[index])
             << "; R36 = " << QString().sprintf("0x%04X",  R36_VALUES[index]);

    int                        result = writeRegister(34, 0xC3F0);
    if (result == globals::OK) result = writeRegister(35, R35_VALUES[index]);
    if (result == globals::OK) result = writeRegister(36, R36_VALUES[index]);
    if (result == globals::OK && doFCal) runFCal();

    if (result != globals::OK)
    {
        qDebug() << "LMX2592: Error setting divide ratio: " << result;
    }
    else
    {
        selectedTrigDivideIndex = index;
    }
    return result;
}


/*!
 \brief Reset device: Part-specific implementation
 \param defaultR0  Value to use for R0. This should have any required bits set
                   EXCEPT the RESET bit which must be 0. The method will modify
                   the RESET bit and use other bits from defaultR0.
 \return [code] Result code from writeRegister
*/
int BertLMX2592Interface::resetPart(uint16_t defaultR0)
{
    int result = writeRegister(0, defaultR0 | R0_RESET);  // Reset bit = High. Nb: Self-clears on the LMX2592.
    globals::sleep(250);
    return result;
}





/*****************************************************************************************/
/* PRIVATE Methods                                                                       */
/*****************************************************************************************/


/*!
 \brief Initialise LMX2592 Chip

 This method resets the LMX2592, then loads initial register
 settings to place the device in a known state (prior to
 setting the frequency and enabling the output).
 Startup procedure is taken from LMX2592 documentation,
 Section 7.5.1

 This method assumes that initAdaptor() has already been
 called successfully (i.e. the I2C->SPI interface is ready).

 \return globals::OK   Comms online; Ack from SC18IS602B
 \return globals::NOT_CONNECTED   No connection to BERT
 \return [Error code]             ???
*/
int BertLMX2592Interface::initPart()
{
    int result;
    qDebug() << "Set up LMX2592 Clock Synth IC";

    // Step 1: Device RESET:
    qDebug() << " -LMX2592 RESET...";
    result = writeRegister(0, R0_DEFAULT | R0_RESET);  // Reset bit = High
    if (result != globals::OK) goto finished;

    globals::sleep(500);
    result = writeRegister(0, R0_DEFAULT);
    if (result != globals::OK) goto finished;
    qDebug() << " -OK.";

    // Step 2: Load default register values:

    // If frequency profiles were set up OK, load 5 GHz:
    result = selectProfile(profileIndexDefault);
    if (result == globals::OK) result = outputsOn();

    if (result != globals::OK)
    {
        // Error... try loading failsafe default register settings:
        size_t initDataIndex;
        qDebug() << " -Set LMX2592 Registers to init values...";
        for (initDataIndex = 1; initDataIndex < INIT_REGISTERS_SIZE; initDataIndex++)
        {
            qDebug() << "  -Set R" << INIT_REGISTERS[initDataIndex].address;
            result = writeRegister( INIT_REGISTERS[initDataIndex].address,
                                    INIT_REGISTERS[initDataIndex].value );
            if (result != globals::OK) break;
        }
    }

  finished:
    if (result == globals::OK) qDebug() << "LMX2592 Ready.";
    else                       qDebug() << "Error: Comms problem?";
    return result;
}


int BertLMX2592Interface::configureOutputs()
{
    uint16_t R46, R47;

    // We need to use MASH_EN and MASH_ORDER fields from
    // profile for Reg 46:
    bool bFound = false;
    if (selectedProfileIndex < frequencyProfiles.count())
    {
        R46 = frequencyProfiles.at(selectedProfileIndex).getRegisterValue(46, &bFound);
    }
    if (!bFound) R46 = 0x0010;  // Default to use if no profile selected.

    qDebug() << "Trig Pow Index: " << selectedTrigOutputPowerIndex << " = " << POWER_CONSTS[selectedTrigOutputPowerIndex];

    R46 = (R46 & 0xC03F) |  // Stored reg value; OUTA_POW, OUTA_PD and OUTB_PD bits set to 0
          (POWER_CONSTS[selectedTrigOutputPowerIndex] << 8) | // OUTA_POW
          (((flagOutputsOn) ? 0x00 : 0x03) << 6);             // OUTA_PD and OUTB_PD


    R47 = (0xC0 | POWER_CONSTS[selectedFOutOutputPowerIndex]);  // OUTB_POW

    qDebug() << "Set R46 to " << R46 << "; R47 to " << R47;
    int result = writeRegister(46, R46);
    if (result != globals::OK) return result;
    result = writeRegister(47, R47);
    qDebug() << "OK.";
    return result;
}


/*!
 \brief Set safe default values for frequency profiles
 This method overrides some register values in each
 profile, to create safe default settings regardless
 of what settings were loaded from the frequency profile
 file.
 The following settings are changed:
  - Power down output drivers (application should turn
    them once all settings have been configured)
  - Set output levels to 0 dBm
  - Set CHDIV ratio to 32
  - Set up output MUX to select CHDIV for output A and
    VCO for output B.
 \return globals::OK
 \return [error code]
*/
void BertLMX2592Interface::setSafeDefaults()
{
    QList<BertLMXFrequencyProfile>::iterator i;
    for (i = frequencyProfiles.begin(); i != frequencyProfiles.end(); ++i)
    {
        qDebug() << "Set SAFE defaults for " << i->getFrequency();

        // Register 0 set up:
        // * First, extract FCAL_HPFD_ADJ and FCAL_LPFD_ADJ from
        //   value recovered from file: We need to preserve these.
        // * Override some fields:
        //   * Lock Detect enabled;
        //   * MUXOUT pin set to LD;
        //   * FCAL_EN, RESET and POWERDOWN to 0.
        bool bFound;
        uint16_t R0 = i->getRegisterValue(0, &bFound);
        if (!bFound) R0 = R0_DEFAULT; // Safe fallback.

        uint16_t maskedR0 = R0 & 0x01E0;
          // ...Leave only FCAL_HPFD_ADJ and FCAL_LPFD_ADJ

        R0 = maskedR0 | R0_DEFAULT;
          // ...Set LD_EN and MUXOUT_SEL

        i->setRegisterValue(0, R0);

        // Set CHDIV ratio to 32 and enable:
        i->setRegisterValue(34, 0xC3F0);
        i->setRegisterValue(35, 0x039B);
        i->setRegisterValue(36, 0x0448);

        // R46 Setup:
        // * Preserve MASH_EN and MASH_ORDER bits
        // * Set other bits to safe defaults:
        //   OUTA_POW = 0dBm; OUT*_PD = 1
        uint16_t R46 = i->getRegisterValue(46, &bFound);
        if (!bFound) R46 = 0x00D0; // Safe fallback.

        uint16_t maskedR46 = R46 & 0x0027;
          // ...Leave only MASH_EN and MASH_ORDER bits

        R46 = maskedR46 | 0x00D0;
          // ...Set LD_EN and MUXOUT_SEL

        // Set OUTA_POW = 0dBm:
        R46 = setRegisterBits( R46,
                               6, 8, POWER_0DBM );
        i->setRegisterValue(46, R46);

        // OUTA_MUX = CHDIV; OUTB_POW = 0dBm:
        uint16_t R47 = setRegisterBits( 0x00C0,
                                        6, 0, POWER_0DBM );
        i->setRegisterValue(47, R47);

        // OUTB_MUX = VCO:
        i->setRegisterValue(48, 0x03FD);
    }
}



