/*!
 \file   BertComms.cpp
 \brief  BERT Board Comms Class Implementation - QT Version
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/

#include <windows.h>
#include <stdio.h>

#include <QDebug>
#include <QSerialPort>

#include "globals.h"
#include "BertComms_I2C.h"


/*
 Nb: For comms via DEVANTECH USB-I2C INTERFACE ADAPTER, see:

 http://www.robot-electronics.co.uk/htm/usb_i2c_tech.htm

 I2C Module Commands:

 I2C_SGL   0x53   Read/Write single byte for non-registered devices,
                  such as the Philips PCF8574 I/O chip (All)
 I2C_MUL   0x54   Read multiple bytes without setting new address (eeprom's,
                  Honeywell pressure sensors, etc) (V5 and higher)
 I2C_AD1   0x55   Read/Write single or multiple bytes for 1 byte addressed devices
                  (the majority of devices will use this one) (All)
 I2C_AD2   0x56   Read/Write single or multiple bytes for 2 byte addressed devices,
                  eeproms from 32kbit (4kx8) and up.  (V6 and higher)
 I2C_USB   0x5A   A range of commands to the USB-I2C module, generally to improve
                  selected communications or provide analogue/digital I/O  (All)

*/

BertComms::BertComms()
  {
  slaveAddress = 0;
  isOpen = FALSE;    // Signifies that the port is closed.
  }



BertComms::~BertComms()
  {
  close();
  }



/*!
 \brief Open comms

 \param serialPort   WCHAR array containing the name of the serial port to use

 \return globals::OK         Success. Comms open.

 \return globals::GEN_ERROR  Error. Comms not open; calls to
                             read and write will be ignored.

***************************************************************/
int BertComms::open(const uint8_t useSlaveAddress, const QString portName)
  {
  slaveAddress = useSlaveAddress;
  serialPort.setPortName(portName);
  // qDebug() << "Serial Port: " << portName << endl;
  bool bResult;
  if (serialPort.open(QIODevice::ReadWrite))
    {
    // qDebug() << "Serial Port Open; Setting options..." << endl;

  /* Options required for USB-I2C adaptor:
     19200 baud, 8 data bits, no parity and two stop bits.  */

    bResult = serialPort.setBaudRate(QSerialPort::Baud19200) &
              serialPort.setStopBits(QSerialPort::TwoStop) &
              serialPort.setDataBits(QSerialPort::Data8) &
              serialPort.setParity(QSerialPort::NoParity) &
              serialPort.setFlowControl(QSerialPort::NoFlowControl);
    qDebug() << "Serial Port Open: " << (bResult ? "OK" : "FAIL") << endl;
    // Serial port should be open:
    isOpen = TRUE;
    return globals::OK;
    }
  else
    {
    qDebug() << "Serial Port Open FAILED: " << serialPort.error() << ": " << serialPort.errorString() << endl;
    // Serial port should be open:
    isOpen = FALSE;
    return globals::GEN_ERROR;
    }
  }









/*!
 \brief Write data to BERT board

  If comms are not opened, the method does nothing.
  This method is also used as part of the 'read' method, to send the 'read' command to the
  board. In that case, no registar data are sent, and nBytes is 0.

 \param address   16 bit address of register or memory to write to
 \param data      Pointer to an array of uint8_t data bytes (may be only one)
 \param nBytes    Number of bytes to write from data.
                  nBytes must be <= 64, otherwise 'OVERFLOW' error will be returned.

 \return globals::OK              Data written
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::WRITE_ERROR     Write error sending command / data to USB-I2C module
 \return globals::WRITE_TIMEOUT       Timeout waiting for bytes to be written out port
 \return globals::WRITE_CONF_TIMEOUT  Timeout waiting for ACK from adaptor
 \return globals::READ_ERROR          Error reading data back result code from USB-I2C module
 \return globals::ADAPTOR_WRITE_ERROR Adaptor returned internal fault code
 \return globals::OVERFLOW            Error (nBytes > 64)

***************************************************************/
int BertComms::write(const uint16_t address, const uint8_t *data, const uint8_t nBytes)
  {
  if (!isOpen)     return globals::NOT_CONNECTED;  // Comms not open!
  if (nBytes > 64) return globals::OVERFLOW;       // Transmission buffer is limited to 64 bytes.

  qDebug() << "--Write to board...";

  // Send the 'I2C_AD2' command (I2C operation with 2 byte register address) for 'write' mode:
  int result = moduleCommand(0, address, nBytes);
  qDebug() << "--Result of module command: " << result;
  if (result != globals::OK) return errorClear(globals::WRITE_ERROR);      // Error sending I2C command.

  // DEBUG:
  moduleDataDump(data, nBytes);

  // Send the register data (Max. 64 bytes):
  quint64 writBytes;
  qDebug() << "--Writing reg data (" << nBytes << " bytes)...";
  writBytes = serialPort.write( (const char *)data, (qint64)nBytes );
  qDebug() << "-- " << writBytes << " bytes writ.";
  if ( !waitForBytesWritten(500) ) return errorClear(globals::WRITE_TIMEOUT);
  if (writBytes < nBytes)          return errorClear(globals::WRITE_ERROR);

  // Read back the write confirm code from the USB adaptor (1 byte):
  qDebug() << "--Reading confirm byte from adaptor...";
  char writeResult = 0;
  qint64 readBytes;
  result = serialPort.waitForReadyRead(750);
  qDebug() << "--Wait Result: " << result;
  if (!result) return errorClear(globals::WRITE_CONF_TIMEOUT);
  readBytes = serialPort.read( &writeResult, 1 );
  qDebug() << "--Bytes read: " << readBytes
           << "; writeResult: " << (int)writeResult
           << "; Error: " << serialPort.error() << "\n"
           << serialPort.errorString();

  if (readBytes < 1)       return errorClear(globals::READ_ERROR);
  if (writeResult == 0)    return errorClear(globals::ADAPTOR_WRITE_ERROR);
  else                     return globals::OK;
  }





/*!
 \brief Write raw data to device memory, using a 24 bit address

 \param addressHi   Upper byte of address (MSB)
 \param addressMid  Middle byte of address
 \param addressLow  Lower bite of address (LSB)

 \param data      Pointer to an array of uint8_t - data to write
 \param nBytes    Number of bytes to write
                  Nb: nBytes is not limited in size - any read size
                  restrictions will be managed internally.
 \return globals::OK              Data written
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::WRITE_ERROR     Write error sending command / data to USB-I2C module
 \return globals::WRITE_TIMEOUT       Timeout waiting for bytes to be written out port
 \return globals::WRITE_CONF_TIMEOUT  Timeout waiting for ACK from adaptor
 \return globals::READ_ERROR          Error reading data back result code from USB-I2C module
 \return globals::ADAPTOR_WRITE_ERROR Adaptor returned internal fault code
 \return globals::OVERFLOW            Error (nBytes > 64)
*/
int BertComms::write24( const uint8_t addressHi,
                        const uint8_t addressMid,
                        const uint8_t addressLow,
                        const uint8_t *data,
                        const size_t nBytes)
{
    // DOESN'T WORK on Devantech Adaptor!!!
    Q_UNUSED(addressHi)
    Q_UNUSED(addressMid)
    Q_UNUSED(addressLow)
    Q_UNUSED(data)
    Q_UNUSED(nBytes)
    return globals::NOT_IMPLEMENTED;
}




/*!
 \brief  Get number of bytes left to write
 Serial port write method usually returns after buffering the
 supplied data, but before writing the data out the port.
 This function may be called to check whether writing of bytes
 has finished.
 NOTE: Event loop must resume during polling of getBytesToWrite,
 otherwise no data will ever be sent!
 \return Number of buffered bytes remaining
*/
uint64_t BertComms::getBytesToWrite()
  {
  if (!isOpen) return 0;
  else         return (uint64_t)serialPort.bytesToWrite();
  }






/*!
 \brief Read data from BERT board

 Reads bytes from the BERT board and stores them to the
 supplied buffer. If the comms are not open, the method
 returns 0 and nothing is written to the buffer.

 \param address   16 bit address of register or memory to read from
 \param data      Pointer to a buffer allocated by the caller.
                  Must be at least nBytes in size.
 \param nBytes    Number of bytes to read from device.
                  nBytes must be <= 64, otherwise 'OVERFLOW' error will be returned.

 \return globals::OK              Success  - nBytes were read back into data buffer
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::WRITE_ERROR     Error writing to USB-I2C module
 \return globals::READ_ERROR      Error reading data back from board via USB-I2C module
 \return globals::OVERFLOW        Error (nBytes > 64)
 \return globals::TIMEOUT         A comms timeout occurred before nBytes were received.
                                  In this case, the content of data is undefined - it may
                                  contain part of the expected data
 \return globals::WRITE_TIMEOUT       Timeout waiting for bytes to be written out port
 \return globals::READ_ERROR          Error reading data back result code from USB-I2C module

*****************************************************************/
int BertComms::read(const uint16_t address, uint8_t *data, const uint8_t nBytes)
  {
  if (!isOpen)     return globals::NOT_CONNECTED;  // Comms not open!
  if (nBytes > 64) return globals::OVERFLOW;       // Transmission buffer is limited to 64 bytes.

  qDebug() << "--Read from board...";

  // Send the 'I2C_AD2' command (I2C operation with 2 byte register address) for 'read' mode:
  int result = moduleCommand(1, address, nBytes);
  if (result != globals::OK) return errorClear(globals::WRITE_ERROR);      // Error sending I2C command.
  result = serialPort.waitForBytesWritten(500);
  if (!result) return errorClear(globals::WRITE_TIMEOUT);    // Timeout waiting for data to write

  // Read back the resulting register data:
  qint64 readBytes;
  qint64 totalRead = 0;
  while (totalRead < nBytes)
    {
    result = serialPort.waitForReadyRead(500);
    if (!result) return errorClear(globals::READ_TIMEOUT);    // Timeout waiting for data to be read
    qDebug() << "--Attempt to read " << (nBytes - totalRead) << " bytes...";
    readBytes = serialPort.read( (char *)data + totalRead, (nBytes - totalRead) );
    qDebug() << "--Bytes read: " << readBytes
             << "; Error: " << serialPort.error() << "\n"
             << serialPort.errorString();

    if (readBytes < 0) return errorClear(globals::READ_ERROR); // Read error
    totalRead += readBytes;
    if (totalRead >= nBytes) break;
    // Not all bytes read yet. Try again:
    }

  return globals::OK; // All data received.
  }



/*!
 \brief Read raw data from device memory, using a 24 bit address

 \param addressHi   Upper byte of address (MSB)
 \param addressMid  Middle byte of address
 \param addressLow  Lower bite of address (LSB)

 \param data      Pointer to an array of uint8_t to store read data
 \param nBytes    Number of bytes to read. Data array must be large enough.
                  Nb: nBytes is not limited in size - any read size
                  restrictions will be managed internally.
 \return globals::OK              Data written
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::WRITE_ERROR     Write error sending command / data to USB-I2C module
 \return globals::WRITE_TIMEOUT       Timeout waiting for bytes to be written out port
 \return globals::WRITE_CONF_TIMEOUT  Timeout waiting for ACK from adaptor
 \return globals::READ_ERROR          Error reading data back result code from USB-I2C module
 \return globals::ADAPTOR_WRITE_ERROR Adaptor returned internal fault code
 \return globals::OVERFLOW            Error (nBytes > 64)
*/
int BertComms::read24( const uint8_t addressHi,
                       const uint8_t addressMid,
                       const uint8_t addressLow,
                       uint8_t *data,
                       const size_t nBytes )
{
    // DOESN'T WORK on Devantech Adaptor!!!
    Q_UNUSED(addressHi)
    Q_UNUSED(addressMid)
    Q_UNUSED(addressLow)
    Q_UNUSED(data)
    Q_UNUSED(nBytes)
    return globals::NOT_IMPLEMENTED;
    /*
    if (!isOpen)     return globals::NOT_CONNECTED;  // Comms not open!

    qDebug() << "--Write 24 bit address to board...";

    // Write the address bytes, MSB first:
    int result = (moduleWriteByte(addressHi)  == globals::OK) &&
                 (moduleWriteByte(addressMid) == globals::OK) &&
                 (moduleWriteByte(addressLow) == globals::OK);
    if (!result)
    {
        qDebug() << "Error writing address for 24 bit read!";
        return globals::WRITE_ERROR;
    }

    // Use multi-byte read to read back the data:
    uint8_t *storePtr = data;
    size_t bytesRemain = nBytes;
    size_t bytesThisRead;
    while (bytesRemain > 0)
    {
        qDebug() << "Read24: " << bytesRemain << " bytes left to read.";
        if (bytesRemain > 64) bytesThisRead = 64;
        else                  bytesThisRead = bytesRemain;
        qDebug() << "Read24: Reading " << bytesThisRead << " bytes.";
        result = moduleReadNBytes( storePtr, bytesThisRead );
        qDebug() << "Read24: Result: " << result;
        if (result != globals::OK) return result; // Read error!
        // Assume that we read bytesThisRead correctly!
        storePtr += bytesThisRead;
        bytesRemain -= bytesThisRead;
    }
    qDebug() << "Read24: Finished OK!";
    return globals::OK;
    */
}





/*!
 \brief Wait for data to be written or timeout
 Blocks waiting for serial port data to be written out the port.
 Doesn't require the event loop (blocks instead)
 \param mSecs Timeout in mSec. -1 to block forever
 \return TRUE  All data written
 \return FALSE Timeout occured
****************************************************/
bool BertComms::waitForBytesWritten(int mSecs)
  {
  return serialPort.waitForBytesWritten(mSecs);
  }



/*!
 \brief Wait for data to be available, or timeout
 Blocks waiting for serial port data to be available.
 Doesn't require the event loop (blocks instead)
 \param mSecs Timeout in mSec. -1 to block forever
 \return TRUE  Data available
 \return FALSE Timeout occured
****************************************************/
bool BertComms::waitForReadyRead(int mSecs)
  {
  return serialPort.waitForReadyRead(mSecs);
  }





/*!
 \brief Flush serial port input buffer
****************************************************/
void BertComms::flushIn()
  {
  if (!isOpen) return;
  qint64 readBytes = 0;
  char tempChar;
  int result;
  while (1)
    {
    result = serialPort.waitForReadyRead(20);
    if (!result) return; // Nothing left to read.
    readBytes = serialPort.read( &tempChar, 1 );
    if (readBytes <= 0) return;   // Read error or nothing to read.
    }
  }



/*!
 \brief Clear Comms Error

 This method flushes the input buffer and stops comms for a delay
 period, to allow error conditions to clear. Hopefully, if the
 board has become confused, it will time out and go back to an
 idle state.

 \param returnCode - An integer return code to return. This
                     makes it convenient to splice the error
                     handler into a Return-On-Error condition,
                     e.g. "if (error) return errorClear( globals::ERROR );"
 \return returnCode
*************************************************************************/
int BertComms::errorClear( const int returnCode )
{
    flushIn();
    sleep( 600 );
    serialPort.clearError();
    return returnCode;
}





/*!
 \brief Close comms

 This method closes the comms if open.
****************************************************/
void BertComms::close()
  {
  if (isOpen)
    {
    serialPort.close();
    isOpen = FALSE;
    }
  }








/*!
 \brief Send a command to the Devantech USB-I2C module

  Commands are used to initiate both reading and writing of data on the
  I2C bus. If comms are not opened, the method does nothing.

 \param isRead     Set to 1 if this is a read operation, or 0 for a write.
                   This value is used to set the lowest bit of the slave address.
 \param address    16 bit address of register to write to
 \param nBytes     Number of bytes to write from data.

 \return globals::OK             Data written
 \return globals::WRITE_ERROR    Error (Error writing data)
 \return globals::NOT_CONNECTED  Error (comms not open)
***************************************************************/
int BertComms::moduleCommand(const uint8_t isRead, const uint16_t address, const uint8_t nBytes)
  {
  if (!isOpen) return globals::NOT_CONNECTED;  // Comms not open!

  // DEBUG: qDebug() << "--moduleCommand: Slave Address: " << (slaveAddress << 1) + isRead;

  uint8_t i2cData[5];
  i2cData[0] = I2C_AD2;                   // USB-I2C adaptor command (WRITE with 2 byte reg. address)
  i2cData[1] = (slaveAddress << 1) + isRead;     // Address of device + R/W bit
  i2cData[2] = address >> 8;              // Register Address - High byte
  i2cData[3] = address & 0x00FF;          // Register Address - Low byte
  i2cData[4] = nBytes;                    // Number of bytes to be read/written to/from registers

  // DEBUG: moduleCommandDump(i2cData);

  quint64 writBytes;
  writBytes = serialPort.write( (const char *)i2cData, 5 );

  if (writBytes < 5) return globals::WRITE_ERROR;
  else               return globals::OK;

  }



/*!
 \brief Write a single byte to the GT1724, WITHOUT using register address
 \param data
 \return globals::OK             Data written
 \return globals::WRITE_ERROR    Error (Error writing data)
 \return globals::NOT_CONNECTED  Error (comms not open)
*******************************************************************************/
int BertComms::moduleWriteByte( const uint8_t data )
  {
    if (!isOpen) return globals::NOT_CONNECTED;  // Comms not open!
    // DEBUG:
    qDebug() << "--moduleRWByte: Slave Address: " << (slaveAddress << 1);

    uint8_t i2cData[3];
    i2cData[0] = I2C_SGL;                // USB-I2C adaptor command (Read / Write one byte)
    i2cData[1] = (slaveAddress << 1);    // Address of device + R/W bit (0 for write)
    i2cData[2] = data;                   // Data to write

    quint64 writBytes;
    writBytes = serialPort.write( (const char *)i2cData, 3 );

    if (writBytes < 3) return globals::WRITE_ERROR;

    if ( !waitForBytesWritten(500) ) return errorClear(globals::WRITE_TIMEOUT);

    // Read back the write confirm code from the USB adaptor (1 byte):
    qDebug() << "--Reading confirm byte from adaptor...";
    char writeResult = 0;
    qint64 readBytes;
    int result;
    result = serialPort.waitForReadyRead(500);
    qDebug() << "--Wait Result: " << result;
    if (!result) return errorClear(globals::WRITE_CONF_TIMEOUT);
    readBytes = serialPort.read( &writeResult, 1 );
    qDebug() << "--Bytes read: " << readBytes
             << "; writeResult: " << (int)writeResult
             << "; Error: " << serialPort.error() << "\n"
             << serialPort.errorString();

    if (readBytes < 1)       return errorClear(globals::READ_ERROR);
    if (writeResult == 0)    return errorClear(globals::ADAPTOR_WRITE_ERROR);
    else                     return globals::OK;
  }




/*!
 \brief Read N Bytes from the GT1724 WITHOUT address
 Memory address to read from (probably 3 bytes) should be sent
 first using moduleWriteByte (MSB first)
 \param data     Pointer to a buffer to store data. Must be allocated by
                 caller, at least nBytes in size.
 \param nBytes   Number of bytes to read. For safety, max 64 is recommended
                 (not sure how many adaptor can handle). Multiple calls
                 can be made if more than 64 bytes to read.
 \return globals::OK             Data written
 \return globals::WRITE_ERROR    Error (Error writing data)
 \return globals::READ_ERROR     Error (Error writing data)
 \return globals::NOT_CONNECTED  Error (comms not open)
************************************************************************/
int BertComms::moduleReadNBytes( uint8_t *data, const uint8_t nBytes )
  {
    if (!isOpen) return globals::NOT_CONNECTED;  // Comms not open!
    // DEBUG:
    qDebug() << "--moduleRWByte: Slave Address: " << (slaveAddress << 1) + 1;

    uint8_t i2cData[3];
    i2cData[0] = I2C_MUL;                  // USB-I2C adaptor command (Read multiple byte)
    i2cData[1] = (slaveAddress << 1) + 1;  // Address of device + R/W bit (0 for write)
    i2cData[2] = nBytes;                   // Number of bytes

    quint64 writBytes;
    writBytes = serialPort.write( (const char *)i2cData, 3 );

    if (writBytes < 3) return globals::WRITE_ERROR;

    if ( !waitForBytesWritten(500) ) return errorClear(globals::WRITE_TIMEOUT);

    // Read back the data:
    qDebug() << "--Reading " << nBytes << " bytes from adaptor...";
    //char writeResult = 0;
    qint64 readBytes = 0;
    qint64 totalReadBytes = 0;
    int result;
    while (totalReadBytes < nBytes)
    {
        result = serialPort.waitForReadyRead(500);
        qDebug() << "--Wait Result: " << result;
        if (!result) return errorClear(globals::READ_TIMEOUT);
        readBytes = serialPort.read( (char *)(data + totalReadBytes), (nBytes-totalReadBytes) );
        if (readBytes < 1) return errorClear(globals::READ_ERROR);
        qDebug() << "--Bytes read: " << readBytes
                 << "; Error: " << serialPort.error() << "\n"
                 << serialPort.errorString();
        totalReadBytes += readBytes;
    }
    if (totalReadBytes < nBytes) return errorClear(globals::READ_ERROR);
    else                         return globals::OK;
  }




/*
 \brief Platform-specific sleep command
 \param milliSeconds  Number of milliseconds to sleep for
*/
void BertComms::sleep(uint32_t milliSeconds)
  {
  Sleep( (DWORD)milliSeconds );
  }








/*** DEBUG!! *********************************************************************/
void BertComms::moduleCommandDump(const uint8_t *data)
  {
  qDebug() << "I2C Adaptor Command:";
  qDebug() << "-------------------------";
  qDebug() << " Adaptor Cmd: "
           << QString("[%1]").arg((int)(data[0]),2,16,QChar('0')) << endl

           << " I2C Slv Add: "
           << QString("[%1]").arg((int)(data[1]),2,16,QChar('0')) << endl

           << " Reg Addr  H: "
           << QString("[%1]").arg((int)(data[2]),2,16,QChar('0'))
           << " L: "
           << QString("[%1]").arg((int)(data[3]),2,16,QChar('0')) << endl

           << " n Bytes:     "
           << QString("[%1]").arg((int)(data[4]),2,16,QChar('0'));

  qDebug() << "-------------------------";
  }
/*********************************************************************************/
void BertComms::moduleDataDump(const uint8_t *data, const int size)
  {
  qDebug() << size << " Bytes Data Chunk: ";
  int j = 0;
  while (j < size)
    {
    qDebug() << QString("[%1] ").arg((int)(data[j]),2,16,QChar('0'))
             << (((j+1) < size) ? QString("[%1] ").arg((int)(data[j+1]),2,16,QChar('0')) : "")
             << (((j+2) < size) ? QString("[%1] ").arg((int)(data[j+2]),2,16,QChar('0')) : "")
             << (((j+3) < size) ? QString("[%1] ").arg((int)(data[j+3]),2,16,QChar('0')) : "")
             << (((j+4) < size) ? QString("[%1] ").arg((int)(data[j+4]),2,16,QChar('0')) : "")
             << (((j+5) < size) ? QString("[%1] ").arg((int)(data[j+5]),2,16,QChar('0')) : "")
             << (((j+6) < size) ? QString("[%1] ").arg((int)(data[j+6]),2,16,QChar('0')) : "")
             << (((j+7) < size) ? QString("[%1] ").arg((int)(data[j+7]),2,16,QChar('0')) : "");
    j += 8;
    }
  }
/*********************************************************************************/


