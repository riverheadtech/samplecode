/*!
 \file   BertLMX2594Interface.cpp
 \brief  LMX2594 Clock Hardware Interface Class Implementation - Derived from BertLMXxxxxInterface
 \author J Cole-Baker (For Smartest)
 \date   Apr 2018
*/

#include <QDebug>

#include "globals.h"
#include "BertComms.h"
#include "BertFile.h"

#include "BertLMX2594Interface.h"

/* Initial Register Values:
 * These values are loaded into the registers during initialisation,
 * and provide default values prior to setting any frequency and
 * turning on the output.
 * NOTE: For LMX2594, registers MUST be programmed in descending address order, i.e.
 * highest address first. When loading registers from an array of registerData_t
 * items, we loop through starting at the beginning, and so the register values must be
 * placed in the array in descending order.
 */
const BertLMXxxxxInterface::registerData_t BertLMX2594Interface::INIT_REGISTERS[] =
{
  // Addr / Data
    { 112, 0x0000 },
    { 111, 0x0000 },
    { 110, 0x0000 },
    { 109, 0x0000 },
    { 108, 0x0000 },
    { 107, 0x0000 },
    { 106, 0x0000 },
    { 105, 0x0021 },
    { 104, 0x0000 },
    { 103, 0x0000 },
    { 102, 0x3F80 },
    { 101, 0x0011 },
    { 100, 0x0000 },
    {  99, 0x0000 },
    {  98, 0x0200 },
    {  97, 0x0888 },
    {  96, 0x0000 },
    {  95, 0x0000 },
    {  94, 0x0000 },
    {  93, 0x0000 },
    {  92, 0x0000 },
    {  91, 0x0000 },
    {  90, 0x0000 },
    {  89, 0x0000 },
    {  88, 0x0000 },
    {  87, 0x0000 },
    {  86, 0x0000 },
    {  85, 0xCE00 },
    {  84, 0x0001 },
    {  83, 0x0000 },
    {  82, 0x1900 },
    {  81, 0x0000 },
    {  80, 0x6666 },
    {  79, 0x0026 },
    {  78, 0x0003 },
    {  77, 0x0000 },
    {  76, 0x000C },
    {  75, 0x0800 },
    {  74, 0x0000 },
    {  73, 0x003F },
    {  72, 0x0001 },
    {  71, 0x0081 },
    {  70, 0xC350 },
    {  69, 0x0000 },
    {  68, 0x03E8 },
    {  67, 0x0000 },
    {  66, 0x01F4 },
    {  65, 0x0000 },
    {  64, 0x1388 },
    {  63, 0x0000 },
    {  62, 0x0322 },
    {  61, 0x00A8 },
    {  60, 0x0000 },
    {  59, 0x0001 },
    {  58, 0x8001 },
    {  57, 0x0020 },
    {  56, 0x0000 },
    {  55, 0x0000 },
    {  54, 0x0000 },
    {  53, 0x0000 },
    {  52, 0x0820 },
    {  51, 0x0080 },
    {  50, 0x0000 },
    {  49, 0x4180 },
    {  48, 0x0300 },
    {  47, 0x0300 },
    {  46, 0x07FC },
    {  45, 0xC6CF },
    {  44, 0x0F23 },
    {  43, 0x0000 },
    {  42, 0x0000 },
    {  41, 0x0000 },
    {  40, 0x0000 },
    {  39, 0x03E8 },
    {  38, 0x0000 },
    {  37, 0x0404 },
    {  36, 0x0046 },
    {  35, 0x0004 },
    {  34, 0x0000 },
    {  33, 0x1E21 },
    {  32, 0x0393 },
    {  31, 0x43EC },
    {  30, 0x318C },
    {  29, 0x318C },
    {  28, 0x0488 },
    {  27, 0x0002 },
    {  26, 0x0DB0 },
    {  25, 0x0624 },
    {  24, 0x071A },
    {  23, 0x007C },
    {  22, 0x0001 },
    {  21, 0x0401 },
    {  20, 0xF848 },
    {  19, 0x27B7 },
    {  18, 0x0064 },
    {  17, 0x012C },
    {  16, 0x0080 },
    {  15, 0x064F },
    {  14, 0x1E70 },
    {  13, 0x4000 },
    {  12, 0x5001 },
    {  11, 0x0018 },
    {  10, 0x10D8 },
    {   9, 0x1604 },
    {   8, 0x2000 },
    {   7, 0x40B2 },
    {   6, 0xC802 },
    {   5, 0x00C8 },
    {   4, 0x0A43 },
    {   3, 0x0642 },
    {   2, 0x0500 },
    {   1, 0x0808 },
    {   0, 0x251C }
};
const size_t BertLMX2594Interface::INIT_REGISTERS_SIZE =
         sizeof(INIT_REGISTERS) / sizeof(INIT_REGISTERS[0]);

// Bit patterns for Register 0:
const uint16_t BertLMX2594Interface::R0_NOTHING           = 0b0010010000010000;  // No bits set other than preset bits (don't change these)

const uint16_t BertLMX2594Interface::R0_RAMP_EN           = 0b1000000000000000;  // RAMP_EN bit
const uint16_t BertLMX2594Interface::R0_VCO_PHASE_SYNC_EN = 0b0100000000000000;  // VCO_PHASE_SYNC_EN bit
const uint16_t BertLMX2594Interface::R0_OUT_MUTE          = 0b0000001000000000;  // OUT_MUTE bit = Mute output during FCAL
const uint16_t BertLMX2594Interface::R0_FCAL_EN           = 0b0000000000001000;  // FCAL_EN bit = Run FCal
const uint16_t BertLMX2594Interface::R0_MUXOUT_LD_SEL     = 0b0000000000000100;  // MUXOUT_LD_SEL bit
const uint16_t BertLMX2594Interface::R0_RESET             = 0b0000000000000010;  // RESET bit = Reset
const uint16_t BertLMX2594Interface::R0_POWERDOWN         = 0b0000000000000001;  // POWERDOWN bit

const uint16_t BertLMX2594Interface::R0_FCAL_XXXX_ADJ_MASK = 0b0000000111100000;  // Mask to preserve ONLY FCAL_XXXX_ADJ bits

// Default value to load into R0 for normal running:
const uint16_t BertLMX2594Interface::R0_DEFAULT = BertLMX2594Interface::R0_NOTHING         // Preset bits only (don't change these)
                                                | BertLMX2594Interface::R0_MUXOUT_LD_SEL;  // Turn on Lock Detect output pin
                                                // | BertLMX2594Interface::R0_FCAL_EN;        // FCal enable (automatic FCal on Freq change)

// On the LMX2594, R1 and R7 are also used as part of basic configuration
// (CAL_CLK_DIV field and OUT_FORCE bit):
const uint16_t BertLMX2594Interface::R1_DEFAULT = 0b0000100000001000;  // Set up CAL_CLK_DIV for 100 MHz Input Oscillator
const uint16_t BertLMX2594Interface::R7_DEFAULT = 0b0100000010110010;  // Make sure OUT_FORCE is ON (required when NOT using OUT_MUTE in R0)

const uint16_t BertLMX2594Interface::R44_DEFAULT = 0b0000000011100000; // OutA Power at minimum; Outputs A and B powered down; Other bits default

const uint16_t BertLMX2594Interface::R45_DEFAULT  = 0b1100011011000000; // OUTA_MUX = CHDIV; ISET = Min (11); OUTB_PRW = Minimum
const uint16_t BertLMX2594Interface::R45_OUTA_VCO = 0b1100111011000000; // OUTA_MUX = VCO; ISET = Min (11); OUTB_PRW = Minimum

const uint16_t BertLMX2594Interface::R46_DEFAULT = 0b0000011111111101; // OUTB_MUX = VCO

// Settings for R31 and R75 for various trigger out divide ratios:
const uint16_t BertLMX2594Interface::R31_VALUES[] =
{
    0x03EC,   //   1/1   // Don't care; will be using VCO out
    0x03EC,   //   1/2
    0x43EC,   //   1/4
#ifdef BERT_DEBUG_DIV_RATIOS
    0x43EC,   //   1/8
    0x43EC,   //   1/16
    0x43EC,   //   1/32
    0x43EC,   //   1/128
    0x43EC    //   1/192
#endif
};

const uint16_t BertLMX2594Interface::R75_VALUES[] =
{
    0x0800,   //   1/1   // Don't care; will be using VCO out
    0x0800,   //   1/2
    0x0840,   //   1/4
#ifdef BERT_DEBUG_DIV_RATIOS
    0x08C0,   //   1/8
    0x0940,   //   1/16
    0x09C0,   //   1/32
    0x0B00,   //   1/128
    0x0B40,   //   1/192
#endif
};

const size_t BertLMX2594Interface::R31R75_VALUES_SIZE = sizeof(R31_VALUES) / sizeof(R31_VALUES[0]);



BertLMX2594Interface::BertLMX2594Interface(BertComms *useBertComms, const uint8_t useSlaveAddress) : BertLMXxxxxInterface(useBertComms, useSlaveAddress, 112)
{
    /* DEBUG
    qDebug() << "BertLMX2594Interface Constructor: Slave Address: " << (int)useSlaveAddress << "; "
             << " selectedProfileIndex: " << selectedProfileIndex
             << " selectedTrigDivideIndex: " << selectedTrigDivideIndex
             << " selectedFOutOutputPowerIndex: " << selectedFOutOutputPowerIndex
             << " selectedTrigOutputPowerIndex: " << selectedTrigOutputPowerIndex;
    */
}

BertLMX2594Interface::~BertLMX2594Interface() { }



/*****************************************************************************************
   LMX2594 Functions
 *****************************************************************************************/

/*!
 \brief Select Frequency Profile by Index - part specfic implementation
 \param index               Index of profile to get frequency from - 0 is first
 \return globals::OK        Item found for requested index.
 \return globals::OVERFLOW  Index was larger than the list of frequency profiles
 \return [Error Code]       Error from writeRegister
*/
int BertLMX2594Interface::selectProfilePart(const size_t index)
{
    if (index >= (size_t)frequencyProfiles.count()) return globals::OVERFLOW;
    qDebug() << "LMX2594: Select frequency profile " << index
             << ": " << frequencyProfiles.at(index).getFrequency() << " MHz";

    uint8_t registerAddress;
    int16_t registerValue;
    bool registerFound;
    int result;

    resetDevice();

    // Load registers in REVERSE ORDER; Don't load R0 (power control)
    for (registerAddress = 112; registerAddress > 0; registerAddress--)
    {
        registerValue = frequencyProfiles.at(index).getRegisterValue(registerAddress, &registerFound);
        if (registerFound)
        {
            //qDebug() << "LMX2594: Write " << QString().sprintf("0x%04X", registerValue)
            //         << " to register " << registerAddress;
            result = writeRegister(registerAddress, registerValue);
            if (result != globals::OK)
            {
                qDebug() << "LMX2594: ERROR writing register! (" << result << ")";
                return globals::WRITE_ERROR;
            }
        }
    }
    selectedProfileIndex = index;

    // Restore the previous power and divider settings
    selectTriggerDivide(selectedTrigDivideIndex, false);

    selectFOutPower(selectedFOutOutputPowerIndex);
    selectTrigOutPower(selectedTrigOutputPowerIndex);

    // Turn outputs back on:
    outputsOn();

    // Calibrate: Required after changing PLL settings
    runFCal();

    qDebug() << "LMX2594: Registers set for profile!";
    return globals::OK;
}






/*!
 \brief Select Trigger Divide Ratio - Part specific
 \param index              Index of new divide ratio (see getTriggerDivideList)
 \return globals::OK       Ratio set OK
 \return globals::OVERFLOW Invalid index
 \return [Error Code]      Error code from writeRegister
*/
int BertLMX2594Interface::selectTriggerDividePart(const size_t index, const bool doFCal)
{
    if (index >= R31R75_VALUES_SIZE) return globals::OVERFLOW;
    qDebug() << "LMX2594: Select trigger divide index " << index;
    uint16_t R45;
    int result;
    if (index == 0)
    {
        qDebug() << "Selecting VCO for OUTA (No divider)";
        R45 = (R45_OUTA_VCO | POWER_CONSTS[selectedTrigOutputPowerIndex]);  // OUTA_MUX = 01 (VCO); OUT_ISET = 11; Set OUTB_POW;
        qDebug() << "Set R45 to " << QString().sprintf("0x%04X", R45);
        result = writeRegister(45, R45);
        if (result != globals::OK) return result;
        qDebug() << "OK.";
        // Don't care about CHDIV settings (R31 / R75).
    }
    else
    {
        qDebug() << "Selecting CHDIV Out for OUTA";
        R45 = (R45_DEFAULT | POWER_CONSTS[selectedTrigOutputPowerIndex]);  // OUTA_MUX = 00; OUT_ISET = 11; Set OUTB_POW;
        qDebug() << "Set R45 to " << QString().sprintf("0x%04X", R45);
        result = writeRegister(45, R45);
        if (result != globals::OK) return result;
        qDebug() << "OK.";
        qDebug() << "R31 = " << QString().sprintf("0x%04X", R31_VALUES[index])
                 << "R75 = " << QString().sprintf("0x%04X", R75_VALUES[index]);
        result                            = writeRegister(31, R31_VALUES[index]);
        if (result == globals::OK) result = writeRegister(75, R75_VALUES[index]);
    }
    if (result == globals::OK && doFCal) runFCal();
    if (result != globals::OK)
    {
        qDebug() << "LMX2594: Error setting divide ratio: " << result;
    }
    else
    {
        selectedTrigDivideIndex = index;
    }
    return result;
}


/*!
 \brief Reset device: Part-specific implementation
 \param defaultR0  Value to use for R0. This should have any required bits set
                   EXCEPT the RESET bit which must be 0. The method will modify
                   the RESET bit and use other bits from defaultR0.
 \return [code] Result code from writeRegister
*/
int BertLMX2594Interface::resetPart(uint16_t defaultR0)
{
    qDebug() << "LMX2594 RESET...";
    int result = writeRegister(0, defaultR0 | R0_RESET);  // Reset bit = High. Nb: DOESN'T Self-clear on the LMX2594!
    if (result != globals::OK) return result;
    globals::sleep(LMX2594_RESET_SLEEP);

    result = writeRegister(0, defaultR0);                 // Reset bit = Low.
    globals::sleep(LMX2594_RESET_POST_SLEEP);
    qDebug() << " -OK.";
    return result;
}





/*****************************************************************************************/
/* PRIVATE Methods                                                                       */
/*****************************************************************************************/


/*!
 \brief Initialise LMX2594 Chip

 This method resets the LMX2594, then loads initial register
 settings to place the device in a known state (prior to
 setting the frequency and enabling the output).
 Startup procedure is taken from LMX2594 documentation,
 Section 7.5.1

 This method assumes that initAdaptor() has already been
 called successfully (i.e. the I2C->SPI interface is ready).

 \return globals::OK   Comms online; Ack from SC18IS602B
 \return globals::NOT_CONNECTED   No connection to BERT
 \return [Error code]             ???
*/
int BertLMX2594Interface::initPart()
{
    int result;
    qDebug() << "initPart: Set up LMX2594 Clock Synth IC";

    // Step 1: Device RESET:
    result = resetPart(R0_DEFAULT);
    if (result != globals::OK) goto finished;

    // Step 2: Load default register values:

    // If frequency profiles were set up OK, load default profile:
    result = selectProfile(profileIndexDefault);
    if (result == globals::OK) result = outputsOn();

    if (result != globals::OK)
    {
        // Error... try loading failsafe default register settings:
        size_t initDataIndex;
        qDebug() << " -Set LMX2594 Registers to init values...";
        for (initDataIndex = 0; initDataIndex < INIT_REGISTERS_SIZE; initDataIndex++)
        {
            if (INIT_REGISTERS[initDataIndex].address == 0) continue;  // SKIP register R0.
            qDebug() << "  -Set R" << INIT_REGISTERS[initDataIndex].address;
            result = writeRegister( INIT_REGISTERS[initDataIndex].address,
                                    INIT_REGISTERS[initDataIndex].value );
            if (result != globals::OK) break;
        }
    }

  finished:
    if (result == globals::OK) qDebug() << "LMX2594 Ready.";
    else                       qDebug() << "ERROR setting up LMX2594!";
    return result;
}


int BertLMX2594Interface::configureOutputs()
{
    uint16_t R44, R45;

    // We need to use MASH_RESET_EN and MASH_ORDER fields from profile for Reg 44:
    bool bFound = false;
    if (selectedProfileIndex < frequencyProfiles.count())
    {
        R44 = frequencyProfiles.at(selectedProfileIndex).getRegisterValue(44, &bFound);
    }
    if (!bFound) R44 = R44_DEFAULT;  // Default to use if no profile selected.

    qDebug() << "Trig Pow Index: " << selectedTrigOutputPowerIndex << " = " << POWER_CONSTS[selectedTrigOutputPowerIndex];

    R44 = (R44 & 0xC03F) |  // Stored reg value; OUTA_POW, OUTA_PD and OUTB_PD bits set to 0
          (POWER_CONSTS[selectedTrigOutputPowerIndex] << 8) | // OUTA_POW
          (((flagOutputsOn) ? 0x00 : 0x03) << 6);             // OUTA_PD and OUTB_PD

    // R45 Setting: For /1 Trigger Out, need to select OUTA_MUX = VCO;
    //              For other divide ratios, need to select CHDIV.
    if (selectedTrigDivideIndex == 0)
    {
        // Divide by 1 trigger out:
        R45 = (R45_OUTA_VCO | POWER_CONSTS[selectedFOutOutputPowerIndex]);  // OUTA_MUX = 01 (VCO); OUT_ISET = 11; Set OUTB_POW;
    }
    else
    {
        // Divide by 2 or more for trigger out:
        R45 = (R45_DEFAULT | POWER_CONSTS[selectedFOutOutputPowerIndex]);  // OUTA_MUX = 00; OUT_ISET = 11; Set OUTB_POW;
    }
    qDebug() << "Set R44 to " << R44 << "; R45 to " << R45;
    int result = writeRegister(44, R44);
    if (result != globals::OK) return result;
    result = writeRegister(45, R45);
    qDebug() << "OK.";
    return result;

}


/*!
 \brief Set safe default values for frequency profiles
 This method overrides some register values in each
 profile, to create safe default settings regardless
 of what settings were loaded from the frequency profile
 file.
 The following settings are changed:
  - Set up R0, R1 and R7 (OUT_MUTE, MUXOUT_LD_SEL, etc)
  - Power down output drivers (application should turn
    them ON once all settings have been configured)
  - Set output levels to 0 dBm
  - Set CHDIV ratio to default
  - Set up output MUX to select CHDIV for output A
    and VCO for output B
 \return globals::OK
 \return [error code]
*/
void BertLMX2594Interface::setSafeDefaults()
{
    QList<BertLMXFrequencyProfile>::iterator i;
    for (i = frequencyProfiles.begin(); i != frequencyProfiles.end(); ++i)
    {
        qDebug() << "Set SAFE defaults for " << i->getFrequency();

        // Register 0 set up:
        // * First, extract FCAL_HPFD_ADJ and FCAL_LPFD_ADJ from
        //   value recovered from file: We need to preserve these.
        // * Override some fields:
        //   * Lock Detect enabled;
        //   * MUXOUT pin set to LD;
        //   * FCAL_EN, RESET and POWERDOWN to 0.
        bool bFound;
        uint16_t R0 = i->getRegisterValue(0, &bFound);
        if (!bFound) R0 = R0_DEFAULT; // Safe fallback.

        uint16_t maskedR0 = R0 & R0_FCAL_XXXX_ADJ_MASK;
          // ...Leave only FCAL_HPFD_ADJ and FCAL_LPFD_ADJ

        R0 = maskedR0 | R0_DEFAULT;
          // ...Set the other bits we want from default

        i->setRegisterValue(0, R0);

        // Register R1 and R7 set up: Just overwrite whatever is in the file.
        i->setRegisterValue(1, R1_DEFAULT);
        i->setRegisterValue(7, R7_DEFAULT);

        // Set default trigger divide ratio:
        i->setRegisterValue(31, R31_VALUES[DEFAULT_DIVIDE_RATIO_INDEX]);
        i->setRegisterValue(75, R75_VALUES[DEFAULT_DIVIDE_RATIO_INDEX]);

        // Setup outputs:
        // * Preserve MASH_EN and MASH_ORDER bits
        // * Set other bits to safe defaults:
        //   OUT*_POW = 0dBm; OUT*_PD = 1
        // These settings use R44, R45 and R46 on LMX2594:
        uint16_t R44 = i->getRegisterValue(44, &bFound);
        if (!bFound) R44 = R44_DEFAULT;
        uint16_t maskedR44 = R44 & 0x0027;
          // ...Leave only MASH_RESET_EN and MASH_ORDER bits
        R44 = maskedR44 | 0x00C0;
          // ...Set OUTA_PD and OUTB_PD (Power down both outputs)
        R44 = setRegisterBits(R44, 6, 8, POWER_0DBM); // Set OUTA_POW = 0dBm:
        i->setRegisterValue(44, R44);

        uint16_t R45 = R45_DEFAULT;
        i->setRegisterValue(45, R45);

        uint16_t R46 = R46_DEFAULT;
        i->setRegisterValue(46, R46);
    }
}



