/*!
 \file   BertEyeMonitor.h
 \brief  BERT Eye Monitor Functions - Class Header
 \author J Cole-Baker (For Smartest)
 \date   Feb 2016
*/

#ifndef BERTEYEMONITOR_H
#define BERTEYEMONITOR_H

#include <QObject>
#include <QMutex>

#include "BertInterface.h"

/*!
 \brief Eye Monitor Functions for Smartest BERT
 This class includes methods for eye scanning and
 bathtub plots.

 Signals:

    void callbackProgressUpdate(int progressPercent)
      Progress update (emited periodically during scan)

*/
class BertEyeMonitor : public QObject
{
Q_OBJECT

public:

    BertEyeMonitor( BertInterface *useBertInterface );

    ~BertEyeMonitor();

    int eyeScan( const uint8_t channel,
                 const uint8_t hStep,
                 const uint8_t vStep,
                 const uint8_t countRes,
                 const double  rate,
                 const bool    reset );

    int bathtubScan( const uint8_t channel,
                     const uint8_t hStep,
                     const uint8_t vOffset,
                     const uint8_t countRes,
                     const double  rate,
                     const bool    reset );

    void cancelScan();

    bool scanInProgress();

    double *getEyeData();

    uint8_t  getChannel()        const { return scanChannel;        }
    uint8_t  getHStep()          const { return scanHStep;          }
    uint8_t  getVStep()          const { return scanVStep;          }
    uint8_t  getHRes()           const { return scanHRes;           }
    uint8_t  getVRes()           const { return scanVRes;           }
    uint8_t  getCountResIndex()  const { return scanCountResIndex;  }
    uint8_t  getCountResBits()   const { return scanCountResBits;   }

signals:
    void progressUpdate(int progressPercent);

private:

    // Scan types:
    static const uint8_t SCAN_EYE_SCAN     = 0;
    static const uint8_t SCAN_BATHTUB_SCAN = 1;

    BertInterface *bertInterface; // Reference to a BertInterface object used to communicate with instrument.
                                  // Must be created and connected elsewhere.

    double *eyeDataBuffer = NULL;

    bool     stopFlag      = false;
    bool     resetFlag     = true;

    uint8_t  scanChannel   = 1;  // ED "Channel" to use: 1 - 4
    uint8_t  scanBoard     = 0;  // Board to use (calculated from channel: 0 = Master; 1 = Slave)
    uint8_t  scanLane      = 0;  // Device lane (calculated from channel: 1 or 3)
    uint8_t  scanHStep     = 1;  // Step between sample points
    uint8_t  scanVStep     = 1;  //
    uint8_t  scanHRes      = 1;  // Number of sample points
    uint8_t  scanVRes      = 1;  //
    uint8_t  scanVOffset   = 0;  // Only used for bathtub scan

    uint8_t  scanType      =  SCAN_EYE_SCAN;  // Full eye scan or just bathtub scan?
    double   bitRate       = 25.59e9;
    int      nShift[5]     = { 0, 0, 0, 0, 0 };       // Per channel...

    uint8_t  scanCountResIndex = 0;
    uint8_t  scanCountResBits  = 1;

    QMutex dataMutex;

    int eyeScanRun();

    uint8_t peakFind( const double *data,
                      const size_t sizeX,
                      const size_t sizeY );

    double *dataShift( const double *data,
                       const size_t sizeX,
                       const size_t sizeY,
                       const int nShift );

    int queryEyeScanMem( const uint8_t board,
                         uint8_t *addressMSB,
                         uint8_t *addressLSB,
                         uint8_t *sizeMSB,
                         uint8_t *sizeLSB );

    int controlEyeSweep( const uint8_t board,
                         const uint8_t lane,
                         const uint8_t phaseStart,
                         const uint8_t phaseStop,
                         const uint8_t phaseStep,
                         const uint8_t offsetStart,
                         const uint8_t offsetStop,
                         const uint8_t offsetStep,
                         const uint8_t resolution,
                         uint8_t *sizeMSB,
                         uint8_t *sizeLSB );

    void channelToBoardLane( const uint8_t channel, uint8_t *board, uint8_t *lane );

};


#endif // BERTEYEMONITOR_H
