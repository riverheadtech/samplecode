/*!
 \file   BertComms.h
 \brief  Comms API Class Header
         This class provides the API for BERT comms (as used by BertInterface).
         It provides a method for queueing comms requests; Actual comms work
         is carried out by the worker thread created by BertCommsWorker.cpp

 \author J Cole-Baker (For Smartest)
 \date   Jan 2015; Modified June 2018
*/

#ifndef BERTCOMMS_H
#define BERTCOMMS_H

#include <QObject>
#include <QEventLoop>

#include "globals.h"
#include "BertCommsWorker.h"


/*!
 \brief Comms API Class

 Implements communications with a GT1724 board via a USB-I2C adaptor.

 The BERT board uses I2C for its communication, so this class
 provides methods for I2C-style communication. The actual
 implementation of these methods depends on the specific USB
 (or similar) to I2C converter in use (or possibly direct I2C
 transmission if using an embedded board with I2C support).

 Internally, a worker thread is used to process all requests, so that the
 comms are independant of the UI thread and are not blocked by UI blockers
 such as dragging the window.

 All public methods will queue a command for the worker thread, then wait
 for the result or a timeout. This means that operations are thread-safe,
 but if the caller needs to send a sequence of opreations (e.g. reads and
 writes) which shouldn't be interrupted, the commsLock and commsFree
 methods can be used to manage a global lock on the comms object,
 preventing other code (e.g. on a timer event) from interrupting a
 sequence.

*/
class BertComms : public QObject
  {
  Q_OBJECT

  public:
    BertComms();
    virtual ~BertComms();

    int   open(const QString useSerialPort);  // E.g.: "\\\\.\\COM1"
    void  close();
    void  reset();

    // DEPRECATED int   commsLock(int timeoutMs);
    // DEPRECATED void  commsFree();

    int   writeRaw(const uint8_t slaveAddress,
                   const uint8_t *data,
                   const uint8_t nBytes);

    int   write(const uint8_t slaveAddress,
                const uint16_t regAddress,
                const uint8_t *data,
                const uint8_t nBytes);

    int   write24(const uint8_t slaveAddress,
                  const uint32_t regAddress,
                  const uint8_t *data,
                  const size_t nBytes);


    int   readRaw(const uint8_t slaveAddress,
                  uint8_t *data,
                  const uint8_t nBytes);

    int   read(const uint8_t slaveAddress,
               const uint16_t regAddress,
               uint8_t *data,
               const uint8_t nBytes);

    int   read24(const uint8_t slaveAddress,
                 const uint32_t regAddress,
                 uint8_t *data,
                 const size_t nBytes);

    // Method to indicate whether the serial port is open:
    bool portIsOpen();

  public slots:
    void commsJobFinished();

  private:
    BertCommsWorker *commsWorker;
    QEventLoop       eventLoop;

};




#endif // BERTCOMMS_H
