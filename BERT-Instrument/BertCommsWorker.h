/*!
 \file   BertCommsWorker.h
 \brief  BERT Board Comms Worker Thread Class Header
 \author J Cole-Baker (For Smartest)
 \date   Jun 2018
*/

#ifndef BERTCOMMSWORKER_H
#define BERTCOMMSWORKER_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QEventLoop>
#include <QTimer>

#include "BertSerial.h"

/*!
 \brief Worker thread class for handling comms in the background
*/
class BertCommsWorker : public QThread
{
    Q_OBJECT

public:
    BertCommsWorker();
    ~BertCommsWorker();

    void requestStop();

    int startCommsJob(const int       opCode,
                      const QString   useSerialPort,
                      const int       timeoutMs,
                      const uint8_t   slaveAddress,
                      const uint16_t  regAddress,
                      const uint32_t  regAddress24,
                      const uint8_t  *dataWrite,
                      uint8_t        *dataRead,
                      const size_t    nBytes,
                      QEventLoop     *eventLoop);

    bool portIsOpen();

    // Job Op Codes:
    static const int COMMS_open       = 0;
    static const int COMMS_close      = 1;
    static const int COMMS_reset      = 2;
    static const int COMMS_commsLock  = 3;
    static const int COMMS_commsFree  = 4;
    static const int COMMS_writeRaw   = 5;
    static const int COMMS_write      = 6;
    static const int COMMS_write24    = 7;
    static const int COMMS_readRaw    = 8;
    static const int COMMS_read       = 9;
    static const int COMMS_read24     = 10;


signals:
    void jobFinished();

public slots:
    void transactionFinished();

private slots:
    void commsTimeout();

private:
    void run();
    int  processJob();
    void commsClose();
    int  i2cOp(const uint8_t  nBytesToWrite,
               const uint8_t *dataWrite,
               const uint8_t  nBytesToRead,
                     uint8_t *dataRead);

    bool probeAdaptor();
    int  errorClear(const int returnCode);

    static const int JOB_WAIT_TIMEOUT = 2000; // Time to wait if another comms job is in progress
    static const int COMMS_TIMEOUT = 50;      // Maximum time when reading data back from serial transaction (mS)
    static const int MAX_RETRIES   = 5;       // Maximum number of times to retry on I2C Error

    // Adaptor Command Bytes:
    static const uint8_t I2C_SGL = 0x53;   // Read/Write single byte for non-registered devices
    static const uint8_t I2C_AD0 = 0x54;   // Read/Write multiple bytes without address
    static const uint8_t I2C_AD1 = 0x55;   // Read/Write single or multiple bytes for 1 byte addressed devices
    static const uint8_t I2C_AD2 = 0x56;   // Read/Write single or multiple bytes for 2 byte addressed devices
    static const uint8_t I2C_DIR = 0x57;   // Build custom I2C sequences
    static const uint8_t I2C_TST = 0x58;   // Check for the existence of an I2C device on the bus
    static const uint8_t ISS_CMD = 0x5A;   // Custom commands for the USB-ISS adaptor

    // Pre-defined I2C adaptor ops:
    static const uint8_t I2C_OP_GET_VERSION[];
    static const uint8_t I2C_OP_GET_VERSION_SIZE;

    static const uint8_t I2C_OP_GET_SERIAL[];
    static const uint8_t I2C_OP_GET_SERIAL_SIZE;

    static const uint8_t I2C_OP_SET_MODE[];
    static const uint8_t I2C_OP_SET_MODE_SIZE;

    // **** Comms Status Constants: *****************
    static const int COMMS_BUSY  =  0;
    static const int COMMS_OK    =  0;
    static const int COMMS_ERROR = -1;

    bool           isOpen;
    int            commsStatus;

    BertSerial    *bertSerial = NULL;
    QTimer        *commsTimer = NULL;

    // Comms Job Parameters:
    // For unused parameters: Pointers should be NULL, other vales should be "" or 0.
    struct CommsJob
    {
        int            opCode;         // Describes the operation to be carried out (see op codes above)
        QString        useSerialPort;  // String specifying the serial port (For 'open' only)
        int            timeoutMs;      // Wait timeout in milliseconds (for commsLock only)
        uint8_t        slaveAddress;   // I2C Slave Address (for ALL Read / Write ops)
        uint16_t       regAddress;     // Register Address for read or write; not used for RAW read / write ops or 24 bit read / write
        uint32_t       regAddress24;   // Register Address for read or write with 24 bit addressing (read24 / write24 only)
        const uint8_t *dataWrite;      // Pointer to buffer of data to write - must contain at least nBytes
        uint8_t       *dataRead;       // Pointer to buffer to read data into - must contain at least nBytes
        size_t         nBytes;         // Number of bytes to read or write (READ / WRITE operations)
        int            returnCode;     // Result of the operation; only valid after operation is finished.
    };
    CommsJob commsJob;   // Details of current comms job (we only carry out one at a time...)

    bool           flagStop;
    QMutex         commsFlagMutex;   // To protect flag variables which are shared via public methods

    QWaitCondition jobStart;         // Condition used to tell the worker thread that there's a new job to start
    QMutex         commsSyncMutex;   // Mutex used to synchronise the worker: This is locked by the worker at startup,
                                     // and is ONLY free once the worker is in the "wait()" call ready to receive a
                                     // new job (i.e. be woken up by the jobStart condition)

    QMutex         jobMutex;         // To make sure only one job can be active at one time (other calls will block)

};



#endif // BERTCOMMSWORKER_H
