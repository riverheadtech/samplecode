/*!
 \file   BertComms.cpp
 \brief  BERT Board Comms Class Implementation - WINDOWS
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/

#include <windows.h>
#include <stdio.h>

#include <QDebug>

#include "globals.h"
#include "BertComms_WIN.h"


/*
 Nb: For comms via DEVANTECH USB-I2C INTERFACE ADAPTER, see:

 http://www.robot-electronics.co.uk/htm/usb_i2c_tech.htm

 I2C Module Commands:

 I2C_SGL   0x53   Read/Write single byte for non-registered devices,
                  such as the Philips PCF8574 I/O chip (All)
 I2C_MUL   0x54   Read multiple bytes without setting new address (eeprom's,
                  Honeywell pressure sensors, etc) (V5 and higher)
 I2C_AD1   0x55   Read/Write single or multiple bytes for 1 byte addressed devices
                  (the majority of devices will use this one) (All)
 I2C_AD2   0x56   Read/Write single or multiple bytes for 2 byte addressed devices,
                  eeproms from 32kbit (4kx8) and up.  (V6 and higher)
 I2C_USB   0x5A   A range of commands to the USB-I2C module, generally to improve
                  selected communications or provide analogue/digital I/O  (All)

*/

BertComms::BertComms(const uint8_t useSlaveAddress)
  {
  slaveAddress = useSlaveAddress;
  isOpen = globals::GFALSE;   // Signifies that the port is closed.
  lastError = 0;
  }


BertComms::~BertComms() {}



/*!
 \brief Open comms

 \param serialPort   WCHAR array containing the name of the serial port to use

 \return globals::OK     Success. Comms open.

 \return globals::GERROR  Error. Comms not open; calls to
                         read and write will be ignored.

***************************************************************/
int BertComms::open(const LPCTSTR serialPort)
  {
  // E.g.: const WCHAR serialPort[] = {L"\\\\.\\COM17"} ;
  hSerial = CreateFile(  serialPort,
                         GENERIC_READ | GENERIC_WRITE,
                         0,
                         0,
                         OPEN_EXISTING,
                         FILE_ATTRIBUTE_NORMAL,
                         0);
  if (hSerial == INVALID_HANDLE_VALUE)
    {
    lastError = (uint32_t)GetLastError();
    qDebug() << "Error opening serial port: " << serialPort << " (" << lastError << ")" << endl;
    return globals::GERROR;
    }


  // Set COMMS timeout:
  COMMTIMEOUTS ctimeouts =
    {
    .ReadIntervalTimeout = 600,
    .ReadTotalTimeoutMultiplier = 10,
    .ReadTotalTimeoutConstant = 1,
    .WriteTotalTimeoutMultiplier = 10,
    .WriteTotalTimeoutConstant = 1,
    };
  SetCommTimeouts( hSerial, &ctimeouts );

  qDebug() << "Serial Port Opened: " <<serialPort << endl;

  // Serial port should be open:
  isOpen = globals::GTRUE;
  return globals::OK;
  }









/*!
 \brief Write data to BERT board

  If comms are not opened, the method does nothing.
  This method is also used as part of the 'read' method, to send the 'read' command to the
  board. In that case, no registar data are sent, and nBytes is 0.

 \param registerAddress  16 bit address of register to write to
 \param registerData     Pointer to an array of uint8_t data bytes (may be only one)
 \param nBytes           Number of bytes to write from registerData.
                         nBytes must be <= 64, otherwise 'OVERFLOW' error will be returned.

 \return globals::OK              Data written
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::GERROR           Write error (timeout waiting for write?)
 \return globals::OVERFLOW        Error (nBytes > 64)

***************************************************************/
int BertComms::write(const uint16_t registerAddress, const uint8_t *registerData, const uint8_t nBytes)
  {
  if (!isOpen)     return globals::NOT_CONNECTED;  // Comms not open!
  if (nBytes > 64) return globals::OVERFLOW;       // Transmission buffer is limited to 64 bytes.

  // Send the 'I2C_AD2' command (I2C operation with 2 byte register address) for 'write' mode:
  int result = i2cCommand(0, registerAddress, nBytes);
  if (result != globals::OK) return globals::GERROR;      // Error sending I2C command.

  // Send the register data (Max. 64 bytes):
  BOOL bResult;
  DWORD writBytes = 0;
  bResult = WriteFile(          hSerial,
                       (LPCVOID)registerData,  // start of data to write
                         (DWORD)nBytes,        // number of bytes to write
                               &writBytes,     // number of bytes that were written
                                NULL   );      // no overlapped structure

  if ( (FALSE == bResult) || (writBytes < nBytes) )
    {
    lastError = (uint32_t)GetLastError();
    return globals::GERROR;
    }

  return globals::OK;
  }












/*!
 \brief Read data from BERT board

 Reads bytes from the BERR board and stores them to the
 supplied buffer. If the comms are not open, the method
 returns 0 and nothing is written to the buffer.

 \param registerAddress  16 bit address of register to read from
 \param registerData     Pointer to a buffer allocated by the caller.
                         Must be at least nBytes in size.
 \param nBytes           Number of bytes to read from device.
                         nBytes must be <= 64, otherwise 'OVERFLOW' error will be returned.

 \return globals::OK              Success  - nBytes were read back into registerData buffer
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::GERROR           Error (Error reading data)
 \return globals::OVERFLOW        Error (nBytes > 64)
 \return globals::TIMEOUT        A comms timeout occurred before nBytes were received.
                                 In this case, the content of registerData is undefined - it may
                                 contain part of the expected data

*****************************************************************/
int BertComms::read(const uint16_t registerAddress, uint8_t *registerData, const uint8_t nBytes)
  {
  if (!isOpen)     return globals::NOT_CONNECTED;  // Comms not open!
  if (nBytes > 64) return globals::OVERFLOW;       // Transmission buffer is limited to 64 bytes.

  // Send the 'I2C_AD2' command (I2C operation with 2 byte register address) for 'read' mode:
  int result = i2cCommand(1, registerAddress, nBytes);
  if (result != globals::OK) return globals::GERROR;      // Error sending I2C command.

  // Read back the resulting register data:
  BOOL bResult;
  DWORD readBytes = 0;

  //LPOVERLAPPED lpOverlapped = NULL;
  //OVERLAPPED myOverlapped;


  bResult = ReadFile(          hSerial,
                               registerData,
                        (DWORD)nBytes,
                               &readBytes,
                               NULL  );


  if ( (FALSE == bResult) || (readBytes < nBytes) )
    {
    lastError = (uint32_t)GetLastError();
    return globals::GERROR;
    }

  return globals::OK;
  }











/*!
 \brief Close comms

 This method closes the comms if open.
****************************************************/
void BertComms::close()
  {
  if (isOpen)
    {
    CloseHandle(hSerial);
    isOpen = globals::GFALSE;
    }
  }















/*!
 \brief Send I2C command

  If comms are not opened, the method does nothing.
  This method is used for both the 'read' and 'write' method., to send
  a command to the I2C adaptor

 \param isRead           Set to 1 if this is a read operation, or 0 for a write.
                         This value is used to set the lowest bit of the slave address.
 \param registerAddress  16 bit address of register to write to
 \param nBytes           Number of bytes to write from registerData.

 \return globals::OK             Data written
 \return globals::GERROR          Error (Error writing data)
 \return globals::NOT_CONNECTED  Error (comms not open)
***************************************************************/
int BertComms::i2cCommand(const uint8_t isRead, const uint16_t registerAddress, const uint8_t nBytes)
  {
  if (!isOpen) return globals::NOT_CONNECTED;  // Comms not open!

  uint8_t i2cData[5];
  i2cData[0] = I2C_AD2;                 // USB-I2C adaptor command (WRITE with 2 byte reg. address)
  i2cData[1] = slaveAddress + isRead;   // Address of device + R/W bit
  i2cData[2] = registerAddress >> 8;    // Register Address - High byte
  i2cData[3] = registerAddress & 0x0F;  // Register Address - Low byte
  i2cData[4] = nBytes;                  // Number of bytes to be read/written to/from registers

  BOOL bResult;
  DWORD writBytes = 0;
  bResult = WriteFile(          hSerial,
                       (LPCVOID)i2cData,     // Start of data to write
                         (DWORD)5,           // Number of bytes to write
                               &writBytes,   // Number of bytes that were written
                                NULL   );    // No overlapped structure

  if ( (FALSE == bResult) || (writBytes < nBytes) )
    {
    lastError = (uint32_t)GetLastError();
    return globals::GERROR;
    }

  return globals::OK;
  }







/*!
 \brief Platform-specific sleep command
 \param milliSeconds  Number of milliseconds to sleep for
*/
void BertComms::sleep(uint32_t milliSeconds)
  {
  Sleep( (DWORD)milliSeconds );
  }




