/*!
 \file   BertInterface.h
 \brief  BERT Hardware Interface Header
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/

#ifndef BERTINTERFACE_H
#define BERTINTERFACE_H

#include <QDebug>

#include <stdint.h>

#include "globals.h"
#include "BertComms.h"


/*!
 \brief BERT Interface Class

 This class implements the interface to the BERT chip, at
 the level of running macros or getting/setting registers
 (as used by BertComms).

 This class uses BertComms to actually connect to the board
 via whatever low level connection is used (e.g. USB to I2S
 adaptor).

*/
class BertInterface
{

  public:
    BertInterface(BertComms *comms, const uint8_t *i2cAddresses, const size_t boardCount);
    ~BertInterface();

    // Special "Broadcast" lane number for applying setRegister to ALL lanes on board:
    static const uint8_t ALL_LANES = 5;

    int   interfaceIsOpen() const { return bertComms->portIsOpen(); }

    int runMacro( const uint8_t  board,
                  const uint8_t  code,
                  const uint8_t *dataIn,
                  const uint8_t  dataInSize,
                        uint8_t *dataOut,
                  const uint8_t  dataOutSize );

    int runLongMacro( const uint8_t  board,
                      const uint8_t  code,
                      const uint8_t *dataIn,
                      const uint8_t  dataInSize,
                            uint8_t *dataOut,
                      const uint8_t  dataOutSize,
                      const uint16_t timeoutMs );

    int runSimpleMacro( const uint8_t board,
                        const uint8_t code,
                        const uint8_t dataIn );

    int runVerySimpleMacro( const uint8_t board,
                            const uint8_t code );


    int setRegister( const uint8_t board, const uint8_t lane, const uint8_t address, const uint8_t  data );
    int getRegister( const uint8_t board, const uint8_t lane, const uint8_t address,       uint8_t *data );

    int setRegister16( const uint8_t board, const uint16_t address, const uint8_t data );
    int getRegister16( const uint8_t board, const uint16_t address, uint8_t *data );


    int rawWrite24( const uint8_t board,
                    const uint8_t addressHi,
                    const uint8_t addressMid,
                    const uint8_t addressLow,
                    const uint8_t *data,
                    const uint8_t nBytes );

    int rawRead24( const uint8_t board,
                   const uint8_t addressHi,
                   const uint8_t addressMid,
                   const uint8_t addressLow,
                   uint8_t *data,
                   const size_t nBytes );

  private:

    BertComms *bertComms;
    uint8_t    nBoards;
    uint8_t   *i2cAddress = NULL;

    static const int BUSY_TIMEOUT = 0;  // milliseconds: time to wait if comms are busy

};

#endif // BERTINTERFACE_H
