/*!
 \file   BertCommands.h
 \brief  BERT Functional Commands Class Implementation
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/

#include <QDebug>
#include <QFile>
#include <QDir>
#include <QSerialPort>
#include <QTimer>

#include <string.h>
#include <math.h>

#include "BertComms.h"
#include "globals.h"

#include "mainwindow.h"

#include "BertCommands.h"

// Macro: Display a message in the debug log and return, if code is NOT globals::OK
#define RETURN_ON_ERROR(code, message) if (result != globals::OK) { qDebug() << message << " (Err:" << code << ")"; return result; }

// Macro for converting "lane" (0-7) to board id and lane id as used by BertInterface
#define BORD_ID  (lane / 4)
#define LANE_ID  (lane % 4)


const double BertCommands::EXT_CLOCK_FREQ_MIN =  0.5;   // GHz
const double BertCommands::EXT_CLOCK_FREQ_MAX = 14.5;   // GHz


BertCommands::BertCommands(BertWindow *useParentWindow, BertInterface *useBertInterface)
{
    parentWindow = useParentWindow;
    bertInterface = useBertInterface;
}



BertCommands::~BertCommands()
{ }



/*****************************************************************************************
   Connect / Disconnect
 *****************************************************************************************/
/*!
 \brief Connect to instrument
 Serial port should already be open (via BertComms class)
 This method checks the macro version (read from board) and downloads macro data
 from a .hex file.
 It DOES NOT set any instrument default settings - See configSetDefaults.

 \param coldStart     Pointer to an integer. Set to TRUE if this is the
                      first connect after the instrument was powered on
                      (i.e. the Macro file had to be loaded).
                      Set to FALSE if the macros were already loaded.

 \return globals::OK         Success. Now connected.
 \return globals::GEN_ERROR  Error. Comms not open.
*/
int BertCommands::connect(int *coldStart)
{
    int result;
    *coldStart = TRUE;
    if (!bertInterface->interfaceIsOpen()) return globals::GEN_ERROR;

    // Check whether the extension macros have been loaded yet:
    int resultMaster, resultSlave;
    macroCheck(&resultMaster, &resultSlave);
    qDebug() << "Macro Check Result: Master: " << resultMaster << "; Slave: " << resultSlave;
    // Check result for Master board - Must be present:
    if (resultMaster == globals::MACRO_ERROR)
    {
        qDebug() << "Error reading macro version from MASTER board. Comms error?";
        parentWindow->updateStatus( "Comms Error!");
        disconnect();
        return globals::GEN_ERROR;
    }
    // Check result for Slave board. If error occurred, assume no slave:
    if (resultSlave == globals::MACRO_ERROR)
    {
        qDebug() << "Couldn't read macro version from SLAVE board. Assuming single board system.";
        maxBoard = 0;
        // DEPRECATED bertInterface->setNBoards(1);
    }
    else
    {
        qDebug() << "Slave board found.";
        maxBoard = 1;
    }

    if ( resultMaster == globals::MACROS_LOADED && ((hasSlaveBoard() && resultSlave == globals::MACROS_LOADED) || !hasSlaveBoard()) )
    {
        qDebug() << "Extension macros already loaded. Connected OK!";
        parentWindow->updateStatus( "Comms Open.");
        *coldStart = FALSE;
        return globals::OK;
    }
    //--------------------------------------------------------------

    parentWindow->updateStatus( "Downloading utilities macro... ");
    parentWindow->repaint();

    result = downloadHexFile(hasSlaveBoard());

    if (result != globals::OK)
    {
        if (result == globals::FILE_ERROR) parentWindow->updateStatus("Couldn't read utilities macro file!");
        else                               parentWindow->updateStatus("Couldn't download utilities macro file!");
        *coldStart = TRUE;
        return globals::GEN_ERROR;
    }

    // Run the macro check again to verify the macro download:
    macroCheck(&resultMaster, &resultSlave);
    qDebug() << "Macro Recheck Result: Master: " << resultMaster << "; Slave: " << resultSlave;
    if (resultMaster != globals::MACROS_LOADED || (hasSlaveBoard() && resultSlave != globals::MACROS_LOADED))
    {
        qDebug() << "Error downloading extension macros!";
        parentWindow->updateStatus( "Error downloading macros!" );
        *coldStart = TRUE;
        return globals::GEN_ERROR;
    }

    parentWindow->updateStatus( "Comms Open.");
    return globals::OK;
}
/*****************************************************************************************/
/*!
 \brief Disconnect from instrument
*/
void BertCommands::disconnect()
{}
/*****************************************************************************************/



/*!
 \brief Comms Check - Read and write registers on all boards for testing comms
 \return <  0  Error code from initial register read
 \return >= 0  Number of errors during write / read tests. Should be 0.
*/
int BertCommands::commsCheck()
{
    // Repeated Register Write / Read to register 1 (LOS threshold):
    // 0 - 63
    qDebug() << "Comms Check:";

    uint8_t board, lane;
    uint8_t i, baseValue, setValue;
    int result, countGood, countError;

    // Get current register contents
    uint8_t initValue[maxBoard+1][4] = {0};
    for (board = 0; board <= maxBoard; board++)
    {
        for (lane = 0; lane < 4; lane++)
        {
            result = bertInterface->getRegister(board, lane, 1, &initValue[board][lane]);
            if (result == globals::OK)
            {
                qDebug() << QString(" Initial Read: B%1 L%2 R1 Got: %3: OK").arg(board).arg(lane).arg(initValue[board][lane]);
            }
            else
            {
                qDebug() << " Error reading initial value: " << result;
                countError = result;
                goto finished;
            }
        }
    }

    countGood = 0;
    countError = 0;
    baseValue = (initValue[0][0] & 0xC0);  // Mask out top 2 bits (preserve)

    // CONTIGUOUS READ / WRITE Test: Read/write each lane on each board in sequence:
    for (board = 0; board <= maxBoard; board++)
    {
        for (lane = 0; lane < 4; lane++)
        {
            for (i = 0; i < 64; i++)
            {
                setValue = baseValue + i;
                result = commsCheckOneRegister(board, lane, setValue, countGood, countError);
            }
        }
    }
    // INTERLEAVED READ / WRITE Test: Read/write alternating between boards:
    for (lane = 0; lane < 4; lane++)
    {
        for (i = 0; i < 64; i++)
        {
            setValue = baseValue + i;
            for (board = 0; board <= maxBoard; board++)
            {
                result = commsCheckOneRegister(board, lane, setValue, countGood, countError);
            }
        }
    }

  finished:

    qDebug() << "Test Finished. " << countGood << " OK; " << countError << " ERRORS";

    // Restore register values:
    for (board = 0; board <= maxBoard; board++)
    {
        for (lane = 0; lane < 4; lane++)
        {
            bertInterface->setRegister(board, lane, 1, initValue[board][lane]);
        }
    }

    return countError;
}

/*!
 \brief Comms Check: Write to and read back ONE register on a bert board (uses register 1).
        Used by commsCheck for debugging / testing comms
 \param board
 \param lane
 \param value
 \param countGood
 \param countError
 \return
*/
int BertCommands::commsCheckOneRegister(const uint8_t board, const uint8_t lane, uint8_t value, int &countGood, int &countError)
{
    uint8_t gotValue;
    int result;
    result = bertInterface->setRegister(board, lane, 1, value);
    if (result == globals::OK)
    {
        qDebug() << QString(" B%1 L%2 R1 Set: %3: OK").arg(board).arg(lane).arg(value);
    }
    else
    {
        qDebug() << QString(" B%1 L%2 R1: Error setting register: %3").arg(board).arg(lane).arg(result);
        countError++;
        return result;
    }
    result = bertInterface->getRegister(board, lane, 1, &gotValue);
    if (result == globals::OK)
    {
        if (gotValue == value)
        {
            qDebug() << QString(" B%1 L%2 R1 Get: %3: OK").arg(board).arg(lane).arg(gotValue);
            countGood++;
        }
        else
        {
            qDebug() << QString(" B%1 L%2 R1 Get: %3: Unexpected value!").arg(board).arg(lane).arg(gotValue);
            countError++;
        }
    }
    else
    {
        qDebug() << QString(" B%1 L%2 R1: Error reading back register: %3").arg(board).arg(lane).arg(result);
        countError++;
    }
    return result;
}



/*!
 \brief Check whether the extension macros have been loaded
 Uses the 'Query Macro Version' macro for each board, and checks
 that the output data is a known macro version.
 \param resultMaster OUT Returns the results of the macro check on the MASTER board
 \param resultSlave  OUT Returns the results of the macro check on the SLAVE board

 Possible results: globals::MACROS_LOADED      Macros have been loaded
                   globals::MACROS_NOT_LOADED  Macros not loaded yet.
                   globals::MACRO_ERROR        Error running macro - board not connected?
*/
void BertCommands::macroCheck(int *resultMaster, int *resultSlave)
{
    uint8_t resultData[4] = { 0 };
    int macroStatus[2];
    uint8_t board;
    for (board = 0; board < 2; board++)
    {
        macroStatus[board] = globals::MACROS_NOT_LOADED;  // Default status... Couldn't confirm that macros are loaded.

        int result = bertInterface->runMacro( board, 0x18, NULL, 0, resultData, 4 );       // Query macro version
        if (result != globals::OK)
        {
            qDebug() << "Error running macro check on board " << board << " (" << result << ") - board not present?";
            macroStatus[board] = globals::MACRO_ERROR;
            continue;
        }
        qDebug() << "Macro version read OK on board " << board;
        // Check macro version data:
        size_t macroIndex;
        for (macroIndex = 0; macroIndex < globals::N_MACRO_FILES; macroIndex++)
        {
            if (  (resultData[0] == globals::MACRO_FILES[macroIndex].macroVersion[0]) &&
                  (resultData[1] == globals::MACRO_FILES[macroIndex].macroVersion[1]) &&
                  (resultData[2] == globals::MACRO_FILES[macroIndex].macroVersion[2]) &&
                  (resultData[3] == globals::MACRO_FILES[macroIndex].macroVersion[3])  )
            {
                macroVersion[board] = &(globals::MACRO_FILES[macroIndex]);
                macroStatus[board] = globals::MACROS_LOADED;
                qDebug() << "Found extension macros version " << macroVersion[board]->macroVersionString;
                break;
            }
        }
        if (macroStatus[board] != globals::MACROS_LOADED)
        {
            // Couldn't match the macro version we read from the chip to a known macro version.
            qDebug() << "Unknown extension macros version: "
                     << QString("[%1][%2][%3][%4]")
                        .arg(resultData[0], 2, 16, QChar('0'))
                        .arg(resultData[1], 2, 16, QChar('0'))
                        .arg(resultData[2], 2, 16, QChar('0'))
                        .arg(resultData[3], 2, 16, QChar('0'));
            qDebug() << "This probably means extension macros haven't been downloaded yet.";
        }
    }
    *resultMaster = macroStatus[0];
    *resultSlave = macroStatus[1];
}
/*****************************************************************************************/



/* DEPRECATED
 \brief Lock command interface
 Prevents other calls (e.g. timer deivern events) from interrupting a sequence of commands.

 \param timeout  Time to wait (milliseconds) for interface to be free

 \return true    Locked OK
 \return false   Already locked by another operation
bool BertCommands::commandsLock(int timeout)
{
    >>> TODO!!!  Delegate to BertComms module?
    TODO return bertInterface->interfaceLock(timeout);
}

*
 \brief Free command interface
*
void BertCommands::commandsFree()
{
    >>> TODO!!!  Delegate to BertComms module?
    TODO bertInterface->interfaceFree();
}
*/





/*****************************************************************************
   BERT Commands
******************************************************************************/

/*!
  \brief Configure the pattern generator

  Call after a change to:
    * Pattern
    * Clock synth frequency
    * Clock synth RF output level
  This method resets the pattern generator and set up the lanes. It selects
  the requested pattern and turns on the output drivers.
  Note this method DOESN'T set various other parameters which can be
  altered from the PG page while the PG is running, e.g. Output Swing,
  pattern inversion, or De-Emphasis controls. These are set to default values
  by configSetDefaults and are NOT reset on pattern change.

 \param board            Board to control: 0 = Master; 1 = Slave
 \param pattern          Index of selected pattern in the combo list.

 \return globals::OK            Success
 \return globals::INVALID_BOARD Invalid board number: E.g. tried to use slave '1' but no slave present.
 \return [error code]           Error setting register or running macro
*/
int BertCommands::configPG(const uint8_t board, const uint8_t pattern, const int forceCDRBypass)
{
    Q_UNUSED(forceCDRBypass)
    if (board > maxBoard) return globals::INVALID_BOARD;
    qDebug() << "Configure PG " << board;

    const uint8_t laneOffset = board * 4; // Lane base id derived from board: 0, 4, 8...

    int result;

    // REMOVED! Shouldn't need?
    //result = bertInterface->runVerySimpleMacro( board, 0x65 );       // --- RESET the PG (Macro 0x65)
    //RETURN_ON_ERROR(result, "Config PG: Reset failed");              //

    result = bertInterface->runSimpleMacro( board, 0x60,  0x01 );    // --- Mission Low Power
    RETURN_ON_ERROR(result, "Config PG: Mission Low Power failed");  //

    // Route LB input to PRBS gen (External clock in):
    result = bertInterface->runSimpleMacro( board, 0x60,  0x08 );    // --- LB to PRBS gen
    RETURN_ON_ERROR(result, "Config PG: LB to PRBS gen failed");     //

    // Run device multi-cast mode to route PRBS.
    // PRBS to channels 0 and 2: Nb: Assume lanes 1 and 3 will get data from inputs!
    // Nb: Calling with 0x05 (LB to CDR): Shouldn't be needed.
    // result = bertInterface->runSimpleMacro( board, 0x64,  0x05 );    // --- Config device multi-cast mode: Lanes 0 and 2 get LB to CDR
    // RETURN_ON_ERROR(result, "Config PG: Lane setup failed");         //
    result = bertInterface->runSimpleMacro( board, 0x64,  0x15 );    // --- Config device multi-cast mode: Lanes 0 and 2 get PRBS Gen to CDR
    RETURN_ON_ERROR(result, "Config PG: Lane setup failed");         //

    result                            = setPDDeEmphasis(0 + laneOffset, 0);  // --- Power up the De-Emphasis path
    if (result == globals::OK) result = setPDDeEmphasis(1 + laneOffset, 0);  //
    if (result == globals::OK) result = setPDDeEmphasis(2 + laneOffset, 0);  //
    if (result == globals::OK) result = setPDDeEmphasis(3 + laneOffset, 0);  //
    RETURN_ON_ERROR(result, "Config PG: Error turning on De-Emphasis path");

    // REMOVED: Stops the PRBS checker from working.
    //result                            = setPDDeEmphasis(1 + laneOffset, 1);  // --- Power DOWN de-emphasis and output driver on lane 1 and 3 (UNUSED output)
    //if (result == globals::OK) result = setPDDeEmphasis(3 + laneOffset, 1);
    //RETURN_ON_ERROR(result, "Config PG: Error powering down de-emphasis path for lane 1 or 3");

    // REMOVED: We don't mute the output lanes, because it stops the
    // PRBS checker from giving meaningful results (undocumented feature?)
    //result                            = setLaneMute(1 + laneOffset, 1, 0);   // --- MUTE outputs for lanes 1 and 3 (these are inputs to the ED)
    //if (result == globals::OK) result = setLaneMute(3 + laneOffset, 1, 0);   //     NOTE: DO NOT POWER DOWN; powering down these lanes stops the PRBS checker!
    //RETURN_ON_ERROR(result, "Config PG: Error muting outputs for lane 1 or 3");

    // Power down main path driver for lanes 1 and 3 (unused outputs; reduces noise)
    result                            = setPDLaneMainPath(1 + laneOffset, 1);  // --- Power DOWN Main path driver on lane 1 and 3 (UNUSED output)
    if (result == globals::OK) result = setPDLaneMainPath(3 + laneOffset, 1);
    RETURN_ON_ERROR(result, "Config PG: Error powering down de-emphasis path for lane 1 or 3");

    result = setLosEnable(board, 1);                                // --- Turn on LOS detect power
    RETURN_ON_ERROR(result, "Config PG: Failed to turn on LOS detect");

    qDebug() << "Change PG pattern to " << pattern;
    result = setPRBSOptions(board, pattern, 0, 0, 1);               // --- Select PG pattern (Macro 0x58)
       // vcoFreq=0, clockSource=0, enable=1
    RETURN_ON_ERROR(result, "Config PG: Error setting PG pattern");

    // Turn on CDR bypass for PG channels
    // (CDR Not used, and can interfere with signals if they are outside the range it can lock):
    /* DISABLED!
    if (forceCDRBypass != 0)
    {
        result                            = setForceCDRBypass(0 + laneOffset,TRUE);
        if (result == globals::OK) result = setForceCDRBypass(2 + laneOffset,TRUE);
    }
    else
    {
        result                            = setForceCDRBypass(0 + laneOffset,FALSE);
        if (result == globals::OK) result = setForceCDRBypass(2 + laneOffset,FALSE);
    }
    RETURN_ON_ERROR(result, "Config PG: Error setting CDR Bypass");
    */

    qDebug() << "PG Configured OK!";
    return globals::OK;
}



/*!
 \brief Load default settings - Run ONCE after connect

 This method sets various PG parameters, e.g. output swing and
 De-Emphasis. It also sets up the pattern generator with the
 default pattern and starts it (by calling config PG).
 \return globals::OK         Success
 \return globals::GEN_ERROR  Error
*/
int BertCommands::configSetDefaults()
{
    int result;
    uint8_t board;
    for (board = 0; board <= maxBoard; board++)
    {
        qDebug() << "Load PG Defaults for board " << board;

        uint8_t laneOffset = board * 4; // Lane base id derived from board: 0, 4, 8...

        result = configPG(board, 2, 0);                                             // --- Set up pattern generator (default pattern PRBS31; Force CDR  Bypass is OFF)
        RETURN_ON_ERROR(result, "Config Set Defaults: PG may not be correctly set up!");

        // For each board:
        uint8_t swingData[4] = { 0xA0, 0xA0, 0xA0, 0xA0 };                          // --- Configure output driver main swing to 800 mV (all lanes)... Lane value = [lane mV] / 5; 0xA0 = 160d = 800 mV
        result = bertInterface->runMacro( board, 0x61, swingData, 4, NULL, 0 );     //
        RETURN_ON_ERROR(result, "Config Set Defaults: Failed to set output swing");

        result                            = setDeEmphasis(0 + laneOffset, 5, 0);    // --- Set default De-Emphasis to 1.9 dBm of Post-cursor
        if (result == globals::OK) result = setDeEmphasis(2 + laneOffset, 5, 0);    //
        RETURN_ON_ERROR(result, "Config Set Defaults: Failed to set up de-emphasis");

        //result                            = setLaneMute(0 + laneOffset, 0, 0);      // --- Enable / unmute outputs for lanes
        //if (result == globals::OK) result = setLaneMute(2 + laneOffset, 0, 0);      //
        //RETURN_ON_ERROR(result, "Config Set Defaults: Failed to unmute outputs");
    }

    qDebug() << "PG default setup completed OK!";
    return globals::OK;
}


/************* Channel ON / OFF: ***************************************/
/*!
 \brief Set Lane Mute On / Off
 \param lane    Lane to set (0 - 7);
 \param muteOn  Mute status; FALSE = Normal; TRUE = Mute.
 \param powerDownOnMute Also power down the output driver when muted
                       (NOTE: This will also disable PRBS checker for the lane!)
 \return globals::OK      Success
 \return [Error Code]     Error from hardware/comms functions
*/
int BertCommands::setLaneMute(const uint8_t lane, const int muteOn, const int powerDownOnMute)
  {
  qDebug() << "Setting Lane " << lane << " mute options - mute: " << muteOn << "; powerDownOnMute: " << powerDownOnMute;
  // Get current register contents (Nb: From docs, shouldn't change bits 3-7):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_0, &data);
  //qDebug() << "Previous Value: ";
  //qDebug() << "  GTREG_DRV_REG_0: " << data;
  data = (data & 0xF8);  // Mask out lower 3 bits, = 0b11111000
  if (powerDownOnMute) data = (data | 0x04);  // Set bit 2 if "Power Down On Mute" is specified.
  if (muteOn)          data = (data | 0x01);  // Set bit 0 if "Mute" is specified.
  return bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_0, data);
  }
/*!
 \brief Read Lane Mute
 \param lane    Lane to read (0 - 7)
 \return 1  Channel is muted
 \return 0  Channel is not muted
*/
uint8_t BertCommands::getLaneMute(const uint8_t lane)
  {
  // qDebug() << "Checking Lane " << lane << " MUTE...";
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_0, &data);
  return (data & 0x01);   // Return only the value of Bit 0 (mute)
  }





/************* CDR Auto-Bypass on LOL: ************************************/
/*!
 \brief Set CDR Auto Bypass On LOL bit
 \param lane           Lane to set (0 - 7);
 \param autoBypassOn   Autobypass status - TRUE = On
 \return globals::OK   Success
 \return [Error Code]  Error from hardware/comms functions
*/
int BertCommands::setAutoBypassOnLOL (const uint8_t lane, const int autoBypassOn )
  {
  qDebug() << "Setting Lane " << lane << " CDR Auto Bypass on LOL to " << autoBypassOn;
  // Get current register contents - Auto bypass is Bit 1
  // (Nb: From docs, should preserve values of other bits):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_CDR_REG_0, &data);
  //qDebug() << "Previous Value: ";
  //qDebug() << "  GTREG_CDR_REG_0: " << data;
  data = (data & 0xFD);  // Mask out bit 1, = 0b11111101
  int result;
  if (autoBypassOn) result = bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_CDR_REG_0, (data | 0x02) );  // Auto bypass ON
  else              result = bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_CDR_REG_0, (data)        );  // Auto bypass OFF
  return result;
  }
/*!
 \brief Read CDR Auto Bypass On LOL bit
 \param lane    Lane to read (0 - 7)
 \return 1  CDR Auto Bypass on LOL is ON
 \return 0  CDR Auto Bypass on LOL is OFF
*/
uint8_t BertCommands::getAutoBypassOnLOL (const uint8_t lane)
  {
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_CDR_REG_0, &data);
  return ( (data & 0x02) >> 1 );   // Return only the value of Bit 1 (Auto Bypass bit)
  }

/************* CDR Force Bypass: ***************************************/
/*!
 \brief Set CDR Force Bypass bit
 \param lane           Lane to set (0 - 7);
 \param forceBypassOn  Force Bypass status - TRUE = On
 \return globals::OK   Success
 \return [Error Code]  Error from hardware/comms functions
*/
int BertCommands::setForceCDRBypass(const uint8_t lane, const int forceBypassOn)
  {
  qDebug() << "Setting Lane " << lane << " CDR Force Bypass to " << forceBypassOn;
  // Get current register contents - Force bypass is Bit 0
  // (Nb: From docs, should preserve values of other bits):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_CDR_REG_0, &data);
  //qDebug() << "Previous Value: ";
  //qDebug() << "  GTREG_CDR_REG_0: " << data;
  data = (data & 0xF8);  // Mask out bit 0, = 0b11111000
  int result;
  if (forceBypassOn) result = bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_CDR_REG_0, (data | 0x05) );  // bypass ON; CDR power DOWN
  else               result = bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_CDR_REG_0, (data)        );  // bypass OFF
  return result;
  }
/*!
 \brief Read CDR Force Bypass bit
 \param lane    Lane to read (0 - 7)
 \return 1  CDR Force Bypass is ON
 \return 0  CDR Force Bypass is OFF
*/
uint8_t BertCommands::getForceCDRBypass (const uint8_t lane)
  {
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_CDR_REG_0, &data);
  return (data & 0x01);   // Return only the value of Bit 0 (Force Bypass bit)
  }







/*************** Output driver swing: *************************************/
/*!
 \brief Set output driver swing
 \param lane    Lane to set (0 - 7)
 \param swing   New voltage swing (millivolts, in the range 200 - 1100).
 \return globals::OK           Success
 \return globals::BAD_LANE_ID  Lane ID was invalid
 \return [Error Code]          Error from hardware/comms functions
*/
int BertCommands::setOutputSwing(const uint8_t lane, const int swing)
  {
  if (lane > 7) return globals::BAD_LANE_ID;
  // We can only set ALL swing values (not a single lane), so first we
  // need to QUERY existing values, then update only the requested lane.
  int result;
  int swings[4];
  result = queryOutputDriverMainSwing(BORD_ID, swings); // Query existing swing values
  if (result != globals::OK)
    {
    qDebug() << "Error reading current output swings: " << result;
    return result;
    }
  // Now update the requested lane and set the new values:
  swings[LANE_ID] = swing;
  result = configOutputDriverMainSwing(BORD_ID, swings);
  if (result != globals::OK)
    {
    qDebug() << "Error setting new output swings: " << result;
    return result;
    }
  return globals::OK;
  }
/*!
 \brief Get output driver swing
 \param lane    Lane to set (0 - 7)
 \return swing  New voltage swing (millivolts, in the range 200 - 1100).
                0 indicates that there was an error reading the swing values.
*/
int BertCommands::getOutputSwing(const uint8_t lane)
  {
  if (lane > 7) return 0;
  int swings[4];
  int result = queryOutputDriverMainSwing(BORD_ID, swings); // Query existing swing values
  if (result == globals::OK) return swings[LANE_ID];
  else                       return 0;
  }
/*!
 \brief Run Config Output Driver Main Swing Macro
 \param  swings  Pointer to an array of 4 integers, used to set voltage swing.
                 Note the range of these values must be 200 to 1100, and the
                 resolution is 5 mV (i.e. 200, 205, 210,...). If a value is
                 outside this range it will be clamped to the min or max.
 \return globals::OK           Success
 \return [Error Code]          Error from hardware/comms functions
*/
int BertCommands::configOutputDriverMainSwing(const uint8_t board, const int swings[4])
{
    uint8_t swingData[4];
    // Convert supplied values (mV) to register settings (= mV/5, uint8_t)
    int i;
    for (i=0; i<4; i++) {
        swingData[i] = swings[i] / 5;
        if (swingData[i] < 40)  swingData[i] = 40;
        if (swingData[i] > 220) swingData[i] = 220;
        qDebug() << "Setting Output Swing Board " << board << " Lane " << i << " to " << swingData[i] << "( " << swings[i] << " mV)";
    }
    int result;
    result = bertInterface->runMacro(board, 0x61, swingData, 4, NULL, 0 );  // Config Output Driver Main Swing Macro
    return result;
}
/*!
 \brief Run Query Output Driver Main Swing Macro
 \param  swings  Pointer to an array of 4 integers, used to return the voltage swings.
                 Note the range of these values will be 200 to 1100, and the
                 resolution is 5 mV (i.e. 200, 205, 210,...).
                 If an error occurs, the values in swings will be set to 0.
 \return globals::OK      Success
 \return [Error Code]     Error from hardware/comms functions
*/
int BertCommands::queryOutputDriverMainSwing(const uint8_t board, int swings[4])
{
    // Default results: Set swings to 0 (in case of error...).
    int i;
    for (i=0; i<4; i++) swings[i] = 0;
    // Use the Query Output Driver Main Swing Macro:
    uint8_t swingData[4] = { 0 };
    int result;
    result = bertInterface->runMacro(board, 0x69, NULL, 0, swingData, 4 );
    if (result != globals::OK) return result;     //  Macro error...
    // Convert retrieved values to mV:
    for (i=0; i<4; i++)
    {
        swings[i] = swingData[i] * 5;
        qDebug() << "Read Output Swing Board " << board << " Lane " << i << ": " << swingData[i] << "( " << swings[i] << " mV)";
    }
    return globals::OK;
}












/********** Query / Configure PRBS Options: **************************/
/*!
 \brief Set PRBS Options
 Calls the Configure and Enable PRBS Generator Macro
 \param board    Board to control: 0 = Master; 1 = Slave, etc
 \param pattern  Pattern to use (0-7, see 1724 specs)
 \param vcoFreq  VCO frequency selection (0-7, see 1724 specs)
 \param source   Source (0 = External, 1 = Internal VCO)
 \param enable   Enable bit (0 = Disable, 1 = Enable)
 \return globals::OK            Success
 \return globals::INVALID_BOARD Invalid board number: E.g. tried to use slave '1' but no slave present.
 \return globals::OVERFLOW      Input parameter out of allowed range
 \return [Error Code]           Error from hardware/comms functions
*/
int BertCommands::setPRBSOptions(const uint8_t board, const uint8_t pattern, const uint8_t vcoFreq, const uint8_t source, const uint8_t enable )
{
    if (board > maxBoard) return globals::INVALID_BOARD;

    if ( (pattern > 7) ||
         (vcoFreq > 7) ||
         (source > 1)  ||
         (enable > 1) ) return globals::OVERFLOW;
    uint8_t prbsOption;
    prbsOption = (pattern << 5) | (vcoFreq << 2) | (source << 1) | enable;
    return bertInterface->runMacro(board, 0x58, &prbsOption, 1, NULL, 0);
}

/*!
 \brief Get Current PRBS Option Setting
 Calls the Query PRBS Generator Macro
 \param board    Board to read from: 0 = Master; 1 = Slave, etc
 \param pattern  uint8 pointer to return pattern (0-7, see 1724 specs)
 \param vcoFreq  uint8 pointer to return VCO frequency selection (0-7, see 1724 specs)
 \param source   uint8 pointer to return source (0 = External, 1 = Internal VCO)
 \param enable   uint8 pointer to return enable bit (0 = Disable, 1 = Enable)
 \return globals::OK           Success
 \return globals::INVALID_BOARD Invalid board number: E.g. tried to use slave '1' but no slave present.
 \return [Error Code]          Error from hardware/comms functions
*/
int BertCommands::getPRBSOptions(const uint8_t board, uint8_t *pattern, uint8_t *vcoFreq, uint8_t *source, uint8_t *enable)
{
    if (board > maxBoard) return globals::INVALID_BOARD;
    uint8_t prbsOption = 0;
    int result = bertInterface->runMacro(board, 0x59, NULL, 0, &prbsOption, 1);
    if (result != globals::OK) return result;
    *pattern = (prbsOption & 0xE0) >> 5;
    *vcoFreq = (prbsOption & 0x1C) >> 2;
    *source  = (prbsOption & 0x02) >> 1;
    *enable  = (prbsOption & 0x01);
    if (*vcoFreq < 2) *vcoFreq = 2;  // vcoFreq is 0 if external source selected. Clip to 2.
    return globals::OK;
}





/********** Query / Configure Error Detector Options: ******************/
/*!
 \brief Set ED Options
 Calls the Configure and Enable PRBS Checker Macro
 The function sets the options for both interlane checkers at the same time.
 \param board       Board to write to: 0 = Master; 1 = Slave, etc.
 \param edOptions   Pointer to an array of 2 pointers to edOptions_t structs
                    containing options for PRBS Checker 0 and 1 respectively.

 \return globals::OK         Success
 \return globals::OVERFLOW   Input parameter out of allowed range
 \return [Error Code]        Error from hardware/comms functions
*/
int BertCommands::setEDOptions(const uint8_t board, const edOptions_t *edOptions[2])
{
    uint8_t edOptionData[2];
    int result;

    edOptionData[0] = (edOptions[1]->pattern << 6) | (edOptions[1]->laneSelect << 5) | (edOptions[1]->enable << 4) |
                      (edOptions[0]->pattern << 2) | (edOptions[0]->laneSelect << 1) | (edOptions[0]->enable);
    edOptionData[1] = (edOptions[1]->invert << 1) | edOptions[0]->invert;

    qDebug() << "Enabling PRBS checker on board " << board
             << QString(" Data: B0 [0x%1] B1 [0x%2]")
                .arg(edOptionData[0], 2, 16, QChar('0'))
                .arg(edOptionData[1], 2, 16, QChar('0'));

    // Note: Enabling the PRBS checker with long patterns (e.g. PRBS31) may take up to 2 seconds
    // to complete, so we use bertInterface->runLongMacro here to specify a longer timeout.
    result = bertInterface->runLongMacro( board, 0x50, edOptionData, 2, NULL, 0, 5000 );
    if (result != globals::OK) return result;

    return globals::OK;
}

/*!
 \brief Get Current ED Option Setting
 Calls the Query PRBS Checker Macro
 \param board       Board to read from: 0 = Master; 1 = Slave, etc.
 \param edOptions   Pointer to an array of 2 pointers to edOptions_t structs.
                    These will be filled with the current settings for
                    PRBS Checkers 0 and 1 respectively.

 \return globals::OK           Success
 \return [Error Code]          Error from hardware/comms functions
*/
int BertCommands::getEDOptions(const uint8_t board, edOptions_t *edOptions[2])
{
    uint8_t edOptionsData[2] = {0,0};
    int result;

    result = bertInterface->runMacro( board, 0x51, NULL, 0, edOptionsData, 2 );
    if (result != globals::OK) return result;

    edOptions[0]->invert     = (edOptionsData[1])      & 0x01;
    edOptions[0]->pattern    = (edOptionsData[0] >> 2) & 0x03;
    edOptions[0]->laneSelect = (edOptionsData[0] >> 1) & 0x01;
    edOptions[0]->enable     = (edOptionsData[0])      & 0x01;

    edOptions[1]->invert     = (edOptionsData[1] >> 1) & 0x01;
    edOptions[1]->pattern    = (edOptionsData[0] >> 6) & 0x03;
    edOptions[1]->laneSelect = (edOptionsData[0] >> 5) & 0x01;
    edOptions[1]->enable     = (edOptionsData[0] >> 4) & 0x01;

    return globals::OK;
}

/*!
 \brief Error Detector Control
 Runs the Control PRBS Checker Enable macro, which turns the PRBS
 checker on or off without changing other settings.
 \param board      Board ID
 \param enableED0  Enable lane 0-1 (0 = Disable, 1 = Enable)
 \param enableED1  Enable lane 2-3 (0 = Disable, 1 = Enable)

 \return globals::OK           Success
 \return globals::OVERFLOW     Input parameter out of allowed range
 \return [Error Code]          Error from hardware/comms functions
*/
int BertCommands::controlED(const uint8_t board, const uint8_t enableED0, const uint8_t enableED1)
{
    // Note: Enabling the PRBS checker with long patterns (e.g. PRBS31) may
    // take up to 2 seconds to complete, so we use bertInterface->runLongMacro here to specify
    // a longer timeout.
    int result;
    uint8_t edEnable = (enableED1 << 4) | enableED0;
    result = bertInterface->runLongMacro(board, 0x52, &edEnable, 1, NULL, 0, 2500 );
    return result;
}


/******** Read Error Counts: ***********************/

/*!
 \brief Get Error Detector Counts
 Runs the Query PRBS Checker Reading macro twice for the specified
 ED lane, retrieving both the bit count and error count.
 Nb: Doesn't stop the error detector, so the two counts won't
 be from exactly the same time point.

 \param edLane  Selects the error detector to read (0 = Master L01; 1 = Master L23; 2 = Slave L01; 3 = Slave L23)
 \param bits    Used to return the bit count.
                Use NULL to skip bit count measurement
 \param errors  Used to return the error count
                Use NULL to skip error count measurement

 \return globals::OK           Success
 \return globals::OVERFLOW     Input parameter out of allowed range
 \return [Error Code]          Error from hardware/comms functions
*/
int BertCommands::getEDCount( const uint8_t edLane, double *bits, double *errors)
{
    uint8_t board, useEDLane;
    edLaneToBoardAndLane(edLane, &board, &useEDLane);
    if (board > maxBoard) return globals::OVERFLOW;

#ifdef BERT_ED_DEBUG
    qDebug() << "Get ED Count: edLane: " << edLane << "; board: " << board << "; useEDLane: " << useEDLane;
#endif

    uint8_t options;
    uint8_t output[2];
    int result;
    if (bits)
    {
        // Read the bit counter:
        *bits = 0;
        options = (0x01 << 1) | useEDLane;
        output[0] = 0; output[1] = 0;
        result = bertInterface->runLongMacro(board, 0x53, &options, 1, output, 2, 1000 );
        if (result != globals::OK) return result;
        // Convert output data to a double:
        *bits = edBytesToDouble(output);
#ifdef BERT_ED_EXTRA_DEBUG
        uint16_t value = (uint16_t)(output[0] << 8) | output[1];
        uint16_t exp   = value >> 10;
        uint16_t mant  = value & 0x03FF;
        double fValue = (double)mant  *  pow( 2.0, (double)exp );
        qDebug() << " -Read Bit count OK:";
        qDebug() << QString("  Raw Data: [0x%1][0x%2];  Exp:[0x%3] Mant:[0x%4] -> [0x%5]; Recalcd: %6; Calcd: %7")
                    .arg(output[0],2,16,QChar('0'))
                    .arg(output[1],2,16,QChar('0'))
                    .arg(exp,4,16,QChar('0'))
                    .arg(mant,4,16,QChar('0'))
                    .arg(value,4,16,QChar('0'))
                    .arg(fValue)
                    .arg(*bits);
#endif
    }
    if (errors)
    {
        *errors = 0;
        // Read the number of errors:
        options = useEDLane;
        output[0] = 0; output[1] = 0;
        result = bertInterface->runLongMacro(board, 0x53, &options, 1, output, 2, 1000 );
        if (result != globals::OK) return result;
        // Convert output data to a double:
        *errors = edBytesToDouble(output);
#ifdef BERT_ED_EXTRA_DEBUG
        uint16_t value = (uint16_t)(output[0] << 8) | output[1];
        uint16_t exp   = value >> 10;
        uint16_t mant  = value & 0x03FF;
        double fValue = (double)mant  *  pow( 2.0, (double)exp );
        qDebug() << " -Read Err count OK:";
        qDebug() << QString("  Raw Data: [0x%1][0x%2];  Exp:[0x%3] Mant:[0x%4] -> [0x%5]; Recalcd: %6; Calcd: %7")
                    .arg(output[0],2,16,QChar('0'))
                    .arg(output[1],2,16,QChar('0'))
                    .arg(exp,4,16,QChar('0'))
                    .arg(mant,4,16,QChar('0'))
                    .arg(value,4,16,QChar('0'))
                    .arg(fValue)
                    .arg(*errors);
#endif
    }
    return globals::OK;
}

/*!
 \brief Convert 2 bytes of data read from PRBS checker into a double
 Data are in 16 bit floating point format as described in
 GT1724 Data Sheet.

 \param bytes  Array of two bytes, as read from PRBS checker
               with the "Query PRBS Checker Reading" macro.
               Bytes in the array should be in the same order
               as the bytes returned by the macro.
 \return double  Converted value
*/
double BertCommands::edBytesToDouble( const uint8_t bytes[2] )
{
    uint16_t mantissa = ( (uint16_t)(bytes[0] & 0x03) << 8 ) | bytes[1];
    uint16_t exponent = (uint16_t)(bytes[0] >> 2);
    return (double)mantissa  *  pow( 2.0, (double)exponent );
}






/********* 'Inverted' On / Off: ***********************

 "Inversion" of the signal is controlled by the
 l#_eq_polarity_invert bit (l#_eq_reg_0 register).

 Note that the Query / Configure Device Multicast Mode
 macros also read / set this bit, along with doing
 other things in the case of the Configure macro.

*******************************************************/
/*!
 \brief Set Lane Inverted On / Off
 \param lane    Lane to set (0 - 7);

 \param inverted  Inverted status:  1 = Inverted;
                                    0 = NOT Inverted.

 \return globals::OK      Success
 \return [Error Code]     Error from hardware/comms functions
*/
int BertCommands::setLaneInverted(const uint8_t lane, const uint8_t inverted)
  {
  qDebug() << "Setting Lane " << lane << " inverted to " << inverted;

  /**** Set invert bit in REG_EQ_REG_0: **************/
  // Get current register contents (Nb: From docs, shouldn't change reserved bits):
  uint8_t dataER = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_EQ_REG_0, &dataER);
  dataER = (dataER & 0xFE);  // Mask out bit 0 (=0b11111110)
  int result;
  if (inverted == 1)
    {
    result = bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_EQ_REG_0, (dataER | 0x01) );  // Inverted (Nb: set bit 0 only)
    }
  else
    {
    result = bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_EQ_REG_0, dataER );  // Not Inverted
    }
  /********************************************************************/

  return result;
  }
/*!
 \brief Read 'Inverted' state
 \param lane    Lane to read (0 - 7)
 \return 1  Channel is inverted
 \return 0  Channel is not inverted
*/
uint8_t BertCommands::getLaneInverted(const uint8_t lane)
  {
  // qDebug() << "Checking Lane " << lane << " inverted...";
  /**** Method using REG_EQ_REG_0: *************/
  uint8_t dataER = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_EQ_REG_0, &dataER);
  dataER = (dataER & 0x01);  // Mask out bit 0
  return (dataER != 0);
  /**********************************************/
  // Get 'inverted' for lane using 'Query device multicast mode' macro:
  //uint8_t data = 0;
  //bertInterface->runMacro( 0x6C, NULL, 0, &data, 1 );
  //return (data >> 7);
  }






/********* Power Control for De-Emphasis Path: ***********************/
/*!
 \brief Set Power Down bit for DeEmphasis Path
 \param lane       Lane to set (0 - 7);
 \param powerDown  Power Down bit; Must be 0 or 1.
                       1 = power down de-emphasis path and de-emphasis output driver
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions
*/
int  BertCommands::setPDDeEmphasis (const uint8_t lane, const uint8_t powerDown)
  {
  qDebug() << "Setting lane " << lane << " de-emphasis power down bit to: " << powerDown;
  if (powerDown > 1) return globals::OVERFLOW;
  // Get current register contents (Nb: From docs, shouldn't change reserved bits):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, &data);
  data =  (data & 0xFD) |    // Use mask to clear bit 1 (need to preserve rest of register)
          (powerDown << 1);  // Add 'De-emph Power Down' bit (shift to place in bit 1)
  return bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, data );
  }

/*!
 \brief Get Power Down bit for DeEmphasis Path
 \param lane       Lane to read settings for (0-3)
 \param powerDown  Pointer to uint8; used to return Power Down bit
 \return globals::OK
*/
int  BertCommands::getPDDeEmphasis (const uint8_t lane, uint8_t *powerDown)
  {
  // qDebug() << "Checking Lane " << lane << " De-emphasis Power Down Settings...";
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, &data);
  *powerDown   = (data & 0x02) >> 1;  // Mask out all bits except 1, and shift right one to give deemph pd bit
  return globals::OK;
  }



/********* Power Control for Lane Output Driver: ***********************/
/*!
 \brief Set Power Down bit for entire lane output driver
 \param lane       Lane to set (0 - 7);
 \param powerDown  Power Down bit; Must be 0 or 1.
                       1 = power down lane output driver
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions
*/
int  BertCommands::setPDLaneOutput (const uint8_t lane, const uint8_t powerDown)
  {
  qDebug() << "Setting lane " << lane << " output driver power down bit to: " << powerDown;
  if (powerDown > 1) return globals::OVERFLOW;
  // Get current register contents (Nb: From docs, shouldn't change reserved bits):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, &data);
  data =  (data & 0xFE) |    // Use mask to clear bit 0 (need to preserve rest of register)
          (powerDown);       // Add 'Lane Power Down' bit (bit 0)
  return bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, data );
  }

/*!
 \brief Get Power Down bit for entire lane output driver
 \param lane       Lane to read settings for (0-7)
 \param powerDown  Pointer to uint8; used to return Power Down bit
 \return globals::OK
*/
int  BertCommands::getPDLaneOutput (const uint8_t lane, uint8_t *powerDown)
  {
  // qDebug() << "Checking Lane " << lane << " Ouput Power Down Settings...";
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, &data);
  *powerDown   = (data & 0x01);  // Mask out all bits except 0
  return globals::OK;
  }

/********* Power Control for Lane "Main Path": ***********************/
/*!
 \brief Set Power Down bit for lane's "main path".
        Note: this is based on undocumented information passed to us by
        Steven Buchinger <SBuchinger@semtech.com>
        "What I’ve found to work is to power down only the output driver of
         the main path. This is similar to 0x*42[0:0] but only powers down the
         main path driver. To do this, you can set 0x*42[3:3] = 1."

 \param lane       Lane to set (0 - 7);
 \param powerDown  Power Down bit; Must be 0 or 1.
                       1 = power down lane main path
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions
*/
int  BertCommands::setPDLaneMainPath (const uint8_t lane, const uint8_t powerDown)
  {
  qDebug() << "Setting lane " << lane << " main path power down bit to: " << powerDown;
  if (powerDown > 1) return globals::OVERFLOW;
  // Get current register contents (Nb: From docs, shouldn't change reserved bits):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, &data);
  data =  (data & 0xF7) |     // Use mask to clear bit 3 (need to preserve rest of register)
          (powerDown << 3);   // Add 'Main Path Power Down' bit (bit 3)
  return bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, data );
  }

/*!
 \brief Get Power Down bit for lane's "main path" - see above
 \param lane       Lane to read settings for (0-7)
 \param powerDown  Pointer to uint8; used to return Power Down bit
 \return globals::OK
*/
int  BertCommands::getPDLaneMainPath (const uint8_t lane, uint8_t *powerDown)
  {
  // qDebug() << "Checking Lane " << lane << " main path power down Settings...";
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_PD_REG_6, &data);
  *powerDown   = (data & 0x04) >> 3;  // Mask out all bits except 3
  return globals::OK;
  }





/********* De-Emphasis Level: ***********************/
/*!
 \brief Set De-Emphasis settings (De-emphasis level and pre/post cursor selection)
 \param lane     Lane to set (0 - 7);
 \param level    De-emphasis level (0-15)
 \param prePost  Pre / Post cursor option (0 = Post, 1 = Pre).
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions
*/
int BertCommands::setDeEmphasis(const uint8_t lane, const uint8_t level, const uint8_t prePost)
  {
  qDebug() << "Setting Lane " << lane << " de-emphasis; level: " << level << "prePost: " << prePost;
  if ( (level > 15) || (prePost > 1) ) return globals::OVERFLOW;
  // Get current register contents (Nb: From docs, shouldn't change reserved bits):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_2, &data);
  data =  (data & 0xE0) |   // Use mask to clear all but bits 5-7 (need to preserve these)
          (level << 1)  |   // Add 'level' bits (shift to place in bits 1-4)
          (prePost);        // Add Pre / Post (bit 0)
  return bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_2, data );
  }

/*!
 \brief Get De-Emphasis settings (De-emphasis level and pre/post cursor selection)
 \param lane     Lane to read settings for (0 - 7)
 \param level    Pointer to uint8, used to return De-emphasis level (0-15)
 \param prePost  Pointer to uint8, used to return Pre / Post cursor option (0 = Post, 1 = Pre).
 \return globals::OK
*/
int BertCommands::getDeEmphasis(const uint8_t lane, uint8_t *level, uint8_t *prePost)
  {
  // qDebug() << "Checking Lane " << lane << " De-emphasis Settings...";
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_2, &data);
  *level   = (data & 0x1E) >> 1;  // Mask out all bits except 1-4, and shift right one to give deemph_level
  *prePost = (data & 0x01);       // Pre / Post given by bit 0.
  return globals::OK;
  }






/********* Cross point adjust *********************************/
/*!
 \brief Set output driver crossing point adjust
 \param lane       Lane to set (0 - 7);
 \param crossPoint Cross point to use (0-18)
                   9 = Default (50%, i.e. CPA disabled)
                   Other values: 35% = 0
                                 40% = 3
                                 45% = 6
                                 50% = 9
                                 55% = 12
                                 60% = 15
                   See GT1724 data sheet for all values
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions
*/
int BertCommands::setCrossPoint(const uint8_t lane, const uint8_t crossPoint)
  {
  if (crossPoint > 18) return globals::OVERFLOW;
  qDebug() << "Setting cross point... level: " << crossPoint;
  // Get current register contents (Bb: From docs, shouldn't change bits 5-7):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_5, &data);
  //qDebug() << "Previous Value: ";
  //qDebug() << "  GTREG_DRV_REG_5: " << data;
  data = (data & 0xE0) | crossPoint;  // Mask out lower 5 bits, and OR with new cross point value:
  return bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_5, data);
  }

/*!
 \brief Get output driver crossing point adjust
 \param lane       Lane to read (0 - 7)
 \param crossPoint Pointer to a uint8 to store the read value
                   For values, see setCrossPoint method and
                   GT1724 data sheet
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions
*/
int BertCommands::getCrossPoint(const uint8_t lane, uint8_t *crossPoint)
  {
  // Get current register contents (Bb: From docs, shouldn't change bits 5-7):
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_DRV_REG_5, crossPoint);
  *crossPoint = (*crossPoint & 0x1F);   // Mask out upper 3 bits (reserved)
  qDebug() << "Read Cross Point for lane " << lane << ": " << *crossPoint;
  return globals::OK;
  }




/********* EQ Boost *********************************/
/*!
 \brief Set input stage eq boost
 \param lane       Lane to set (0 - 7);
 \param eqBoost    EQ Boost register value (0 - 127)
                   0 = Default (0 dB Boost)
                   Other values: 0   = 0dB
                                 40  = 1.2dB
                                 59  = 2.4dB
                                 74  = 3.6dB
                                 87  = 4.8dB
                                 96  = 6.0dB
                                 105 = 7.2dB
                                 112 = 8.4dB
                                 118 = 9.6dB
                                 123 = 10.8dB
                                 127 = 12.0dB
                   See GT1724 data sheet
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions
*/
int BertCommands::setEQBoost(const uint8_t lane, const uint8_t eqBoost)
  {
  if (eqBoost > 127) return globals::OVERFLOW;
  qDebug() << "Setting EQ Boost... level: " << eqBoost;
  // Get current register contents (Bb: From docs, shouldn't change bit 7):
  uint8_t data = 0;
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_EQ_REG_2, &data);
  //qDebug() << "Previous Value: ";
  //qDebug() << "  GTREG_EQ_REG_2: " << data;
  data = (data & 0x80) | eqBoost;  // Mask out lower 7 bits, and OR with new EQ value
  return bertInterface->setRegister(BORD_ID, LANE_ID, GTREG_EQ_REG_2, data);
  }

/*!
 \brief Get input stage eq boost
 \param lane       Lane to read (0 - 7)
 \param eqBoost    Pointer to a uint8 to store the read value
                   For values, see setEQBoost method and
                   GT1724 data sheet
 \return globals::OK        Success
 \return [Error Code]       Error from hardware/comms functions
*/
int BertCommands::getEQBoost(const uint8_t lane, uint8_t *eqBoost)
  {
  // Get current register contents:
  bertInterface->getRegister(BORD_ID, LANE_ID, GTREG_EQ_REG_2, eqBoost);
  *eqBoost = (*eqBoost & 0x7F);   // Mask out upper bit (reserved)
  qDebug() << "Read EQ Boost for lane " << lane << ": " << *eqBoost;
  return globals::OK;
  }




/******** LOS and LOL ****************************************/

/*!
 \brief Set the pd_los_all bit and los_det_enable bits to enable or disable LOS
        Note: If two boards are present (master and slave), both are set.
 \param state  0 = Disable LOS and power down
               1 = Enable and power up
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions
*/
int BertCommands::setLosEnable(const uint8_t board, const uint8_t state)
{
    if (state > 1) return globals::OVERFLOW;
    qDebug() << "Setting board " << board << " setLosEnable to " << state;
    const uint8_t BIT_PD = (state == 0) ? 1 : 0;  // pd_los_all bit is opposite of state setting.
    uint8_t data; //, board;
    //    for (board = 0; board <= maxBoard; board++)
    //    {
    // ---- pd_los_all Bit (in pd_reg_1): ---------------
    // Get current register contents (Nb: From docs, shouldn't change reserved bits):
    data = 0;
    bertInterface->getRegister(board, 0, GTREG_PD_REG_1, &data);
    data =  (data & 0xFE) |   // Use mask to clear bit 0 (preserve other bits)
            (BIT_PD);         // Add new pd_los_all bit.
    int result = bertInterface->setRegister(board, BertInterface::ALL_LANES, GTREG_PD_REG_1, data);
    if (result != globals::OK) return result;
    // ---- los_det_enable Bit (in los_reg_0): ---------------
    data = 0;
    bertInterface->getRegister16(board, GTREG_LOS_REG_0, &data);
    data =  (data & 0xFC) |   // Use mask to clear bit 1,0 (preserve other bits)
            (state);          // Add new los_det_enable bit. (Nb: lp_mode bit always 0).
    result = bertInterface->setRegister16(board, GTREG_LOS_REG_0, data);
    if (result != globals::OK) return result;
    //    }
    return globals::OK;
}

/*!
 \brief Get the LOS and LOL latched register values for all lanes
 Note: If two boards are present (master and slave), both are read;
 In the output arrays (los and lol), Indicies 0-3 are for Master; 4-7 are for Slave.
 This method reads the register to get the current value, then writes
 0xFF to the register to clear all latches. (??)
 \param los  OUT: Pointer to an array of uint8_t values used to return results.
             index 0 = Lane 0 LOS value, index 1 = Lane 1 LOS value, etc
 \param lol  OUT: Pointer to an array of uint8_t values used to return results.
             index 0 = Lane 0 LOL value, index 1 = Lane 1 LOL value, etc
 \return globals::OK        Success
 \return [Error Code]       Error from hardware/comms functions
*/
int BertCommands::getLosLol(uint8_t los[8], uint8_t lol[8])
{
    uint8_t data = 0;
    int result;
    uint8_t useMaxBoard = maxBoard;
    if (useMaxBoard > 1) useMaxBoard = 1;   // Currently limited to 2 boards! TODO: add 'board' as a parameter to this method.

    uint8_t board;
    for (board = 0; board <= maxBoard; board++)
    {
#ifdef BERT_ED_EXTRA_DEBUG
        qDebug() << "Reading LOS and LOL: Board " << board;
#endif
        result = bertInterface->getRegister16(board, GTREG_LOSL_OUTPUT, &data);
        // qDebug() << "Result: " << result << "; Data (dec): " << data;
        if (result != globals::OK) return result;
        int i, arrIndex;
        for (i=0; i<4; i++)
        {
            arrIndex = (board * 4) + i;
            los[arrIndex] = (data >> i)     & 0x01;
            lol[arrIndex] = (data >> (i+4)) & 0x01;
        }
        // Only if using latched version:
        //return bertInterface->setRegister16(board, GTREG_LOSL_OUTPUT_LATCHED, 0xFF);
    }
#ifdef BERT_ED_EXTRA_DEBUG
    qDebug() << "LOS: M: [" << los[3] << "][" << los[2] << "][" << los[1] << "][" << los[0] << "] S: [" << los[7] << "][" << los[6] << "][" << los[5] << "][" << los[4] << "]";
    qDebug() << "LOL: M: [" << lol[3] << "][" << lol[2] << "][" << lol[1] << "][" << lol[0] << "] S: [" << lol[7] << "][" << lol[6] << "][" << lol[5] << "][" << lol[4] << "]";
#endif
    return globals::OK;
}




/********** Device lane mode ************************************/

/* DEPRECATED Needs to be modified to use lane id correctly with multiple boards


 \brief Runs the Configure Device Lane Mode macro (0x63) to change the
 signal routing to LB and MCK from a lane (see data sheet)
 \param lane         Lane to configure (0 - 7)
 \param mode         Lane mode (0 - 4):
                            0x00 = RECOVERED CLOCK TO MCK
                            0x01 = RECOVERED CLOCK TO LB
                            0x02 = RECOVERED CLOCK TO PRBS GEN
                            0x03 = RETIMED DATA TO LB
                            0x04 = RETIMED DATA TO LB INVERTED
 \param freqDivider  Frequency divide ratio for recovered clock
                     (0 - 5, applies to modes 0 and 1 only):
                            0 = 1/2 (default)
                            1 = 1/4
                            2 = 1/8
                            3 = 1/16
                            4 = 1/64
                            5 = 1/256
 \return globals::OK        Success
 \return globals::OVERFLOW  Input parameter out of allowed range
 \return [Error Code]       Error from hardware/comms functions

int BertCommands::configDeviceLaneMode( const uint8_t lane, const uint8_t mode, const uint8_t freqDivider )
{
    if ( (lane > 7) ||
         (mode > 4) ||
         (freqDivider > 5) ) return globals::OVERFLOW;  // Make sure parameters are valid.
    uint8_t devLane = LANE_ID;
    uint8_t data = (freqDivider << 5) | (mode << 2) | (devLane);
    return bertInterface->runSimpleMacro(BORD_ID, 0x63,  data );         // Configure device lane mode
}


 \brief Runs the Query Device Lane Mode macro (0x6B) to get the
 signal routing to LB and MCK from a lane (see data sheet)
 \param lane                Returns the configured lane (see configDeviceLaneMode)
 \param mode                Returns the lane mode (see configDeviceLaneMode)
 \param freqDivider         Returns the frequency divide ratio (see configDeviceLaneMode)
 \return globals::OK        Success
 \return [Error Code]       Error from hardware/comms functions

int BertCommands::queryDeviceLaneMode( uint8_t *lane, uint8_t *mode, uint8_t *freqDivider )
{
    uint8_t data = 0x00;
    *lane        = 0x00;
    *mode        = 0x00;
    *freqDivider = 0x00;
    int result = bertInterface->runMacro(0, 0x6B, NULL, 0, &data, 1 );
    if (result != globals::OK) return result;
    *lane        = data & 0x03;
    *mode        = (data >> 2) & 0x07;
    *freqDivider = (data >> 5) & 0x07;
    return globals::OK;
}
*/


/*!
 \brief Get IC Temperature from Gennum GTxxxx chip
 \param board
 \param temperatureDegrees  Pointer to an int, used to return temperature in degrees C
 \return [Error Code]       Error from hardware/comms functions
*/
int BertCommands::getTemperature(const uint8_t board, int *temperatureDegrees)
{
    if (board > maxBoard) return globals::OVERFLOW;
    uint8_t output[2] = { 0, 0 };
    int result;

    // Read the on-chip temperature sensor:
    *temperatureDegrees = 0;  // Default if read fails.
    result = bertInterface->runMacro(board, 0x28, NULL, 0, output, 2);
    if (result != globals::OK) return result;

    // Convert output data to degrees C:
    uint16_t tempRaw = ((uint16_t)output[0] << 8) | (uint16_t)output[1];
    if (tempRaw == 0) return globals::READ_ERROR;  // Improbable result!

    *temperatureDegrees = (int)( ((float)tempRaw * 0.415) - 277.565 );

#define BERT_EXTRA_DEBUG
#ifdef BERT_EXTRA_DEBUG
    qDebug() << "Read Chip Temperature OK for board: " << board;
    qDebug() << QString("  Raw Data: [0x%1][0x%2]; Raw Int: %3; Deg C: %4")
                .arg(output[0],2,16,QChar('0'))
                .arg(output[1],2,16,QChar('0'))
                .arg(tempRaw)
                .arg(*temperatureDegrees);
#endif

    return globals::OK;
}





int BertCommands::debugPowerStatus(const uint8_t lane)
{
    int result;
    uint8_t data;
    qDebug() << "Lane Power Debug: Lane " << lane << "(Board: " << BORD_ID << "; Lane Id: " << LANE_ID << ")";

    result = bertInterface->getRegister(BORD_ID, LANE_ID, 0, &data);
    qDebug() << " -cdr_reg_0 (0) : " << result << ": " << QString("0x%1").arg(data,2,16,QChar('0'));

    result = bertInterface->getRegister(BORD_ID, LANE_ID, 50, &data);
    qDebug() << " -drv_reg_0 (50): " << result << ": " << QString("0x%1").arg(data,2,16,QChar('0'));

    result = bertInterface->getRegister(BORD_ID, LANE_ID, 60, &data);
    qDebug() << " -pd_reg_0  (60): " << result << ": " << QString("0x%1").arg(data,2,16,QChar('0'));

    result = bertInterface->getRegister(BORD_ID, LANE_ID, 61, &data);
    qDebug() << " -pd_reg_1  (61): " << result << ": " << QString("0x%1").arg(data,2,16,QChar('0'));

    result = bertInterface->getRegister(BORD_ID, LANE_ID, 63, &data);
    qDebug() << " -pd_reg_3  (63): " << result << ": " << QString("0x%1").arg(data,2,16,QChar('0'));

    result = bertInterface->getRegister(BORD_ID, LANE_ID, 66, &data);
    qDebug() << " -pd_reg_6  (66): " << result << ": " << QString("0x%1").arg(data,2,16,QChar('0'));

    return result;
}









/******************************************************************************************
 * PRIVATE Methods - Implement the BERT commands
 ******************************************************************************************/


/*!
 \brief Load a hex file from disk and convert to binary array
 Uses Intel hex file format

 NOTE: This is a very simple implementation of a hex file reader.
 It is designed to support the macro file used by the GT1724;
 It DOESN'T fully implement all HEX file features!

 \param includeSlave  Also download macro data to slave board

 \return globals::OK              Macros downloaded OK.
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::FILE_ERROR      Error reading macro file
 \return globals::WRITE_ERROR     Write error sending command / data to USB-I2C module
 \return globals::READ_ERROR      Read error - Error reading data back from USB-I2C module
 \return globals::OVERFLOW        Error (> 64 data bytes in a line)

*/
int BertCommands::downloadHexFile(const bool includeSlave)
{
    QString fileName;
    size_t fileIndex;
    size_t fileLines;
    int result;
    bool isOpen = false;
    QFile hexFile;
    QString useMacroVersion;

#ifdef BERT_MACRO_VERSION
    // If the build defines a default macro version to use, search for that file in the resources:
    useMacroVersion = QString(XSTR(BERT_MACRO_VERSION));
    qDebug() << "Looking for Macro Version: " << useMacroVersion;
#else
    // No default macro version. Use the first file we find from the list in globals::MACRO_FILES.
    useMacroVersion = QString("");
#endif

    for (fileIndex=0; fileIndex<globals::N_MACRO_FILES; fileIndex++)
    {
        if (useMacroVersion != "" && useMacroVersion != globals::MACRO_FILES[fileIndex].macroVersionString) continue;   // Not interested in this macro version!

        fileName = QDir( parentWindow->getAppPath() ).absoluteFilePath(globals::MACRO_FILES[fileIndex].hexFileName);
        fileLines = globals::MACRO_FILES[fileIndex].lineCount;
        qDebug() << "Opening file: " << fileName << endl;
        hexFile.setFileName(fileName);
        isOpen = hexFile.open(QIODevice::ReadOnly | QIODevice::Text);
        if (isOpen)
        {
            qDebug() << "HEX File Opened.";
            break;
        }
        else qDebug() << "File not found. Trying next. ";
    }
    if (!isOpen)
    {
        qDebug() << "WARNING: No macro file found!";
        return globals::FILE_ERROR; // No HEX file found!
    }

    int lineNo = 0;
    bool bytesOK;
    bool lineOK;
    uint8_t nBytes;
    int offset;
    uint8_t dataByte;
    uint8_t addressHi;
    uint8_t addressLo;

    uint8_t lineData[256];   // The lazy way to buffer line data (max hex file record length is 255)...
    size_t totalBytes = 0;
    int percent = 0;
    int lastPercent = 0;

    while (!hexFile.atEnd()) {
        QByteArray hexLine = hexFile.readLine();
        lineNo++;
        if ( (hexLine.size() < 9) ||
             (hexLine[0] != ':')  ) {
            qDebug() << "WARNING: Skipped line " << lineNo << " (bad format)" << endl;
            continue;
        }

        bytesOK = (  hexCharsToInt( hexLine[1], hexLine[2], &nBytes    ) &
                hexCharsToInt( hexLine[3], hexLine[4], &addressHi ) &
                hexCharsToInt( hexLine[5], hexLine[6], &addressLo )  );
        if (!bytesOK) {
            qDebug() << "WARNING: Skipped line " << lineNo << " (bad byte count or address)" << endl;
            continue;
        }
        if (nBytes == 0) {
            qDebug() << "WARNING: Skipped line " << lineNo << " (No Data)" << endl;
            continue;
        }

        totalBytes += nBytes;

        qDebug() << "Line " << lineNo << ": " << nBytes << " bytes";

        // DEPRECATED! lineData[0] = addressLo;  // Module only uses 2 bytes for address, so we send low byte of address as first data byte.

        lineOK = TRUE;
        offset = 0;
        uint16_t i;
        for (i=9; i<((nBytes*2)+9); i+=2) {
            if ( i > (hexLine.size()-2) ) {
                // Not enough data fields in line!
                qDebug() << "WARNING: Skipped line " << lineNo << " (Not enough data fields)" << endl;
                lineOK = FALSE;
                break;
            }
            bytesOK = hexCharsToInt( hexLine[i], hexLine[i+1], &dataByte);
            if (!bytesOK) {
                qDebug() << "WARNING: Skipped line " << lineNo << " (bad character found)" << endl;
                lineOK = FALSE;
                break;
            }
            lineData[offset] = dataByte;
            offset++;   // Added a byte from the record!
        }  // [for...]

        // Line read. Send to each board:
        if (lineOK)
        {
qDebug() << "Macro DL: Line OK; Sending " << nBytes << " Bytes to board 0.";
            result = bertInterface->rawWrite24( 0,
                                                0xFB,
                                                addressHi,
                                                addressLo,
                                                lineData,
                                                nBytes );
qDebug() << "Macro DL 0: --->Result: " << result;
qDebug() << "Macro DL: Line OK; Sending " << nBytes << " Bytes to board 1.";
            if (result == globals::OK && includeSlave) result =
                    bertInterface->rawWrite24( 1,
                                               0xFB,
                                               addressHi,
                                               addressLo,
                                               lineData,
                                               nBytes );
qDebug() << "Macro DL 1: --->Result: " << result;
            if (result != globals::OK)
            {
                hexFile.close();
                return result;  // I2C command error!
            }
        }

        // Update status %:
        percent = ( (lineNo*100) / fileLines);
        if (percent >= (lastPercent + 20) )
        {
            lastPercent += 20;
            parentWindow->appendStatus( QString("%1%... ").arg(lastPercent,2,10,QChar('0')) );
            parentWindow->repaint();
        }

    }  //  [while (!hexFile.atEnd())]

    qDebug() << lineNo << " lines read! (" << totalBytes << " bytes)." << endl;
    // Finished reading!
    hexFile.close();
    return globals::OK;
}





uint8_t BertCommands::hexCharToInt(uint8_t byte)
{
    if (  (byte < 48) ||
          ( (byte > 57) && (byte < 65) ) ||
          (byte > 90)  ) return 255;
    if (byte < 58) return byte - 48;   // Digit 0-9
    return byte - 55;                  // Letter A-Z
}

bool BertCommands::hexCharsToInt(uint8_t charHi, uint8_t charLo, uint8_t *result )
{
    uint8_t byteHi = hexCharToInt(charHi);
    if (byteHi > 15) return FALSE;
    uint8_t byteLo = hexCharToInt(charLo);
    if (byteLo > 15) return FALSE;
    *result = (byteHi << 4) + byteLo;
    return TRUE;
}



