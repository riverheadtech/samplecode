/*!
 \file   BertComms.cpp
 \brief  Comms API Class Implementation
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015; Modified Jun 2018
*/


#include <QDebug>
#include <QEventLoop>

#include "globals.h"
#include "BertComms.h"
#include "BertCommsWorker.h"


BertComms::BertComms()
{
    // Launch a worker thread to handle comms in the background:
#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Setting up Comms Worker from main thread: " << QThread::currentThreadId();
#endif
    commsWorker = new BertCommsWorker();
    commsWorker->moveToThread(commsWorker);
    connect(commsWorker, SIGNAL(jobFinished()), this, SLOT(commsJobFinished()));
    commsWorker->start();
}



BertComms::~BertComms()
{
    // Stop the worker thread, and wait for it to exit:
    qDebug() << "Stopping Comms Worker...";

    commsWorker->requestStop();
    bool waitResult = commsWorker->wait(5000);
    if (!waitResult)
    {
        // Thread didn't exit cleanly:
        qDebug() << "WARNING: Comms Worker failed to exit cleanly";
        commsWorker->terminate(); // Last resort.
    }
    delete commsWorker;
}



/*!
 \brief Method to indicate whether the serial port is open:
 \return
*/
bool BertComms::portIsOpen()
{
    return commsWorker->portIsOpen();
}


/*!
 * \brief BertCommsWorker::commsJobFinished
 */
void BertComms::commsJobFinished()
{
#ifdef BERT_COMMSWORKER_DEBUG
    qDebug() << "Comms (UI thread): Received jobFinished signal";
#endif
    // Exit the current wait loop:
    eventLoop.exit();
}



/* DEPRECATED
 \brief Lock the comms module
 Prevents other events running commands until we free it
 \param timeoutMs  Time to wait for lock if already locked (milliseconds)
 \return globals::OK          We locked the comms OK
 \return globals::BUSY_ERROR  Lock timed out (already locked by another call!)
*
int BertComms::commsLock(int timeoutMs)
{
    return commsWorker->startCommsJob(BertCommsWorker::COMMS_commsLock,
                                      QString(""),
                                      timeoutMs,
                                      0,
                                      0,
                                      0,
                                      NULL,
                                      NULL,
                                      0,
                                      &eventLoop);
}
*/

/* DEPRECATED
 \brief Free the comms module (previously locked by commsLock)
*
void BertComms::commsFree()
{
   commsWorker->startCommsJob(BertCommsWorker::COMMS_commsFree,
                              QString(""),
                              0,
                              0,
                              0,
                              0,
                              NULL,
                              NULL,
                              0,
                              &eventLoop);
}
*/


/*!
 \brief Open comms
 Creates a serial comms class (BertSerial) and calls
 the 'open' method to open comms.

 Also sets up a QEventLoop to allow comms methods to wait
 for serial port operations to finish. The BertSerial
 "transactionFinished" signal is connected to our slot so that
 we get notified when the serial port has finished an operation.

 After opening the serial port, the I2C adaptor is probed
 to make sure it is responding (see probeAdaptor).

 \param useSerialPort    QString containing the name of the serial
                         port to use

 \return globals::OK         Success. Comms open.
 \return globals::GEN_ERROR  Error. Comms not open; calls to
                             read and write will be ignored.
*/
int BertComms::open(const QString useSerialPort)
{
    return commsWorker->startCommsJob(BertCommsWorker::COMMS_open,
                                      useSerialPort,
                                      0,
                                      0,
                                      0,
                                      0,
                                      NULL,
                                      NULL,
                                      0,
                                      &eventLoop);
}


/*!
 \brief Close comms
 This method closes the comms if open.
*/
void BertComms::close()
{
    commsWorker->startCommsJob(BertCommsWorker::COMMS_close,
                               QString(""),
                               0,
                               0,
                               0,
                               0,
                               NULL,
                               NULL,
                               0,
                               &eventLoop);
}




/*!
 \brief Reset I2C Adaptor
 Used after error
 \return
*/
void BertComms::reset()
{
    commsWorker->startCommsJob(BertCommsWorker::COMMS_reset,
                               QString(""),
                               0,
                               0,
                               0,
                               0,
                               NULL,
                               NULL,
                               0,
                               &eventLoop);
}


/*!
 \brief Write raw bytes out I2C device
 This is used for a device without any internal address.
 Max 255 bytes per write.
 \param slaveAddress  I2C Slave address of the device
                      (NB: 7 bits, i.e. not including R/W bit)
 \param data          Pointer to an array of uint8_t data bytes (may be only one)
 \param nBytes        Number of bytes to write from data.
 \return globals::OK              Success - nBytes were read back into data buffer
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::OVERFLOW        Error (nBytes > 255)
 \return [error code]             Error from i2cOp
*/
int BertComms::writeRaw(const uint8_t slaveAddress,
                        const uint8_t *data,
                        const uint8_t nBytes)
{
    return commsWorker->startCommsJob(BertCommsWorker::COMMS_writeRaw,
                                      QString(""),       // Serial port
                                      0,                 // Timeout
                                      slaveAddress,      // I2C Slave Address
                                      0,                 // Register Address 16 bit
                                      0,                 // Register Address 24 bit
                                      data,              // Data Write
                                      NULL,              // Data Read
                                      nBytes,            // n Bytes
                                      &eventLoop);
}


/*!
 \brief Write data to I2C Device (16 bit address)

 \param slaveAddress  I2C Slave address of the device
                      (NB: 7 bits, i.e. not including R/W bit)
 \param address   16 bit address of register or memory to write to
 \param data      Pointer to an array of uint8_t data bytes (may be only one)
 \param nBytes    Number of bytes to write from data.
                  nBytes must be <= 59, otherwise 'OVERFLOW' error will be returned.

 \return globals::OK                   Data written
 \return globals::NOT_CONNECTED        Error (comms not open)
 \return globals::OVERFLOW             Data too big (nBytes > 59)
 \return globals::ADAPTOR_WRITE_ERROR  Adaptor returned internal fault code
 \return [error code]                  Error from i2cOp
*/
int BertComms::write(const uint8_t slaveAddress,
                     const uint16_t address,
                     const uint8_t *data,
                     const uint8_t nBytes)
{
    return commsWorker->startCommsJob(BertCommsWorker::COMMS_write,
                                      QString(""),       // Serial port
                                      0,                 // Timeout
                                      slaveAddress,      // I2C Slave Address
                                      address,           // Register Address 16 bit
                                      0,                 // Register Address 24 bit
                                      data,              // Data write
                                      NULL,              // Data read
                                      nBytes,            // n Bytes
                                      &eventLoop);
}





/*!
 \brief Write data to device memory, using a 24 bit address

 \param slaveAddress  I2C Slave address of the device
                      (NB: 7 bits, i.e. not including R/W bit)

 \param address   Device register address (32 bit unsigned; upper 8 bits ignored).

 \param data      Pointer to an array of uint8_t - data to write
 \param nBytes    Number of bytes to write
                  Nb: nBytes is not limited in size - any read size
                  restrictions will be managed internally.

 \return globals::OK                  Data written
 \return globals::NOT_CONNECTED       Error (comms not open)
 \return globals::ADAPTOR_WRITE_ERROR Adaptor returned internal fault code

*/
int BertComms::write24(const uint8_t slaveAddress,
                       const uint32_t address,
                       const uint8_t *data,
                       const size_t nBytes)
{
    return commsWorker->startCommsJob(BertCommsWorker::COMMS_write24,
                                      QString(""),       // Serial port
                                      0,                 // Timeout
                                      slaveAddress,      // I2C Slave Address
                                      0,                 // Register address - 16 bit
                                      address,           // Register Address - 24 bit
                                      data,              // Data write
                                      NULL,              // Data read
                                      nBytes,            // n Bytes
                                      &eventLoop);
}










/*!
 \brief Read raw bytes from I2C device
 This is used for a device without any internal address
 \param slaveAddress  I2C Slave address of the device
                      (NB: 7 bits, i.e. not including R/W bit)
 \param data          Pointer to an array of uint8_t data bytes (may be only one)
 \param nBytes        Number of bytes to write from data.
 \return globals::OK              Success - nBytes were read back into data buffer
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::OVERFLOW        Error (nBytes > 255)
 \return [error code]             Error from i2cOp
*/
int BertComms::readRaw(const uint8_t slaveAddress,
                       uint8_t *data,
                       const uint8_t nBytes)
{
    return commsWorker->startCommsJob(BertCommsWorker::COMMS_readRaw,
                                      QString(""),       // Serial port
                                      0,                 // Timeout
                                      slaveAddress,      // I2C Slave Address
                                      0,                 // Register address - 16 bit
                                      0,                 // Register Address - 24 bit
                                      NULL,              // Data Write
                                      data,              // Data Read
                                      nBytes,            // n Bytes
                                      &eventLoop);
}



/*!
 \brief Read data from I2C Device (16 bit address)

 Reads bytes from the I2C Device and stores them to the
 supplied buffer.

 \param slaveAddress  I2C Slave address of the device
                      (NB: 7 bits, i.e. not including R/W bit)
 \param address   16 bit address of register or memory to read from
 \param data      Pointer to a buffer allocated by the caller.
                  Must be at least nBytes in size.
 \param nBytes    Number of bytes to read from device.
                  nBytes must be <= 64, otherwise 'OVERFLOW' error
                  will be returned.

 \return globals::OK              Success - nBytes were read back into data buffer
 \return globals::NOT_CONNECTED   Error (comms not open)
 \return globals::OVERFLOW        Error (nBytes > 64)
 \return [error code]             Error from i2cOp
*/
int BertComms::read(const uint8_t slaveAddress,
                    const uint16_t address,
                    uint8_t *data,
                    const uint8_t nBytes)
{
    return commsWorker->startCommsJob(BertCommsWorker::COMMS_read,
                                      QString(""),       // Serial port
                                      0,                 // Timeout
                                      slaveAddress,      // I2C Slave Address
                                      address,           // Register address - 16 bit
                                      0,                 // Register Address - 24 bit
                                      NULL,              // Data Write
                                      data,              // Data Read
                                      nBytes,            // n Bytes
                                      &eventLoop);
}






/*!
 \brief Read raw data from device memory, using a 24 bit address
 \param slaveAddress  I2C Slave address of the device
                      (NB: 7 bits, i.e. not including R/W bit)
 \param address   Device register address (32 bit unsigned; upper 8 bits ignored).
 \param data      Pointer to an array of uint8_t to store read data
 \param nBytes    Number of bytes to read. Data array must be large enough.
                  Nb: nBytes is not limited in size - any read size
                  restrictions will be managed internally.

 \return globals::OK                   Data written
 \return globals::NOT_CONNECTED        Error (comms not open)
 \return globals::ADAPTOR_WRITE_ERROR  Adaptor returned internal fault code
 \return globals::READ_ERROR           Data size returned by adaptor read was wrong
 \return [error code]                  Error from i2cOp
*/
int BertComms::read24(const uint8_t slaveAddress,
                      const uint32_t address,
                      uint8_t *data,
                      const size_t nBytes )
{
    return commsWorker->startCommsJob(BertCommsWorker::COMMS_read24,
                                      QString(""),       // Serial port
                                      0,                 // Timeout
                                      slaveAddress,      // I2C Slave Address
                                      0,                 // Register address - 16 bit
                                      address,           // Register Address - 24 bit
                                      NULL,              // Data Write
                                      data,              // Data Read
                                      nBytes,            // n Bytes
                                      &eventLoop);
}




