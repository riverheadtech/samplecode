/*!
 \file   BertComms.cpp
 \brief  BERT Board Comms Class Header - WINDOWS
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/

#ifndef BERTCOMMS_H
#define BERTCOMMS_H

#include <windows.h>
#include <stdint.h>

#include "globals.h"


/*!
 \brief BERT Board Comms Class

 The purpose of this class is to abstract the means by which the UI (whatever
 that may be) will communicate with the BERT board (GT1724). "BertHardware"
 can then use a suitable Comms implementation depending on platform.

 The BERT board uses I2C for its communication, so this class provides methods
 I2C-style communication methods. The actual implementation of these methods
 depends on the specific USB (or similar) to I2C converter in use (or possibly
 direct I2C transmission if using an embedded board with I2C support).

*/
class BertComms
  {
  public:
    BertComms(const uint8_t useSlaveAddress);
    virtual ~BertComms();

    int   open(const LPCTSTR serialPort);  // E.g.: "\\\\.\\COM1"

    int   write(const uint16_t registerAddress, const uint8_t *registerData, const uint8_t nBytes);
    int   read(const uint16_t registerAddress, uint8_t *registerData, const uint8_t nBytes);

    void  close();

    // Method to indicate whether the serial port is open:
    int portOpen() const {  return isOpen;  }

    // Method to return the last error code (only valid if a method returns a non-success result):
    uint32_t getLastError() const {  return lastError;  }

    // Platform-specific sleep:
    void sleep(uint32_t milliSeconds);


    // **** I2C Commands: ***********
    static const uint8_t I2C_SGL = 0x53;   // Read/Write single byte for non-registered devices
    static const uint8_t I2C_MUL = 0x54;   // Read multiple bytes without setting new address
    static const uint8_t I2C_AD1 = 0x55;   // Read/Write single or multiple bytes for 1 byte addressed devices
    static const uint8_t I2C_AD2 = 0x56;   // Read/Write single or multiple bytes for 2 byte addressed devices
    static const uint8_t I2C_USB = 0x5A;   // A range of commands to the USB-I2C module

  protected:

  private:
    uint8_t slaveAddress;

    int isOpen;

    int32_t lastError;

    // Windows-specific members:
    HANDLE hSerial;
    // // Maybe? DWORD WINAPI lastError;

    int i2cCommand(const uint8_t isRead, const uint16_t registerAddress, const uint8_t nBytes);

  };

#endif // BERTCOMMS_H
