/*!
 \file   globals.cpp
 \brief  Global Constants Class Implementation
 \author J Cole-Baker (For Smartest)
 \date   Jan 2015
*/

#include "globals.h"

#include <QString>
#include <QDateTime>

// Macro file information:
const globals::MacroFileInfo globals::MACRO_FILES[] =
{
    // [File Name], [Line Count], [Version (byte array)], [Version (String)]
    { ":/UNKNOWN.hex",           0,   { 0x00, 0x00, 0x00, 0x00 }, "Unknown" },   // Placeholder for unrecognised macro version
    { ":/MACRO_VER_1_E_0_C.hex", 309, { 0x01, 0x45, 0x00, 0x43 }, "1E0C"    },
    { ":/MACRO_VER_1_E_1_C.hex", 317, { 0x01, 0x45, 0x01, 0x43 }, "1E1C"    }
};
const size_t globals::N_MACRO_FILES = sizeof(MACRO_FILES) / sizeof(MACRO_FILES[0]);


const QString globals::BUILD_VERSION = QString( "2.1.3" );
//const QString globals::BUILD_DATE = QString( "2-July-2018" );
const QString globals::BUILD_DATE = __DATE__ " " __TIME__;

#ifdef BRAND_CEYEAR
const QString globals::BRAND         = QString( "CeYear" );
const QString globals::BUILD_MODEL   = QString( "5233D-S" );
#else
const QString globals::BRAND         = QString( "SmarTest" );
const QString globals::BUILD_MODEL   = QString( "SB3204" );
#endif

