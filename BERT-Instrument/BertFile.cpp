/*!
 \file   BertFile.cpp
 \brief  BERT File System Helper Class Implementation
 \author J Cole-Baker (For Smartest)
 \date   Jul 2016
*/

#include <QDebug>
#include <QFile>
#include <QDir>

#include "globals.h"
#include "BertFile.h"

BertFile::BertFile()
{}

BertFile::~BertFile()
{}

/*!
 \brief Read the contents of a directory

 \param path
 \param errorCode  Indicates whether there was an error reading the directory listing.
              globals::OK                   Directory found and file list read
              globals::DIRECTORY_NOT_FOUND  Directory didn't exist

 \return List of files in the directory. May be empty. If empty, check errorCode
         for possible error.
*/
int BertFile::readDirectory(const QString &path, QStringList &fileList)
{
    // Get executable directory:
    if (!QDir().exists(path)) return globals::DIRECTORY_NOT_FOUND;

    // qDebug() << "Checking for files in " << path << endl;

    QDir dir = QDir(path);
    fileList << dir.entryList(QDir::Files);
    return globals::OK;
}


/*!
 \brief Read the contents of a file, and return a list of lines

 \param fileName
 \param maxLines
 \param fileLines
 \return
*/
int BertFile::readFile(const QString &fileName, const size_t maxLines, QStringList &fileLines)
{
    QFile thisFile;
    size_t lineCount = 0;

    // qDebug() << "Opening file: " << fileName << endl;
    thisFile.setFileName(fileName);
    int result = thisFile.open(QIODevice::ReadOnly | QIODevice::Text);
    if (!result) return globals::FILE_ERROR;

    while ( !thisFile.atEnd() && (lineCount < maxLines) )
    {
        QByteArray fileLine = thisFile.readLine();
        fileLines << QString(fileLine);
        lineCount++;
    }
    thisFile.close();
    return globals::OK;
}


